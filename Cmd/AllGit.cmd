@   echo off
    @echo.
    @echo **********************************************************************
    @echo * Preparing list of GIT repositories
    @echo **********************************************************************
    for /f "delims=" %%d in ('dir /a:d /o:n /b /s p:\.git') do (
        call :repository "%%d" %*
        if ErrorLevel 1   exit/b
    )
    goto :eof

:repository
    PushD %1
    cd ..
    @echo.
    @echo **********************************************************************
    @echo * Processing %cd%
    @echo **********************************************************************
    @echo git %2 %3 %4 %5 %6 %7 %8 %9
    @git %2 %3 %4 %5 %6 %7 %8 %9
    @echo off
    if ErrorLevel 1 (
        @echo Problem w folderze %cd%
        @choice /m "Continue "
        PopD
        @if ErrorLevel 2   exit /b 1
        PushD
    )
    @echo * OK
    PopD
