@echo off
for %%f in (*.pas;*.dp?;*.inc;*.txt;*.cs;*.md) do (
  echo %%f...
  @REM start "%%f" "p:\Utilities\Cmd\ConvertCP\convertcp.exe" 1250 65001 /i "%%f" /o "%%f.utf8"
  "p:\Utilities\Cmd\ConvertCP\convertcp.exe" 1250 65001 /b /i "%%f" /o "%%f.utf8"
)
echo.
echo Przejrzyj pliki z rozszerzeniem ".utf8".
timeout 10
