﻿# Utilities

## My small (and not so small) utilities.
* **CleanTemp** - program to clean "temp" folders. By default, files and folders older than 3 days are removed from standard TEMP folder(s) do Windows Recycle Bin.
* **FilesMonitor** - checks for new version of specified files. If new version is detected, specified action is launched.
* **UpdateCopyright** - program to run once per year, at January. Will browse for all *.pas, *.inc, *.dpk and *.dpr files to update Copyright comment.
* **JobRegister** - program to register your work (manually). When you start working on specific project, select it. Also, when you stop working on the project, select *None* project.

If you want to contact me:
- http://bit.ly/2Le7zn8 or
- http://bit.ly/2Likp3J
