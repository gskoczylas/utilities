﻿unit  LogPerformanceAnalyser.MainForm;

(**********************************************
 *                                            *
 *          Log performance analyser          *
       ——————————————————————————————————     *
 *  Copyright © 2023-2024 Grzegorz Skoczylas  *
 *                                            *
 *         mgr Grzegorz Skoczylas             *
 *             ul. Grażyny 13/15              *
 *             43-300 Bielsko-Biała           *
 *             tel. +48 503 064 953           *
 *             gskoczylas@gmail.com           *
 *                                            *
 **********************************************)


interface


uses
  Winapi.Windows, Winapi.Messages,
  System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, JvToolEdit, Vcl.StdCtrls,
  Vcl.Mask,
  JvExMask;


const
  msgFileProcessing = WM_APP + 1;


type
  TfrmMain = class(TForm)
    lblFolder:  TLabel;
    edtFolder:  TJvDirectoryEdit;
    lblStart:   TLabel;
    lblStop:    TLabel;
    edtStart:   TEdit;
    edtStop:    TEdit;
    lblResult:  TLabel;
    edtResult:  TJvFilenameEdit;
    btnProcess: TButton;
    btnClose:   TButton;
    lblStatus:  TLabel;
    procedure  btnProcessClick(Sender:  TObject);
    procedure  btnCloseClick(Sender:  TObject);
  strict private
    var
      fnProcessingFiles:  Integer;
    procedure  ProcessingFile(var   Msg:  TMessage);                 message msgFileProcessing;
  strict protected
    procedure  DoShow();                                             override;
  end { TfrmMain };


var
  frmMain:  TfrmMain;


implementation


uses
  LogPerformanceAnalyser.ProcessLogFiles, LogPerformanceAnalyser.Data;


{$R *.dfm}


{$REGION 'TfrmMain'}

procedure  TfrmMain.btnCloseClick(Sender:  TObject);
begin
  Close()
end { btnCloseClick };


procedure  TfrmMain.btnProcessClick(Sender:  TObject);
begin
  TProcessLogFiles.ProcessLogFiles(Trim(edtFolder.Directory),
                                   Trim(edtStart.Text),
                                   Trim(edtStop.Text));
  TPerformanceStore.WriteReport(Trim(edtResult.FileName));
  lblStatus.Caption := 'All files processed and report generated'
end { btnProcessClick };


procedure  TfrmMain.DoShow();
begin
  inherited;
  lblStatus.Caption := '';
  fnProcessingFiles := 0;
  {$IFDEF DEBUG}
    edtFolder.Text := 'k:\~ Jantar ~\CHD';
    edtStart.Text := 'Commands = \[';
    edtStop.Text := 'ReceiptNo = \d+';
    edtResult.Text := '"K:\~ Jantar ~\CHD\Result.txt"'
  {$ENDIF DEBUG}
end { DoShow };


procedure  TfrmMain.ProcessingFile(var   Msg:  TMessage);
begin
  Inc(fnProcessingFiles, Msg.lParam);
  case  fnProcessingFiles  of
    0:  lblStatus.Caption := 'All files processed';
    1:  lblStatus.Caption := 'Processing 1 file';
   else lblStatus.Caption := Format('Processing %d file(s)', [fnProcessingFiles])
  end { case fnProcessingFiles };
  lblStatus.Update()
end { ProcessingFile };

{$ENDREGION 'TfrmMain'}


end.
