﻿program  TestLogFileProcessor;

(**********************************************
 *                                            *
 *          Log performance analyser          *
       ——————————————————————————————————     *
 *  Copyright © 2023-2024 Grzegorz Skoczylas  *
 *                                            *
 *         mgr Grzegorz Skoczylas             *
 *             ul. Grażyny 13/15              *
 *             43-300 Bielsko-Biała           *
 *             tel. +48 503 064 953           *
 *             gskoczylas@gmail.com           *
 *                                            *
 **********************************************)


{$APPTYPE CONSOLE}

{$R *.res}

uses
  madExcept,
  madListHardware,
  madListProcesses,
  madListModules,
  System.SysUtils, System.DateUtils,
  LogPerformanceAnalyser.Data in 'LogPerformanceAnalyser.Data.pas',
  LogPerformanceAnalyser.LogFileProcessor in 'LogPerformanceAnalyser.LogFileProcessor.pas';

begin
  ProcessLogFile('k:\~ Jantar ~\CHD\Log - S53\Jantar-DESKTOP-ELMKDPM.log',
                 'Commands = \[',
                 'ReceiptNo = \d+',
                 procedure(sFileName:  string;
                           StartPos, EndPos:  TMarker)
                   begin
                     Writeln(ExtractFileName(sFileName), ': ',
                             StartPos.LineNo:5, ' ', FormatDateTime('yyyy-mm-dd hh:nn:ss.zzz', StartPos.Timestamp), ' ',
                             EndPos.LineNo:5, ' ', FormatDateTime('yyyy-mm-dd hh:nn:ss.zzz', EndPos.Timestamp), ' --> ',
                             MilliSecondsBetween(StartPos.Timestamp, EndPos.Timestamp) * 0.001:6:3)
                   end);
  Writeln;
  Write('Czekam na Enter... ');
  Readln;
end.
