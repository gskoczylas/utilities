﻿unit  LogPerformanceAnalyser.ProcessLogFiles;

(**********************************************
 *                                            *
 *          Log performance analyser          *
       ——————————————————————————————————     *
 *  Copyright © 2023-2024 Grzegorz Skoczylas  *
 *                                            *
 *         mgr Grzegorz Skoczylas             *
 *             ul. Grażyny 13/15              *
 *             43-300 Bielsko-Biała           *
 *             tel. +48 503 064 953           *
 *             gskoczylas@gmail.com           *
 *                                            *
 **********************************************)


interface


{$WARN SYMBOL_PLATFORM OFF}


uses
  WinAPI.Windows,
  System.Types, System.SysUtils, System.Classes, System.IOUtils, System.Threading,
  VCL.Forms;


type
  TProcessLogFiles = class
  public
    class procedure  ProcessLogFiles(const   sDir:  string;
                                     const   sStart, sStop:  string);
  end { TProcessLogFiles };


implementation


uses
  LogPerformanceAnalyser.LogFileProcessor, LogPerformanceAnalyser.Data,
  LogPerformanceAnalyser.MainForm;


{$REGION 'TProcessLogFiles'}

class  procedure  TProcessLogFiles.ProcessLogFiles(const   sDir, sStart, sStop:  string);

var
  lFiles:  TArray<string>;
  sFile:   string;
//   lPool:   TThreadPool;
//   lTasks:  TArray<ITask>;
//   lTask:   ITask;

begin  { ProcessLogFiles }
  TPerformanceStore.SetRootDir(sDir);
  lFiles := TDirectory.GetFiles(sDir, '*.log', TSearchOption.soAllDirectories, nil);
  {-}
//   SetLength(lTasks, 0);
//   lPool := TThreadPool.Create();
//   try
//    PostMessage(frmMain.Handle, msgFileProcessing, 0, Length(lFiles));
      SendMessage(frmMain.Handle, msgFileProcessing, 0, Length(lFiles));
    for  sFile in lFiles  do
      begin
//         SetLength(lTasks, Succ(Length(lTasks)));
//         lTasks[High(lTasks)] := TTask.Create(procedure
//                                                begin
//                                                  ProcessLogFile(sFile, sStart, sStop,
//                                                                 procedure(sFileName:  TFileName;
//                                                                           Start, Stop:  TMarker)
//                                                                   begin
//                                                                     TPerformanceStore.RegisterPerformance(sFileName, Start, Stop)
//                                                                   end)
//                                                end,
//                                              lPool).Start();
        ProcessLogFile(sFile, sStart, sStop,
                       procedure(sFileName:  TFileName;
                                 Start, Stop:  TMarker)
                         begin
                           TPerformanceStore.RegisterPerformance(sFileName, Start, Stop)
                         end)
      end { for sFile };
    {-}
//     repeat
//       if  Application.Terminated  then
//         for  lTask in lTasks  do
//           lTask.Cancel();
//       Application.ProcessMessages()
//     until  TTask.WaitForAll(lTasks, 100)
//   finally
//     lPool.Free()
//   end { try-finally }
end { ProcessLogFiles };

{$ENDREGION 'TProcessLogFiles'}


end.
