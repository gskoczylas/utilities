﻿unit  LogPerformanceAnalyser.LogFileProcessor;

(**********************************************
 *                                            *
 *          Log performance analyser          *
       ——————————————————————————————————     *
 *  Copyright © 2023-2024 Grzegorz Skoczylas  *
 *                                            *
 *         mgr Grzegorz Skoczylas             *
 *             ul. Grażyny 13/15              *
 *             43-300 Bielsko-Biała           *
 *             tel. +48 503 064 953           *
 *             gskoczylas@gmail.com           *
 *                                            *
 **********************************************)


interface


uses
  WinAPI.Windows,
  System.SysUtils, System.DateUtils, System.Classes, System.RegularExpressions,
  LogPerformanceAnalyser.Data;


type
  EProcessLogFile = class(Exception);


procedure  ProcessLogFile(const   sFileName:  TFileName;
                          const   sStart, sStop:  string;
                          const   fnFound:  TProc<TFileName, TMarker, TMarker>);


implementation


uses
  LogPerformanceAnalyser.MainForm;


procedure  ProcessLogFile(const   sFileName:  TFileName;
                          const   sStart, sStop:  string;
                          const   fnFound:  TProc<TFileName, TMarker, TMarker>);
const
  cnInputBufferSize = 4096;
  csRegExPrefix = '^.\s(\d{4}(?:-\d{2}){2}\s(?:\d{2}:){2}\d{2}\.\d{3})\s[\da-fA-F]+\s[^ ]*\s+';

var
  lInput:     TStringList;
  nLine:      Cardinal;
  sLine:      string;
  lStartExpr: TRegEx;
  lEndExpr:   TRegEx;
  lStartPos:  TMarker;
  lEndPos:    TMarker;

  function  GetTimestamp(const   sText:  string)
                         : TDateTime;

    function  GetVal(const   sText:  string;
                     const   nStart:  Integer;
                     const   nLength:  Integer = 2)
                     : Word;
    begin
      Result := StrToInt(Copy(sText, nStart, nLength))
    end { GetVal };

  begin  { GetTimestamp }
    // yyyy-mm-dd hh:nn:ss.zzz
    // 123456789o123456789o123
    Result := EncodeDateTime(GetVal(sText, 1, 4),  // yyyy
                             GetVal(sText, 6),     // mm
                             GetVal(sText, 9),     // dd
                             GetVal(sText, 12),    // hh
                             GetVal(sText, 15),    // nn
                             GetVal(sText, 18),    // ss
                             GetVal(sText, 21, 3)) // zzz
  end { GetTimestamp };

begin  { ProcessLogFile }
  if  SameText(sStart, sStop)  then
    raise  EProcessLogFile.CreateFmt('The start (%s) and the end (%s) of the range must be different',
                                     [sStart, sStop]);
  lInput := TStringList.Create();
  try
    try
      lInput.LoadFromFile(sFileName)
    except
      on  eErr : Exception  do
        Exception.RaiseOuterException(EProcessLogFile.CreateFmt('It is not possible to read the "%s" file',
                                                                [sFileName]))
    end { try-except };
    {-}
    lStartPos.LineNo := 0;
    lStartExpr.Create(csRegExPrefix + sStart, [roCompiled]);
    lEndExpr.Create(csRegExPrefix + sStop, [roCompiled]);
    for  nLine :=  0  to  lInput.Count - 1  do
      begin
        sLine := lInput.Strings[nLine];
        with  lStartExpr.Match(sLine)  do
          if  Success  then
            begin
              lStartPos.Timestamp := GetTimestamp(Groups.Item[1].Value);
              lStartPos.LineNo := nLine + 1;
              {-}
              Continue
            end { if };
          {-}
          if  lStartPos.LineNo <> 0  then
            with lEndExpr.Match(sLine)  do
              if  Success  then
                begin
                  lEndPos.Timestamp := GetTimestamp(Groups.Item[1].Value);
                  lEndPos.LineNo := nLine + 1;
                  {-}
                  fnFound(sFileName, lStartPos, lEndPos);
                  {-}
                  lStartPos.LineNo := 0
                end { if }
      end { for nLine }
  finally
    lInput.Free();
//    PostMessage(frmMain.Handle, msgFileProcessing, 0, -1)
      SendMessage(frmMain.Handle, msgFileProcessing, 0, -1)
  end { try-finally }
end { ProcessLogFile };


end.
