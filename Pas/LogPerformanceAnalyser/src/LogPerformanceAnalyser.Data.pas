﻿unit  LogPerformanceAnalyser.Data;

(**********************************************
 *                                            *
 *          Log performance analyser          *
       ——————————————————————————————————     *
 *  Copyright © 2023-2024 Grzegorz Skoczylas  *
 *                                            *
 *         mgr Grzegorz Skoczylas             *
 *             ul. Grażyny 13/15              *
 *             43-300 Bielsko-Biała           *
 *             tel. +48 503 064 953           *
 *             gskoczylas@gmail.com           *
 *                                            *
 **********************************************)


interface


uses
  System.SysUtils, System.DateUtils, System.StrUtils, System.Classes,
  System.SyncObjs, System.Generics.Collections;


type
  TMarker = record
    Timestamp:  TDateTime;
    LineNo:     Cardinal;
  end { TMarker };

  TPerformance = record
  strict private
    fStart:  TMarker;
    fStop:   TMarker;
  public
    constructor  Init(const   Start, Stop:  TMarker);
    {-}
    function  GetPerformanceTicks() : Int64;
    function  GetPerformanceStr() : string;                          overload;
    class function  GetPerformanceStr(const   nPerf:  Int64)
                                      : string;                      overload; static;
    {-}
    property  Start:  TMarker
              read  fStart;
    property  Stop:  TMarker
              read  fStop;
    property  PerformanceTicks:  Int64
              read  GetPerformanceTicks;
  end { TPerformance };

  TPerformanceList = TList<TPerformance>;

  TPerformanceData = class(TStringList)
  strict private
    function  GetPerformanceList(const   nIndex:  Integer)
                                 : TPerformanceList;                 inline;
  public
    constructor  Create();
    {-}
    function  NewPerformanceList(const   sFilename:  TFileName)
                                 : TPerformanceList;
    {-}
    property  PerformanceList[const   nIndex:  Integer]
                             : TPerformanceList
              read  GetPerformanceList;
  end { TPerformanceData };

  TPerformanceStore = class
  strict private
    class var
      fData:  TPerformanceData;
      fSnch:  TCriticalSection;
      fsRoot: TFileName;
    class procedure  WriteSummary(const   ReportFile:   TextFile;
                                  const   PerfData:  TList<Int64>;
                                  const   sCaption:  string);
  public
    class constructor  Create();
    class destructor  Destroy();
    {-}
    class procedure  SetRootDir(const  sRootDir:  string);
    class procedure  RegisterPerformance({const} sFileName:  TFileName;
                                         const   Start, Stop:  TMarker);
    class procedure  WriteReport(const   sReport:  TFileName);
  end { TPerformanceData };


implementation


uses
  GSkPasLib.StrUtils;


{$REGION 'TPerformanceData'}

constructor  TPerformanceData.Create();
begin
  inherited Create(dupError, True, False);
  OwnsObjects := True
end { Create };


function  TPerformanceData.GetPerformanceList(const   nIndex:  Integer)
                                              : TPerformanceList;
begin
  Result := TPerformanceList(Objects[nIndex])
end { GetPerformanceList };


function  TPerformanceData.NewPerformanceList(const   sFilename:  TFileName)
                                              : TPerformanceList;
begin
  Result := TPerformanceList.Create();
  AddObject(sFilename, Result)
end { NewPerformanceList };

{$ENDREGION 'TPerformanceData'}


{$REGION 'TPerformanceStore'}

class  constructor TPerformanceStore.Create();
begin
  fData := TPerformanceData.Create();
  fSnch := TCriticalSection.Create()
end { Create };


class destructor  TPerformanceStore.Destroy();
begin
  FreeAndNil(fSnch);
  FreeAndNil(fData)
end { Destroy };


class procedure  TPerformanceStore.RegisterPerformance({const} sFileName:  TFileName;
                                                       const   Start, Stop:  TMarker);
var
  nInd:  Integer;
  lDat:  TPerformanceList;
  lPrf:  TPerformance;

begin  { RegisterPerformance }
  if  fsRoot <> ''  then
    if  StartsText(fsRoot, sFileName)  then
      Delete(sFileName, 1, Length(fsRoot));
  lPrf.Init(Start, Stop);
  {-}
  fSnch.Enter();
  try
    nInd := fData.IndexOf(sFileName);
    if  nInd >= 0  then
      lDat := fData.PerformanceList[nInd]
    else
      lDat := fData.NewPerformanceList(sFileName);
    lDat.Add(lPrf)
  finally
    fSnch.Leave()
  end { try-finally }
end { RegisterPerformance };


class procedure  TPerformanceStore.SetRootDir(const   sRootDir:  string);
begin
  fsRoot := IncludeTrailingPathDelimiter(sRootDir)
end { SetRootDir };


class procedure  TPerformanceStore.WriteReport(const   sReport:  TFileName);

const
  cnBufSize = 8 * 1024;

var
  lReportFile:   TextFile;
  lReportBuf:    array[1..cnBufSize] of  Byte;
  nFile:         Integer;
  nPerf:         Integer;
  sFile:         TFileName;
  sDir:          TFileName;
  sPrevDir:      TFileName;
  lPerfDataF:    TList<Int64>;   // File
  lPerfDataD:    TList<Int64>;   // Dirtectory
  lPerfDataA:    TList<Int64>;   // All
  nPerfTicks:    Int64;

begin  { WriteReport }
  AssignFile(lReportFile, sReport);
  SetTextBuf(lReportFile, lReportBuf);
  Rewrite(lReportFile);
  try
    sPrevDir := '';
    lPerfDataA := TList<Int64>.Create();
    try
      lPerfDataD := TList<Int64>.Create();
      try
        lPerfDataF := TList<Int64>.Create();
        try
          for  nFile :=  0  to  fData.Count - 1  do
            begin
              sFile := fData.Strings[nFile];
              lPerfDataF.Clear();
              {-}
              sDir := ExtractFileDir(sFile);
              if  (sDir <> sPrevDir)
                  and (sPrevDir <> '')  then
                begin
                  WriteSummary(lReportFile, lPerfDataD, 'for folder "' + sPrevDir + '"');
                  lPerfDataD.Clear()
                end { sDir <> sPrevDir };
              sPrevDir := sDir;
              {-}
              { Header }
              Writeln(lReportFile, 'File "' + sFile + '"');
              Writeln(lReportFile, StringOfChar('=', 7 + Length(sFile)));
              Writeln(lReportFile);
              Writeln(lReportFile,
                      Format('| %s | %s | %s | Performance |',
                             [JustCenter('Lines', 15),
                              JustCenter('Start time', 23),
                              JustCenter('End time', 12)]));
              Writeln(lReportFile,
                      '|:',
                      StringOfChar('-', 15), ':|:',
                      StringOfChar('-', 23), ':|:',
                      StringOfChar('-', 12), ':|',
                      StringOfChar('-', 12), ':|');
              { Data }
              with  fData.PerformanceList[nFile]  do
                for  nPerf :=  0  to  Count - 1  do
                  begin
                    nPerfTicks := Items[nPerf].GetPerformanceTicks();
                    lPerfDataF.Add(nPerfTicks);
                    lPerfDataD.Add(nPerfTicks);
                    lPerfDataA.Add(nPerfTicks);
                    {-}
                    Writeln(lReportFile,
                            Format('|%s| %s | %s |%12s |',
                                   [JustCenter(IntToStr(Items[nPerf].Start.LineNo)
                                                 + '-'
                                                 + IntToStr(Items[nPerf].Stop.LineNo),
                                               17),
                                    FormatDateTime('yyyy-mm-dd hh:nn:ss.zzz',
                                                   Items[nPerf].Start.Timestamp),
                                    FormatDateTime('hh:nn:ss.zzz',
                                                   Items[nPerf].Stop.Timestamp),
                                    Items[nPerf].GetPerformanceStr()]))
                  end { for nPerf };
              WriteSummary(lReportFile, lPerfDataF, 'for file "' + sFile + '"')
            end { for nFile };
          if  lPerfDataD.Count < lPerfDataA.Count  then
            WriteSummary(lReportFile, lPerfDataD, 'for FOLDER "' + sDir + '"');
          if  lPerfDataA.Count > 1  then
            WriteSummary(lReportFile, lPerfDataA, 'ALL data')
        finally
          lPerfDataF.Free()
        end { try-finally }
      finally
        lPerfDataD.Free()
      end { try-finally }
    finally
      lPerfDataA.Free()
    end { try-finally }
  finally
    CloseFile(lReportFile)
  end { try-finally }
end { WriteReport };


class procedure  TPerformanceStore.WriteSummary(const   ReportFile:   TextFile;
                                                const   PerfData:  TList<Int64>;
                                                const   sCaption:  string);
var
  nInd:  Integer;
  nSum:  Int64;

begin  { WriteSummary }
  PerfData.Sort();
  Writeln(ReportFile);
  Writeln(ReportFile, 'Summary for ' + sCaption);
  Writeln(ReportFile, StringOfChar('-', 12 + Length(sCaption)));
  Writeln(ReportFile, '- Count: ', PerfData.Count);
  Writeln(ReportFile, '- Shortest: ', TPerformance.GetPerformanceStr(PerfData.First()));
  Writeln(ReportFile, '- Longest: ', TPerformance.GetPerformanceStr(PerfData.Last()));
  {-}
  nSum := 0;
  for  nInd :=  0  to  PerfData.Count - 1  do
    Inc(nSum, PerfData.Items[nInd]);
  Writeln(ReportFile, '- Average: ', TPerformance.GetPerformanceStr(nSum div PerfData.Count));
  {-}
  Write(ReportFile, '- Median: ');
  nInd := PerfData.Count div 2;
  if  Odd(PerfData.Count)  then
    Write(ReportFile, TPerformance.GetPerformanceStr(PerfData.Items[nInd]))
  else
    write(ReportFile,
          TPerformance.GetPerformanceStr((PerfData.Items[nInd -1] + PerfData[nInd]) div 2));
  Writeln(ReportFile);
  Writeln(ReportFile)
end { WriteSummary };

{$ENDREGION 'TPerformanceStore'}


{$REGION 'TPerformance'}

function  TPerformance.GetPerformanceStr() : string;
begin
  Result := GetPerformanceStr(GetPerformanceTicks())
end { GetPerformanceStr };


class function  TPerformance.GetPerformanceStr(const   nPerf:  Int64)
                                               : string;
var
  nTicks:  Int64;
  nSecs:   Int64;

begin  { GetPerformanceStr }
  nTicks := nPerf mod 1000;
  nSecs := nPerf div 1000;
  if  nSecs >= 60  then
    Result := Format('%d:%2.2d.%3.3d',
                     [nSecs div 60, nSecs mod 60, nTicks])
  else
    Result := Format('%d.%3.3d',
                     [nSecs, nTicks])
end { GetPerformanceStr };


function  TPerformance.GetPerformanceTicks() : Int64;
begin
  Result := MilliSecondsBetween(fStart.Timestamp, fStop.Timestamp)
end { GetPerformanceTicks };


constructor  TPerformance.Init(const   Start, Stop:  TMarker);
begin
  fStart := Start;
  fStop := Stop
end { Init };

{$ENDREGION 'TPerformance'}


end.
