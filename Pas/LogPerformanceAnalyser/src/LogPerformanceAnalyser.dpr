﻿program  LogPerformanceAnalyser;

(**********************************************
 *                                            *
 *          Log performance analyser          *
       ——————————————————————————————————     *
 *  Copyright © 2023-2024 Grzegorz Skoczylas  *
 *                                            *
 *         mgr Grzegorz Skoczylas             *
 *             ul. Grażyny 13/15              *
 *             43-300 Bielsko-Biała           *
 *             tel. +48 503 064 953           *
 *             gskoczylas@gmail.com           *
 *                                            *
 **********************************************)


uses
  madExcept, madLinkDisAsm, madListHardware, madListProcesses, madListModules,
  Vcl.Forms,
  LogPerformanceAnalyser.MainForm in 'LogPerformanceAnalyser.MainForm.pas' {frmMain},
  LogPerformanceAnalyser.Data in 'LogPerformanceAnalyser.Data.pas',
  LogPerformanceAnalyser.LogFileProcessor in 'LogPerformanceAnalyser.LogFileProcessor.pas',
  LogPerformanceAnalyser.ProcessLogFiles in 'LogPerformanceAnalyser.ProcessLogFiles.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'LogPerfAnalyser';
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
