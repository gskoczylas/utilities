object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 'Log Performance Analyser'
  ClientHeight = 259
  ClientWidth = 628
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  StyleName = 'Windows'
  DesignSize = (
    628
    259)
  TextHeight = 15
  object lblFolder: TLabel
    Left = 29
    Top = 19
    Width = 36
    Height = 15
    Caption = '&Folder:'
    FocusControl = edtFolder
  end
  object lblStart: TLabel
    Left = 29
    Top = 59
    Width = 27
    Height = 15
    Caption = '&Start:'
    FocusControl = edtStart
  end
  object lblStop: TLabel
    Left = 29
    Top = 91
    Width = 27
    Height = 15
    Caption = 'S&top:'
    FocusControl = edtStop
  end
  object lblResult: TLabel
    Left = 29
    Top = 129
    Width = 35
    Height = 15
    Caption = '&Result:'
  end
  object lblStatus: TLabel
    Left = 71
    Top = 216
    Width = 519
    Height = 15
    AutoSize = False
    Caption = 'lblStatus'
  end
  object edtFolder: TJvDirectoryEdit
    Left = 71
    Top = 16
    Width = 519
    Height = 23
    Hint = 'Root folder path'
    TextHint = 'root of the folder tree'
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    Text = ''
  end
  object edtStart: TEdit
    Left = 71
    Top = 56
    Width = 519
    Height = 23
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 1
    TextHint = 'regular expression'
  end
  object edtStop: TEdit
    Left = 71
    Top = 88
    Width = 519
    Height = 23
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 2
    TextHint = 'regular expression'
  end
  object edtResult: TJvFilenameEdit
    Left = 71
    Top = 126
    Width = 519
    Height = 23
    Hint = 'Results file'
    DialogKind = dkSave
    DefaultExt = 'txt'
    Filter = 
      'Markdown files (*.md)|*.md|Text files (*.txt)|*.txt|All files (*' +
      '.*)|*.*'
    DialogOptions = [ofOverwritePrompt, ofNoChangeDir, ofPathMustExist, ofNoReadOnlyReturn, ofEnableSizing]
    HideSelection = False
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 3
    Text = ''
  end
  object btnProcess: TButton
    Left = 71
    Top = 176
    Width = 75
    Height = 25
    Caption = '&Process'
    Default = True
    TabOrder = 4
    OnClick = btnProcessClick
  end
  object btnClose: TButton
    Left = 515
    Top = 176
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Cancel = True
    Caption = '&Close'
    TabOrder = 5
    OnClick = btnCloseClick
  end
end
