# LogPerformanceAnalyser

## Przeznaczenie programu

Program jest przeznaczony do analizy plików `*.log`. W tych plikach wyszukuje początek i koniec
działań. Dla każdego znalezionego w ten sposób działania rejestruje zakres numerów wierszy w
pliku `*.log` czas rozpoczęcia i zakończenia oraz czas trwania operacji. Na koniec każdego pliku
program generuje statystykę pliku, zawierającą liczbę znalezionych operacji, najkrótszy i
najdłuższy czas działania operacji, średni czas działania operacji oraz medianę czasu działania
operacji.

Jeżeli w folderze jest więcej plików `*.log`, to oprócz statystyk poszczególnych plików program
generuje również statystykę wszystkich plików w folderze.

Jeżeli pliki `*.log` są w więcej niż jednym folderze, to program również generuje statystykę dla
dla wszystkich analizowanych plików `*.log`

## Przykładowe wyniki

```
File "Jantar-DESKTOP-KBM6I5L.log"
=================================

|      Lines      |       Start time        |   End time   | Performance |
|:---------------:|:-----------------------:|:------------:|------------:|
|    4019-4022    | 2023-03-24 13:37:22.019 | 13:44:49.787 |    7:27.768 |
|    4025-4028    | 2023-03-24 13:44:49.788 | 13:45:29.829 |      40.041 |
|    5362-5369    | 2023-03-24 15:08:15.925 | 15:08:23.017 |       7.092 |

Summary for for file "Jantar-DESKTOP-KBM6I5L.log"
-------------------------------------------------
- Count: 3
- Shortest: 7.092
- Longest: 7:27.768
- Average: 2:44.967
- Median: 40.041
```