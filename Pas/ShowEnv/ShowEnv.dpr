program ShowEnv;

uses
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Forms,
  u_MainForm in 'u_MainForm.pas' {frmMain};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'Show Environment';
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.

