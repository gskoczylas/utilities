unit  u_MainForm;


{$WARN SYMBOL_PLATFORM OFF}


interface


uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls;


type
  TfrmMain = class(TForm)
    edtEnvironment:  TRichEdit;
    procedure  FormCreate(Sender:  TObject);
    procedure  FormKeyDown(Sender:  TObject;
                           var   Key:  Word;
                           Shift:  TShiftState);
  end { TfrmMain };


var
  frmMain:  TfrmMain;


implementation


{$R *.dfm}


procedure  TfrmMain.FormCreate(Sender:  TObject);

var
  nInd:  Integer;

  procedure  Header(const   sHeader:  string);
  begin
    with  edtEnvironment  do
      begin
        with  SelAttributes  do
          begin
            Name := 'Arial';
            Size := 11;
            Style := [fsBold]
          end { with SelAttributes };
        SelText := sHeader + ':' + sLineBreak
      end { with edtEnvironment }
  end { Header };

  procedure  Line(const   sPrefix, sLine:  string);
  begin
    with  edtEnvironment  do
      begin
        with  SelAttributes  do
          begin
            Name := 'Arial';
            Size := 10;
            Style := []
          end { with SelAttributes };
        SelText := sPrefix + ' ';
        with  SelAttributes  do
          begin
            Name := 'Consolas';
            Size := 11;
            Style := []
          end { SelAttributes };
        SelText := sLine + sLineBreak
      end { with edtEnvironment }
  end { Line };

begin  { FormCreate }
  with  edtEnvironment  do
    begin
      SelectAll();
      SelAttributes.Name := 'Consolas';
      SelAttributes.Size := 10;
      SelAttributes.Style := [];
      Header('Command Line');
      Line('� ', CmdLine);
      SelText := sLineBreak;
      Header('Parameters');
      for  nInd := 0  to  ParamCount()  do
        Line(Format('�%2d:', [nInd]), ParamStr(nInd));
      SelText := sLineBreak;
      Header('Working Directory');
      Line('� ', GetCurrentDir());
      SelText := sLineBreak;
    end { with edtEnvironment }
end { FormCreate };


procedure  TfrmMain.FormKeyDown(Sender:  TObject;
                                var   Key:  Word;
                                Shift:  TShiftState);
begin
  if  (Key = VK_ESCAPE)
      and (Shift = [])  then
    begin
      Key := 0;
      Close()
    end { [Esc] }    
end { FormKeyDown };


end.

