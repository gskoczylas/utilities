program MaxInWords4;

{$APPTYPE CONSOLE}

uses
  Windows, SysUtils, Classes, SyncObjs, IniFiles,
  zz_GSkStrUtils, zz_GSkASCII, zz_GSkIniFileUtils;

type
  TWorker = class(TThread)
  strict private
    class var
      fnValue:   Currency;
      fnMaxVal:  Currency;
      fsMaxVal:  string;
      fnMaxLen:  Cardinal;
      fnThreads: Cardinal;
      fnTick:    Cardinal;
      fSynchro:  TCriticalSection;
    class constructor  Create();
    class destructor  Destroy();
  strict protected
    procedure  Execute();                                            override;
  end { TWorker };


class constructor  TWorker.Create();
begin
  fnValue := ReadCurrency('Values', 'Current');
  fnMaxVal := ReadCurrency('Values', 'Max');
  if  (fnValue = 0) or (fnMaxVal = 0)  then
    begin
      fnValue := 999;
      while  fnValue < MaxCurrency / 1000  do
        fnValue := fnValue * 1000 + 999;
      fnMaxVal := fnValue;
      WriteCurrency('Values', 'Current', fnValue);
      WriteCurrency('Values', 'Max', fnMaxVal)
    end { if };
  fsMaxVal := InWords(fnMaxVal);
  fnMaxLen := Length(fsMaxVal);
  WriteString('Values', 'Text', fsMaxVal);
  WriteInteger('Values', 'Length', fnMaxLen);
  fnThreads := 0;
  fnTick := GetTickCount();
  fSynchro := TCriticalSection.Create();
end { Create };


const
  cnMaxWorkers = 12;


var
  Workers:  array[1..cnMaxWorkers]  of TWorker;
  nWorker:  Integer;


class destructor TWorker.Destroy;
begin
  fSynchro.Free();
  WriteCurrency('Values', 'Current', fnValue);
  WriteString('Values', 'Text', fsMaxVal);
  WriteInteger('Values', 'Length', fnMaxLen);
end { Destroy };


procedure  TWorker.Execute();

var
  nVal:  Currency;
  nEnd:  Currency;
  sVal:  string;
  nMax:  Cardinal;
  nLen:  Cardinal;
  nCnt:  Cardinal;

begin  { Execute }
  // inherited;
  fSynchro.Enter();
  Inc(fnThreads);
  fSynchro.Leave();
  nVal := 0;
  nMax := 0;
  nEnd := 0;
  while  not Terminated  do
    begin
      fSynchro.Enter();
      try
        if  fnValue = MaxCurrency  then
          Break;
        nVal := fnValue;
        nCnt := Random(50000);
        if  nVal < MaxCurrency - nCnt  then
          nEnd := nVal + nCnt
        else
          nEnd := Int(MaxCurrency);
        fnValue := nEnd;
        nMax := fnMaxLen;
        if  GetTickCount() - fnTick >= 200  then
          begin
            write(CR, nVal:1:0);
            fnTick := GetTickCount()
          end { if }
      finally
        fSynchro.Leave()
      end { try-finally };
      repeat
        nVal := nVal + 1;
        sVal := InWords(nVal);
        nLen := Length(sVal);
        if  nLen > nMax  then
          begin
            fSynchro.Enter();
            try
              if  nLen > fnMaxLen  then
                begin
                  fnMaxVal := nVal;
                  fsMaxVal := sVal;
                  fnMaxLen := nLen;
                  writeln(nVal:1:0);
                  WriteCurrency('Values', 'Max', fnMaxVal);
                  WriteString('Values', 'Text', fsMaxVal);
                  WriteInteger('Values', 'Length', fnMaxLen);
                  fnTick := GetTickCount()
                end { nLen > fnMaxLen }
            finally
              fSynchro.Leave()
            end { try-finally }
          end { nLen > nMax }
      until  nVal = nEnd;
      fSynchro.Enter();
      try
        WriteCurrency('Values', 'Current', fnValue)
      finally
        fSynchro.Leave()
      end { try-finally }
    end { while };
  fSynchro.Enter();
  try
    Dec(fnThreads);
    if  fnThreads = 0  then
      begin
        writeln('Najd�u�szy napis:');
        writeln('- ', fsMaxVal);
        writeln('- ', fnMaxVal:1:0);
        writeln('- ', fnMaxLen, ' zn.');
        writeln;
        write('[Enter] '); readln;
      end { fnThreads = 0 };
  finally
    fSynchro.Leave()
  end { try-finally }
end { Execute };


begin  { MaxInWords3 }
  try
    for  nWorker := 1  to  cnMaxWorkers  do
      Workers[nWorker] := TWorker.Create();
    for  nWorker := 1  to  cnMaxWorkers  do
      Workers[nWorker].WaitFor()
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
