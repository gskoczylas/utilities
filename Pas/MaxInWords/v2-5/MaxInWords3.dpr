program MaxInWords3;

{$APPTYPE CONSOLE}

uses
  Windows, SysUtils, Classes, SyncObjs,
  zz_GSkStrUtils, zz_GSkASCII;

type
  TWorker = class(TThread)
  strict private
    class var
      fnValue:   Currency;
      fnMaxVal:  Currency;
      fsMaxVal:  string;
      fnMaxLen:  Cardinal;
      fnThreads: Cardinal;
      fnTick:    Cardinal;
      fSynchro:  TCriticalSection;
    class constructor  Create();
    class destructor  Destroy();
  strict protected
    procedure  Execute();                                            override;
  end { TWorker };


class constructor  TWorker.Create();
begin
  fnValue := 999;
  while  fnValue < MaxCurrency / 1000  do
    fnValue := fnValue * 1000 + 999;
  fnMaxVal := fnValue;
  fsMaxVal := InWords(fnMaxVal);
  fnMaxLen := Length(fsMaxVal);
  fnThreads := 0;
  fnTick := GetTickCount();
  fSynchro := TCriticalSection.Create();
end { Create };


const
  cnMaxWorkers = 80;


var
  Workers:  array[1..cnMaxWorkers]  of TWorker;
  nWorker:  Integer;


class destructor TWorker.Destroy;
begin
  fSynchro.Free()
end;

procedure  TWorker.Execute();

var
  nVal:  Currency;
  sVal:  string;
  nMax:  Cardinal;
  nLen:  Cardinal;

begin  { Execute }
  // inherited;
  fSynchro.Enter();
  Inc(fnThreads);
  fSynchro.Leave();
  nVal := 0;
  nMax := 0;
  while  not Terminated  do
    begin
      fSynchro.Enter();
      try
        if  fnValue = MaxCurrency  then
          Break;
        fnValue := fnValue + 1;
        nVal := fnValue;
        nMax := fnMaxLen;
        if  GetTickCount() - fnTick >= 200  then
          begin
            write(CR, nVal:1:0);
            fnTick := GetTickCount()
          end { if }
      finally
        fSynchro.Leave()
      end { try-finally };
      sVal := InWords(nVal);
      nLen := Length(sVal);
      if  nLen > nMax  then
        begin
          fSynchro.Enter();
          try
            if  nLen > fnMaxLen  then
              begin
                fnMaxVal := nVal;
                fsMaxVal := sVal;
                fnMaxLen := nLen;
                writeln(nVal);
                fnTick := GetTickCount()
              end { nLen > fnMaxLen }
          finally
            fSynchro.Leave()
          end { try-finally }
        end { nLen > nMax }
    end { while };
  fSynchro.Enter();
  try
    Dec(fnThreads);
    if  fnThreads = 0  then
      begin
        writeln('Najd�u�szy napis:');
        writeln('- ', fsMaxVal);
        writeln('- ', fnMaxVal);
        writeln('- ', fnMaxLen, ' zn.');
        writeln;
        write('[Enter] '); readln;
      end { fnThreads = 0 };
  finally
    fSynchro.Leave()
  end { try-finally }
end { Execute };

begin  { MaxInWords3 }
  try
    for  nWorker := 1  to  cnMaxWorkers  do
      Workers[nWorker] := TWorker.Create();
    for  nWorker := 1  to  cnMaxWorkers  do
      Workers[nWorker].WaitFor()
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
