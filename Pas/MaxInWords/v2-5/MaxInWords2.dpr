program MaxInWords2;

{$APPTYPE CONSOLE}

uses
  Windows, SysUtils, zz_GSkStrUtils, zz_GSkASCII;

var
  nVal:  Currency;
  nMV:   Currency;
  nML:   Cardinal;
  nLen:  Cardinal;
  nTck:  Cardinal;
  sBuf:  string;
  sMax:  string;

begin
  try
    nML := 0;
    nVal := 0;
    nMV := 0;
    repeat
      nVal := nVal + 1;
      write(CR, nVal:1:0);
      sBuf := InWords(nVal);
      nLen := Length(sBuf);
      if  nLen > nML  then
        begin
          nMV := nVal;
          sMax := sBuf;
          nML := nLen
        end { nLen > nMax }
    until  nVal = 999;
    nVal := nMV;
    while  nVal < MaxCurrency / 1000  do
      nVal := nVal * 1000 + nMV;
    nTck := GetTickCount();
    while  nVal < MaxCurrency  do
      begin
        if  GetTickCount() - nTck >= 333   then
          begin
            write(CR, nVal:1:0);
            nTck := GetTickCount()
          end { if };
        sBuf := InWords(nVal);
        nLen := Length(sBuf);
        if  nLen > nML  then
          begin
            nMV := nVal;
            sMax := sBuf;
            nML := nLen
          end { nLen > nMax };
        nVal := nVal + 1
      end { while };
    writeln(CR, 'Najd�u�szy napis to:');
    writeln('- ', nMV);
    writeln('- ', sMax);
    writeln('- ', nML);
    readln;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
