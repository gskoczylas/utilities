program MaxInWords5;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  zz_GSkStrUtils, zz_GSkIniFileUtils;

var
  nVal:  Currency;
  nTmp:  Currency;
  nMul:  Currency;
  fnValue:   Currency;
  fnMaxVal:  Currency;
  fsMaxVal:  string;
  fnMaxLen:  Cardinal;

  procedure  ChkLen(const   nValue:  Currency);

  var
    nLen:  Cardinal;

  begin  { ChkLen }
    nLen := Length(InWords(nValue));
    if  nLen >= fnMaxLen  then
      begin
        fnMaxVal := nValue;
        fnMaxLen := nLen
      end { nLen >= fnMaxLen }
  end { ChkLen };

  procedure  Mul(const   nMul:  Currency);
  begin
    while  MaxCurrency - fnValue >= nMul  do
      begin
        fnValue := fnValue + nMul;
        ChkLen(fnValue)
      end { while }
  end { Mul };

begin  { MaxInWords5 }
  try
    fnValue := 999;
    nMul := 1000;
    while  MaxCurrency/10 - fnValue >= 10  do
      begin
        fnValue := fnValue * 10 + 9;
        nMul := nMul * 10
      end { while };
    fnMaxVal := fnValue;
    nTmp := fnValue;
    fnMaxLen := Length(InWords(fnMaxVal));
    Mul(nMul);
    fnValue := fnValue + 1;
    nVal := nTmp;
    while  nVal <> 0  do
      begin
        if  MaxCurrency - fnValue > nVal  then
          begin
            fnValue := fnValue + nVal;
            ChkLen(fnValue);
            fnValue := fnValue + 1;
            Mul(nMul)
          end { if };
        nVal := Int(nVal / 10);
        nMul := Int(nMul / 10)
      end { while nVal <> 0 };
    fsMaxVal := InWords(fnMaxVal);
    fnMaxLen := Length(fsMaxVal);
    while  MaxCurrency - fnValue > 1  do
      begin
        fnValue := fnValue + 1;
        ChkLen(fnValue)
      end { while MaxCurrency - fnValue > 1 };
    WriteCurrency('Result', 'MaxVal', fnMaxVal);
    WriteString('Result', 'MaxTxt', fsMaxVal);
    WriteInteger('Result', 'MaxLen', fnMaxLen);
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
