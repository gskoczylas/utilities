unit  MainForm;


interface


uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, IniFiles, SyncObjs, ExtCtrls, AppEvnts, JvComponent,
  JvTrayIcon, ActnList, Menus, StrUtils, DateUtils, JvComponentBase;


type
  TMaxInWords = class(TThread)
  private
    nCurr, nMax, nMin:  Currency;
    nCurrLen, nMaxLen, nMinLen:  Integer;
    nLastTime:  LongWord;
    bFound, bFoundMax:  Boolean;
    sCurrText:  string;
    nSum, nCnt:  Int64;
    procedure  SaveData();
    procedure  Finished();
  protected
    procedure  Execute();                                            override;
  public
    constructor  Create();
  end { TMaxInWords };

  TfMainForm = class(TForm)
    lbMaxCurrencyInWords:  TLabel;
    stMaxCurrencyInWords:  TStaticText;
    lbCurrencyInWordsLen:  TLabel;
    stCurrencyInWordsLen:  TStaticText;
    stCurrCurrency:        TStaticText;
    btStop:                TButton;
    lbCurrCurrency:        TLabel;
    tmFound:               TTimer;
    appEvents:             TApplicationEvents;
    appTrayIcon:           TJvTrayIcon;
    mnTray:                TPopupMenu;
    alActions:             TActionList;
    aTerminate:            TAction;
    aFormShow:             TAction;
    aFormHide:             TAction;
    mnFormShow:            TMenuItem;
    mnFormHide:            TMenuItem;
    N1:                    TMenuItem;
    mnTerminate:           TMenuItem;
    tmMarker:              TTimer;
    lbSpeed:               TLabel;
    stSpeed:               TStaticText;
    lbAverage:             TLabel;
    stAverage:             TStaticText;
    procedure  FormCreate(Sender:  TObject);
    procedure  FormClose(Sender:  TObject;
                         var   Action:  TCloseAction);
    procedure  FormShow(Sender:  TObject);
    procedure  tmFoundTimer(Sender:  TObject);
    procedure  appEventsRestore(Sender:  TObject);
    procedure  aTerminateExecute(Sender:  TObject);
    procedure  aFormShowExecute(Sender:  TObject);
    procedure  aFormHideExecute(Sender:  TObject);
    procedure  aFormShowUpdate(Sender:  TObject);
    procedure  aFormHideUpdate(Sender:  TObject);
    procedure  appEventsMinimize(Sender:  TObject);
    procedure  tmMarkerTimer(Sender:  TObject);
  private
    iniConfig:  TIniFile;
    csSynchro:  TCriticalSection;
    MaxInWords:  TMaxInWords;
    nMaxCurr, nMinCurr, nLastCurr, nStartCurr, nPrevCurr:  Currency;
    nMaxLen, nMinLen, nLastLen:  Integer;
    bMinimized:  Boolean;
    nStartTime:  LongWord;
    nSumLen, nCntLen:  Int64;
    function  AverageSpeed() : Double;
    procedure  ShowState(const   bShowInWords:  Boolean);
    procedure  SaveState(const   sStep:  string);
    procedure  SaveStep(const   sStep:  string);
    procedure  WMQueryEndSession(var   Msg:  TWMQueryEndSession);    message WM_QUERYENDSESSION;
    procedure  WMEndSession(var   Msg:  TWMEndSession);              message WM_ENDSESSION;
  end { TfMainForm };


var
  fMainForm:  TfMainForm;


implementation


uses
  zz_GSkSystemUtils;


{$R *.dfm}


const
  csState      = 'State';
  csFound      = 'Found';
  csSpeed      = 'Average speed';
  csFormLeft   = 'Form Left';
  csFormTop    = 'Form Top';
  csFormWidth  = 'Form Width';
  csFormHeight = 'Form Height';
  csMaxCurr    = 'Max Currency';
  csMinCurr    = 'Min Currency';
  csLastCurr   = 'Last Currency';
  csStarted    = 'Started';
  csMarker     = 'Marker';
  csStopped    = 'Stopped';
  csEndSession = 'End Session';
  csFinished   = 'Finished';
  csLastPrefix = 'Last ';


function  InWords(nAmount:  Currency;
                  const   bCurrency:  Boolean)
                  : string;
const
  Units1:  array[1..19] of string = (
             'jeden',
             'dwa',
             'trzy',
             'cztery',
             'pi��',
             'sze��',
             'siedem',
             'osiem',
             'dziewi��',
             'dziesi��',
             'jedena�cie',
             'dwana�cie',
             'trzyna�cie',
             'czterna�cie',
             'pi�tna�cie',
             'szesna�cie',
             'siedemna�cie',
             'osiemna�cie',
             'dziewi�tna�cie');
  Units2:  array[2..9] of string = (
             'dwadzie�cia',
             'trzydzie�ci',
             'czterdzie�ci',
             'pi��dziesi�t',
             'sze��dziesi�t',
             'siedemdziesi�t',
             'osiemdziesi�t',
             'dziewi��dziesi�t');
  Units3:  array[1..9] of string = (
             'sto',
             'dwie�cie',
             'trzysta',
             'czterysta',
             'pi��set',
             'sze��set',
             'siedemset',
             'osiemset',
             'dziewi��set');

type
  TScope = array[1..3] of String;

const
  NamesE3:  TScope = ('tysi�c',    //  1
                      'tysi�ce',   //  2..4, 22..24, 32..34, 42..44, ...
                      'tysi�cy');  //  5..21, 25..31, 35..41, ...
  NamesE6:  TScope = ('milion',
                      'miliony',
                      'milion�w');
  NamesE9:  TScope = ('miliard',
                      'miliardy',
                      'miliard�w');
  NamesE12: TScope = ('bilion',
                      'biliony',
                      'bilion�w');

var
  nPos:  integer;
  nGrosze:  integer;

  function  InWordsNNN(nAmount:  Cardinal)
                       : string;
  begin
    Assert(nAmount < 1000);
    if  nAmount >= 100  then
      Result := Units3[nAmount div 100]
    else
      Result := '';
    nAmount := nAmount mod 100;
    if  nAmount >= 20  then
      begin
        Result := Result + ' ' + Units2[nAmount div 10];
        nAmount := nAmount mod 10
      end { nAmount >= 20 };
    Assert(nAmount < 20);
    if  nAmount > 0  then
      Result := Result + ' ' + Units1[nAmount];
  end { InWordsNNN };

  function  ScopeName(nAmount:  Cardinal;
                      Names:  TScope)
                      : string;
  begin
    Assert(nAmount < 1000);
    if  nAmount = 0  then
      Result := ''
    else if  nAmount = 1  then
      Result := Names[1]
    else if  ((nAmount mod 10) in [2..4])
             and not ((nAmount mod 100) in [12..14])  then
      Result := Names[2]
    else
      Result := Names[3]
  end { ScopeName };

  procedure  ConvertScope(var   nAmount:  Currency;
                          nScope:  Currency;
                          NameOfScope:  TScope;
                          var   sOutput: string);
  var
    nTmp:  Cardinal;

  begin  { ConvertScope }
    nTmp := Trunc(nAmount / nScope);
    sOutput := sOutput + ' '
               + InWordsNNN(nTmp) + ' ' + ScopeName(nTmp, NameOfScope);
    nAmount := nAmount - nTmp * nScope
  end { ConvertScope };

begin  { InWords }
  if  nAmount < 0  then
    Result := 'minus ' + InWords(-nAmount, bCurrency)
  else
    begin
      nGrosze := Round(Frac(nAmount) * 100);
      nAmount := Trunc(nAmount);
      if  nAmount = 0  then
        Result := 'zero'
      else
        begin
          Result := '';
          Assert(nAmount < 1E15);
          ConvertScope(nAmount, 1E12, NamesE12, Result);   //  Biliony
          ConvertScope(nAmount, 1E9,  NamesE9,  Result);   //  Miliardy
          ConvertScope(nAmount, 1E6,  NamesE6,  Result);   //  Miliony
          ConvertScope(nAmount, 1E3,  NamesE3,  Result);   //  Tysi�ce
          Result := Result + ' ' + InWordsNNN(Trunc(nAmount))
        end { nAmount <> 0 };
      Result := Trim(Result);
      nPos := Pos('  ', Result);
      while  nPos > 0  do
        begin
          Delete(Result, nPos, 1);
          nPos := Pos('  ', Result)
        end { while nPos > 0 };
      if  bCurrency  then
        Result := Format('%s z� %2.2u gr', [Result, nGrosze])
    end { InWords }
end { InWords };


{ TMaxInWords }

constructor  TMaxInWords.Create();
begin
  inherited Create(False);
  nCurr := fMainForm.nLastCurr;
  nMax := fMainForm.nMaxCurr;
  nMin := nMax;   // fMainForm.nMinCurr;
  nCurrLen := fMainForm.nLastLen;
  nMaxLen := fMainForm.nMaxLen;
  nMinLen := nMaxLen;   // fMainForm.nMinLen;
  Priority := tpIdle
end { Create };


procedure  TMaxInWords.Execute();
begin
  // inherited;  -- abstract
  repeat
    nCurr := nCurr + 1;
    sCurrText := InWords(nCurr, True);
    nCurrLen := Length(sCurrText);
    nSum := nSum + nCurrLen;
    nCnt := nCnt + 1;
    if  nCurrLen < nMinLen  then
      begin
        nMin := nCurr;
        nMinLen := nCurrLen
      end { nCurrLen < nMinLen };
    bFound := nCurrLen >= nMaxLen;
    if  bFound  then
      begin
        bFoundMax := nCurrLen > nMaxLen;
        nMax := nCurr;
        nMaxLen := nCurrLen;
        Synchronize(SaveData);
      end { bFound }
    else
      if  GetTickCount() - nLastTime >= 1000  then
        Synchronize(SaveData)
  until  Terminated
         or (nCurr = MaxCurrency);
  if  not Terminated  then
    Synchronize(Finished)
end { Execute };


procedure  TMaxInWords.Finished();
begin
  fMainForm.SaveState(csFinished);
  fMainForm.Update();
  fMainForm.appTrayIcon.ShowApplication();
  fMainForm.ShowState(True);
  SetForegroundWindow(fMainForm.Handle);
  BringWindowToTop(fMainForm.Handle);
  MessageBeep(MB_ICONINFORMATION);
  fMainForm.tmFound.Tag := 60000{minuta} div fMainForm.tmFound.Interval;
  fMainForm.tmFound.Enabled := True;
  MessageDlg('Koniec oblicze�', mtInformation, [mbOk], 0)
end { Finished };


procedure  TMaxInWords.SaveData();

var
  cIdent:  Char;
  sIdent:  string;

begin  { SaveData }
  fMainForm.nMaxCurr := nMax;
  fMainForm.nMinCurr := nMin;
  fMainForm.nLastCurr := nCurr;
  fMainForm.nMaxLen := nMaxLen;
  fMainForm.nMinLen := nMinLen;
  fMainForm.nLastLen := nCurrLen;
  fMainForm.appTrayIcon.Hint := sCurrText;
  fMainForm.nSumLen := fMainForm.nSumLen + nSum;
  fMainForm.nCntLen := fMainForm.nCntLen + nCnt;
  nSum := 0;
  nCnt := 0;
  if  bFoundMax  then
    begin
      fMainForm.tmFound.Tag := 60000{minuta} div fMainForm.tmFound.Interval;
      fMainForm.tmFound.Enabled := True
    end { bFoundMax };
  fMainForm.ShowState(bFound);
  if  bFound  then
    begin
      fMainForm.SaveState(csFound);
      cIdent := Pred('A');
      sIdent := IntToStr(nCurrLen);
      repeat
        Inc(cIdent);
        sIdent := Format('%d.%s', [nCurrLen, cIdent])
      until  not fMainForm.iniConfig.ValueExists(csFound, sIdent);
      fMainForm.iniConfig.WriteString(csFound,
                                      sIdent,
                                      Format('%m - %s',
                                             [nCurr, sCurrText]));
      fMainForm.iniConfig.UpdateFile();
      fMainForm.appTrayIcon.BalloonHint(csFound,
                                        Format('%m = %d zn.'#13#13'%s',
                                               [nMax, Length(sCurrText),
                                                sCurrText]),
                                        btInfo)
    end { bFound };
  nLastTime := GetTickCount()
end { SaveData };


{ TfMainForm }

procedure  TfMainForm.FormCreate(Sender:  TObject);
begin
  SetPriority(cpIdle);
  csSynchro := TCriticalSection.Create();
  iniConfig := TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
  Left := iniConfig.ReadInteger(csState, csFormLeft, Left);
  Top := iniConfig.ReadInteger(csState, csFormTop, Top);
  Width := iniConfig.ReadInteger(csState, csFormWidth, Width);
  Height := iniConfig.ReadInteger(csState, csFormHeight, Height);
  nLastCurr := iniConfig.ReadFloat(csState, csLastCurr, -1);
  if  nLastCurr >= 0  then
    begin
      nMaxCurr := iniConfig.ReadFloat(csState, csMaxCurr, nMaxCurr);
      nMinCurr := nMaxCurr;
      nLastLen := Length(InWords(nLastCurr, True));
      nMaxLen := Length(InWords(nMaxCurr, True));
      nMinLen := nMaxLen;
      nPrevCurr := nLastCurr;
      nStartCurr := nLastCurr;
    end { nLastCurr >= 0 }
  else
    begin
      nMaxCurr := 0.0;
      nMinCurr := 0.0;
      nLastLen := 0;
      nMaxLen := 0;
      nMinLen := MaxInt;
      nPrevCurr := 0.0;
      nStartCurr := 0.0;
    end { nLastCurr < 0 };
  nStartTime := GetTickCount();
  SaveStep(csStarted);
  iniConfig.UpdateFile();
  ShowState(True);
  bMinimized := True;
  MaxInWords := TMaxInWords.Create()
end { FormCreate };


procedure  TfMainForm.FormClose(Sender:  TObject;
                                var   Action:  TCloseAction);
begin
  MaxInWords.Terminate();
  MaxInWords.WaitFor();
  if  WindowState = wsNormal  then
    begin
      iniConfig.WriteInteger(csState, csFormLeft, Left);
      iniConfig.WriteInteger(csState, csFormTop, Top);
      iniConfig.WriteInteger(csState, csFormWidth, Width);
      iniConfig.WriteInteger(csState, csFormHeight, Height)
    end { WindowState <> wsNormal };
  iniConfig.WriteString(csSpeed,
                        Format('%s',
                               [FormatDateTime('ddddd t', Now())]),
                        Format('%d -> %d - %d %.1f',
                               [Length(Format('%.0f', [nLastCurr])),
                                nMinLen, nMaxLen,
                                AverageSpeed()]));
  SaveState(csStopped);
  iniConfig.Free();
  csSynchro.Free()
end { FormClose };


procedure  TfMainForm.FormShow(Sender:  TObject);
begin
  ShowState(True)
end { FormShow };


procedure  TfMainForm.ShowState(const   bShowInWords:  Boolean);

var
  nAvg:  Double;

begin
  csSynchro.Enter();
  try
    stCurrCurrency.Caption := Format(' %m ', [nLastCurr]);
    if  nCntLen = 0  then
      nAvg := 0
    else
      nAvg := nSumLen / nCntLen;
    stCurrencyInWordsLen.Caption := Format(' %d / %d / %d / %.1f ',
                                           [nLastLen, nMinLen, nMaxLen, nAvg]);
    stSpeed.Caption := Format(' %.0n ', [nLastCurr - nPrevCurr]);
    stAverage.Caption := Format(' %.0n ',
                                [AverageSpeed()]);
    nPrevCurr := nLastCurr;
    if  bShowInWords  then
      begin
        stMaxCurrencyInWords.Caption := InWords(nMaxCurr, True);
        Caption := stCurrCurrency.Caption;
        Application.Title := Caption
      end { bShowInWords };
    if  not tmFound.Enabled  then
      if  Tag = 0  then
        begin
          Caption := '* ' + stCurrCurrency.Caption + ' *';
          Application.Title := '*' + stCurrCurrency.Caption + '*';
          Tag := 5
        end { Tag = 0 }
      else
        Tag := Tag - 1
  finally
    csSynchro.Leave()
  end { try-finally }
end { ShowState} ;


procedure  TfMainForm.tmFoundTimer(Sender:  TObject);
begin
  csSynchro.Enter();
  try
    if  tmFound.Tag > 0  then
      begin
        FlashWindow(Handle, True);
        FlashWindow(Application.Handle, True);
        tmFound.Tag := tmFound.Tag - 1
      end { tmFound.Tag }
    else
      begin
        FlashWindow(Handle, False);
        FlashWindow(Application.Handle, False);
        tmFound.Enabled := False
      end { tmFount.Tag = 0 }
  finally
    csSynchro.Leave()
  end { try-finally }
end { tmFoundTimer };


procedure  TfMainForm.appEventsRestore(Sender:  TObject);
begin
  csSynchro.Enter();
  try
    if  tmFound.Enabled  then
      tmFound.Tag := 1
  finally
    csSynchro.Leave()
  end { try-finally };
  bMinimized := False
end { appEventsRestore };


procedure  TfMainForm.aTerminateExecute(Sender:  TObject);
begin
  if  MessageDlg('Program zako�czy prac�',
                 mtConfirmation, mbOKCancel, 0) = mrOk  then
    Close()
end { aTerminateExecute };


procedure  TfMainForm.aFormShowExecute(Sender:  TObject);
begin
  appTrayIcon.ShowApplication()
end { aFormShowExecute };


procedure  TfMainForm.aFormHideExecute(Sender:  TObject);
begin
  appTrayIcon.HideApplication()
end { aFormHideExecute };


procedure  TfMainForm.aFormShowUpdate(Sender:  TObject);
begin
  aFormShow.Enabled := bMinimized
end { aFormShowUpdate };


procedure  TfMainForm.aFormHideUpdate(Sender:  TObject);
begin
  aFormHide.Enabled := not bMinimized
end { aFormHideUpdate };


procedure  TfMainForm.appEventsMinimize(Sender:  TObject);
begin
  bMinimized := True
end { appEventsMinimize };


procedure  TfMainForm.tmMarkerTimer(Sender:  TObject);
begin
  SaveState(csMarker)
end { tmMarkerTimer };


procedure  TfMainForm.SaveState(const   sStep:  string);
begin
  iniConfig.WriteFloat(csState, csMaxCurr, nMaxCurr);
  iniConfig.WriteString(csState, csMinCurr,
                        IntToStr(nMinLen)
                          + ' --> '
                          + FloatToStr(nMinCurr)
                          + ' = '
                          + InWords(nMinCurr, True));
  iniConfig.WriteFloat(csState, csLastCurr, nLastCurr);
  SaveStep(sStep);
  iniConfig.UpdateFile()
end { SaveState };


function  TfMainForm.AverageSpeed() : Double;

var
  nDelay:  Cardinal;

begin  { AverageSpeed }
  nDelay := GetTickCount() - nStartTime;
  if  nDelay <> 0  then
    Result := (nLastCurr - nStartCurr)
                / (nDelay * 0.001)
  else
    Result := 0
end { AverageSpeed };


procedure  TfMainForm.WMQueryEndSession(var   Msg:  TWMQueryEndSession);
begin
  inherited
end { WMQueryEndSession };


procedure  TfMainForm.WMEndSession(var   Msg:  TWMEndSession);
begin
  if  Msg.EndSession  then
    begin
//    SaveState(csEndSession);
      SaveStep(csEndSession);
      Close()
    end  { Msg.EndSession };  
  inherited
end { WMEndSession };


procedure  TfMainForm.SaveStep(const   sStep:  string);
begin
  iniConfig.WriteDateTime(csState,
                          csLastPrefix +  sStep,
                          Now());
end { SaveStep };


end.
