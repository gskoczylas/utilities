object fMainForm: TfMainForm
  Left = 459
  Top = 83
  Caption = 'MaxInWords'
  ClientHeight = 103
  ClientWidth = 434
  Color = clBtnFace
  Constraints.MinHeight = 105
  Constraints.MinWidth = 450
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  ScreenSnap = True
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  DesignSize = (
    434
    103)
  PixelsPerInch = 96
  TextHeight = 13
  object lbMaxCurrencyInWords: TLabel
    Left = 8
    Top = 8
    Width = 88
    Height = 13
    Alignment = taRightJustify
    Caption = 'Najd'#322'u'#380'sza kwota:'
  end
  object lbCurrencyInWordsLen: TLabel
    Left = 8
    Top = 87
    Width = 74
    Height = 13
    Alignment = taRightJustify
    Anchors = [akLeft, akBottom]
    Caption = 'Liczba znak'#243'w:'
  end
  object lbCurrCurrency: TLabel
    Left = 8
    Top = 68
    Width = 73
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'Bie'#380#261'ca kwota:'
  end
  object lbSpeed: TLabel
    Left = 240
    Top = 68
    Width = 49
    Height = 13
    Anchors = [akRight, akBottom]
    Caption = 'Szybko'#347#263':'
  end
  object lbAverage: TLabel
    Left = 240
    Top = 87
    Width = 39
    Height = 13
    Anchors = [akRight, akBottom]
    Caption = #346'rednio:'
  end
  object stMaxCurrencyInWords: TStaticText
    Left = 104
    Top = 8
    Width = 333
    Height = 55
    Anchors = [akLeft, akTop, akRight, akBottom]
    AutoSize = False
    BorderStyle = sbsSunken
    Caption = 'stMaxCurrencyInWords'
    TabOrder = 0
  end
  object stCurrencyInWordsLen: TStaticText
    Left = 104
    Top = 87
    Width = 57
    Height = 17
    Anchors = [akLeft, akBottom]
    BorderStyle = sbsSunken
    Caption = ' 123 / 123 '
    TabOrder = 1
  end
  object stCurrCurrency: TStaticText
    Left = 104
    Top = 68
    Width = 67
    Height = 17
    Alignment = taRightJustify
    Anchors = [akLeft, akBottom]
    BorderStyle = sbsSunken
    Caption = ' 12345,00 z'#322' '
    TabOrder = 2
  end
  object btStop: TButton
    Left = 362
    Top = 76
    Width = 75
    Height = 25
    Action = aTerminate
    Anchors = [akRight, akBottom]
    TabOrder = 3
  end
  object stSpeed: TStaticText
    Left = 296
    Top = 68
    Width = 16
    Height = 17
    Anchors = [akRight, akBottom]
    BorderStyle = sbsSunken
    Caption = ' 0 '
    TabOrder = 4
  end
  object stAverage: TStaticText
    Left = 296
    Top = 87
    Width = 16
    Height = 17
    Anchors = [akRight, akBottom]
    BorderStyle = sbsSunken
    Caption = ' 0 '
    TabOrder = 5
  end
  object tmFound: TTimer
    Enabled = False
    Interval = 2000
    OnTimer = tmFoundTimer
    Left = 216
    Top = 32
  end
  object appEvents: TApplicationEvents
    OnMinimize = appEventsMinimize
    OnRestore = appEventsRestore
    Left = 280
    Top = 32
  end
  object appTrayIcon: TJvTrayIcon
    Active = True
    IconIndex = -1
    DropDownMenu = mnTray
    PopupMenu = mnTray
    Snap = True
    Visibility = [tvVisibleTaskList, tvAutoHide, tvRestoreClick, tvMinimizeClick]
    Left = 312
    Top = 32
  end
  object mnTray: TPopupMenu
    Left = 344
    Top = 32
    object mnFormShow: TMenuItem
      Action = aFormShow
    end
    object mnFormHide: TMenuItem
      Action = aFormHide
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object mnTerminate: TMenuItem
      Action = aTerminate
    end
  end
  object alActions: TActionList
    Left = 376
    Top = 32
    object aTerminate: TAction
      Caption = '&Stop'
      OnExecute = aTerminateExecute
    end
    object aFormShow: TAction
      Caption = '&Poka'#380
      OnExecute = aFormShowExecute
      OnUpdate = aFormShowUpdate
    end
    object aFormHide: TAction
      Caption = '&Ukryj'
      OnExecute = aFormHideExecute
      OnUpdate = aFormHideUpdate
    end
  end
  object tmMarker: TTimer
    Interval = 900000
    OnTimer = tmMarkerTimer
    Left = 248
    Top = 32
  end
end
