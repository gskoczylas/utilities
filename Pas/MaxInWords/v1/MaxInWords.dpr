program MaxInWords;

uses
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Forms,
  MainForm in 'MainForm.pas' {fMainForm};

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TfMainForm, fMainForm);
  Application.ShowMainForm := False;
  Application.Run;
end.

