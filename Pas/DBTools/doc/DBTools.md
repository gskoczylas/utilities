﻿DBTools
=======

Niewielkie programy pomocnicze, przeznaczone do pracy z bazami danych _SLQ Server_.
Łączą się z bazą danych używając _Windows Authentication_.

ExportDB
--------

Program zapisuje zawartość wszystkich tabel systemu do pliku lub plików CSV.

### Parametry

#### Output

W tym polu należy wpisać ścieżkę do wynikowego pliku.

#### Seperate file for every table

Jeżeli to pole jest _zaznaczone_, to do powyższej nazwy pliku
będą dołączane nazwy tabel.

FindInDB
--------

Program szuka wskazanego tekstu w całej bazie danych.

### Parametry

#### First match

Jeżeli to pole jest zaznaczone, to program wyświetli tylko pierwsze
wystąpienie wskazanego tekstu w każdej tabeli. Taki tryb jest przydatny wtedy,
gdy chcemy się tylko dowiedzieć, w których tabelach występuje szukany tekst.

Jeżeli to pole nie jest zaznaczone, to program przeszukuje wszystkie rekordy
w każdej tabeli bazy danych.

#### Exact match

Jeżeli to pole _nie_ jest zaznaczone, to program znajduje również pozycje,
gdzie szukany tekst jest fragmentem wartości danego pola.

#### Regular expression

Można wskazać standardowe wyrażenie regularne w formacie PCPRE.

Wyrażenia regularne można testować między innymi na stronie internetowej
https://regex101.com lub https://regexr.com.

#### Order

Tabele są przeszukiwane wielowątkowo. Liczba wątków jest proporcjonalna
do liczby procesorów w systemie.

Jeżeli tabele są przeszukiwane w kolejności alfabetycznej, to może się
zdarzyć, że jedna z ostatnich tabel jest duża. Wtedy czasem ten ostatni
wątek może dość długo jeszcze działać.

Często przeszukiwanie bazy danych zakończy się szybciej, jeżeli tabele są
przeszukiwane w kolejności malejącej liczby rekordów. Wtedy, zanim duże
tabele zostaną przeszukane, w międzyczasie inne wątki przeszukają mniejsze
tabele.

Problemy
--------

1. Jeżeli program informuje, że nie może zalogować z niezaufanej witryny,
   to w pliku `c:\Windows\System32\drivers\etc\hosts` dodaj wiersz
   `127.0.0.1  localhost`.
2. Jeżeli _SQL Server_ nie chce zaakceptować _Windows Authentication_,
   to zaloguj się do serwera w _SQL Server Management Studio_
   i dopisz swojego użytkownika Windows do listy użytkowników.
