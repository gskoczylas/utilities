object frmMain: TfrmMain
  Left = 0
  Top = 0
  ActiveControl = edtHost
  Caption = 'Find text in database'
  ClientHeight = 461
  ClientWidth = 569
  Color = clWindow
  Constraints.MinHeight = 500
  Constraints.MinWidth = 585
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  ScreenSnap = True
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  DesignSize = (
    569
    461)
  PixelsPerInch = 96
  TextHeight = 13
  object lblHost: TLabel
    Left = 16
    Top = 24
    Width = 26
    Height = 13
    Caption = '&Host:'
    FocusControl = edtHost
  end
  object lblDtabase: TLabel
    Left = 16
    Top = 106
    Width = 50
    Height = 13
    Caption = '&Database:'
    FocusControl = edtDatabase
  end
  object lblFind: TLabel
    Left = 16
    Top = 142
    Width = 24
    Height = 13
    Caption = '&Find:'
    FocusControl = edtFind
  end
  object lblResults: TLabel
    Left = 16
    Top = 253
    Width = 35
    Height = 13
    Caption = '&Results'
    FocusControl = edtResults
  end
  object lblUserName: TLabel
    Left = 184
    Top = 50
    Width = 26
    Height = 13
    Caption = '&User:'
    Enabled = False
    FocusControl = edtUserName
  end
  object lblPassword: TLabel
    Left = 184
    Top = 75
    Width = 50
    Height = 13
    Caption = '&Password:'
    Enabled = False
    FocusControl = edtHost
  end
  object edtHost: TJvComboEdit
    Left = 72
    Top = 21
    Width = 205
    Height = 19
    Hint = 'Server name or IP address'
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    Text = '127.0.0.1'
    OnButtonClick = edtHostButtonClick
    OnChange = edtHostChange
    TextHint = 'Server'
  end
  object edtDatabase: TJvComboBox
    Left = 72
    Top = 103
    Width = 297
    Height = 21
    Style = csDropDownList
    Anchors = [akLeft, akTop, akRight]
    Flat = True
    TabOrder = 5
    Text = ''
    OnChange = edtDatabaseChange
    OnEnter = edtDatabaseEnter
  end
  object edtFind: TJvEdit
    Left = 72
    Top = 139
    Width = 297
    Height = 19
    TabOrder = 6
    Text = ''
    OnChange = edtFindChange
  end
  object edtCaseSensitive: TJvCheckBox
    Left = 72
    Top = 166
    Width = 91
    Height = 17
    Caption = '&Case Sensitive'
    TabOrder = 7
    LinkedControls = <>
  end
  object btnSearch: TJvBitBtn
    Left = 408
    Top = 15
    Width = 113
    Height = 25
    Anchors = [akTop, akRight]
    Caption = '&Search'
    Default = True
    Enabled = False
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000F00A0000F00A00000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8F8E2
      E3E3FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFE6E6E67E7571957D6E807E7EF7F7F7FFFFFFFFFFFFE3E3
      E3F2F2F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5E5E5717273F9C694FF
      CC9ADEB48FA5A6A8FCFCFC999899977D6D7E766FCDCDCDFFFFFFFFFFFFFFFFFF
      FFFFFFE4E4E46E6E6F908C8BFFE4C0FFE8CDFFE2B36C6C6C7D7F81C0A388FFD3
      A1FFCA9984827FFFFFFFFFFFFFFFFFFFDEDEDE5050509D9D9DB6B8BAC5B0A7FF
      F5EEF2D3B643454896989AD5BBA7FFF0DAFFE8C4908275FFFFFFFFFFFFD4D4D4
      7171717A7A7A929292EEEEEEE3E2E4897D7B6864655D5E5ECBCBCCC1BBBAE4D3
      CEF9DBC3989693FFFFFFFFFFFF555555AAAAAAAEAEAE9D9D9D9A9A9A73737348
      48485A5A5A818181FFFFFFFFFFFF868484AEADADF5F5F5FFFFFFFFFFFF656565
      858585D2D2D2C3C3C3B3B3B39898988181817171717373736161617E7E7EE4E4
      E4FFFFFFFFFFFFFFFFFFFFFFFFD9D9D98585859A9A9A9E9E9E8A8A8A8D8D8D97
      97979494947D7D7DCCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFF7F7F7BEBEBE969696C7C7C7FBFBFBFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    TabOrder = 12
    OnClick = btnSearchClick
  end
  object btnExit: TJvBitBtn
    Left = 408
    Top = 50
    Width = 113
    Height = 25
    Anchors = [akTop, akRight]
    Cancel = True
    Caption = 'E&xit'
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000F00A0000F00A00000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFF6F1F19A6C6CA37F7FDECFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFABAAAA848182807C7DD3ACAD9D69698E5B5B986A
      6ACBB5B5FAF8F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF83828368
      716B89B499DAB7B5A26E6EA47171A371718E5F5F635E5EFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFF3CA63DC1BFC15B726007A72FCEB7ADA67172A57272A572
      72AA74745D5353FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF05900645A046A6
      B2A9008814D2B6AFAA7576A57272A87474AD7777635959FFFFFFFFFFFF0C940D
      0F97110F97111399162DC649109C19399A3A7BB57CD2B4AEAD898A998888A875
      75AF7A7A665C5CFFFFFFFFFFFF0B950D4EE7814EE7814EE7814EE7814CE57E13
      9F1D36A83FEDDADBC8B0B0C9C1C1AF7A7AB27C7C6B6262FFFFFFFFFFFF0C960E
      56EF8956EF8956EF8956EF8954ED8715A1203AA73BF1DCDCB27C7CB47F7FB380
      80B681816F6666FFFFFFFFFFFF1D9B1D179F1D179F1D159C1A36CF5414A21F30
      9930FFFFFDE9C5C5B68282B78484B68383B98282736969FFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFF04900544A245C0BEBDFFF7E3E9C6C5C99797D6A9A9C693
      93BB8585746D6DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF48AC49CECECE88
      837FFFEACAECC7C5D5ADAEFFF8F8FFECECE6B5B5786E6EFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFA6A7A78C857DFFE3B5EFC9C9C59494D5AEAEDCB4
      B4D19D9D7B7272FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFADADAD82
      8382858481C5A6A6C6A0A0C59999C49494C78E8E7F7575FFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFB2B2B2ACACADA7A7A89FA0A0979999909393878B
      8B868888A0A1A1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    TabOrder = 14
    OnClick = btnExitClick
  end
  object edtResults: TJvListView
    Left = 8
    Top = 269
    Width = 553
    Height = 184
    Anchors = [akLeft, akTop, akRight, akBottom]
    Columns = <
      item
        Caption = 'Table'
        Width = -1
        WidthType = (
          -1)
      end
      item
        Alignment = taRightJustify
        Caption = 'Row#'
        Width = -1
        WidthType = (
          -1)
      end
      item
        Caption = 'Field'
        Width = -1
        WidthType = (
          -1)
      end
      item
        Caption = 'Value'
        Width = -1
        WidthType = (
          -1)
      end>
    HotTrack = True
    IconOptions.AutoArrange = True
    ReadOnly = True
    RowSelect = True
    SortType = stText
    TabOrder = 15
    ViewStyle = vsReport
    AutoSelect = False
    ColumnsOrder = '0=-1,1=-1,2=-1,3=-1'
    ExtendedColumns = <
      item
      end
      item
      end
      item
      end
      item
      end>
  end
  object txtThreads: TJvStaticText
    Left = 96
    Top = 249
    Width = 71
    Height = 17
    Caption = 'Threads: NNN'
    Layout = tlTop
    TabOrder = 16
    TextMargins.X = 0
    TextMargins.Y = 0
    WordWrap = False
  end
  object edtFirstMatch: TJvCheckBox
    Left = 72
    Top = 186
    Width = 74
    Height = 17
    Hint = 'Show only the first record found for each table in the database.'
    Caption = 'First &Match'
    TabOrder = 8
    LinkedControls = <>
  end
  object txtFound: TJvStaticText
    Left = 209
    Top = 249
    Width = 62
    Height = 17
    Caption = 'Found: NNN'
    Layout = tlTop
    TabOrder = 17
    TextMargins.X = 0
    TextMargins.Y = 0
    WordWrap = False
  end
  object btnStop: TBitBtn
    Left = 527
    Top = 15
    Width = 25
    Height = 25
    Anchors = [akTop, akRight]
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000F00A0000F00A00000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F2FC8488D5383CBA14
      1BAF141AAC383AB28585CDF2F2FAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFB2B6F11324C40020D80023E00020D90019CC0011BD000BB01417A7B4B4
      E8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB2B7F40420D6002FFF002AFB002DF800
      2BF20029EE001FE50012CA000DB50409A4B4B4E7FFFFFFFFFFFFFFFFFFF5F5FD
      1932D50035FF97A9E86888F40034FF002DFF0033FA95B1FD4570FB0019D4000D
      B61316A7F2F2FBFFFFFFFFFFFF818BE4254EF80034FFCACDDCF5EFDEB1C0EF06
      3DFFB1C4FAFFFFFBFFFFFF6187FD0012CA000CB28484CEFFFFFFFFFFFF3A51DF
      3D6AFF083CFE97A3E2F4F1E1F6F3EAE4E8F3FFFFF8FFFFFFF3F7FF446DFD0022
      E70012BE373BB6FFFFFFFFFFFF2341E34B76FF2255FF0D41FE5C7AF0EFEDEBF6
      F6F3FFFFFBCDDAFE174AFF002AFC002CF30019CB121AB2FFFFFFFFFFFF2545E7
      6489FF2E5FFF3364FF0639FCDADCEDFBF9F3FFFFFBADC0FE0033FF0236FF0030
      FA0020DA121CB5FFFFFFFFFFFF3C58E983A2FF3D6CFF3C6CFF526DE9FFFBE9B2
      BFF3D4DAF9FFFFFF6D8FFF0033FF0032FF0023E23941C2FFFFFFFFFFFF8492F1
      688AFA7096FF2958F7BABEDEF5F3E71543F70E41FDE7ECFBFFFFFF2958FE0035
      FF0021DA878DDAFFFFFFFFFFFFF3F4FF294EEF97B6FF4C75F95B6CD8546BE133
      65FF2D5FFF1745FCC7D1FA4C73FF0135FF1629CBF4F4FDFFFFFFFFFFFFFFFFFF
      B7C2FF2F53F396B3FF6F95FF3966FA396AFF2F60FF2153FE083CFF0C40FF0725
      DCBABFF6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB8C3FF294DF26688FA88A9FF6A
      8FFF4F7AFF3E6DFF204CF61E38DDBAC0F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFF4F5FE8C99F5405DED2C4CEC2A4AE94059E58D99ECF5F6FDFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    TabOrder = 13
    Visible = False
    OnClick = btnStopClick
  end
  object edtLog: TJvListBox
    Left = 384
    Top = 81
    Width = 177
    Height = 182
    TabStop = False
    DotNetHighlighting = True
    Anchors = [akLeft, akTop, akRight]
    BorderStyle = bsNone
    Enabled = False
    ExtendedSelect = False
    ItemHeight = 13
    SelectedColor = clWindow
    SelectedTextColor = clWindowText
    DisabledTextColor = clWindowText
    ShowFocusRect = False
    Background.FillMode = bfmTile
    Background.Visible = False
    Flat = True
    ScrollBars = ssNone
    TabOrder = 18
  end
  object edtExactMatch: TJvCheckBox
    Left = 72
    Top = 206
    Width = 80
    Height = 17
    Caption = '&Exact Match'
    TabOrder = 9
    LinkedControls = <>
  end
  object edtOrder: TRadioGroup
    Left = 271
    Top = 164
    Width = 98
    Height = 75
    Hint = 
      'The more rows there are in the table, the longer it takes to sea' +
      'rch it.'#13#10'The quickest way to get results is to search the tables' +
      ' in decreasing order of the number of records in a table.'
    Caption = '&Order'
    ItemIndex = 0
    Items.Strings = (
      'Optimal'
      'Alphabetical')
    TabOrder = 11
  end
  object edtRegEx: TJvCheckBox
    Left = 72
    Top = 226
    Width = 113
    Height = 17
    Caption = '&Regular expression'
    TabOrder = 10
    LinkedControls = <>
  end
  object edtAuthentication: TRadioGroup
    Left = 72
    Top = 46
    Width = 91
    Height = 51
    Caption = '&Authentication'
    ItemIndex = 0
    Items.Strings = (
      'Windows'
      'SQL Server')
    TabOrder = 2
    OnClick = edtAuthenticationClick
  end
  object edtUserName: TEdit
    Left = 240
    Top = 48
    Width = 129
    Height = 19
    Enabled = False
    TabOrder = 3
    Text = 'sa'
    TextHint = 'SQL User Name'
    OnChange = edtUserNameChange
  end
  object edtPassword: TEdit
    Left = 240
    Top = 73
    Width = 129
    Height = 19
    Enabled = False
    PasswordChar = #183
    TabOrder = 4
    TextHint = 'SQL User Password'
    OnChange = edtPasswordChange
  end
  object edtProvider: TComboBox
    Left = 283
    Top = 21
    Width = 88
    Height = 21
    Hint = 'Provider'
    Style = csDropDownList
    Anchors = [akTop, akRight]
    ItemIndex = 0
    TabOrder = 1
    Text = 'OleDB'
    Items.Strings = (
      'OleDB'
      'NativeClient')
  end
  object dlgSelectComputer: TJvComputerNameDialog
    Caption = 'Select computer'
    Left = 448
    Top = 105
  end
end
