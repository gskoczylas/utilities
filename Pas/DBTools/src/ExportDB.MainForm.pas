﻿unit  ExportDB.MainForm;

(**********************************************
 *                                            *
 *          Export database contents          *
 *     ——————————————————————————————————     *
 *  Copyright © 2017-2024 Grzegorz Skoczylas  *
 *                                            *
 *         mgr Grzegorz Skoczylas             *
 *             ul. Grażyny 13/15              *
 *             43-300 Bielsko-Biała           *
 *             tel. (0) 503 064 953           *
 *             gskoczylas@gmail.com           *
 *                                            *
 **********************************************)


interface


uses
  Winapi.Windows, Winapi.Messages,
  System.SysUtils, System.Variants, System.Classes, System.StrUtils,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.Mask,
  JvExComCtrls, JvExStdCtrls, JvListBox, JvBaseDlg, JvWinDialogs, JvExControls,
  JvStaticText, JvListView, JvExButtons, JvBitBtn, JvCheckBox, JvEdit,
  JvCombobox, JvExMask, JvToolEdit, Vcl.ExtCtrls;


type
  TfrmMain = class(TForm)
    lblHost:           TLabel;
    edtHost:           TJvComboEdit;
    dlgSelectComputer: TJvComputerNameDialog;
    lblDtabase:        TLabel;
    edtDatabase:       TJvComboBox;
    lblOutput:         TLabel;
    btnExport:         TJvBitBtn;
    btnExit:           TJvBitBtn;
    lblExported:       TLabel;
    edtResults:        TJvListView;
    txtThreads:        TJvStaticText;
    btnStop:           TBitBtn;
    edtLog:            TJvListBox;
    edtOutput:         TJvFilenameEdit;
    edtSeparateFiles:  TCheckBox;
    edtSeparator:      TRadioGroup;
    edtProvider:       TComboBox;
    edtAuthentication: TRadioGroup;
    lblUserName:       TLabel;
    lblPassword:       TLabel;
    edtUserName:       TEdit;
    edtPassword:       TEdit;
    procedure  edtHostButtonClick(Sender:  TObject);
    procedure  edtHostChange(Sender:  TObject);
    procedure  edtDatabaseChange(Sender:  TObject);
    procedure  btnExitClick(Sender:  TObject);
    procedure  FormCreate(Sender:  TObject);
    procedure  btnStopClick(Sender:  TObject);
    procedure  btnExportClick(Sender:  TObject);
    procedure  FormClose(Sender:  TObject;
                         var   Action:  TCloseAction);
    procedure  edtOutputChange(Sender:  TObject);
    procedure  edtOutputExit(Sender:  TObject);
    procedure  edtAuthenticationClick(Sender:  TObject);
    procedure  edtUserNameChange(Sender:  TObject);
    procedure  edtPasswordChange(Sender:  TObject);
    procedure  edtDatabaseEnter(Sender:  TObject);
  strict private
    var
      fbExporting:  Boolean;
    procedure  SetExportEnabled();
    procedure  BrowseForHost();
    procedure  DefineListOfDatabasesOnHost();
    procedure  KillExporting();
    procedure  SetCommonParams();
    procedure  EnableParameters(const   bEnable:  Boolean);
    procedure  ShowCounter(const   nCounter:  Integer;
                           const   txtVisual:  TJvStaticText;
                           const   sCaption:  string);
  end { TfrmMain };


var
  frmMain:  TfrmMain;


implementation


uses
  GSkPasLib.ASCII, GSkPasLib.TimeUtils,
  DBTools.Processing;


{$R *.dfm}


{$REGION 'TfrmMain'}

procedure  TfrmMain.BrowseForHost();
begin
  if  dlgSelectComputer.Execute()  then
    edtHost.Text := dlgSelectComputer.ComputerName
end { BrowseForHost };


procedure  TfrmMain.btnExitClick(Sender:  TObject);
begin
  Close()
end { btnExitClick };


procedure  TfrmMain.btnExportClick(Sender:  TObject);

type
  TConfiguration = record
    fOutput:  TStreamWriter;
    fCache:   TStringList;
  end { TConfiguration };
  PConfiguration = ^TConfiguration;

var
  chSep:  Char;
  lConf:  TConfiguration;
  setOpt: TProcessingOptions;

begin  { btnExportClick }
  setOpt := [poDelimitedString];
  if   edtSeparateFiles.Checked  then
    setOpt := setOpt + [poBackgroundProcessing, poOrderOptimal];
  {-}
  case  edtSeparator.ItemIndex  of
    0:  chSep := ',';
    1:  chSep := ';';
    2:  chSep := '|';
    3:  chSep := Tab
  end { case edtSeparator.ItemIndex };
  {-}
  if  not (poBackgroundProcessing in setOpt)  then
    begin
      lConf.fOutput := TStreamWriter.Create(edtOutput.FileName, False, TEncoding.Default);
      lConf.fCache := TStringList.Create();
      {-}
      EnableParameters(False)
    end { not (poBackgroundProcessing in setOpt) };
  {-}
  SetCommonParams();
  TProcessing.DatabaseName := edtDatabase.Text;
  {-}
  edtResults.Clear();
  TProcessing.ProcessTables({Table action}
                            procedure(sTableName:  string;
                                      bIsBeginning:  Boolean;
                                      nThreadCount:  Integer;
                                      var   pConfig:  Pointer)
                              var
                                pConf:  PConfiguration;
                              begin
                                ShowCounter(nThreadCount, txtThreads, 'Threads');
                                case  nThreadCount  of
                                  0:  begin
                                        EnableParameters(True);
                                        btnStop.Hide();
                                        fbExporting := False;
                                        edtOutput.SetFocus();
                                        Sleep(500);
                                        if  poBackgroundProcessing in setOpt  then
                                          edtLog.Items.Clear()
                                      end { 0 --> all threads are done };
                                  1:  if  bIsBeginning  then
                                        begin
                                          EnableParameters(False);
                                          fbExporting := True;
                                          btnStop.Show()
                                        end { 1 }
                                end { case nThreadCount };
                                if  bIsBeginning  then
                                  begin
                                    if  poBackgroundProcessing in setOpt  then
                                      begin
                                        New(pConf);
                                        with  pConf^  do
                                          begin
                                            fOutput := TStreamWriter.Create(ChangeFileExt(edtOutput.FileName,
                                                                                          '-' + sTableName
                                                                                              + ExtractFileExt(edtOutput.FileName)),
                                                                            False, TEncoding.Default);
                                            fCache := TStringList.Create()
                                          end { with pConf^ };
                                        {-}
                                        pConfig := pConf
                                      end { poBackgroundProcessing in setOpt }
                                    else
                                      pConfig := Addr(lConf);
                                    if  not (poBackgroundProcessing in setOpt)  then
                                      with  PConfiguration(pConfig)^.fOutput  do
                                        begin
                                          if  BaseStream.Position > 0  then
                                            WriteLine();
                                          WriteLine(sTableName);
                                          WriteLine(StringOfChar('~', Length(sTableName)))
                                        end { with; if }
                                  end { bIsBeginning }
                                else
                                  begin
                                    if  poBackgroundProcessing in setOpt  then
                                      begin
                                        with  PConfiguration(pConfig)^  do
                                          begin
                                            fCache.Free();
                                            fOutput.Free()
                                          end { with };
                                        Dispose(PConfiguration(pConfig));
                                        pConfig := nil
                                      end { poBackgroundProcessing in setOpt };                                    
                                    edtResults.Items.Add().Caption := sTableName  
                                  end { not bIsBeginning }
                              end,
                            { Check field }
                            function(sFieldValue:  string)
                                     : Boolean
                              begin
                                Result := True
                              end,
                            { Field found }
                            procedure(sTableName:  string;
                                      nRecNo, nFieldNo, nFieldCount:  Integer;
                                      sFieldName:  string;
                                      sFieldValue:  string;
                                      pConfig:  Pointer)
                              begin
                                with  PConfiguration(pConfig)^  do
                                  if  nRecNo = 1  then
                                    begin
                                      if  nFieldNo > 0  then
                                        fOutput.Write(chSep);
                                      fOutput.Write(sFieldName);
                                      fCache.Add(sFieldValue);
                                      if  nFieldNo = nFieldCount - 1  then
                                        begin
                                          fOutput.WriteLine();
                                          fOutput.Write(fCache[0]);
                                          for  nFieldNo := 1  to  nFieldCount - 1  do
                                            begin
                                              fOutput.Write(chSep);
                                              fOutput.Write(fCache[nFieldNo])
                                            end { for nFieldNo };
                                          fCache.Clear();
                                          fOutput.WriteLine();
                                          {-}
                                          Application.ProcessMessages()
                                        end { nFieldNo = nFieldCount - 1 }
                                    end { nRecNo = 1 }
                                  else
                                    begin
                                      if  nFieldNo > 0  then
                                        fOutput.Write(chSep);
                                      fOutput.Write(sFieldValue);
                                      if  nFieldNo = nFieldCount - 1  then
                                        begin
                                          fOutput.WriteLine();
                                          {-}
                                          Application.ProcessMessages()
                                        end { nFieldNo = nFieldCount - 1 }
                                    end { nRecNo > 1 }
                              end,
                            { Log message }
                            procedure(sLogMsg:  string)
                              const
                                cnLogSize = 8;
                              begin
                                while  edtLog.Items.Count >= cnLogSize  do
                                  edtLog.Items.Delete(0);
                                edtLog.Items.Add(sLogMsg)
                              end,
                            { Processing options }
                            setOpt);
  if  not (poBackgroundProcessing in setOpt)  then
    begin
      edtLog.Items.Clear();
      EnableParameters(False)
    end { not (poBackgroundProcessing in setOpt) };
end { btnExportClick };


procedure  TfrmMain.btnStopClick(Sender:  TObject);
begin
  KillExporting()
end { btnStopClick };


procedure  TfrmMain.DefineListOfDatabasesOnHost();
begin
  TProcessing.GetListOfDatabases(edtDatabase.Items);
  if  edtDatabase.Items.Count = 1  then
    edtDatabase.ItemIndex := 0
end { DefineListOfDatabasesOnHost };


procedure  TfrmMain.edtAuthenticationClick(Sender:  TObject);

var
  bSQLAuth:  Boolean;

begin  { edtAuthenticationClick }
  bSQLAuth := edtAuthentication.ItemIndex <> 0;
  lblUserName.Enabled := bSQLAuth;
  edtUserName.Enabled := bSQLAuth;
  lblPassword.Enabled := bSQLAuth;
  edtPassword.Enabled := bSQLAuth;
  SetExportEnabled()
end { edtAuthenticationClick };


procedure  TfrmMain.edtDatabaseChange(Sender:  TObject);
begin
  SetExportEnabled()
end { edtDatabaseChange };


procedure  TfrmMain.edtDatabaseEnter(Sender:  TObject);
begin
  SetCommonParams();
  Screen.Cursor := crHourGlass;
  try
    DefineListOfDatabasesOnHost()
  finally
    Screen.Cursor := crDefault
  end { try-finally }
end { edtDatabaseEnter };


procedure  TfrmMain.edtHostButtonClick(Sender:  TObject);
begin
  BrowseForHost()
end { edtHostButtonClick };


procedure  TfrmMain.edtHostChange(Sender:  TObject);
begin
  SetExportEnabled()
end { edtHostChange };


procedure  TfrmMain.edtOutputChange(Sender:  TObject);
begin
  SetExportEnabled()
end { edtOutputChange };


procedure  TfrmMain.edtOutputExit(Sender:  TObject);
begin
  if  edtOutput.FileName <> ''  then
    if  ExtractFileExt(edtOutput.FileName) = ''  then
      edtOutput.FileName := edtOutput.FileName + '.csv'
end { edtOutputExit };


procedure  TfrmMain.edtPasswordChange(Sender:  TObject);
begin
  SetExportEnabled()
end { edtPasswordChange };


procedure  TfrmMain.edtUserNameChange(Sender:  TObject);
begin
  SetExportEnabled()
end { edtUserNameChange };


procedure  TfrmMain.EnableParameters(const   bEnable:  Boolean);
begin
  btnExport.Enabled := bEnable;
  edtHost.Enabled := bEnable;
  edtDatabase.Enabled := bEnable;
  edtOutput.Enabled := bEnable;
  edtSeparateFiles.Enabled := bEnable;
  edtSeparator.Enabled := bEnable
end { EnableParameters };


procedure  TfrmMain.FormClose(Sender:  TObject;
                              var   Action:  TCloseAction);
begin
  KillExporting()
end { FormClose };


procedure  TfrmMain.FormCreate(Sender:  TObject);
begin
  txtThreads.Caption := ''
end { FormCreate };


procedure  TfrmMain.KillExporting();

const
  cnDeadline = SecsPerMin * MSecsPerSec;

var
  nStart:  Cardinal;

begin  { KillExporting }
  TProcessing.TerminateProcessing();
  { Wait until all thread finished }
  nStart := GetTickCount();
  while  fbExporting  do
    begin
      if  TicksSince(nStart) >= cnDeadline  then   // workaround
        Break;
      Sleep(50);
      Application.ProcessMessages()
    end { while }
end { KillExporting };


procedure  TfrmMain.SetCommonParams();
begin
  TProcessing.HostName := Trim(edtHost.Text);
  {-}
  Assert(edtProvider.ItemIndex in [0..1]);
  case  edtProvider.ItemIndex  of
    0:  TProcessing.DatabaseProvider := dpOleDB;
    1:  TProcessing.DatabaseProvider := dpNativeClient
  end { case edtProvider.ItemIndex };
  {-}
  TProcessing.WindowsAuthentication := edtAuthentication.ItemIndex = 0;
  TProcessing.UserName := Trim(edtUserName.Text);
  TProcessing.Password := Trim(edtPassword.Text)
end { SetCommonParams };


procedure  TfrmMain.SetExportEnabled();
begin
  btnExport.Enabled := (Trim(edtHost.Text) <> '')
                       and (edtDatabase.ItemIndex >= 0)
                       and ((edtAuthentication.ItemIndex = 0)
                            or (Trim(edtUserName.Text) <> '')
                               and (Trim(edtPassword.Text) <> ''))
                       and (Trim(edtOutput.FileName) <> '')
end { SetExportEnabled };


procedure  TfrmMain.ShowCounter(const   nCounter:  Integer;
                                const   txtVisual:  TJvStaticText;
                                const   sCaption:  string);
begin
  txtVisual.Caption := Format('%s: %d', [sCaption, nCounter]);
  txtVisual.Update();
  {-}
  Application.ProcessMessages()
end { ShowCounter };

{$ENDREGION 'TfrmMain'}


end.

