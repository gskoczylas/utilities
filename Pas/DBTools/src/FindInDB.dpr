﻿program  FindInDB;

(**********************************************
 *                                            *
 *              Find in database              *
 *     ——————————————————————————————————     *
 *  Copyright © 2017-2024 Grzegorz Skoczylas  *
 *                                            *
 *         mgr Grzegorz Skoczylas             *
 *             ul. Grażyny 13/15              *
 *             43-300 Bielsko-Biała           *
 *             tel. (0) 503 064 953           *
 *             gskoczylas@gmail.com           *
 *                                            *
 **********************************************)


uses
  Vcl.Forms,
  GSkPasLib.TimeUtils in 'p:\GSkPasLib\GSkPasLib.TimeUtils.pas',
  DBTools.Processing in 'DBTools.Processing.pas',
  FindInDB.MainForm in 'FindInDB.MainForm.pas' {frmMain};

{$R *.res}


begin
  Application.Initialize();
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run()
end.

