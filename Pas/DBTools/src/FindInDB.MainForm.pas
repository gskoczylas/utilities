﻿unit  FindInDB.MainForm;

(**********************************************
 *                                            *
 *              Find in database              *
 *     ——————————————————————————————————     *
 *  Copyright © 2017-2024 Grzegorz Skoczylas  *
 *                                            *
 *         mgr Grzegorz Skoczylas             *
 *             ul. Grażyny 13/15              *
 *             43-300 Bielsko-Biała           *
 *             tel. (0) 503 064 953           *
 *             gskoczylas@gmail.com           *
 *                                            *
 **********************************************)


interface


uses
  Winapi.Windows, Winapi.Messages,
  System.SysUtils, System.Variants, System.Classes, System.StrUtils, System.Math,
  System.RegularExpressions, System.UITypes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.Mask, Vcl.ExtCtrls,
  JvExComCtrls, JvExStdCtrls, JvListBox, JvBaseDlg, JvWinDialogs, JvExControls,
  JvStaticText, JvListView, JvExButtons, JvBitBtn, JvCheckBox, JvEdit,
  JvCombobox, JvExMask, JvToolEdit, JvRadioButton;


type
  TfrmMain = class(TForm)
    lblHost:           TLabel;
    edtHost:           TJvComboEdit;
    dlgSelectComputer: TJvComputerNameDialog;
    lblDtabase:        TLabel;
    edtDatabase:       TJvComboBox;
    lblFind:           TLabel;
    edtFind:           TJvEdit;
    edtCaseSensitive:  TJvCheckBox;
    btnSearch:         TJvBitBtn;
    btnExit:           TJvBitBtn;
    lblResults:        TLabel;
    edtResults:        TJvListView;
    txtThreads:        TJvStaticText;
    edtFirstMatch:     TJvCheckBox;
    txtFound:          TJvStaticText;
    btnStop:           TBitBtn;
    edtLog:            TJvListBox;
    edtExactMatch:     TJvCheckBox;
    edtOrder:          TRadioGroup;
    edtRegEx:          TJvCheckBox;
    edtAuthentication: TRadioGroup;
    lblUserName:       TLabel;
    edtUserName:       TEdit;
    lblPassword:       TLabel;
    edtPassword:       TEdit;
    edtProvider:       TComboBox;
    procedure  edtHostButtonClick(Sender:  TObject);
    procedure  edtHostChange(Sender:  TObject);
    procedure  edtDatabaseChange(Sender:  TObject);
    procedure  edtFindChange(Sender:  TObject);
    procedure  btnExitClick(Sender:  TObject);
    procedure  FormCreate(Sender:  TObject);
    procedure  btnStopClick(Sender:  TObject);
    procedure  btnSearchClick(Sender:  TObject);
    procedure  FormClose(Sender:  TObject;
                         var   Action:  TCloseAction);
    procedure  edtAuthenticationClick(Sender:  TObject);
    procedure  edtDatabaseEnter(Sender:  TObject);
    procedure  edtUserNameChange(Sender:  TObject);
    procedure  edtPasswordChange(Sender:  TObject);
  strict private
    var
      fnFoundCnt:   Integer;
      fbSearching:  Boolean;
    procedure  SetSearchEnabled();
    procedure  BrowseForHost();
    procedure  DefineListOfDatabasesOnHost();
    procedure  KillSearchThreads();
    procedure  SetCommonParams();
    procedure  EnableParameters(const   bEnable:  Boolean);
    procedure  ShowCounter(const   nCounter:  Integer;
                           const   txtVisual:  TJvStaticText;
                           const   sCaption:  string);
  end { TfrmMain };


var
  frmMain:  TfrmMain;


implementation


uses
  GSkPasLib.TimeUtils,
  DBTools.Processing;


{$R *.dfm}


var
  RegEx:  TRegEx;


{$REGION 'Local subroutines'}

function  CompareRegEx(const   sValue, sTemplate:  string)
                       : Boolean;
begin
  Result := RegEx.IsMatch(sValue)
end { CompareRegEx };

{$ENDREGION 'Local subroutines'}


{$REGION 'TfrmMain'}

procedure  TfrmMain.BrowseForHost();
begin
  if  dlgSelectComputer.Execute()  then
    edtHost.Text := dlgSelectComputer.ComputerName
end { BrowseForHost };


procedure  TfrmMain.btnExitClick(Sender:  TObject);
begin
  Close()
end { btnExitClick };


procedure  TfrmMain.btnSearchClick(Sender:  TObject);

var
  sTextToFind:  string;
  setOptions:  TProcessingOptions;
  fnCompare:  function(const   sText1, sText2:  string)
                       : Boolean;

begin  { btnSearchClick }
  EnableParameters(False);
  fbSearching := True;
  sTextToFind := Trim(edtFind.Text);
  edtResults.Clear();
  fnFoundCnt := 0;
  ShowCounter(fnFoundCnt, txtFound, 'Found');
  {-}
  setOptions := [poBackgroundProcessing];
  if  edtOrder.ItemIndex = 0  then
    Include(setOptions, poOrderOptimal);
  if  edtFirstMatch.Checked  then
    Include(setOptions, poFirstMatch);
  {-}
  if  edtRegEx.Checked  then
    begin
      try
        if  edtExactMatch.Checked  then
          sTextToFind := '^' + sTextToFind + '$';
        if  edtCaseSensitive.Checked  then
          RegEx.Create(sTextToFind, [roCompiled, roSingleLine])
        else
          RegEx.Create(sTextToFind, [roCompiled, roSingleLine, roIgnoreCase])
      except
        on  eErr : Exception  do
          begin
            MessageDlg(eErr.ClassName() + ': ' + eErr.Message, mtError, [mbOK], 0);
            Abort()
          end { on Exception }
      end { try-except };
      fnCompare := CompareRegEx
    end { edtRegEx.Checked }
  else
    if  edtExactMatch.Checked  then
      if  edtCaseSensitive.Checked  then
        fnCompare := SameStr
      else
        fnCompare := SameText
    else
      if  edtCaseSensitive.Checked  then
        fnCompare := ContainsStr
      else
        fnCompare := ContainsText;
  {-}
  SetCommonParams();
  TProcessing.DatabaseName := edtDatabase.Text;
  TProcessing.ProcessTables({ Table actions }
                            procedure(sTableName:  string;
                                      bStart:  Boolean;
                                      nThreads:  Integer;
                                      var   pConfig:  Pointer)
                              begin
                                ShowCounter(nThreads,
                                            txtThreads, 'Threads');
                                case  nThreads  of
                                  0:  begin
                                        EnableParameters(True);
                                        btnStop.Hide();
                                        fbSearching := False;
                                        edtFind.SetFocus();
                                        Sleep(500);
                                        edtLog.Items.Clear()
                                      end { 0 -> all threads done };
                                  1:  if  bStart  then
                                        btnStop.Show()
                                end { case nThreads }
                              end,
                            { Check field }
                            function(sFieldValue:  string)
                                     : Boolean
                              begin
                                Result := fnCompare(sFieldValue, sTextToFind)
                              end,
                            { Field found }
                            procedure(sTableName:  string;
                                      nRecNo, FieldNo, FieldCount:  Integer;
                                      sFieldName:  string;
                                      sFieldValue:  string;
                                      pConfig:  Pointer)
                              begin
                                with  edtResults.Items.Add()  do
                                  begin
                                    Caption := sTableName;
                                    SubItems.Add(IntToStr(nRecNo));
                                    SubItems.Add(sFieldName);
                                    SubItems.Add(sFieldValue)
                                  end { with edtResults.Items.Add() };
                                Inc(fnFoundCnt);
                                ShowCounter(fnFoundCnt, txtFound, 'Found')
                              end,
                            { Log message }
                            procedure(sLogMsg:  string)
                              const
                                cnLogSize = 9;
                              begin
                                if  edtLog.Items.Count = cnLogSize  then
                                  edtLog.Items.Delete(0);
                                edtLog.Items.Add(sLogMsg)
                              end,
                            { Processing options }
                            setOptions)
end { btnSearchClick };


procedure  TfrmMain.btnStopClick(Sender:  TObject);
begin
  KillSearchThreads()
end { btnStopClick };


procedure  TfrmMain.DefineListOfDatabasesOnHost();
begin
  TProcessing.GetListOfDatabases(edtDatabase.Items)
end { DefineListOfDatabasesOnHost };


procedure  TfrmMain.edtAuthenticationClick(Sender:  TObject);

var
  bSQLAuth:  Boolean;

begin  { edtAuthenticationClick }
  bSQLAuth := edtAuthentication.ItemIndex <> 0;
  lblUserName.Enabled := bSQLAuth;
  edtUserName.Enabled := bSQLAuth;
  lblPassword.Enabled := bSQLAuth;
  edtPassword.Enabled := bSQLAuth;
  SetSearchEnabled()
end { edtAuthenticationClick };


procedure  TfrmMain.edtDatabaseChange(Sender:  TObject);
begin
  SetSearchEnabled()
end { edtDatabaseChange };


procedure TfrmMain.edtDatabaseEnter(Sender: TObject);
begin
  SetCommonParams();
  Screen.Cursor := crHourGlass;
  try
    DefineListOfDatabasesOnHost()
  finally
    Screen.Cursor := crDefault
  end { try-finally }
end { edtDatabaseEnter };


procedure  TfrmMain.edtFindChange(Sender:  TObject);
begin
  SetSearchEnabled()
end { edtFindChange };


procedure  TfrmMain.edtHostButtonClick(Sender:  TObject);
begin
  BrowseForHost()
end { edtHostButtonClick };


procedure  TfrmMain.edtHostChange(Sender:  TObject);
begin
  SetSearchEnabled()
end { edtHostChange };


procedure  TfrmMain.edtPasswordChange(Sender:  TObject);
begin
  SetSearchEnabled()
end { edtPasswordChange };


procedure TfrmMain.edtUserNameChange(Sender: TObject);
begin
  SetSearchEnabled()
end { edtUserNameChange };


procedure  TfrmMain.EnableParameters(const   bEnable:  Boolean);
begin
  btnSearch.Enabled := bEnable;
  edtHost.Enabled := bEnable;
  edtAuthentication.Enabled := bEnable;
  edtUserName.Enabled := bEnable and (edtAuthentication.ItemIndex <> 0);
  edtPassword.Enabled := bEnable and (edtAuthentication.ItemIndex <> 0);
  edtDatabase.Enabled := bEnable;
  edtFind.Enabled := bEnable;
  edtCaseSensitive.Enabled := bEnable;
  edtFirstMatch.Enabled := bEnable;
  edtExactMatch.Enabled := bEnable;
  edtRegEx.Enabled := bEnable
end { EnableParameters };


procedure  TfrmMain.FormClose(Sender:  TObject;
                              var   Action:  TCloseAction);
begin
  KillSearchThreads()
end { FormClose };


procedure  TfrmMain.FormCreate(Sender:  TObject);
begin
  txtThreads.Caption := '';
  txtFound.Caption := ''
end { FormCreate };


procedure  TfrmMain.KillSearchThreads();

const
  cnDeadline = SecsPerMin * MSecsPerSec;

var
  nStart:  Cardinal;

begin  { KillSearchThreads }
  TProcessing.TerminateProcessing();
  { Wait until all thread finished }
  nStart := GetTickCount();
  while  fbSearching  do
    begin
      if  TicksSince(nStart) >= cnDeadline  then   // workaround
        Break;
      Sleep(50);
      Application.ProcessMessages()
    end { while }
end { KillSearchThreads };


procedure  TfrmMain.SetCommonParams();
begin
  TProcessing.HostName := Trim(edtHost.Text);
  {-}
  Assert(edtProvider.ItemIndex in [0..1]);
  case  edtProvider.ItemIndex  of
    0:  TProcessing.DatabaseProvider := dpOleDB;
    1:  TProcessing.DatabaseProvider := dpNativeClient
  end { case edtProvider.ItemIndex };
  {-}
  TProcessing.WindowsAuthentication := edtAuthentication.ItemIndex = 0;
  TProcessing.UserName := Trim(edtUserName.Text);
  TProcessing.Password := Trim(edtPassword.Text)
end { SetCommonParams };


procedure  TfrmMain.SetSearchEnabled();
begin
  btnSearch.Enabled := (Trim(edtHost.Text) <> '')
                       and (edtDatabase.ItemIndex >= 0)
                       and (Trim(edtFind.Text) <> '')
                       and ((edtAuthentication.ItemIndex = 0)
                            or (Trim(edtUserName.Text) <> '')
                               and (Trim(edtPassword.Text) <> ''))
end { SetSearchEnabled };


procedure  TfrmMain.ShowCounter(const   nCounter:  Integer;
                                const   txtVisual:  TJvStaticText;
                                const   sCaption:  string);
begin
  txtVisual.Caption := Format('%s: %d', [sCaption, nCounter]);
  txtVisual.Update();
  {-}
  Application.ProcessMessages()
end { ShowCounter };

{$ENDREGION 'TfrmMain'}


end.

