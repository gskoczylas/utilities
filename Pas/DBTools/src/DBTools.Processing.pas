﻿unit  DBTools.Processing;

(**********************************************
 *                                            *
 *    Database tools background processing    *
 *     ——————————————————————————————————     *
 *  Copyright © 2017-2024 Grzegorz Skoczylas  *
 *                                            *
 *         mgr Grzegorz Skoczylas             *
 *             ul. Grażyny 13/15              *
 *             43-300 Bielsko-Biała           *
 *             tel. (0) 503 064 953           *
 *             gskoczylas@gmail.com           *
 *                                            *
 **********************************************)


interface


uses
  System.SysUtils, System.DateUtils, System.Classes,
  System.Threading, System.SyncObjs, System.Types, System.Generics.Collections,
  WinAPI.ActiveX,
  Data.DB, Data.Win.ADODB;


type
  TProc<C1,C2,C3,C4,C5,C6,C7> = reference to procedure(Arg1: C1; Arg2: C2; Arg3: C3; Arg4: C4; Arg5: C5; Arg6: C6; Arg7: C7);
  TProc<C1,C2,C3,V4> = reference to procedure(Art1: C1; Arg2: C2; Arg3: C3; var Arg4: V4);
  {-}
  TProcessingOption = (poFirstMatch, poBackgroundProcessing, poDelimitedString,
                       poOrderOptimal);
  TProcessingOptions = set of  TProcessingOption;
  {-}
  TTableAction = TProc<string,Boolean,Integer,Pointer>;         // TableName, IsBeginning, ThreadCount, @Config
  TCheckField  = TFunc<string,Boolean>;                         // FieldValue -> Boolean
  TOnFound     = TProc<string,Integer,Integer,Integer,string,string,Pointer>;   // TableName, RecNo, FieldNo, FieldCount, FieldName, FieldValue, @Config
  TLogMessage  = TProc<string>;                                 // MessageText

  TDatabaseProvider = (dpOleDB, dpNativeClient);

  TProcessing = class
  strict private
    class var
      fsHost:      string;
      fProviderDB: TDatabaseProvider;
      fbWinAuth:   Boolean;
      fsUserSQL:   string;
      fsPassSQL:   string;
      fsDatabase:  string;
      fnThreads:   Integer;
      fbStopAll:   Boolean;
      fThreads:    TList<ITask>;
      fSynchro:    TCriticalSection;
      fSyncStop:   TMultiReadExclusiveWriteSynchronizer;
    class constructor  Create();
    class destructor  Destroy();
    {-}
    class function  StopAll() : Boolean;
    class function  CreateQuery(const   sSQL:  string)
                                : TADOQuery;
    class function  GetConnectionString(const   bConnectDatabase:  Boolean)
                                        : string;
    class procedure  GetListOfDatabaseTables(const   bOrderOptimal:  Boolean;
                                             const   Tables:  TStringList);
    class procedure  InternalProcessTable(const   sTableName:  string;
                                          const   fnTableAction:  TTableAction;
                                          const   fnCheckField:  TCheckField;
                                          const   fnOnFound:  TOnFound;
                                          const   fnLogMessage:  TLogMessage;
                                          const   setOptions:  TProcessingOptions);
    class procedure  ProcessTable(const   sTable:  string;
                                  const   fnTableAction:  TTableAction;
                                  const   fnCheckField:  TCheckField;
                                  const   fnOnFound:  TOnFound;
                                  const   fnLogMessage:  TLogMessage;
                                  const   setOptions:  TProcessingOptions);
  public
    class procedure  GetListOfDatabases(const   Databases:  TStrings);
    class procedure  ProcessTables(const   fnTableAction:  TTableAction;
                                   const   fnCheckField:  TCheckField;
                                   const   fnOnFound:  TOnFound;
                                   const   fnLogMessage:  TLogMessage;
                                   const   setOptions:  TProcessingOptions);
    class procedure  TerminateProcessing();
    {-}
    class property  HostName:  string
                    read  fsHost
                    write fsHost;
    class property  DatabaseProvider:  TDatabaseProvider
                    read  fProviderDB
                    write fProviderDB;
    class property  WindowsAuthentication:  Boolean
                    read  fbWinAuth
                    write fbWinAuth;
    class property  UserName:  string
                    read  fsUserSQL
                    write fsUserSQL;
    class property  Password:  string
                    read  fsPassSQL
                    write fsPassSQL;
    class property  DatabaseName:  string
                    read  fsDatabase
                    write fsDatabase;
    class property  ThreadCount:  Integer
                    read  fnThreads;
  end { TProcessing };


implementation


uses
  GSkPasLib.StrUtils;


{$REGION 'TBackgroundProcessing' }

class constructor  TProcessing.Create();
begin
  fThreads := TList<ITask>.Create();
  fSynchro := TCriticalSection.Create();
  fSyncStop := TMultiReadExclusiveWriteSynchronizer.Create()
end { Create };


class function  TProcessing.CreateQuery(const   sSQL:  string)
                                        : TADOQuery;
begin
  Result := TADOQuery.Create(nil);
  try
    Result.ConnectionString := GetConnectionString(DatabaseName <> '');
    Result.SQL.Text := sSQL;
    Result.CursorType := ctOpenForwardOnly;
    Result.LockType := ltReadOnly;
    Result.CursorLocation := clUseServer;
    Result.CommandTimeout := 0
  except
    FreeAndNil(Result);
    raise
  end { try-except }
end { CreateQuery };


class destructor  TProcessing.Destroy();
begin
  FreeAndNil(fThreads);
  FreeAndNil(fSynchro);
  FreeAndNil(fSyncStop)
end { Destroy };


class procedure  TProcessing.GetListOfDatabases(const   Databases:  TStrings);

var
  qryDatabases:  TADOQuery;

begin  { GetListOfDatabases }
  qryDatabases := CreateQuery('SELECT name'
                              + ' FROM master.dbo.sysdatabases'
                              + ' WHERE name NOT IN (''master'', ''tempdb'', ''model'', ''msdb'')'
                              + ' ORDER BY name');
  try
    Databases.BeginUpdate();
    try
      Databases.Clear();
      qryDatabases.Open();
      try
        while  not qryDatabases.Eof  do
          begin
            Databases.Add(qryDatabases.Fields[0].AsString);
            qryDatabases.Next()
          end { while }
      finally
        qryDatabases.Close()
      end { try-finally }
    finally
      Databases.EndUpdate()
    end { try-finally }
  finally
    qryDatabases.Free()
  end { try-finally }
end { GetListOfDatabases };


class function  TProcessing.GetConnectionString(const   bConnectDatabase:  Boolean)
                                                : string;
begin
  case  DatabaseProvider  of
    dpOleDB:
      Result := 'Provider=SQLOLEDB;';
    dpNativeClient:
      Result := 'Provider=SQLNCLI11;'
  end { case DatabaseProvider };
  Result := Result + 'Persist Security Info=False;'
                   + 'Data Source=' + fsHost + ';';
  if  DatabaseName <> ''  then
    Result := Result + 'Initial Catalog=' + DatabaseName + ';';
  if  WindowsAuthentication  then
    Result := Result
              + 'Integrated Security=SSPI;'
  else
    Result := Result
              + 'User ID=' + fsUserSQL + ';'
              + 'Password="' + fsPassSQL + '";'
end { GetConnectionString };


class procedure  TProcessing.GetListOfDatabaseTables(const   bOrderOptimal:  Boolean;
                                                     const   Tables:  TStringList);
var
  sSelectSQL:  string;
  qryTables:   TADOQuery;

begin  { GetListOfDatabaseTables }
  if  bOrderOptimal  then
    sSelectSQL := 'SELECT DISTINCT t.name, p.rows '
                + 'FROM sys.tables t '
                +   'INNER JOIN sys.partitions p ON t.object_id = p.object_id '
                + 'ORDER BY p.rows DESC'
  else
    sSelectSQL := 'SELECT table_name FROM INFORMATION_SCHEMA.TABLES '
                + 'WHERE table_type = ''BASE TABLE'' '
                + 'ORDER BY table_name';
  qryTables := CreateQuery(sSelectSQL);
  try
    qryTables.Open();
    try
      Tables.BeginUpdate();
      try
        Tables.Clear();
        while  not qryTables.Eof  do
          begin
            Tables.Add(qryTables.Fields[0].AsString);
            qryTables.Next()
          end { while }
      finally
        Tables.EndUpdate()
      end { try-finally }
    finally
      qryTables.Close()
    end { try-finally }
  finally
    qryTables.Free()
  end { try-finally }
end { GetListOfDatabaseTables };


class procedure  TProcessing.InternalProcessTable(const   sTableName:  string;
                                                  const   fnTableAction:  TTableAction;
                                                  const   fnCheckField:  TCheckField;
                                                  const   fnOnFound:  TOnFound;
                                                  const   fnLogMessage:  TLogMessage;
                                                  const   setOptions:  TProcessingOptions);
const
  cnReportEveryMS = 1500;   // 1500 ms = 1,5 s

var
  lTable:  TADOQuery;
  nField:  Integer;
  nRecCnt: Integer;
  nRecNo:  Integer;
  tmPrev:  TDateTime;
  fnExec:  TProc<TThreadProcedure>;
  fpConf:  Pointer;

begin  { InternalProcessTable }
  if  poBackgroundProcessing in setOptions  then
    fnExec := procedure(fnProc:  TThreadProcedure)
                begin
                  TThread.Synchronize(TThread.CurrentThread, fnProc)
                end
  else
    fnExec := procedure(fnProc:  TThreadProcedure)
                begin
                  fnProc()
                end;
  fpConf := nil;
  fnExec(procedure()
         begin
           TInterlocked.Increment(fnThreads);
           fnTableAction(sTableName, True, fnThreads, fpConf);
           fnLogMessage(sTableName + ' - START')
         end);
  if  poBackgroundProcessing in setOptions  then
    CoInitialize(nil);
  try
    { Get record count }
    lTable := CreateQuery('SELECT Count(*) FROM "' + sTableName + '"');
    try
      lTable.Open();
      try
        nRecCnt := lTable.Fields[0].AsInteger
      finally
        lTable.Close()
      end { try-finally }
    finally
      lTable.Free()
    end { try-finally };
    { Process table }
    lTable := CreateQuery('SELECT * FROM "' + sTableName + '"');
    try
      lTable.Open();
      try
        tmPrev := Now();
        nRecNo := 0;
        while  not lTable.Eof  do
          begin
            if  StopAll()  then
              Break;
            Inc(nRecNo);
            if  MilliSecondsBetween(Now(), tmPrev) >= cnReportEveryMS  then
              begin
                fnExec(procedure()
                         begin
                           fnLogMessage(Format('%s - %.3f%%',
                                        [sTableName,
                                         nRecNo / nRecCnt * 100]))
                         end);
                tmPrev := Now()
              end { if };
            for  nField := 0  to  lTable.FieldCount - 1  do
              with  lTable.Fields[nField]  do
                if  fnCheckField(AsString)  then
                  begin
                    fnExec(procedure()
                             var
                               sVal:  string;
                             begin
                               FormatSettings.DecimalSeparator := '.';
                               FormatSettings.ShortDateFormat := 'yyyy-mm-dd';
                               {-}
                               sVal := AsString;
                               if  (poDelimitedString in setOptions)
                                   and (lTable.Fields[nField] is TStringField)  then
                                 sVal := QuoteStr(sVal, '"');
                               fnOnFound(sTableName, nRecNo, nField, lTable.FieldCount,
                                         FieldName, sVal, fpConf)
                             end);
                    if  (poFirstMatch in setOptions)
                        or StopAll()  then
                      Break
                  end { if fnCompare(...) };
              lTable.Next()
          end { while }
      finally
        lTable.Close()
      end { try-finally }
    finally
      lTable.Free()
    end { try-finally }
  finally
    if  poBackgroundProcessing in setOptions  then
      CoUninitialize();
    fnExec(procedure()
             begin
               fnLogMessage(Format('%s - STOP [%d rec(s)]',
                            [sTableName, nRecCnt]));
               TInterlocked.Decrement(fnThreads);
               fnTableAction(sTableName, False, fnThreads, fpConf)
             end)
  end { try-finally }
end { InternalProcessTable };


class procedure  TProcessing.ProcessTable(const   sTable:  string;
                                          const   fnTableAction:  TTableAction;
                                          const   fnCheckField:  TCheckField;
                                          const   fnOnFound:  TOnFound;
                                          const   fnLogMessage:  TLogMessage;
                                          const   setOptions:  TProcessingOptions);
var
  iTsk:  ITask;

begin  { ProcessTable }
  if  StopAll()  then
    Exit;
  if  poBackgroundProcessing in setOptions  then
    begin
      iTsk := TTask.Create(procedure()
                             begin
                               if  StopAll()  then
                                 Exit;
                               try
                                 InternalProcessTable(sTable,
                                                      fnTableAction, fnCheckField,
                                                      fnOnFound, fnLogMessage,
                                                      setOptions)
                               finally
                                 fSynchro.Enter();
                                 try
                                   fThreads.Remove(TTask.CurrentTask)
                                 finally
                                   fSynchro.Leave()
                                 end { try-finally }
                               end { try-finally }
                             end,
                           TThreadPool.Default);
      fSynchro.Enter();
      try
        fThreads.Add(iTsk)
      finally
        fSynchro.Leave()
      end { try-finally };
      iTsk.Start()
    end { poBackgroundProcessing in setOptions }
  else
    InternalProcessTable(sTable, fnTableAction, fnCheckField, fnOnFound,
                         fnLogMessage, setOptions)
end { ProcessTable };


class procedure  TProcessing.ProcessTables(const   fnTableAction:  TTableAction;
                                           const   fnCheckField:  TCheckField;
                                           const   fnOnFound:  TOnFound;
                                           const   fnLogMessage:  TLogMessage;
                                           const   setOptions:  TProcessingOptions);
var
  lTables:  TStringList;
  nTable:   Integer;

begin  { ProcessTables }
  lTables := TStringList.Create();
  try
    GetListOfDatabaseTables(poOrderOptimal in setOptions,
                            lTables);
    fbStopAll := False;
    for  nTable := 0  to  lTables.Count - 1  do
      ProcessTable(lTables.Strings[nTable],
                   fnTableAction, fnCheckField, fnOnFound, fnLogMessage,
                   setOptions)
  finally
    lTables.Free()
  end { try-finally }
end { ProcessTables };


class function  TProcessing.StopAll() : Boolean;
begin
  fSyncStop.BeginRead();
  try
    Result := fbStopAll
  finally
    fSyncStop.EndRead()
  end { try-finally }
end { StopAll };


class  procedure TProcessing.TerminateProcessing();
begin
  fSyncStop.BeginWrite();
  try
    fbStopAll := True
  finally
    fSyncStop.EndWrite()
  end { try-finally }
end { TerminateProcessing };

{$ENDREGION 'TProcessing'}


end.

