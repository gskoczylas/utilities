﻿unit  MainForm;

(**********************************************
 *                                            *
 *    Add missing UTF-8 BOM to source files   *
 *     ——————————————————————————————————     *
 *  Copyright © 2023-2024 Grzegorz Skoczylas  *
 *                                            *
 *   Zakład Usług Informatycznych PROGRAM     *
 *         mgr Grzegorz Skoczylas             *
 *             ul. Grażyny 13/15              *
 *             43-300 Bielsko-Biała           *
 *             tel. +48 503 064 953           *
 *             gskoczylas@gmail.com           *
 *                                            *
 **********************************************)


interface


{$WARN SYMBOL_PLATFORM OFF}


uses
  Winapi.Windows, Winapi.Messages,
  System.SysUtils, System.Variants, System.Classes, System.Threading,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Mask, Vcl.ComCtrls,
  JvExMask, JvToolEdit, Vcl.WinXCtrls;


type
  TDynArrTasks = array of ITask;

  TfrmMain = class(TForm)
    edtRootFolder:  TJvDirectoryEdit;
    btnStart:       TButton;
    btnExit:        TButton;
    lblRootFolder:  TLabel;
    edtLog:         TRichEdit;
    lblLog:         TLabel;
    actProcessing:  TActivityIndicator;
    txtFolder:      TStaticText;
    btnSave:        TButton;
    dlgSaveLog:     TFileSaveDialog;
    procedure  btnStartClick(Sender:  TObject);
    procedure  btnExitClick(Sender:  TObject);
    procedure  btnSaveClick(Sender:  TObject);
  private
    var
      fTasks:  TDynArrTasks;
    function  GetRootFolder() : string;
    procedure  ProcessFolder(const   sRootFolder:  string;
                             var     Tasks:  TDynArrTasks);
  end { TfrmMain };


var
  frmMain:  TfrmMain;


implementation


uses
  EnqueueFilesForProcessing;


{$R *.dfm}


procedure  TfrmMain.btnExitClick(Sender:  TObject);

var
  iJob:  ITask;

begin  { btnExitClick }
  for  iJob  in  fTasks  do
    iJob.Cancel();
  Close()
end { btnExitClick };


procedure  TfrmMain.btnSaveClick(Sender:  TObject);
begin
  dlgSaveLog.DefaultFolder := GetRootFolder();
  dlgSaveLog.FileName := 'AddMissingBOM_' + FormatDateTime('yyyy-mm-dd_hh;nn', Now());
  if  dlgSaveLog.Execute()  then
    edtLog.Lines.SaveToFile(dlgSaveLog.FileName)
end { btnSaveClick };


procedure  TfrmMain.btnStartClick(Sender:  TObject);
begin
  {$IFDEF DEBUG}
    with  TThreadPool.Default  do
      begin
        SetMinWorkerThreads(0);
        SetMaxWorkerThreads(1)
      end ;
  {$ENDIF DEBUG}
  actProcessing.Animate := True;
  btnStart.Enabled := False;
  btnSave.Hide();
  try
    btnStart.Repaint();
    SetLength(fTasks, 0);
    ProcessFolder(GetRootFolder(), fTasks);
    repeat
      Application.ProcessMessages()
    until  TTask.WaitForAll(fTasks, 100)
  finally
    actProcessing.Animate := False;
    btnStart.Enabled := True;
    if  edtLog.Lines.Count <> 0  then
      begin
        btnStart.Default := False;
        btnSave.Show();
        btnSave.Default := True
      end { edtLog is not empty }
  end { try-finally }
end { btnStartClick };


function  TfrmMain.GetRootFolder() : string;
begin
  Result := IncludeTrailingPathDelimiter(Trim(edtRootFolder.Directory))
end { GetRootDir };


procedure  TfrmMain.ProcessFolder(const   sRootFolder:  string;
                                  var     Tasks:  TDynArrTasks);
var
  lFiles:  TSearchRec;
  sFileExt:  string;
  sPrevFolder:  string;

begin  { ProcessFolder }
  sPrevFolder := txtFolder.Caption;
  txtFolder.Caption := ExcludeTrailingPathDelimiter(sRootFolder);
  txtFolder.Update();
  if  FindFirst(sRootFolder + '*.*', faAnyFile, lFiles) = 0  then
    try
      repeat
        if  Application.Terminated  then
          Exit;
        if  (lFiles.Name = '.')
            or (lFiles.Name = '..')  then
          Continue;
        if  lFiles.Attr and faDirectory <> 0  then
          ProcessFolder(sRootFolder + lFiles.Name + PathDelim,
                        Tasks)
        else
          begin
            sFileExt := ExtractFileExt(lFiles.Name);
            if  SameFileName(sFileExt, '.txt')
                or SameFileName(sFileExt, '.pas')
                or SameFileName(sFileExt, '.inc')
                or SameFileName(sFileExt, '.dpr')
                or SameFileName(sFileExt, '.dpk')
                or SameFileName(sFileExt, '.cs')
                or SameFileName(sFileExt, '.md')  then
              InsertMissingBOMInFile(sRootFolder + lFiles.Name,
                                     edtLog, Tasks);
            Application.ProcessMessages()
          end { if not }
      until  FindNext(lFiles) <> 0
    finally
      FindClose(lFiles)
    end { try-finally };
  txtFolder.Caption := sPrevFolder
end { ProcessFolder };


end.
