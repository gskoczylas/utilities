﻿unit  FileProcessor;

(**********************************************
 *                                            *
 *        Update Copyright information        *
 *     ——————————————————————————————————     *
 *  Copyright © 2016-2024 Grzegorz Skoczylas  *
 *                                            *
 *   Zakład Usług Informatycznych PROGRAM     *
 *         mgr Grzegorz Skoczylas             *
 *             ul. Grażyny 13/15              *
 *             43-300 Bielsko-Biała           *
 *             tel. +48 503 064 953           *
 *             gskoczylas@gmail.com           *
 *                                            *
 **********************************************)


interface


uses
  Winapi.Windows,
  System.Classes, System.RegularExpressions, System.StrUtils, System.SysUtils;


type
  EFileProcessing = class(Exception);


function  ProcessFile(const   sFileName:  string)
                      : Boolean;


implementation


procedure  ReadData(const   Src:  TFileStream;
                    var     Buf:  TBytes);
  forward;


procedure   WriteData(const   Dst:  TFileStream;
                      const   Buf:  TBytes);
  forward;


function  OpenInputFile(const   sFileName:  string)
                        : TFileStream;
begin
  try
    Result := TFileStream.Create(sFileName, fmOpenRead, fmShareDenyWrite)
  except
    on  eErr : Exception  do
      raise EFileProcessing.CreateFmt('Error opening file "%s": %s',
                                      [sFileName, eErr.Message])
  end { try-except };
end { OpenInputFile };


function  OpenOutputFile({const}   sFileName:  string)
                         : TFileStream;
begin
  sFileName := sFileName + '.UTF8';
  try
    Result := TFileStream.Create(sFileName, fmCreate, fmExclusive)
  except
    on  eErr : Exception  do
      raise  EFileProcessing.CreateFmt('Error creating file "%s": %s',
                                       [sFileName, eErr.Message])
  end { try-except }
end { OpenOutputFile };


function  MissingBOM(const   Src:  TFileStream;
                     const   BOM:  TBytes;
                     out     Buf:  TBytes;
                     out     nLen:  Int64)
                     : Boolean;
var
  nInd:  Integer;

begin  { MissingBOM }
  nLen := Length(BOM);
  if  Src.Size < nLen  then
    Exit(False);
  SetLength(Buf, nLen);
  ReadData(Src, Buf);
  Assert(Length(BOM) = Length(Buf));
  for  nInd := Low(BOM)  to  High(BOM)  do
    if  BOM[nInd] <> Buf[nInd]  then
      Exit(True);
  Result := False
end { MissingBOM };


procedure  ReadData(const   Src:  TFileStream;
                    var     Buf:  TBytes);
begin
  try
    Src.ReadBuffer(Buf, Length(Buf))
  except
    on  eErr : Exception  do
      raise  EFileProcessing.Create('Error reading input file: ' + eErr.Message)
  end { try-except }
end { ReadData };


procedure   WriteData(const   Dst:  TFileStream;
                      const   Buf:  TBytes);
begin
  try
    Dst.WriteBuffer(Buf, Length(Buf))
  except
    on  eErr : Exception  do
      raise  EFileProcessing.Create('Error writing output file: ' + eErr.Message)
  end { try-except }
end { WriteData };


function  ProcessFile(const   sFileName:  string)
                      : Boolean;
var
  lInp:  TFileStream;
  lOut:  TFileStream;
  lBOM:  TBytes;
  lBuf:  TBytes;
  nLen:  Int64;

begin  { ProcessFile }
  Result := False;
  {-}
  lInp := OpenInputFile(sFileName);
  try
    lBOM := TEncoding.UTF8.GetPreamble();
    if  not MissingBOM(lInp, lBOM, lBuf, nLen)  then
      Exit;
    {-}
    lOut := OpenOutputFile(sFileName);
    try
      WriteData(lOut, lBOM);
      WriteData(lOut, lBuf);
      {-}
      nLen := lInp.Size - nLen;
      SetLength(lBuf, nLen);
      {-}
      ReadData(lInp, lBuf);
      WriteData(lOut, lBuf)
    finally
      lOut.Free()
    end { try-finally };
    {-}
    Result := True
  finally
    lInp.Free()
  end { try-finally }
end { ProcessFile };


end.
