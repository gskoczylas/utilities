﻿unit  EnqueueFilesForProcessing;

(**********************************************
 *                                            *
 *    Add missing UTF-8 BOM to source files   *
 *     ——————————————————————————————————     *
 *  Copyright © 2023-2024 Grzegorz Skoczylas  *
 *                                            *
 *   Zakład Usług Informatycznych PROGRAM     *
 *         mgr Grzegorz Skoczylas             *
 *             ul. Grażyny 13/15              *
 *             43-300 Bielsko-Biała           *
 *             tel. +48 503 064 953           *
 *             gskoczylas@gmail.com           *
 *                                            *
 **********************************************)


interface


uses
  Winapi.Messages,
  System.Threading, System.Classes, System.SysUtils,
  Vcl.ComCtrls,
  MainForm;


procedure  InsertMissingBOMInFile(const   sFileName:  string;
                                  const   edtLog:  trichEdit;
                                  var     Tasks:  TDynArrTasks);


implementation


uses
  FileProcessor;


var
  gThreadPool:  TThreadPool;


procedure  InsertMissingBOMInFile(const   sFileName:  string;
                                  const   edtLog:  trichEdit;
                                  var     Tasks:  TDynArrTasks);
begin
  SetLength(Tasks, Succ(Length(Tasks)));
  Tasks[High(Tasks)] := TTask.Create(procedure
                                       begin
                                         if  ProcessFile(sFileName)  then
                                           TThread.Synchronize(nil,
                                                               procedure
                                                                 begin
                                                                   edtLog.Lines.Add(sFileName);
                                                                   edtLog.SetFocus();
                                                                   edtLog.SelStart := edtLog.GetTextLen();
                                                                   edtLog.Perform(EM_SCROLLCARET, 0, 0)
                                                                 end)
                                       end,
                                     gThreadPool).Start()
end { InsertMissingBOMInFile };


initialization
  gThreadPool := TThreadPool.Create();


finalization
  gThreadPool.Free()


end.
