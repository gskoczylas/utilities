﻿program  AddMissingBOM;

(**********************************************
 *                                            *
 *    Add missing UTF-8 BOM to source files   *
 *     ——————————————————————————————————     *
 *  Copyright © 2023-2024 Grzegorz Skoczylas  *
 *                                            *
 *   Zakład Usług Informatycznych PROGRAM     *
 *         mgr Grzegorz Skoczylas             *
 *             ul. Grażyny 13/15              *
 *             43-300 Bielsko-Biała           *
 *             tel. +48 503 064 953           *
 *             gskoczylas@gmail.com           *
 *                                            *
 **********************************************)


uses
  Vcl.Forms,
  MainForm in 'MainForm.pas' {frmMain},
  FileProcessor in 'FileProcessor.pas',
  EnqueueFilesForProcessing in 'EnqueueFilesForProcessing.pas',
  Vcl.Themes,
  Vcl.Styles;

{$R *.res}


begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'Add UTF-8 BOM';
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
