program  RegBinConv;


{$APPTYPE CONSOLE}


uses
//   Forms,
  SysUtils;


var
  fInp:  TextFile;
  fOut:  TextFile;
  nCnt:  Cardinal;
  nVal:  Byte;
  sBuf:  string;
  chBuf: AnsiChar;


{$R *.res}


function  ReadChar() : Byte;

var
  chBuf:  AnsiChar;

begin  { ReadChar }
  Read(fInp, chBuf);
  Result := StrToInt('$' + chBuf)
end { ReadChar };


begin
//   Application.Initialize;
//   Application.MainFormOnTaskbar := True;
//   Application.Title := 'Registry Binary Converter';
//   Application.Run;
  AssignFile(fInp, 'Input.txt');
  Reset(fInp);
  AssignFile(fOut, 'Output.txt');
  Rewrite(fOut);
  nCnt := 0;
  while  not SeekEof(fInp)  do
    begin
      nVal := ReadChar() * 16 + ReadChar();
      Inc(nCnt);
      if  nCnt mod 10 = 1  then
        begin
          if  nCnt > 1  then
            Writeln(fOut, ' |', sBuf);
          Write(fOut, nCnt:5, ':');
          sBuf := ''
        end { nCnt mod 10 = 1 };
      Write(fOut, ' ', IntToHex(nVal, 2));
      chBuf := AnsiChar(nVal);
      if  chBuf in [#0..Pred(' '), #127, #255]  then
        chBuf := '�';
      sBuf := sBuf + ' ' + WideChar(chBuf)
    end { not not SeekEof(fInp) };
  if  sBuf <> ''  then
    Write(fOut, ' |', sBuf);
  Writeln(fOut);
  Write('[Enter] '); Readln;
end.
