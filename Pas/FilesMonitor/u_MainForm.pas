﻿unit  u_MainForm;

(**********************************************
 *                                            *
 *                FilesMonitor                *
 *     ——————————————————————————————————     *
 *  Copyright © 2012-2024 Grzegorz Skoczylas  *
 *                                            *
 *         mgr Grzegorz Skoczylas             *
 *             ul. Grażyny 13/15              *
 *             43-300 Bielsko-Biała           *
 *             tel. (0) 503 064 953           *
 *             gskoczylas@gmail.com           *
 *                                            *
 **********************************************)


interface


uses
  System.SysUtils, System.Variants, System.Classes, System.IniFiles,
  System.IOUtils, System.Win.ComObj, System.ImageList, System.StrUtils,
  System.SyncObjs, System.Math, System.Generics.Collections,
  WinAPI.Windows, WinAPI.Messages, WinAPI.ShellAPI, WinAPI.ShlObj, WinAPI.ActiveX,
  VCL.Graphics, VCL.Controls, VCL.Forms, VCL.Dialogs, VCL.ExtCtrls, VCL.Menus,
  VCL.ImgList,



{$IFDEF DEBUG}
  GSkPasLib.Log, GSkPasLib.LogFile,
{$ENDIF DEBUG}
  u_Monitor;


const
  gcsVarFrequency  = 'Frequency';
  gcsVarCommand    = 'Command';
  gcsVarParameters = 'Parameters';
  gcsVarFolder     = 'Folder';
  gcsVarAction     = 'Action';
  gcsVarTarget     = 'Target';
  gcsVarCreated    = 'Created';
  gcsVarUpdated    = 'Updated';
  gcsVarSize       = 'Size';
  gcsVarLoSuffix   = 'Lo';
  gcsVarHiSuffix   = 'Hi';

  gcFileActions:  array[TFileAction] of  string = (
      'RunCmd', 'RunCmdMin', 'CopyFile', 'MoveFile', 'CopyFileTo', 'MoveFileTo');


type
  TIniFileHelper = class helper for TIniFile
  public
    function  ReadInt64(const   sSection:  string;
                        const   sName:  string;
                        const   nDefault:  Int64)
                        : Int64;
    function  ReadAction(const   sSection:  string;
                         const   sName:  string;
                         const   Default:  TFileAction)
                         : TFileAction;
    procedure  WriteInt64(const   sSection:  string;
                          const   sName:  string;
                          const   nValue:  Int64);
    procedure  WriteAction(const   sSection:  string;
                           const   sName:  string;
                           const   Value:  TFileAction);
  end { TIniFileHelper };

  TfrmMain = class(TForm)
    tryMenu:       TTrayIcon;
    pmnMenu:       TPopupMenu;
    mnuExit:       TMenuItem;
    imlSuspended:  TImageList;
    mnuSuspend:    TMenuItem;
    mnuResume:     TMenuItem;
    mnuSep1:       TMenuItem;
    mnuReset:      TMenuItem;
    mnuSep2:       TMenuItem;
    procedure  FormCreate(Sender:  TObject);
    procedure  FormDestroy(Sender:  TObject);
    procedure  mnuExitClick(Sender:  TObject);
    procedure  mnuSuspendClick(Sender:  TObject);
    procedure  mnuResumeClick(Sender:  TObject);
    procedure  mnuResetClick(Sender:  TObject);
  strict private
    var
      fTasks:   TList<TMonitorFile>;
      fsConfig: string;
    function  GetTargets(const   sSection:  string)
                         : TStringList;
    procedure  Balloon(const   sTitle:  string;
                       const   Flag:  TBalloonFlags;
                       const   nTimeout:  Integer;
                       const   sMessage:  string);                     overload;
    procedure  Balloon(const   sTitle:  string;
                       const   Flag:  TBalloonFlags;
                       const   nTimeout:  Integer;
                       const   sMessageFmt:  string;
                       const   MessageArgs:  array of const);          overload;
    procedure  FileUpdated(const   sPath, sFile:  string;
                           const   tmCreated, tmWritten:  TDateTime;
                           const   nSize:  Int64;
                           const   Action:  TFileAction;
                           const   Targets:  TStringList;
                           const   sCommand, sParameters, sFolder:  string);
    procedure  CreateTasks();
    procedure  ReleaseTask(const   Task:  TMonitorFile);
    procedure  ReleaseTasks();
    procedure  VerifyTasks();
  end { TfrmMain };


var
  frmMain:    TfrmMain;
  gcsConfig:  TCriticalSection;
  gConfig:    TIniFile;
{$IFDEF DEBUG}
  gLog:       TGSkLog;
{$ENDIF DEBUG}


implementation


{$R *.dfm}


{$REGION 'TIniFileHelper'}

function  TIniFileHelper.ReadAction(const   sSection, sName:  string;
                                    const   Default:  TFileAction)
                                    : TFileAction;
var
  sBuf:  string;

begin  { ReadAction }
  sBuf := ReadString(sSection, sName, gcFileActions[Default]);
  for  Result := High(TFileAction)  downto  Low(TFileAction)  do
    if  SameText(gcFileActions[Result], sBuf)  then
      Exit;
  raise  EFilesMonitor.CreateFmt('''%s'' is not known action', [sBuf])
end { ReadAction };


function  TIniFileHelper.ReadInt64(const   sSection, sName:  string;
                                   const   nDefault:  Int64)
                                   : Int64;
begin  { ReadInt64 }
  if  ValueExists(sSection, sName)  then
    Result := Cardinal(ReadInteger(sSection, sName, 0))
  else
    begin
      Int64Rec(Result).Lo := Cardinal(ReadInteger(sSection,
                                                  sName + gcsVarLoSuffix,
                                                  Integer(Int64Rec(nDefault).Lo)));
      Int64Rec(Result).Hi := Cardinal(ReadInteger(sSection,
                                                  sName + gcsVarHiSuffix,
                                                  Integer(Int64Rec(nDefault).Hi)))
    end { not ValueExists(...) }
end { ReadInt64 };


procedure  TIniFileHelper.WriteAction(const   sSection, sName:  string;
                                      const   Value:  TFileAction);
begin
  WriteString(sSection, sName, gcFileActions[Value])
end { WriteAction };


procedure  TIniFileHelper.WriteInt64(const   sSection, sName:  string;
                                     const   nValue:  Int64);
begin
  if  InRange(nValue, Low(Integer), High(Integer))  then   // if  Int64Rec(nValue).Hi <> 0  then
    begin
      DeleteKey(sSection, sName + gcsVarLoSuffix);
      DeleteKey(sSection, sName + gcsVarHiSuffix);
      WriteInteger(sSection, sName, Integer(Int64Rec(nValue).Lo))
    end { if value is 32-bit integer }
  else
    begin
      DeleteKey(sSection, sName);
      WriteInteger(sSection,
                   sName + gcsVarLoSuffix,
                   Integer(Int64Rec(nValue).Lo));
      WriteInteger(sSection,
                   sName + gcsVarHiSuffix,
                   Integer(Int64Rec(nValue).Hi))
    end { if value is 64-bit integer }
end { WriteInt64 };

{$ENDREGION 'TIniFileHelper'}


{$REGION 'TfrmMain'}

procedure  TfrmMain.Balloon(const   sTitle:  string;
                            const   Flag:  TBalloonFlags;
                            const   nTimeout:  Integer;
                            const   sMessage:  string);
begin
  with  tryMenu  do
    begin
      BalloonTitle := sTitle;
      BalloonFlags := Flag;
      BalloonTimeout := nTimeout;
      BalloonHint := sMessage;
      ShowBalloonHint()
    end { with tryMenu }
end { Balloon };


procedure  TfrmMain.Balloon(const   sTitle:  string;
                            const   Flag:  TBalloonFlags;
                            const   nTimeout:  Integer;
                            const   sMessageFmt:  string;
                            const   MessageArgs:  array of const);
begin
  Balloon(sTitle, Flag, nTimeout, Format(sMessageFmt, MessageArgs))
end { Balloon };


procedure  TfrmMain.CreateTasks();

var
  sSec:  string;
  lSec:  TStringList;
  nInd:  Integer;
 {$IFDEF DEBUG}
  nLen:  Integer;
  nThr:  Integer;
 {$ENDIF DEBUG}
  sHint: string;

  function  AppTitle() : string;
  begin
    Result := Application.Title + ' (';
    {$IF Defined(WIN32)}
      Result := Result + '32';
    {$ELSEIF Defined(WIN64)}
      Result := Result + '64';
    {$ELSE}
      {$MESSAGE ERROR 'Unsupported platform'}
    {$ENDIF}
    Result := Result + ' bit.)'
  end { AppTitle };

begin  { CreateTasks }
  if  not FileExists(fsConfig)  then
    Exit;
  {$IFDEF DEBUG}
    gLog.LogEnter('CreateTasks');
  {$ENDIF DEBUG}
  gcsConfig.Enter();
  try
    with  gConfig  do
      begin
        lSec := TStringList.Create();
        try
          ReadSections(lSec);
          {$IFDEF DEBUG}
            nLen := 0;
            for  nInd := 0  to  lSec.Count - 1  do
              nLen := Max(nLen, Length(ExtractFileName(lSec[nInd])));
            gLog.Prefix := StringOfChar('-', nLen);
          {$ENDIF DEBUG}
          if  lSec.Count = 1  then
            sHint := AppTitle() + sLineBreak + '1 path:'
          else
            sHint := AppTitle() + sLineBreak + IntToStr(lSec.Count) + ' paths:';
          for  nInd := 0  to  lSec.Count - 1  do
            begin
              sSec := Trim(lSec[nInd]);
              {$IFDEF DEBUG}
                gLog.LogValue('Path', sSec);
              {$ENDIF DEBUG}
              sHint := sHint + sLineBreak + '— ' + sSec;
              fTasks.Add(TMonitorFile.Create(sSec, {$IFDEF DEBUG} nLen, {$ENDIF}
                                             ReadInteger(sSec, gcsVarFrequency, 100),
                                             ReadAction(sSec, gcsVarAction, faRunCmd),
                                             GetTargets(sSec),
                                             Trim(ReadString(sSec, gcsVarCommand, '')),
                                             Trim(ReadString(sSec, gcsVarParameters, '')),
                                             Trim(ReadString(sSec, gcsVarFolder, '')),
                                             FileUpdated));
              {$IFDEF DEBUG}
                gLog.BeginLog();
                try
                  gLog.LogMessage('* Threads');
                  for  nThr := 0  to  fTasks.Count - 1  do
                    gLog.LogMessage('%5d. %s', [nThr+1, fTasks.Items[nThr].Path])
                finally
                  gLog.EndLog()
                end { try-finally }
              {$ENDIF DEBUG}
            end { for nInd };
          tryMenu.Hint := sHint
        finally
          lSec.Free()
        end { try-finally }
      end { try-finally }
  finally
    gcsConfig.Leave()
  end { try-finally };
  {$IFDEF DEBUG}
    gLog.LogExit('CreateTasks')
  {$ENDIF DEBUG}
end { CreateTasks };


procedure  TfrmMain.FileUpdated(const   sPath, sFile:  string;
                                const   tmCreated, tmWritten:  TDateTime;
                                const   nSize:  Int64;
                                const   Action:  TFileAction;
                                const   Targets:  TStringList;
                                const   sCommand, sParameters, sFolder:  string);
const
  cnRetry = 3;

type
  TRunMode = (rmNormal, rmMinimized);

var
  sBuf:  string;
  sErr:  string;
  nInd:  Integer;
  nTry:  Integer;
  bCnt:  Boolean;

  procedure  RunCmd(const   sCommand, sParameters, sFolder:  string;
                    const   runMode:  TRunMode);
  const
    cShowCmd:  array[TRunMode] of  Integer = (
                 SW_SHOWNORMAL,         // rmNormal
                 SW_SHOWMINNOACTIVE);   // rmMinimized

  var
    hRes:  HINST;
    sPar:  string;

  begin  { RunCmd }
    hRes := ShellExecute(0, nil,
                         PChar(sCommand), PChar(sParameters), PChar(sFolder),
                         cShowCmd[runMode]);
    if  hRes <= 32  then
      begin
        if  sParameters <> ''  then
          sPar := sLineBreak + 'Parameters:'
                    + sLineBreak + ' »' + sParameters
        else
          sPar := '';
        if  sFolder <> ''  then
          begin
            if  sPar <> ''  then
              sPar := sPar + sLineBreak;
            sPar := sPar + 'Folder: ' + sFolder
          end { sFolder <> '' };
        Balloon('Error', bfError, 10000{10 s},
                SysErrorMessage(hRes)
                  + sLineBreak + sLineBreak + 'Command:'
                  + sLineBreak + ' »' + sCommand
                  + sPar)
      end { hRes <= 32 }
    else
      Balloon('', bfNone, 0, '')
  end { RunCmd };

  procedure  FileOp(const   sSource, sTarget:  string;
                    const   Action:  TFileAction);
  var
    hStat:  HRESULT;
    iFileOp:  IFileOperation;
    iFrom:  IShellItem;
    iTo:  IShellItem;
    sDir:  string;

  begin  { FileOp }
    hStat := CoInitializeEx(nil, COINIT_APARTMENTTHREADED or COINIT_DISABLE_OLE1DDE);
    if  Succeeded(hStat)  then
      begin
        hStat := CoCreateInstance(CLSID_FileOperation, nil, CLSCTX_ALL,
                                  IFileOperation, iFileOp);
        if  Succeeded(hStat)  then
          begin
            hStat := iFileOp.SetOperationFlags(FOF_NO_UI);
            if  Succeeded(hStat)  then
              begin
                iFrom := nil;
                hStat := SHCreateItemFromParsingName(PChar(sSource), nil, IShellItem, iFrom);
                if  Succeeded(hStat)  then
                  begin
                    iTo := nil;
                    case  Action  of
                      faRunCmd:
                        Assert(False, Format('Action %s is not allowed here', [gcFileActions[Action]]));
                      faCopyFile, faMoveFile:
                        begin
                          sDir := ExtractFileDir(sTarget);
                          if  sDir <> ''  then
                            hStat := SHCreateItemFromParsingName(PChar(sDir), nil, IShellItem, iTo)
                        end { faCopyFile, faMoveFile };
                      faCopyFileTo, faMoveFileTo:
                        begin
                          hStat := SHCreateItemFromParsingName(PChar(sTarget), nil, IShellItem, iTo)
                        end { faCopyFileTo, faMoveFileTo }
                    end { case Action };
                    if  Succeeded(hStat)  then
                      case  Action  of
                        faCopyFile:
                          hStat := iFileOp.CopyItem(iFrom, iTo, PChar(ExtractFileName(sTarget)), nil);
                        faMoveFile:
                          hStat := iFileOp.MoveItem(iFrom, iTo, PChar(ExtractFileName(sTarget)), nil);
                        faCopyFileTo:
                          hStat := iFileOp.CopyItem(iFrom, iTo, PChar(sFile), nil);
                        faMoveFileTo:
                          hStat := iFileOp.MoveItem(iFrom, iTo, PChar(sFile), nil)
                        else
                          Assert(False, Format('Action %s is not allowed here', [gcFileActions[Action]]))
                      end { case Action }
                  end { if };
                if  Succeeded(hStat)  then
                  hStat := iFileOp.PerformOperations();
                if  Failed(hStat)  then
                  OleCheck(hStat)
              end
          end { if };
        CoUninitialize()
      end { if }
  end { FileOp };

begin  { FileUpdated }
  {$IFDEF DEBUG}
    gLog.BeginLog();
    try
      gLog.LogEnter('FileUpdated');
      gLog.LogValue('File', sFile);
      gLog.LogValue('Path', sPath);
      gLog.LogValue('Created', tmCreated);
      gLog.LogValue('Written', tmWritten);
      gLog.LogValue('Size', nSize);
      gLog.LogMessage('Action = ' + gcFileActions[Action]);
      if  Targets.Count = 1  then
        gLog.LogValue('Target', Targets[0])
      else
        gLog.LogValue('Target', Targets);
      gLog.LogValue('Command', sCommand);
      gLog.LogValue('Parameters', sParameters)
    finally
      gLog.EndLog()
    end { try-finally };
    try
  {$ENDIF DEBUG}
  sBuf := sFile + '|';
  gcsConfig.Enter();
  try
    nTry := cnRetry;
    bCnt := False;
    repeat
      try
        gConfig.WriteDateTime(sPath, sBuf + gcsVarCreated, tmCreated);
        gConfig.WriteDateTime(sPath, sBuf + gcsVarUpdated, tmWritten);
        gConfig.WriteInt64(sPath, sBuf + gcsVarSize, nSize);
        bCnt := True
      except
        on  eErr : Exception  do
          begin
            Dec(nTry);
            {$IFDEF DEBUG}
              gLog.LogException(eErr, 'Trying to save data in the configuration file');
              if  nTry > 0  then
                gLog.LogMessage('Will try again (%d)', [nTry], lmWarning)
              else
                gLog.LogMessage('I give up', lmError);
            {$ENDIF DEBUG}
            if  nTry > 0  then
              begin
                Sleep(500 + Random(1500));
                bCnt := False
              end { nTry > 0 }
            else
              Exit
          end { on Exception }
      end { try-except }
    until  bCnt
  finally
    gcsConfig.Leave()
  end { try-finally };
  try
    sErr := '';
    sBuf := ExtractFilePath(sPath) + sFile;
    if  TFile.Exists(sBuf, False)  then
      case  Action  of
        faRunCmd:
          begin
            sErr := 'RUN "' + sCommand + '" ' + sParameters;
            if  sFolder <> ''  then
              sErr := sErr + sLineBreak + '> ' + sFolder;
            RunCmd(sCommand, sParameters, sFolder, rmNormal)
          end { faRunCmd };
        faRunCmdMin:
          begin
            sErr := 'RUN/min "' + sCommand + '" ' + sParameters;
            if  sFolder <> ''  then
              sErr := sErr + sLineBreak + '> ' + sFolder;
            RunCmd(sCommand, sParameters, sFolder, rmMinimized)
          end { faRunCmdMin };
        faCopyFile, faMoveFile,
        faCopyFileTo, faMoveFileTo:
          for  nInd := 0  to  Targets.Count - 1  do
            begin
              sErr := 'COPY/MOVE "' + sBuf + '" TO "' + Targets[nInd] + '"';
              FileOp(sBuf, Targets[nInd], Action)
            end { for nInd }
        else
          Assert(False, Format('Action %s is not allowed here', [gcFileActions[Action]]))
      end { case Action }
    else
      Balloon('Warning', bfWarning, 5000{5 s},
              'File ''%s'' does not exists', [sFile]);
    Application.ProcessMessages()
  except
    on  eErr : Exception  do
      begin
        if  sErr <> ''  then
          sErr := sErr + sLineBreak;
        sErr := sErr + eErr.Message;
        Balloon('Error', bfError, 10000{10 s}, sErr)
      end { on Exception }
  end { try-except }
  {$IFDEF DEBUG}
    finally
      gLog.LogExit('FileUpdated')
    end { try-finally }
  {$ENDIF DEBUG}
end { FileUpdated };


procedure  TfrmMain.FormCreate(Sender:  TObject);
begin
  fsConfig := ChangeFileExt(Application.ExeName, '.ini');
  gConfig := TIniFile.Create(fsConfig);
  fTasks := TList<TMonitorFile>.Create();
  CreateTasks();
end { FormCreate };


procedure  TfrmMain.FormDestroy(Sender:  TObject);
begin
  {$IFDEF DEBUG}
    gLog.LogEnter('FormDestroy');
  {$ENDIF DEBUG}
  ReleaseTasks();
  fTasks.Free();
  {$IFDEF DEBUG}
    gLog.LogExit('FormDestroy')
  {$ENDIF DEBUG}
end { FormDestroy };


function  TfrmMain.GetTargets(const   sSection:  string)
                              : TStringList;
var
  nInd:  Integer;

begin  { GetTargets }
  Result := TStringList.Create();
  gConfig.ReadSectionValues(sSection, Result);
  for  nInd := Result.Count - 1  downto  0  do
    if  not StartsText(gcsVarTarget, Result.Names[nInd])  then
      Result.Delete(nInd);
  Assert(not Result.CaseSensitive);
  Result.Sort();
  for  nInd := Result.Count - 1  downto  0  do
    Result[nInd] := Result.ValueFromIndex[nInd]
end { GetTargets };


procedure  TfrmMain.mnuExitClick(Sender:  TObject);
begin
  {$IFDEF DEBUG}
    gLog.LogEnter('Exiting...');
  {$ENDIF DEBUG}
  Application.Terminate();
  {$IFDEF DEBUG}
    gLog.LogExit('Exited.')
  {$ENDIF DEBUG}
end { mnuExitClick };


procedure  TfrmMain.mnuResetClick(Sender:  TObject);
begin
  {$IFDEF DEBUG}
    gLog.LogEnter('Reseting...');
  {$ENDIF DEBUG}
  TMonitorFile.SuspendAll();
  VerifyTasks();
  Balloon('Reset', bfInfo, 2000{2 s}, 'Done');
  {$IFDEF DEBUG}
    gLog.LogExit('Reseting...');
  {$ENDIF DEBUG}
end { mnuResetClick };


procedure  TfrmMain.mnuResumeClick(Sender:  TObject);
begin
  {$IFDEF DEBUG}
    gLog.LogEnter('Resuming...');
  {$ENDIF DEBUG}
  VerifyTasks();
  tryMenu.Animate := False;
  tryMenu.IconIndex := 0;
  mnuResume.Enabled := False;
  mnuSuspend.Enabled := True;
  mnuReset.Enabled := True;
  Balloon('Resume', bfInfo, 2000{2 s}, 'Done');
  {$IFDEF DEBUG}
    gLog.LogExit('Resumed.')
  {$ENDIF DEBUG}
end { mnuResumeClick };


procedure  TfrmMain.mnuSuspendClick(Sender:  TObject);
begin
  {$IFDEF DEBUG}
    gLog.LogEnter('Suspending...');
  {$ENDIF DEBUG}
  TMonitorFile.SuspendAll();
  tryMenu.Animate := True;
  mnuResume.Enabled := True;
  mnuSuspend.Enabled := False;
  mnuReset.Enabled := False;
  Balloon('Suspend', bfInfo, 2000{2 s}, 'Done');
  {$IFDEF DEBUG}
    gLog.LogExit('Suspended.')
  {$ENDIF DEBUG}
end { mnuSuspendClick };


procedure  TfrmMain.ReleaseTask(const   Task:  TMonitorFile);
begin
  {$IFDEF DEBUG}
    gLog.LogEnter('ReleaseTask(%s)', [Task.Path]);
  {$ENDIF DEBUG}
  Task.Terminate();
  {$IFDEF DEBUG}
    gLog.LogMessage('Waiting for thread to terminate...');
  {$ENDIF DEBUG}
  Task.WaitFor();
  {$IFDEF DEBUG}
    gLog.LogMessage('Thread terminated.');
  {$ENDIF DEBUG}
  Task.Free();
  {$IFDEF DEBUG}
    gLog.LogExit('ReleaseTask')
  {$ENDIF DEBUG}
end { ReleaseTask };


procedure  TfrmMain.ReleaseTasks();

var
  nInd:  Integer;

begin  { ReleaseTasks }
  {$IFDEF DEBUG}
    gLog.LogEnter('ReleaseTasks');
  {$ENDIF DEBUG}
  TMonitorFile.ResumeAll();
  for  nInd := fTasks.Count - 1  downto  0  do
    begin
      ReleaseTask(fTasks[nInd]);
      fTasks.Delete(nInd)
    end { for nInd };
  {$IFDEF DEBUG}
    gLog.LogExit('ReleaseTasks')
  {$ENDIF DEBUG}
end { ReleaseTasks };


procedure  TfrmMain.VerifyTasks();
begin
  {$IFDEF DEBUG}
    gLog.LogEnter('VerifyTasks');
  {$ENDIF DEBUG}
  ReleaseTasks();
  CreateTasks();
  {$IFDEF DEBUG}
    gLog.LogExit('VerifyTasks')
  {$ENDIF DEBUG}
end { VerifyTasks };

{$ENDREGION 'TfrmMain'}


initialization
  {$IFDEF DEBUG}
    gLog := TGSkLog.Create(nil);
    gLog.ModuleName := 'FilesMonitor';
    gLog.AddLogAdapter(TGSkLogFile.Create(gLog));
  {$ENDIF DEBUG}
  gcsConfig := TCriticalSection.Create();


finalization
  gConfig.Free();
  gcsConfig.Free();
  {$IFDEF DEBUG}
    gLog.Free();
  {$ENDIF DEBUG}


end.
