﻿unit  u_Monitor;

(**********************************************
 *                                            *
 *                FilesMonitor                *
 *     ——————————————————————————————————     *
 *  Copyright © 2012-2024 Grzegorz Skoczylas  *
 *                                            *
 *         mgr Grzegorz Skoczylas             *
 *             ul. Grażyny 13/15              *
 *             43-300 Bielsko-Biała           *
 *             tel. (0) 503 064 953           *
 *             gskoczylas@gmail.com           *
 *                                            *
 **********************************************)


interface


uses
  System.Classes, System.IOUtils, System.SyncObjs, System.SysUtils, System.DateUtils,
  System.IniFiles, System.StrUtils, System.Types,
  System.Generics.Collections{$IFDEF DEBUG},
  GSkPasLib.Log{$ENDIF DEBUG};


const
  gcnCheckFreq = 100;   // [ms]


type
  TFileAction = (faRunCmd, faRunCmdMin,
                 faCopyFile, faMoveFile, faCopyFileTo, faMoveFileTo);

  TUpdateMethod = procedure(const   sPath, sFile:  string;
                            const   tmCreation, tmWritting:  TDateTime;
                            const   nSize:  Int64;
                            const   Action:  TFileAction;
                            const   Targets:  TStringList;
                            const   sCommand, sParams, sFolder:  string)  of object;

  TMonitorFile = class(TThread)
  strict private
    type
      TFileData = record
        strict private
          var
            ftmCrt:  TDateTime;
            ftmWrt:  TDateTime;
            fnSize:  Int64;
            fbChck:  Boolean;
        public
          property  Created:  TDateTime
                    read  ftmCrt
                    write ftmCrt;
          property  Updated:  TDateTime
                    read  ftmWrt
                    write ftmWrt;
          property  Size:  Int64
                    read  fnSize
                    write fnSize;
          property  CheckIt:  Boolean
                    read  fbChck
                    write fbChck;
      end { TFileData };
    var
      fsPath:  string;
      fnFreq:  Integer;
      fAction: TFileAction;
      fTrgts:  TStringList;
      fsCmnd:  string;
      fsParm:  string;
      fsFldr:  string;
      fFiles:  TDictionary<string,TFileData>;
      fOnUpd:  TUpdateMethod;
      fnTick:  Integer;
     {$IFDEF DEBUG}
      fLog:    TGSkLog;
     {$ENDIF DEBUG}
    class var
      fcsSynchro:  TCriticalSection;
      fevEnabled:  TEvent;
      flsThreads:  TThreadList;
    class constructor  Create();
    class destructor  Destory();
  strict protected
    procedure  Execute();                                            override;
  public
    constructor  Create(const   sFilePath:  string;
                       {$IFDEF DEBUG}
                        const   nPrefixLen:  Integer;
                       {$ENDIF DEBUG}
                        const   nCheckFrequency:  Integer;
                        const   Action:  TFileAction;
                        const   Targets:  TStringList;
                        const   sCommand, sParams, sFolder:  string;
                        const   OnUpdate:  TUpdateMethod);
    destructor  Destroy();                                           override;
    {-}
    procedure  ResetTimer();                                         inline;
    {-}
    class procedure  SuspendAll();                                   inline;
    class procedure  ResumeAll();
    {-}
    property  Path:  string
              read  fsPath;
    property  Frequency:  Integer
              read  fnFreq;
    property  Action:  TFileAction
              read  fAction;
    property  Targets:  TStringList
              read  fTrgts;
    property  Command:  string
              read  fsCmnd;
    property  Param:  string
              read  fsParm;
    property  Folder:  string
              read  fsFldr;
  end { TMonitorFile };

  EFilesMonitor = class(Exception);


implementation


uses
  {$IFDEF DEBUG}
    GSkPasLib.LogFile,
  {$ENDIF DEBUG}
  u_MainForm;


{$REGION 'TMonitorFile'}

constructor  TMonitorFile.Create(const   sFilePath:  string;
                                {$IFDEF DEBUG}
                                 const   nPrefixLen:  Integer;
                                {$ENDIF DEBUG}
                                 const   nCheckFrequency:  Integer;
                                 const   Action:  TFileAction;
                                 const   Targets:  TStringList;
                                 const   sCommand, sParams, sFolder:  string;
                                 const   OnUpdate:  TUpdateMethod);
var
  nInd:  Integer;

  procedure  Error(const   sErrInfo:  string);                       overload;
  begin
    {$IFDEF DEBUG}
      fLog.LogMessage(sErrInfo, lmError);
    {$ENDIF DEBUG}
    raise  EFilesMonitor.Create(sErrInfo)
  end { Error };

  procedure  Error(const   sErrFmt:  string;
                   const   ErrArgs:  array of const);                overload;
  begin
    Error(Format(sErrFmt, ErrArgs))
  end { Error };

begin  { Create }
  {$IFDEF DEBUG}
    try
      try
        fLog := TGSkLog.Create(nil);
        fLog.Prefix := LeftStr(ExtractFileName(sFilePath) + StringOfChar('-', nPrefixLen),
                               nPrefixLen);
        fLog.AddLogAdapter(TGSkLogFile.Create(fLog));
        {-}
        fLog.BeginLog();
        try
          fLog.LogEnter('TMonitorFile.Create');
          fLog.LogValue('FilePath', sFilePath);
          fLog.LogValue('CheckFrequency', nCheckFrequency);
          fLog.LogMessage('Action = ' + gcFileActions[Action]);
          fLog.LogValue('Target(s)', Targets);
          fLog.LogValue('Command', sCommand);
          fLog.LogValue('Params', sParams)
        finally
          fLog.EndLog()
        end { try-finally };
  {$ENDIF DEBUG}
        if  sFilePath = ''  then
          Error('Path is empty');
        case  Action  of
          faRunCmd, faRunCmdMin:
            if  sCommand = ''  then
              Error('Command for file ''%s'' is empty', [sFilePath]);
          faCopyFile, faMoveFile,
          faCopyFileTo, faMoveFileTo:
            if  Targets.Count = 0  then
              Error('Action: %s; missing ''' + gcsVarTarget + ''' parameter', [gcFileActions[Action]]);
          else
            Assert(False, Format('Action %s is not allowed here', [gcFileActions[Action]]))
        end { case Action };
        fsPath := sFilePath;
        fnFreq := nCheckFrequency;
        fAction := Action;
        fTrgts := Targets;
        if  Action in [faCopyFileTo, faMoveFileTo]  then
          for  nInd := fTrgts.Count - 1  downto  0  do
            fTrgts.Strings[nInd] := ExcludeTrailingPathDelimiter(fTrgts.Strings[nInd]);
        fsCmnd := sCommand;
        fsParm := sParams;
        fsFldr := sFolder;
        fFiles := TDictionary<string,TFileData>.Create();
        fOnUpd := OnUpdate;
        inherited Create(False);
        with  flsThreads.LockList()  do
          try
            Add(Self);
            {$IFDEF DEBUG}
              fLog.LogValue('ThreadCount', Count);
            {$ENDIF DEBUG}
          finally
            flsThreads.UnlockList()
          end { try-finally }
  {$IFDEF DEBUG}
      except
        on  eErr : Exception  do
          begin
            fLog.LogException(eErr);
            raise
          end { on Exception }
      end { try-exce[t }
    finally
      fLog.LogExit('TMonitorFile.Create')
    end { try-finally }
  {$ENDIF DEBUG}
end { Create };


class constructor  TMonitorFile.Create();
begin
  inherited;
  fcsSynchro := TCriticalSection.Create();
  fevEnabled := TEvent.Create(nil, True, True, 'FilesMonitor');
  flsThreads := TThreadList.Create()
end { Create };


class destructor  TMonitorFile.Destory();
begin
  flsThreads.Free();
  fevEnabled.Free();
  fcsSynchro.Free();
  inherited
end { Destory };


destructor  TMonitorFile.Destroy();

{$IFDEF DEBUG}
var
  lLog:  TGSkLog;
{$ENDIF DEBUG}

begin  { Destroy }
  {$IFDEF DEBUG}
    fLog.LogEnter('TMonitorFile.Destroy');
    lLog := fLog;
  {$ENDIF DEBUG}
  with  flsThreads.LockList()  do
    try
      Remove(Self);
      {$IFDEF DEBUG}
        fLog.LogValue('ThreadCount', Count);
      {$ENDIF DEBUG}
    finally
      flsThreads.UnlockList()
    end { try-finally };
  fFiles.Free();
  fTrgts.Free();
  inherited;
  {$IFDEF DEBUG}
    lLog.LogExit('TMonitorFile.Destroy');
    fLog.Free()
  {$ENDIF DEBUG}
end { Destroy };


procedure  TMonitorFile.Execute();

var
  tmCrt:  TDateTime;
  tmWrt:  TDateTime;
  nSize:  Int64;
  lSrch:  TSearchRec;
  lFile:  TFileData;
  sName:  string;
  bExec:  Boolean;
  lBuf:   TPair<string,TFileData>;

  function  AllFilesExists(const   Files:  TStringList)
                           : Boolean;                                overload;
  var
    nInd:  Integer;

  begin  { AllFilesExists }
    Result := False;
    for  nInd := Files.Count - 1  downto  0  do
      if  not TFile.Exists(Files[nInd])  then
        Exit;
    Result := True
  end { AllFilesExists };

  function  AllFilesExists(const   Dirs:  TStringList;
                           const   sFileName:  string)
                           : Boolean;                                overload;
  var
    nInd:  Integer;

  begin  { AllFilesExists }
    Result := False;
    for  nInd := Dirs.Count - 1  downto  0  do
      if  not TFile.Exists(IncludeTrailingPathDelimiter(Dirs[nInd]) + sFileName)  then
        Exit;
    Result := True
  end { AllFilesExists };

begin  { Execute }
  {$IFDEF DEBUG}
    {$WARN SYMBOL_PLATFORM OFF}   // DebugHook
    if  DebugHook <> 0  then
      NameThreadForDebugging(AnsiString(fsPath));
    {$WARN SYMBOL_PLATFORM ON}
    fLog.LogEnter('TMonitorFile.Execute(%s)', [fsPath]);
  {$ENDIF DEBUG}
  Sleep(Random(1000));   // losowe opóźnienie startu, aby wiele wątków nie aktywowało się w tym samym czasie
  tmCrt := EncodeDate(1900, 1, 1);   // Eliminuje zbędne ostrzeżenie kompilatora,
  tmWrt := tmCrt;                    // że zmienne mogą mieć nieznaną wartość
  nSize := -1;                       // (w przypisaniu bExec := ...)
  fnTick := fnFreq;   // --> check immediately
  while  not Terminated  do
    begin
      {$IFDEF DEBUG}
        fLog.LogMessage('Terminated = %s  -- %s',
                        [BoolToStr(Terminated, True),
                         fsPath]);
      {$ENDIF DEBUG}
      if  Terminated  then
        Break;
      if  fevEnabled.WaitFor(gcnCheckFreq) = wrTimeout  then
        Continue;
      if  Terminated  then
        Break;
      if  fnTick >= fnFreq  then
        begin
          for  lBuf in fFiles  do
            begin
              lFile := lBuf.Value;
              lFile.CheckIt := True;
              fFiles.Items[lBuf.Key] := lFile
            end { for lBuf };
//           {$IFDEF DEBUG}
//             for  lBuf in fFiles  do
//               fLog.LogValue(lBuf.Key, lBuf.Value.CheckIt);
//           {$ENDIF DEBUG}
          { Sprawdzam pliki pasujące do maski }
          {$IFDEF DEBUG}
            fLog.LogEnter('Search path = ' + fsPath);
          {$ENDIF DEBUG}
          if  FindFirst(fsPath, Integer($FFFFFFFF), lSrch) = 0  then
            try
              repeat
                if  Terminated  then
                  Break;
                {$IFDEF DEBUG}
                  fLog.LogMessage('Checking file ' + lSrch.Name);
                {$ENDIF DEBUG}
                sName := ExtractFilePath(fsPath) + lSrch.Name;
                if  not fFiles.TryGetValue(lSrch.Name, lFile)  then
                  begin
                    {$IFDEF DEBUG}
                      fLog.LogMessage('File ' + lSrch.Name + ' is new');
                    {$ENDIF DEBUG}
                    gcsConfig.Enter();
                    try
                      lFile.Created := gConfig.ReadDateTime(fsPath, lSrch.Name + '|' + gcsVarCreated, 0);
                      lFile.Updated := gConfig.ReadDateTime(fsPath, lSrch.Name + '|' + gcsVarUpdated, 0);
                      lFile.Size := gConfig.ReadInt64(fsPath, lSrch.Name + '|' + gcsVarSize, -1);
                      lFile.CheckIt := False
                    finally
                      gcsConfig.Leave()
                    end { try-finally };
                    fFiles.Add(lSrch.Name, lFile);
                  end { not TryGetValue(...) }   {$IFDEF DEBUG}
                else
                  fLog.LogMessage('File ' + lSrch.Name + ' is known'){$ENDIF DEBUG};
                if  Terminated  then
                  Break;
                try
                  tmCrt := RecodeMilliSecond(TFile.GetCreationTimeUTC(sName), 0);
                  tmWrt := RecodeMilliSecond(TFile.GetLastWriteTimeUTC(sName), 0);
                  nSize := lSrch.Size;
                except
                  on  EFileNotFoundException  do
                    begin
                      { W międzyczasie plik został usunięty z dysku }
                      tmCrt := EncodeDate(1900, 1, 1);
                      tmWrt := tmCrt;
                      nSize := -1
                    end { on EFileNotFoundException }
                  else
                    raise
                end { try-except };
                bExec := not SameDateTime(tmCrt, lFile.Created)
                         or not SameDateTime(tmWrt, lFile.Updated)
                         or (nSize <> lFile.Size)
                         or (fAction in [faCopyFile, faMoveFile])
                            and not AllFilesExists(fTrgts)
                         or (fAction in [faCopyFileTo, faMoveFileTo])
                            and not AllFilesExists(fTrgts, lSrch.Name);
                lFile.Created := tmCrt;
                lFile.Updated := tmWrt;
                lFile.Size := nSize;
                lFile.CheckIt := False;
                fFiles.Items[lSrch.Name] := lFile;
                if  Terminated  then
                  Break;
                if  bExec  then
                  try
                    fcsSynchro.Enter();
                    {$IFDEF DEBUG}
                      fLog.LogMessage('Nowy lub zmieniony plik: ' + lSrch.Name);
                    {$ENDIF DEBUG}
                    Synchronize(procedure
                                begin
                                  fOnUpd(fsPath, lSrch.Name,
                                         lFile.Created, lFile.Updated, lFile.Size,
                                         fAction, fTrgts, fsCmnd, fsParm, fsFldr)
                                end)
                  finally
                    fcsSynchro.Leave()
                  end { try-finally }
              until  FindNext(lSrch) <> 0
            finally
              FindClose(lSrch)
            end { try-finally };
          {$IFDEF DEBUG}
            fLog.LogExit('Search done');
          {$ENDIF DEBUG}
          if  Terminated  then
            Break;
          { Sprawdzam pliki, które zostały usunięte z dysku }
          tmCrt := EncodeDate(1900, 1, 1);
          tmWrt := tmCrt;
          nSize := -1;
          for  lBuf in fFiles  do
            begin
              if  Terminated  then
                Break;
              lFile := lBuf.Value;
              {$IFDEF DEBUG}
                fLog.LogValue(lBuf.Key, lFile.CheckIt);
              {$ENDIF DEBUG}
              with  lFile  do
                if  CheckIt  then
                  begin
                    { Plik nie został znaleziony przez powyższą pętlę znacza to,
                      że plik został usunięty z dysku. }
                    bExec := not SameDateTime(Created, tmCrt)
                             or not SameDateTime(Updated, tmWrt)
                             or (Size <> nSize);
                    if  bExec  then
                      begin
                        {$IFDEF DEBUG}
                          fLog.LogMessage('File ' + lBuf.Key + ' has been removed', lmWarning);
                        {$ENDIF DEBUG}
                        lFile.Created := tmCrt;
                        lFile.Updated := tmWrt;
                        lFile.Size := nSize;
                        fFiles.Items[lBuf.Key] := lFile;
                        fcsSynchro.Enter();
                        try
                          Synchronize(procedure
                                      begin
                                        fOnUpd(fsPath, lBuf.Key,
                                               lFile.Created, lFile.Updated, lFile.Size,
                                               fAction, fTrgts, fsCmnd, fsFldr, fsParm)
                                      end)
                        finally
                          fcsSynchro.Leave()
                        end { try-finally }
                      end
                  end
            end { for nInd };
          { Czekam na następną analizę plików }
          fnTick := 0
        end { nTick >= fnFreq };
      if  Terminated  then
        Break;
      Sleep(gcnCheckFreq);
      Inc(fnTick, gcnCheckFreq)
    end { while not Terminated };
  {$IFDEF DEBUG}
    fLog.LogExit('TMonitorFile.Execute(%s)', [fsPath])
  {$ENDIF DEBUG}
end { Execute };


procedure  TMonitorFile.ResetTimer();
begin
  fnTick := fnFreq
end { ResetTimer };


class procedure  TMonitorFile.ResumeAll();

var
  nInd:  Integer;

begin  { ResumeAll }
  {$IFDEF DEBUG}
    gLog.LogEnter('TMonitorFile.ResumeAll');
  {$ENDIF DEBUG}
  with  flsThreads.LockList()  do
    try
      for  nInd := Count -1  downto  0  do
        begin
          {$IFDEF DEBUG}
            gLog.LogMessage('ResetTimer for ' + TMonitorFile(Items[nInd]).Path);
          {$ENDIF DEBUG}
          TMonitorFile(Items[nInd]).ResetTimer()
        end { for nInd };
      {$IFDEF DEBUG}
        gLog.LogMessage('Done')
      {$ENDIF DEBUG}
    finally
      flsThreads.UnlockList()
    end { try-finally };
  fevEnabled.SetEvent();
  {$IFDEF DEBUG}
    gLog.LogExit('TMonitorFile.ResumeAll')
  {$ENDIF DEBUG}
end { ResumeAll };


class procedure  TMonitorFile.SuspendAll();
begin
  {$IFDEF DEBUG}
    gLog.LogMessage('TMonitorFile.SuspendAll');
  {$ENDIF DEBUG}
  fevEnabled.ResetEvent()
end { SuspendAll };

{$ENDREGION 'TMonitorFile'}


end.
