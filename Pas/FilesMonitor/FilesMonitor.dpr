﻿program  FilesMonitor;

(**********************************************
 *                                            *
 *                FilesMonitor                *
 *     ——————————————————————————————————     *
 *  Copyright © 2012-2024 Grzegorz Skoczylas  *
 *                                            *
 *         mgr Grzegorz Skoczylas             *
 *             ul. Grażyny 13/15              *
 *             43-300 Bielsko-Biała           *
 *             tel. (0) 503 064 953           *
 *             gskoczylas@gmail.com           *
 *                                            *
 **********************************************)


uses
  madExcept, madLinkDisAsm, madListHardware, madListProcesses, madListModules,
  VCL.Forms,
  u_MainForm in 'u_MainForm.pas' {frmMain},
  u_Monitor in 'u_Monitor.pas';

{$R *.res}


begin
  Application.Initialize;
  Application.MainFormOnTaskbar := False;
  Application.ShowMainForm := False;
  Application.Title := 'Files Monitor';
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
