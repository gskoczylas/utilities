# GSkLauncher

- [Przeznaczenie](#przeznaczenie)
- [Menu programu](#menu-programu)
  * [Prawy przycisk myszy](#prawy-przycisk-myszy)
  * [Lewy przycisk myszy](#lewy-przycisk-myszy)
- [Plik konfiguracyjny](#plik-konfiguracyjny)
  * [`options`](#options)
    + [`language`](#language)
    + [Przykładowy obiekt `options`](#przykladowy-obiekt-options)
  * [`data`](#data)
    + [`program`](#program)
    + [`list`](#list)
	+ [`separator`](#separator)
    + [Przykładowy obiekt `data`](#przykladowy-obiekt-data)
  * [Przykładowy kompletny plik konfiguracyjny](#przykladowy-kompletny-plik-konfiguracyjny)

## Przeznaczenie

Program po uruchomieniu wyświetla ikonę w podajniku Windows. Ta ikona umożliwia
wygodne i szybkie uruchamianie wcześniej zdefiniowanych programów lub skryptów.

## Menu programu

W zależności od tego, którym przyciskiem myszy klikniesz w tę ikonę, program
wyświetli różne menu.

### Prawy przycisk myszy

Jeżeli klikniesz ikonę programu prawym przyciskiem myszy, to program wyświetli
menu programu.

- **Reload** powoduje, że program ponownie wczytuje plik konfiguracyjny.
- **Exit** kończy działanie programu

### Lewy przycisk myszy

Jeżeli klikniesz ikonę programu lewym przyciskiem myszy, to program wyświetli
listę programów lub skryptów do uruchomienia. Najpierw jednak musisz te programy
zdefiniować w pliku konfiguracyjnym.

## Plik konfiguracyjny

Plik konfiguracyjny jest plikiem w formacie JSON. Plik musi mieć taką samą nazwę,
jak nazwa programu, i być w tym samym folderze.

W obecnej wersji programu plik konfiguracyjny trzeba modyfikować ręcznie. Można
do tego celu użyć dowolnego edytora tekstowego, na przykład
[_Notepad++_](https://notepad-plus-plus.org/) lub standardowy _Notatnik_.

W serwisach internetowych można sprawdzić, czy plik konfiguracyjny jest poprawnym
plikiem JSON. Na przykład można w tym celu skopiować plik konfiguracyjny do
do serwisu na stronie internetowej
[JSON Parser](https://jsonformatter.org/json-parser).

Plik konfiguracyjny składa się z dwóch obiektów: `options` i `data`.

### `options`

W tym obiekcie są zdefiniowane opcje programu.

#### `language`

Ta opcja wskazuje język, w którym jest wyświetlane menu programu oraz ewentualne
komunikaty błędów. Jeżeli ta opcja nie jest zdefiniowana, to menu programu będzie
wyświetlane w języku zgodnym z językiem interfejsu użytkownika Windows.

Jako język interfejsu należy wskazać 2-literowy kod odpowiedniego języka. W razie
potrzeby można również wskazać dialekt języka (np. francuski kanadyjski).
Listę kodów możesz znaleźć między innymi na stronie internetowej
[List of locale codes](https://simplelocalize.io/data/locales).

Wsparcie dla wyświetlania menu w różnych językach bazuje na plikach językowych.
Jeżeli dla danego języka nie ma odpowiedniego pliku językowego, to program wyswietli
menu w języku angielskim.

Obecnie dostępny jest plik językowy tylko dla języka polskiego. Jeżeli jesteś
zainteresowany utworzeniem pliku językowego w innym języku, to
[daj mi znać](mailto:gskoczylas+GSkLauncher@gmail.com).

#### Przykładowy obiekt `options`

``` json
"options": {
	"language": "pl",
}
```

### `data`

Obiekt `data` jest tablicą. Każdy element tablicy jest obiektem. Mogą występować
dwa rodzaje obiektów:

1. programy
2. listy
3. separatory

- Obiekt typu `program` wskazuje program lub skrypt do uruchomienia.
- Obiekt typy `list` definiuje podrzędną listę programów.
- Obiekt typu `separator` definiuje linię oddzielającą sąsiadujące pozycje.

#### `program`

Obiekt typu `program` definiuje program lub skrypt do wykonania. Poniższa tabela
opisuje pola obiektu.

| Pole         | Rodzaj      | Opis                                            |
|:-------------|:------------|:------------------------------------------------|
| `name`       | obowiązkowe | Nazwa programu wyświetlana w menu _GSkLauncher_ |
| `type`       | opcjonalne  | Rodzaj pola (domyślnie: `program`)              |
| `path`       | obowiązkowe | Ścieżka do pliku `exe`, `bat`, `cmd` lub `ps1`  |
| `params`     | opcjonalne  | Parametry programu lub skryptu                  |
| `workingDir` | opcjonalne  | W jakim folderze uruchomić program lub skrypt   |
| `run`        | opcjonalne  | `normal`, `maximized` lub `minimized`           |

#### `list`

Obiekty typu `list` pozwalają na utworzenie kolejnego poziomu programów w menu
programu GSkLauncher. Poniższa tabela opisuje pola takiego obiektu.

| Pole   | Rodzaj      | Opis                                   |
|:-------|:-----------:|:---------------------------------------|
| `name` | obowiazkowe | Nazwa wyświetlana w menu _GSkLauncher_ |
| `type` | obowiązkowe | Rodzaj pola: `list`                    |
| `data` | obowiązkowe | Definicja kolejnego poziomu menu       |

#### `separator`

Obiekty typu `separator` pozwalają na rozdzielenie linią sąsiednich pozycji
menu. Obiekty tego typu składają się z jednego pola.

| Pole   | Rodzaj      | Opis                     |
|:-------|:-----------:|:-------------------------|
| `type` | obowiązkowe | Rodzaj pola: `separator` |

#### Przykładowy obiekt `data`

``` json
"data": [
	{
		"name": "VPN",
		"type": "program",
		"path": "c:\\Tools\\VPN\\VPN.cmd",
		"params": "",
		"workingDir": "c:\\Tools\\VPN",
		"run": "normal"
	},
	{
		"type": "separator"
	}
	{
		"name": "Dysk W:",
		"type": "program",
		"path": "cmd.exe",
		"params": "/c net  use w: \\\\SERVER\\Work /persistent:no",
		"workingDir": "",
		"run": "normal"
	}
]
```

### Przykładowy kompletny plik konfiguracyjny

``` json
{
	"options": {
		"language": "pl"
	},
	"data": [
		{
			"name": "VPN",
			"type": "program",
			"path": "c:\\Tools\\VPN\\VPN.cmd",
			"params": "",
			"workingDir": "c:\\Tools\\VPN",
			"run": "normal"
		},
		{
			"name": "Dysk W:",
			"type": "program",
			"path": "cmd.exe",
			"params": "/c net  use w: \\\\SERVER\\Work /persistent:no",
			"workingDir": "",
			"run": "normal"
		},
		{
			"type": "separator"
		},
		{
			"name": "Dodatkowe",
			"type": "list",
			"data": [
				{
					"name": "Tryb znakowy",
					"path": "cmd.exe"
				},
				{
					"name": "PowerShell",
					"path": "PowerShell.exe"
				}
			]
		}
	]
}
```

Składnia JSON nie przewiduje żadnej formalnej składni komentarzy. Jeżeli jednak
chcesz dodać swój komentarz do takiego pliku, zawsze możesz go dodać jako pole
o nazwie innej, niż węzły analizowane przez program. Na przykład możesz dodać
pole o nazwie `comment`. Również prostym sposobem na wykomentowanie jakiegoś
pola jest zmiana jego nazwy, na przykład przed wstawienie na początku nazwy
pola znaku „`_`”.

---
