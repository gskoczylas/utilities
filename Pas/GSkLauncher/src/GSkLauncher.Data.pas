﻿unit  GSkLauncher.Data;


interface


uses
  System.RegularExpressions, System.Generics.Collections,
  JvGnugettext;


type
  TDataType = (dtProgram, dtList, dtSeparator);

  TProgramRun = (prNormalWindow, prMaximized, prMinimized);

  TDataTag = class
  strict private
    var
      fDataType:  TDataType;
  public
    constructor  Create(const   DataType:  TDataType);
    {-}
    property  DataType:  TDataType
              read  fDataType;
  end { TDataTag };

  TProgramTag = class(TDataTag)
  strict private
    var
      fsPath:    string;
      fsParams:  string;
      fsDir:     string;
      fRun:      TProgramRun;
  public
    constructor  Create(const   sPath, sParams, sDir:  string;
                        const   Run:  TProgramRun);
    {-}
    property  Path:  string
              read  fsPath;
    property  Params:  string
              read  fsParams;
    property  WorkingDir:  string
              read  fsDir;
    property  Run:  TProgramRun
              read  fRun;
  end { TProgramTag };


function  StrToDataType(const   sType:  string)
                        : TDataType;

function  StrToProgramRun(const   sRun:  string)
                          : TProgramRun;

implementation


uses
  GSkLauncher.Main;


{$REGION 'Global subroutines'}

function  StrToDataType(const   sType:  string)
                        : TDataType;
begin
  if  sType = 'program'  then
    Result := dtProgram
  else if  sType = 'list'  then
    Result := dtList
  else if  sType = 'separator'  then
    Result := dtSeparator
  else
    raise   EGSkLauncher.CreateFmt(_('Unknown data item type "%s"'),
                                   [sType])
end { StrToDataType };


function  StrToProgramRun(const   sRun:  string)
                          : TProgramRun;
begin
  if  sRun = 'normal'  then
    Result := prNormalWindow
  else if  sRun = 'maximized'  then
    Result := prMaximized
  else if  sRun = 'minimized'  then
    Result := prMinimized
  else
    raise  EGSkLauncher.CreateFmt(_('Unknown program run type "%s"'),
                                  [sRun]);
end{ StrToProgramRun };

{$ENDREGION 'Global subroutines'}


{$REGION 'TDataTag'}

constructor  TDataTag.Create(const   DataType:  TDataType);
begin
  inherited Create();
  {-}
  fDataType := DataType
end { Create };

{$ENDREGION 'TDataTag'}


{$REGION 'TProgramTag'}

constructor  TProgramTag.Create(const   sPath, sParams, sDir:  string;
                                const   Run:  TProgramRun);
begin
  inherited Create(dtProgram);
  {-}
  fsPath := sPath;
  fsParams := sParams;
  fsDir := sDir;
  fRun := Run
end { Create };

{$ENDREGION 'TProgramTag'}


end.
