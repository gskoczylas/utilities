﻿unit  GSkLauncher.Main;


interface


uses
  WinAPI.Windows, WinAPI.Messages,
  System.SysUtils, System.Classes,
  VCL.Forms, VCL.ExtCtrls, VCL.Menus, VCL.Controls, Vcl.AppEvnts,
  JvGnugettext,
  GSkLauncher.Config.Load, GSkLauncher.Data;


type
  TfrmMain = class(TForm)
    tryLauncher:    TTrayIcon;
    mnuLeftClick:   TPopupMenu;
    mnuRightClick:  TPopupMenu;
    mnuReload:      TMenuItem;
    mnuSep1:        TMenuItem;
    mnuExit:        TMenuItem;
    procedure  tryLauncherMouseUp(Sender:  TObject;
                                  Button:  TMouseButton;
                                  Shift:  TShiftState;
                                  X, Y:  Integer);
    procedure  mnuReloadClick(Sender:  TObject);
    procedure  mnuExitClick(Sender:  TObject);
    procedure  FormCreate(Sender:  TObject);
    procedure  FormDestroy(Sender:  TObject);
  strict private
    var
      fConf:  TConfigLoader;
    procedure  LoadConfiguration();
    procedure  ItemAction(const   sName:  string;
                          const   ItemTag:  TDataTag);
    procedure  RunProgram(const   sName:  string;
                          const   Data:  TProgramTag);
  end { TfrmMain };

  EGSkLauncher = class(Exception);


var
  frmMain:  TfrmMain;


implementation


uses
  GSkPasLib.SystemUtils, GSkPasLib.Dialogs, GSkPasLib.TrayIconHelper;


{$R *.dfm}


procedure  TfrmMain.FormCreate(Sender:  TObject);
begin
  TranslateComponent(Self);
  LoadConfiguration()
end { FormCreate };


procedure  TfrmMain.FormDestroy(Sender:  TObject);
begin
  fConf.Free()
end { FormDestroy };


procedure  TfrmMain.ItemAction(const   sName:  string;
                               const   ItemTag:  TDataTag);
begin
  case  ItemTag.DataType  of
    dtProgram:
      RunProgram(sName, ItemTag as TProgramTag);
    dtList:
      Assert(False)
  end { case ItemTag.DataType }
end { ItemAction };


procedure  TfrmMain.LoadConfiguration();
begin
  try
    fConf := TConfigLoader.Create(ItemAction);
    {-}
    fConf.LoadConfiguration(ChangeFileExt(Application.ExeName, '.json'),
                            mnuLeftClick.Items,
                            procedure(sLangCode:  string)
                              begin
                                UseLanguage(sLangCode);
                                RetranslateComponent(Self)
                              end)
  except
    on  eErr : EGSkLauncher  do
      MsgBox(eErr.Message, _('Error'), MB_ICONERROR)
    else
      raise   // process by madExcept
  end { try-except }
end { LoadConfiguration };


procedure  TfrmMain.mnuExitClick(Sender:  TObject);
begin
  Close()
end { mnuExitClick };


procedure  TfrmMain.mnuReloadClick(Sender:  TObject);
begin
  fConf.Free();
  mnuLeftClick.Items.Clear();
  {-}
  LoadConfiguration()
end { mnuReloadClick };


procedure  TfrmMain.RunProgram(const   sName:  string;
                               const   Data:  TProgramTag);
const
  cRun:  array[TProgramRun] of  Integer = (
             SW_SHOWNORMAL,
             SW_SHOWMAXIMIZED,
             SW_SHOWMINIMIZED);

begin  { RunProgram }
  try
    if  ShellExec(Data.Path, Data.Params, '', Data.WorkingDir,
                  cRun[Data.Run], wmNoWait) <> NO_ERROR  then
      RaiseLastOSError();
    tryLauncher.ShowBalloon(_('The programme has been launched'), sName, bfInfo, 3000)
  except
    on  eErr : Exception  do
      MsgBox(_('An attempt to run the "%0:s" programme ended with an error %1:s:')
               + sLineBreak + '%2:s',
             [sName, eErr.ClassName(), eErr.Message],
             _('Error'), MB_ICONERROR)
    else
      raise
  end { try-except }
end { RunProgram };


procedure  TfrmMain.tryLauncherMouseUp(Sender:  TObject;
                                       Button:  TMouseButton;
                                       Shift:   TShiftState;
                                       X, Y:  Integer);
begin
  SetForegroundWindow(Handle);
  case  Button  of
    TMouseButton.mbLeft:
      mnuLeftClick.Popup(X, Y);
    TMouseButton.mbRight:
      mnuRightClick.Popup(X, Y);
    TMouseButton.mbMiddle:
      Exit;  { ignore }
  end { case Button };
  PostMessage(Handle, WM_NULL, 0, 0)   // Automatycznie zamknij menu, gdy użytkownik kliknie poza menu
end { tryLauncherMouseUp };


end.
