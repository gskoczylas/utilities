﻿unit  GSkLauncher.Config.Load;


interface


uses
  System.SysUtils, System.Classes, System.JSON,
  System.RegularExpressions, System.Generics.Collections,
  VCL.Menus,
  JvGnugettext,
  GSkLauncher.Data;


type
  TItemAction = procedure(const   sName:  string;
                          const   ItemTag:  TDataTag)
    of object;

  TDataItem = class(TMenuItem)
  strict private
    function  GetDataTag() : TDataTag;                               inline;
    procedure  SetDataTag(const   Tag:  TDataTag);                   inline;
  public
    property  Tag:  TDataTag
              read  GetDataTag
              write SetDataTag;
  end { TDataItem };

  TConfigLoader = class sealed   { singleton }
  strict private
    var
      fsLanguageCode:  string;
      ffnAction:       TItemAction;
    function  LoadConfig(const   sConfPath:  string)
                         : TJSONObject;
    function  CreateItem(const   mnuParent:  TMenuItem;
                         const   sCaption:  string;
                         const   DataTag:  TDataTag;
                         const   bAction:  Boolean)
                         : TDataItem;
    procedure  ItemClick(Sender:  TObject);
    procedure  ProcessConfigOptions(const   Options:  TJSONObject;
                                    const   UseLanguage:  TProc<string>);
    procedure  ProcessConfigData(const   mnuParent:  TMenuItem;
                                 const   jsonData:  TJSONArray);
    procedure  ProcessConfigItem(const   mnuParent:  TMenuItem;
                                 const   jsonItem:  TJSONValue);
    procedure  ProcessConfigProgram(const   mnuParent:  TMenuItem;
                                    const   jsonItem:  TJSONValue);
    procedure  ProcessConfigList(const   mnuParent:  TMenuItem;
                                 const   jsonItem:  TJSONValue);
    procedure  ProcessConfigSeparator(const   mnuParent:  TMenuItem);
  public
    constructor  Create(const   fnAction:  TItemAction);
    {-}
    procedure  LoadConfiguration(const   sConfPath:  string;
                                 const   mnuRoot:  TMenuItem;
                                 const   UseLanguage:  TProc<string>);
  end { TConfigLoader };


implementation


uses
  GSkLauncher.Main;


{$REGION 'TDataItem'}

function  TDataItem.GetDataTag() : TDataTag;
begin
  Result := TDataTag(inherited Tag)
end { GetDataTag };


procedure  TDataItem.SetDataTag(const   Tag:  TDataTag);
begin
  inherited Tag := NativeInt(Tag)
end { SetDataTag };

{$ENDREGION 'TDataItem'}


{$REGION 'TConfigLoader'}

constructor  TConfigLoader.Create(const   fnAction:  TItemAction);
begin
  inherited Create();
  ffnAction := fnAction
end { Create };


function  TConfigLoader.CreateItem(const   mnuParent:  TMenuItem;
                                   const   sCaption:  string;
                                   const   DataTag:  TDataTag;
                                   const   bAction:  Boolean)
                                   : TDataItem;
begin
  Result := TDataItem.Create(mnuParent);
  Result.Caption := sCaption;
  Result.Tag := DataTag;
  if  bAction  then
    Result.OnClick := ItemClick;
  mnuParent.Add(Result)
end { CreateItem };


procedure  TConfigLoader.ItemClick(Sender:  TObject);

var
  mnuItem:  TMenuItem absolute Sender;

begin
  ffnAction(mnuItem.Caption, TDataTag(mnuItem.Tag))
end { ItemClick };


function  TConfigLoader.LoadConfig(const   sConfPath:  string)
                                   : TJSONObject;
var
  lFile:  TStringList;

begin  { LoadConfig }
  if  FileExists(sConfPath)  then
    begin
      lFile := TStringList.Create();
      try
        lFile.LoadFromFile(sConfPath);
        Result := TJSONObject.ParseJSONValue(lFile.Text, True) as TJSONObject;
        if  (Result = nil)  then
          if  lFile.Count <> 0  then
            raise  EGSkLauncher.Create(_('Incorrect configuration file.') + sLineBreak
                                       + _('Correct it and reload it.'))
      finally
        lFile.Free()
      end { try-finally }
    end { FileExists(sConfPath) }
  else
    Result := nil
end { LoadConfig };


procedure  TConfigLoader.LoadConfiguration(const   sConfPath:  string;
                                           const   mnuRoot:  TMenuItem;
                                           const   UseLanguage:  TProc<string>);
var
  lConf:  TJSONObject;

begin  { LoadConfiguration }
  lConf := LoadConfig(sConfPath);
  if  lConf <> nil  then
    try
      ProcessConfigOptions(lConf.GetValue<TJSONObject>('options', nil),
                           UseLanguage);
      ProcessConfigData(mnuRoot,
                        lConf.GetValue<TJSONArray>('data', nil))
    finally
      lConf.Free()
    end { try-finally }
end { LoadConfiguration };


procedure  TConfigLoader.ProcessConfigData(const   mnuParent:  TMenuItem;
                                           const   jsonData:  TJSONArray);
var
  lVal:  TJSONValue;

begin  { ProcessConfigData }
  if  jsonData <> nil  then
    for  lVal in jsonData  do
      ProcessConfigItem(mnuParent, lVal)
end { ProcessConfigData };


procedure  TConfigLoader.ProcessConfigItem(const   mnuParent:  TMenuItem;
                                           const   jsonItem:  TJSONValue);
var
  lType:  TDataType;

begin  { ProcessConfigItem }
  Assert(jsonItem is TJSONObject);
  lType := StrToDataType(jsonItem.GetValue<string>('type', 'program'));
  case  lType  of
    dtProgram:
      ProcessConfigProgram(mnuParent, jsonItem);
    dtList:
      ProcessConfigList(mnuParent, jsonItem);
    dtSeparator:
      ProcessConfigSeparator(mnuParent)
  end { case lType }
end { ProcessConfigItem };


procedure  TConfigLoader.ProcessConfigList(const   mnuParent:  TMenuItem;
                                           const   jsonItem:  TJSONValue);
var
  lList:  TDataItem;

begin  { ProcessConfigList }
  lList := CreateItem(mnuParent,
                      jsonItem.GetValue<string>('name'),
                      TDataTag.Create(dtList),
                      False);
  {-}
  ProcessConfigData(lList, jsonItem.GetValue<TJSONArray>('data', nil))
end { ProcessConfigList };


procedure  TConfigLoader.ProcessConfigOptions(const   Options:  TJSONObject;
                                              const   UseLanguage:  TProc<string>);
begin
  if  Options <> nil  then
    fsLanguageCode := Options.GetValue<string>('language', '');
  UseLanguage(fsLanguageCode)
end { ProcessConfigOptions };


procedure  TConfigLoader.ProcessConfigProgram(const   mnuParent:  TMenuItem;
                                              const   jsonItem:  TJSONValue);
begin
  CreateItem(mnuParent,
             jsonItem.GetValue<string>('name'),
             TProgramTag.Create(jsonItem.GetValue<string>('path'),
                                jsonItem.GetValue<string>('params', ''),
                                jsonItem.GetValue<string>('workingDir', ''),
                                StrToProgramRun(jsonItem.GetValue<string>('run', 'normal'))),
             True);
end { ProcessConfigProgram };


procedure  TConfigLoader.ProcessConfigSeparator(const   mnuParent:  TMenuItem);
begin
  CreateItem(mnuParent, '-', nil, False)
end { ProcessConfigSeparator };

{$ENDREGION 'TConfigLoader'}


end.
