﻿program  GSkLauncher;


uses
  Vcl.Forms,
  JvGnugettext,
  GSkPasLib.SystemUtils in 'p:\GSkPasLib\GSkPasLib.SystemUtils.pas',
  GSkPasLib.Dialogs in 'p:\GSkPasLib\GSkPasLib.Dialogs.pas',
  GSkPasLib.TrayIconHelper in 'P:\GSkPasLib\GSkPasLib.TrayIconHelper.pas',
  GSkLauncher.Main in 'GSkLauncher.Main.pas' {frmMain},
  GSkLauncher.Config.Load in 'GSkLauncher.Config.Load.pas',
  GSkLauncher.Data in 'GSkLauncher.Data.pas';

{$R *.res}


begin
  Application.Initialize();
  Application.ShowMainForm := False;
  Application.MainFormOnTaskbar := False;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run()
end.
