��    
      l      �       �   @   �      2     L     R     W     u     �     �     �  z  �  D   _     �     �     �  #   �     �       !   *  (   L        
             	                        An attempt to run the "%0:s" programme ended with an error %1:s: Correct it and reload it. Error Exit Incorrect configuration file. Reload configuration The programme has been launched Unknown data item type "%s" Unknown program run type "%s" Project-Id-Version: GSkLauncher
PO-Revision-Date: 2024-06-13 09:26+0200
Last-Translator: Grzegorz Skoczylas <gskoczylas@jantar.pl>
Language-Team: gskoczylas+GSkLauncher@gmail.com
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.4.4
X-Poedit-Flags-xgettext: --add-comments
X-Poedit-SourceCharset: UTF-8
 Próba uruchomienia programu "%0:s" zakończyła się błędem %1:s: Popraw go i załaduj ponownie. Błąd Wyjdź Nieprawidłowy plik konfiguracyjny. Przeładuj konfigurację Program został uruchomiony Nieznany typ elementu danych "%s" Nieznany typ uruchamianego programu "%s" 