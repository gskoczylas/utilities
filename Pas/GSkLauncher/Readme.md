# GSkLauncher

Spis treści:

- [Działanie programu](#działanie-programu)
  - [Prawy przycisk myszy](#prawy-przycisk-myszy)
  - [Lewy przycisk myszy](#lewy-przycisk-myszy)

Prosty program pozwalający szybko i wygodnie uruchomić program lub wyświetlić
zawartość folderu. Po uruchomieniu ikona programu jest wyświetlona w podajniku
Windows.

## Działanie programu

### Prawy przycisk myszy

Po kliknięciu ikony programu prawym przyciskiem myszy zostanie wyświetlone
menu programu:

- Zarządzanie listą ścieżek do programów lub folderów.
  - Dodawanie, usuwanie, modyfikacja
  - Definiowanie dowolnie wielu poziomów folderów
  - Ewentualnie: dodawanie dowolnych plików
    (wtedy będzie uruchamiany program skojarzony z takim plikiem)
- Opcje programu
  - Wybór języka interfejsu
- Zakończenie działania programu.

### Lewy przycisk myszy

Po kliknięciu ikony programu lewym przyciskiem myszy program wyświetla
zdefiniowaną listę programów i folderów.
