﻿program  UpdateCopyright;

(**********************************************
 *                                            *
 *        Update Copyright information        *
 *     ——————————————————————————————————     *
 *  Copyright © 2016-2024 Grzegorz Skoczylas  *
 *                                            *
 *   Zakład Usług Informatycznych PROGRAM     *
 *         mgr Grzegorz Skoczylas             *
 *             ul. Grażyny 13/15              *
 *             43-300 Bielsko-Biała           *
 *             tel. +48 503 064 953           *
 *             gskoczylas@gmail.com           *
 *                                            *
 **********************************************)


uses
  Vcl.Forms,
  Vcl.Themes,
  Vcl.Styles,
  u_FileProcessor in 'u_FileProcessor.pas',
  u_MainForm in 'u_MainForm.pas' {frmMain},
  u_UpdateCopyright in 'u_UpdateCopyright.pas';

{$R *.res}


begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'Update Copyright';
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
