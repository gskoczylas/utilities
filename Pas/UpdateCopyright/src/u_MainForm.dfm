object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 'Update Copyright comments'
  ClientHeight = 415
  ClientWidth = 472
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    472
    415)
  PixelsPerInch = 96
  TextHeight = 13
  object lblRootFolder: TLabel
    Left = 16
    Top = 16
    Width = 60
    Height = 13
    Caption = '&Root Folder:'
    FocusControl = edtRootFolder
  end
  object lblLog: TLabel
    Left = 16
    Top = 88
    Width = 21
    Height = 13
    Caption = 'Log:'
  end
  object edtRootFolder: TJvDirectoryEdit
    Left = 82
    Top = 13
    Width = 375
    Height = 21
    TextHint = 'default: current folder'
    DialogKind = dkWin32
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    Text = ''
  end
  object btnStart: TButton
    Left = 301
    Top = 48
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = '&Start'
    Default = True
    TabOrder = 1
    OnClick = btnStartClick
  end
  object btnExit: TButton
    Left = 382
    Top = 48
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Cancel = True
    Caption = '&Exit'
    TabOrder = 2
    OnClick = btnExitClick
  end
  object edtLog: TRichEdit
    Left = 16
    Top = 104
    Width = 441
    Height = 273
    TabStop = False
    Anchors = [akLeft, akTop, akRight, akBottom]
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    PlainText = True
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 3
    WantReturns = False
    WordWrap = False
    Zoom = 100
  end
  object actProcessing: TActivityIndicator
    Left = 82
    Top = 48
    IndicatorType = aitRotatingSector
  end
  object txtFolder: TStaticText
    Left = 16
    Top = 383
    Width = 441
    Height = 24
    Anchors = [akLeft, akRight, akBottom]
    AutoSize = False
    ShowAccelChar = False
    TabOrder = 5
  end
  object btnSave: TButton
    Left = 382
    Top = 383
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Save'
    TabOrder = 6
    Visible = False
    OnClick = btnSaveClick
  end
  object dlgSaveLog: TFileSaveDialog
    DefaultExtension = '.txt'
    FavoriteLinks = <>
    FileTypes = <
      item
        DisplayName = 'Pliki tekstowe (*.txt)'
        FileMask = '*.txt'
      end
      item
        DisplayName = 'Wszystkie pliki'
        FileMask = '*.*'
      end>
    Options = [fdoOverWritePrompt]
    Left = 272
    Top = 256
  end
end
