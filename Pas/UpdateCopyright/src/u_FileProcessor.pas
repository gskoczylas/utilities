﻿unit  u_FileProcessor;

(**********************************************
 *                                            *
 *        Update Copyright information        *
 *     ——————————————————————————————————     *
 *  Copyright © 2016-2024 Grzegorz Skoczylas  *
 *                                            *
 *   Zakład Usług Informatycznych PROGRAM     *
 *         mgr Grzegorz Skoczylas             *
 *             ul. Grażyny 13/15              *
 *             43-300 Bielsko-Biała           *
 *             tel. +48 503 064 953           *
 *             gskoczylas@gmail.com           *
 *                                            *
 **********************************************)


interface


uses
  Winapi.Windows,
  System.Classes, System.RegularExpressions, System.StrUtils, System.SysUtils;


function  ProcessFile(const   sFileName:  string)
                      : Boolean;


implementation


var
  sCurrentYear:  string;


function  ProcessFile(const   sFileName:  string)
                      : Boolean;
var
  lText:   TStringList;
  nLineNo: Integer;
  sLine:   string;
  lRegEx:  TRegEx;
  lMatch:  TMatch;

begin  { ProcessFile }
  Result := False;
  lText := TStringList.Create();
  try
    lText.LoadFromFile(sFileName);
    lRegEx.Create('^(\s*)\*\s+Copyright\s+©\s+(\d{4})-(\d{4})\s+(.*)\*$');
    for  nLineNo := 0  to  lText.Count - 1  do
      begin
        sLine := lText.Strings[nLineNo];
        lMatch := lRegEx.Match(sLine);
        if  lMatch.Success  then
          if  lMatch.Groups.Item[3].Value <> sCurrentYear  then
            begin
              { Update copyright information }
              sLine := StuffString(sLine,
                                   lMatch.Groups.Item[3].Index,
                                   lMatch.Groups.Item[3].Length,
                                   sCurrentYear);
              lText.Strings[nLineNo] := sLine;
              { Save changes to source file }
              RenameFile(sFileName, sFileName + '~~');
              lText.SaveToFile(sFileName);
              { Copyright updated }
              Result := True;
              Exit
            end { with; if }
      end { for nLineNo }
  finally
    lText.Free()
  end { try-finally }
end { ProcessFile };


initialization
  sCurrentYear := FormatDateTime('yyyy', Date())


end.
