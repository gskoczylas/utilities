﻿# Działanie programu

Oprócz plików zaktualizowanych przez program, dodatkowo należy samodzielnie
wyszukać i zaktualizować niektóre pliki.

* folder: `p:\`
  * pliki: `*.pas; *.inc; *.dpr; *.cs; *.html; *.txt; *.rc; *.sql; *.is?; *.bat; *.cmd; *.ps1; *proj *.java *.kt`
    lub pliki `* | *~* *.ico *.gif *.bmp *.jpg *.png *.pdf *.od? *.exe *.dll *.ocx *.zip *.7z *.pack *.dat *.rsm *.map *.log`
  * zawierające wyrażenie regularne: `Copyright \d{4}-20(15|16|17|18|19|20|21|22|23)|\(c\) \d{4}-20(15|16|17|18|19|20|21)|© \d{4}-20(15|16|17|18|19|20|21|22|23)`

[^]
