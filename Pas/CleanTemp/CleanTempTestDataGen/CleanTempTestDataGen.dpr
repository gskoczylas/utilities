﻿program  CleanTempTestDataGen;

{$APPTYPE CONSOLE}

{$WARN SYMBOL_PLATFORM off}

{$R *.res}

uses
  System.SysUtils, System.Classes;

const
  Folder = '.\';
  Count = 200000;

var
  Fmt:  string;
  Ind:  Integer;
  Cnt:  Integer;

  procedure  WrongParams();

  const        { 123456789o123456789o123456789o123456789o123456789o123456789o123456789o123456789 }
    csMessage = 'You can run this program with one parameter indicating the number of test files' + sLineBreak +
                'to create. You can also run this program without parameters. This results' + sLineBreak +
                'in the creation of %.0n test files.';

  begin  { WrongParams }
    Writeln(Format(csMessage, [Count + 0.0]));
    Writeln;
    if  ParamCount() > 1  then
      begin
        Writeln(Format('The program was launched with %d parameters.', [ParamCount()]));
        Writeln
      end { ParamCount() > 1 };
    Writeln('> ', CmdLine);
    Writeln;
    Write('Press [Enter]');
    Readln;
    Halt(1)
  end { WrongParams };

begin  { CleanTempTestDataGen }
  case  ParamCount()  of
    0:
      Cnt := Count;
    1:
      begin
        if  TryStrToInt(ParamStr(1), Cnt)  then
          begin
            if  Cnt <= 0  then
              WrongParams()
          end { if }
        else
          WrongParams()
      end { 1 }
    else
      WrongParams()
  end { case ParamCount() };
  try
    Fmt := Format('%%%0:d.%0:dd', [Length(IntToStr(Cnt))]);
    for  Ind := 1  to  Cnt  do
      begin
        TFileStream.Create(Folder + Format(Fmt, [Ind]) + '.dat', fmCreate, fmShareExclusive).Free();
        if  Ind mod 1000 = 0  then
          Write(#13, Ind)
      end { for Ind };
    Writeln(#13, Cnt, ' files were created')
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end { try-except };
  Write('Press [Enter]');
  Readln
end.

