﻿program  CleanTemp;

(**********************************************
 *                                            *
 *                 CleanTemp                  *
 *     ——————————————————————————————————     *
 *  Copyright © 2009-2024 Grzegorz Skoczylas  *
 *                                            *
 *         mgr Grzegorz Skoczylas             *
 *             ul. Grażyny 13/15              *
 *             43-300 Bielsko-Biała           *
 *             tel. (0) 503 064 953           *
 *             gskoczylas@gmail.com           *
 *                                            *
 **********************************************)


{$R *.res}

{$WARN SYMBOL_PLATFORM OFF}


(*
  madExcept, madLinkDisAsm, madListHardware, madListProcesses, madListModules,
  Windows, Forms, Classes, IniFiles, SysUtils, Math, ShlObj, ShellAPI, ActiveX, ComObj,
  JclFileUtils, JclSysInfo, JclSysUtils, JvGnugettext,
  GSkPasLib.Log in 'p:\GSkPasLib\GSkPasLib.Log.pas',
  GSkPasLib.LogFile in 'p:\GSkPasLib\GSkPasLib.LogFile.pas',
  GSkPasLib.Dialogs in 'p:\GSkPasLib\GSkPasLib.Dialogs.pas',
  GSkPasLib.CmdLineOptionParser in 'p:\GSkPasLib\GSkPasLib.CmdLineOptionParser.pas',
  GSkPasLib.FileUtils in 'p:\GSkPasLib\GSkPasLib.FileUtils.pas',
  GSkPasLib.SystemUtils in 'p:\GSkPasLib\GSkPasLib.SystemUtils.pas',
  GSkPasLib.StrUtils in 'p:\GSkPasLib\GSkPasLib.StrUtils.pas',
  GSkPasLib.TimeUtils in 'p:\GSkPasLib\GSkPasLib.TimeUtils.pas',
  GSkPasLib.MadExcept in 'p:\GSkPasLib\GSkPasLib.MadExcept.pas';
*)
uses
  madExcept, madLinkDisAsm, madListHardware, madListProcesses, madListModules,
  WinAPI.Windows, WinAPI.ShlObj, WinAPI.ShellAPI, WinAPI.ActiveX,
  System.Classes, System.IniFiles, System.SysUtils, System.Math, System.Win.ComObj,
  VCL.Forms,
  JclFileUtils, JclSysInfo, JclSysUtils, JvGnugettext,
  GSkPasLib.Log in 'p:\GSkPasLib\GSkPasLib.Log.pas',
  GSkPasLib.LogFile in 'p:\GSkPasLib\GSkPasLib.LogFile.pas',
  GSkPasLib.Dialogs in 'p:\GSkPasLib\GSkPasLib.Dialogs.pas',
  GSkPasLib.CmdLineOptionParser in 'p:\GSkPasLib\GSkPasLib.CmdLineOptionParser.pas',
  GSkPasLib.FileUtils in 'p:\GSkPasLib\GSkPasLib.FileUtils.pas',
  GSkPasLib.SystemUtils in 'p:\GSkPasLib\GSkPasLib.SystemUtils.pas',
  GSkPasLib.StrUtils in 'p:\GSkPasLib\GSkPasLib.StrUtils.pas',
  GSkPasLib.TimeUtils in 'p:\GSkPasLib\GSkPasLib.TimeUtils.pas',
  GSkPasLib.MadExcept in 'p:\GSkPasLib\GSkPasLib.MadExcept.pas';


const
  { Domyślnie usuwane są pliki starsze niż 3 dni }
  gcnStdMinFileAge = 3;

  { Program jest jednowątkowy, ale Windows realizuje usuwanie plików w oddzielnych
    wątkach (każdy plik w innym wątku).
    Windows XP zarządzał tą pulą wątków — nie było problemu.
    W Windows Vista może zostać utworzona bardzo duża liczba wątków. W efekcie
    system jest zajęty głównie zarządzaniem wątkami. Zanim znalazłem bardziej
    efektywny sposób usuwania plików, można było temu zaradzić sprawdzając liczbę
    wątków i wstrzymując działania dopóki liczba wątków nie spadnie poniżej
    limitu. Z eksperymentów na moim komputerze (Windows Vista Business 32 bit,
    3 GB RAM, Dual Core) w miarę optymalnym kompromisem między wydajnością
    i liczbą wątków były 32 wątki.
    Na szczęście dla Windows Vista i późniejszych wersji systemu znalazłem bardziej
    optymalny sposób — użycie interfesju IFileOperation.
    W starszych wersjach Windows maksymalną liczbę wątków można zmienić poprzez
    parametr „--threads” (lub „-t”). }
  gcnMaxThreadCount = 32;


type
  TVerboseLevel = TGSkLogStdMessageType;

  TVerboseOption = class(TGSkStringOption)
  strict private
    fVerboseLevel:  TVerboseLevel;
  strict protected
    function  CheckValue(const   sValue:  string)
                         : Boolean;                                  override;
  public
    class function  VerboseLevelToStr(const   Level:  TVerboseLevel)
                                      : string;
    {-}
    property  VerboseLevel:  TVerboseLevel
              read  fVerboseLevel;
  end { TVerboseOption };

  TOptions = class(TGSkOptionParser)
  public
    HelpOption:     TGSkBoolOption;
    LogOption:      TGSkStringOption;
    FileAgeOption:  TGSkIntegerOption;
    PathsOption:    TGSkPathListOption;
    ForceOption:    TGSkBoolOption;
    SilentOption:   TGSkBoolOption;
    VerboseOption:  TVerboseOption;
    RecurseOption:  TGSkBoolOption;
  public
    constructor  Create();                                           override;
  end { TOptions };

  EExecuteAndContinue = class(EAbort)
  public
    constructor  Create();
  end { EExecuteAndContinue };


var
  gOptions:  TOptions;
  gPathList:  TStringList;
  gnMinAge:  Integer;
  gbForce:   Boolean;
  gbRecursively:  Boolean;
  gLog:      TGSkLog;
  gLogFile:  TGSkLogFile;
  gnCntParsedDirs:    Integer;
  gnCntFilesDeleted:  Integer;
  gnCntDirsDeleted:   Integer;
  gnCntFilesNotDeleted:  Integer;
  gnCntDirsNotDeleted:   Integer;
 {$IF Defined(CPU32BITS)}
  gnMemSizeLimit:  SIZE_T = 1024 * 1024 * 700;   // 700 MiB
 {$ELSE}
  gnMemSizeLimit:  SIZE_T = High(SIZE_T);        // bez limitu
 {$ENDIF}
  gWindowsVersion:  TWindowsVersion;


const
  gVerboseLevelString: array[TVerboseLevel] of string = (
                         'Information',  // vlInformation,
                         'Warnings',     // vlWarnings,
                         'Errors');      // vlErrors


{$REGION 'Global subroutinies'}

procedure  ProcessCommandLine();

var
  sName:  string;
  nInd:  Integer;

begin  { ProcessCommandLine }
  gOptions.ParseOptions();
  if  gOptions.HelpOption.TurnedOn  then
    begin
      MsgDlg(_('%0:s version %1:s')
                 + #13#13 + _('Options:') + #13
                 + gOptions.GetExplanations()
                 + #13 + _('Examples:')
                 + #13'  > %0:s --%2:s 3'
                 + #13'  > %0:s --%3:s --%4:s=%5:s',
             [{0} Application.ExeName,
              {1} GetModuleVersion(),
              {2} gOptions.FileAgeOption.LongForm,
              {3} gOptions.ForceOption.LongForm,
              {4} gOptions.VerboseOption.LongForm,
              {5} TVerboseOption.VerboseLevelToStr(lmWarning)],
             mtInformation);
      Exit
    end { lOptions.HelpOption.TurnedOn };
  gLogFile := TGSkLogFile.Create(nil);
  gLog := TGSkLog.Create(nil);
  gLog.AddLogAdapter(gLogFile);
  gLog.Active := False;
  if  gOptions.VerboseOption.WasSpecified  then
    gLog.LogFilter := gOptions.VerboseOption.VerboseLevel;
  if  gOptions.LogOption.WasSpecified  then
    begin
      sName := gOptions.LogOption.Value;
      if  sName = ''  then
        sName := GetAppdataFolder() + '\Grzegorz Skoczylas\CleanTemp\CleanTemp.log';
      if  ExtractFileDir(sName) = ''  then
        sName := GetCurrentDir() + PathDelim + sName
      else
        ForceDirectories(ExtractFileDir(sName));
      with  gLogFile  do
        begin
          FileName := sName;
          Flags := Flags + [lfKeepFileOpen]   // performance optymization
        end { with gLogFile };
      with  gLog  do
        begin
          Active := True;
//             LogLines(1);
//             LogMessage('*** Start ***')
        end { with gLog }
    end { lOptions.LogOption.WasSpecified };
  if  gOptions.LeftList.Count <> 0  then
    begin
      if  gLog.Active  then
        begin
          gLog.BeginLog();
          try
            gLog.LogMessage(_('Unknown command line parameters:'),
                            lmError);
            gLog.BeginLevel();
            try
              for  nInd := 0  to  gOptions.LeftList.Count - 1  do
                gLog.LogMessage('-> ' + gOptions.LeftList.Strings[nInd],
                                lmWarning)
            finally
              gLog.EndLevel()
            end { try-finally };
            gLog.LogMessage(_('Program aborted.'), lmWarning)
          finally
            gLog.EndLog()
          end { try-finally }
        end { gLog.Active }
      else
        begin
          sName := _('Unknown command line parameters:') + sLineBreak;
          for  nInd := 0  to  gOptions.LeftList.Count - 1  do
            sName := sName + '-> ' + gOptions.LeftList.Strings[nInd] + sLineBreak;
          sName := sName + sLineBreak + _('Program aborted.');
          MsgBox(sName, MB_ICONERROR or MB_OK)
        end { not gLog.Active };
      gPathList.Clear();
      Exit
    end { lOptions.LeftList.Count <> 0 };
  {-}
  gnMinAge := gOptions.FileAgeOption.Value;
  if  gnMinAge <= 0  then
    begin
      gLog.LogMessage(_('%d is not correct value of the option %s'),
                      [gnMinAge, gOptions.FileAgeOption.LongForm],
                      lmError);
      gnMinAge := gcnStdMinFileAge;
    end { gnMinAge <= 0 };
  gPathList.AddStrings(gOptions.PathsOption.Values);
  if  gPathList.Count = 0  then
    begin
      gPathList.Add(GetEnvironmentVariable('Temp'));
      gPathList.Add(GetSysEnvironmentVariable('Temp'));
      { Java }
      sName := GetLocalAppData();
      if  sName <> ''  then
        if  DirectoryExists(sName)  then
          begin
            sName := IncludeTrailingPathDelimiter(sName) + 'Sun\Java\Deployment\';
            if  DirectoryExists(sName + 'cache')  then
              gPathList.Add(sName + 'cache');
            if  DirectoryExists(sName + 'SystemCache')  then
              gPathList.Add(sName + 'SystemCache')
          end { if }
    end { gDirList.Count = 0 }
  else
    for  nInd := gPathList.Count - 1  downto  0  do
      begin
        sName := gPathList.Strings[nInd];
        if  (Pos('*', sName) = 0)
            and (Pos('?', sName) = 0)  then
          if  not DirectoryExists(sName)  then
            begin
              gLog.LogMessage(_('Directory ''%s'' does not exist'),
                              [sName],
                              lmError);
              gPathList.Delete(nInd)
            end { not DirectoryExists(...) }
      end { for nInd };
  gbForce := gOptions.ForceOption.TurnedOn;
  gbRecursively := gOptions.RecurseOption.TurnedOn;
  {-}
  gLog.BeginLog();
  try
    gLog.LogMessage(_('* Program will remove all files older than %d %s.'),
                    [gnMinAge, dngettext('variety', 'day', 'days', gnMinAge)]);
    gLog.LogMessage(_('* The program will remove files from the folders:'));
    gLog.BeginLevel();
    try
      for  nInd := 0  to  gPathList.Count - 1  do
        gLog.LogMessage('- ' + gPathList.Strings[nInd])
    finally
      gLog.EndLevel()
    end { try-finally };
    if  gbForce  then
      gLog.LogMessage(_('* Program will remove also files with the read-only or system or hidden attribute.'))
  finally
    gLog.EndLog()
  end { try-finally; if gLog <> nil }
end { ProcessCommandLine };


procedure  CleanPaths();

var
  nStart:  Int64;

  function  DoCleanPaths()
                         : Boolean;
  var
    iFileOp:  IFileOperation;
    nInd:     Integer;
    sPath:    string;

    function  IsFailed(const   hStatus:  HRESULT;
                       const   sAction:  string;
                       const   bAborted:  Boolean = True)
                       : Boolean;                                    overload;

      procedure  LogHResult(const   sAction:  string;
                            const   Result:  HRESULT;
                            const   bAborted:  Boolean);
      begin
        Assert(Failed(Result));
        gLog.BeginLog();
        try
          gLog.LogMessage(_('Action ''%s'' failed'), [sAction], lmError);
          gLog.BeginLevel();
          try
            gLog.LogValue('Result Code', ResultCode(Result), lmError);
            gLog.LogValue('Facility', ResultFacility(Result), lmError);
            gLog.LogValue('Severity', ResultSeverity(Result), lmError);
          finally
            gLog.EndLevel()
          end { try-except };
          if  bAborted  then
            gLog.LogMessage('Operation aborted', lmWarning)
        finally
          gLog.EndLog()
        end { try-finally }
      end { LogHResult };

    begin  { IsFailed }
      Result := Failed(hStatus);
      if  Result  then
        LogHResult(sAction, hStatus, bAborted)
    end { IsFailed };

    function  IsFailed(const   hStatus:  HRESULT;
                       const   sActionFmt:  string;
                       const   ActionArgs:  array of const;
                       const   bAborted:  Boolean = True)
                       : Boolean;                                    overload;
    begin
      Result := IsFailed(hStatus, Format(sActionFmt, ActionArgs), bAborted)
    end { IsFailed };

    function  IsSuccedeed(const   hStatus:  HRESULT;
                          const   sAction:  string)
                          : Boolean;
    begin
      Result := not IsFailed(hStatus, sAction, False)
    end { IsSuccedeed };

    function  CleanDir(const   sBaseDir, sName:  string;
                       const   iFileOp:  IFileOperation)
                       : Boolean;
    var
      lFind:   TSearchRec;
      lFileTm: TDateTimeInfoRec;
      tmDel:   TDateTime;
      tmFileW: TDateTime;
      tmFileA: TDateTime;
      sDir:    string;
      sLog:    string;
      sFile:   string;
      iItem:   IShellItem;
      bDel:    Boolean;

    begin  { CleanDir }
      Result := True;   // directory can be removed
      if  (sName = '.')
          or (sName = '..')  then
        Exit;
      Inc(gnCntParsedDirs);
      if  sName <> ''  then
        sDir := IncludeTrailingPathDelimiter(sBaseDir) + sName
      else
        sDir := ExcludeTrailingPathDelimiter(sBaseDir);
      Assert(sDir <> '');
      Assert(sDir[Length(sDir)] <> PathDelim);
      gLog.LogEnter(_('Directory ''%s'''), [sDir]);
      try
        Result := sName <> '';   // can't remove root directory
        tmDel := Date() - gnMinAge;
        { Najpierw usuwam wszystkie podrzędne foldery oraz pliki }
        if  FindFirst(sDir + '\*.*',
                      faAnyFile, lFind) = 0  then
          try
            repeat
              // gLog.LogValue('ThreadCount', GetCurrentThreadCount());
              bDel := False;
              if  lFind.Attr and faDirectory <> 0  then
                begin
                  if  CleanDir(sDir, lFind.Name, iFileOp)  then
                    bDel := True
                end { if Directory }
              else
                begin  { if File }
                  sFile := sDir + PathDelim + lFind.Name;
                  // tmFile := lFind.TimeStamp;   // FileDateToDateTime(lFind.Time);
                  if  not FileGetDateTimeInfo(sFile, lFileTm, False)  then
                    begin
                      gLog.LogMessage(_('File ''%s'' no longer exists'),
                                      [sFile]);
                      Continue
                    end { not FileGetDateTimeInfo(...) };
                  tmFileA := lFileTm.LastAccessTime;
                  tmFileW := lFileTm.TimeStamp;
                  sLog := Format(_('File ''%s'' [%s]'),
                                 [sFile, DateTimeToStr(Max(tmFileW, tmFileA))]);
                  if  (tmFileW > tmDel)
                      and (tmFileA > tmDel)  then
                    begin
                      Inc(gnCntFilesNotDeleted);
                      gLog.LogMessage(sLog + ' ' + _('is too new or has been used recently'))
                    end { Plik jest zbyt nowy lub był ostatnio używany }
                  else
                    begin
                      if  gbForce  then
                        FileSetAttr(sFile, 0);
                      if  iFileOp <> nil  then
                        begin  { Vista and up }
                          if  IsFailed(SHCreateItemFromParsingName(PChar(sFile), nil,
                                                                   IShellItem, iItem),
                                       'SHCreateItemFromParsingName(''%s'')',
                                       [sFile])  then
                            Inc(gnCntFilesNotDeleted)
                          else if  IsFailed(iFileOp.DeleteItem(iItem, nil),
                                            'DeleteItem({%s})', [sFile])  then
                            Inc(gnCntFilesNotDeleted)
                          else
                            begin
                              bDel := True;
                              Inc(gnCntFilesDeleted);
                              gLog.LogMessage(sLog + ' ' + _('has been marked for remove to the Trash'),
                                              lmWarning)
                            end { if };
                          if  GetCurrentProcessMemorySize() >= gnMemSizeLimit  then
                            raise  EExecuteAndContinue.Create()
                        end { Vista and up }
                      else
                        begin  { up to XP }
                          if  FileDelete(sFile, True)  then
                            begin
                              bDel := True;
                              Inc(gnCntFilesDeleted);
                              gLog.LogMessage(sLog + ' ' + _('has been removed to the Trash'),
                                              lmWarning)
                            end { FileDelete(...) }
                          else
                            begin
                              Inc(gnCntFilesNotDeleted);
                              gLog.LogMessage(sLog + ' ' + _('was not removed'),
                                              lmError)
                            end { not FileDelete(...) }
                        end { up to XP }
                    end { file is old enough }
                end { not directory };
              Result := Result and bDel
            until  FindNext(lFind) <> 0
          finally
            FindClose(lFind)
          end { try-finally };
        { Teraz usuwam bieżący folder }
        if  Result  then
          begin
            if  (FindFirst(sDir, faAnyFile, lFind) = 0)
                and ((lFind.Attr and faDirectory) <> 0)  then
              try
                tmFileW := lFind.TimeStamp;   // FileDateToDateTime(lFind.Time);
                sLog := Format(_('Directory ''%s'' [%s] '),
                               [sDir, DateTimeToStr(tmFileW)]);
                if  tmFileW <= tmDel  then
                  begin
                    if  gbForce  then
                      FileSetAttr(sDir, 0);
                    if  iFileOp <> nil  then
                      begin   { Vista and up }
                        if  IsFailed(SHCreateItemFromParsingName(PChar(sDir), nil,
                                                                 IShellItem, iItem),
                                     'SHCreateItemFromParsingName(''%s'')',
                                     [sDir])  then
                          begin
                            Inc(gnCntDirsNotDeleted);
                            Result := False
                          end { IsFailed(...) }
                        else if  IsFailed(iFileOp.DeleteItem(iItem, nil),
                                          'DeleteItem({%s})', [sDir])  then
                          begin
                            Inc(gnCntDirsNotDeleted);
                            Result := False
                          end { IsFailed(DeleteItem) };
                        if  GetCurrentProcessMemorySize() >= gnMemSizeLimit  then
                          raise  EExecuteAndContinue.Create()
                      end { Vista and up }
                    else
                      begin  { up to XP }
                        if  DeleteDirectory(sDir, True)  then
                          begin
                            Inc(gnCntDirsDeleted);
                            gLog.LogMessage(sLog + ' ' + _('has beem removed to the Trash'),  // directory
                                            lmWarning);
                            Assert(Result)
                          end { DeleteDirectory(...) }
                        else
                          begin
                            Inc(gnCntDirsNotDeleted);
                            gLog.LogMessage(sLog + ' ' + _('was NOT removed'),  // directory
                                            lmError);
                            Result := False
                          end { not DeleteDirectory(...) }
                      end { up to XP }
                  end { tmFileW <= tmDel}
                else  { tmFileW > tmDel }
                  begin
                    Inc(gnCntDirsNotDeleted);
                    gLog.LogMessage(sLog + ' ' + _('is too new'));   // directory
                    Result := False
                  end { tmFile > tmDel }
              finally
                FindClose(lFind)
              end { try-finally }
            else
              gLog.LogMessage(_('Directory ''%s'' no longer exists'), [sDir],
                              lmError)
          end { if Result }
        else
          if  sName <> ''  then
            begin
              Inc(gnCntDirsNotDeleted);
              gLog.LogMessage(_('The directory ''%s'' was not deleted because it is not empty'),
                              [sDir]);
              Result := False
            end { sName <> '' }
      finally
        gLog.LogExit(_('Directory ''%s'''), [sDir])
      end { try-finally }
    end { CleanDir };

    procedure  CleanFiles(const   sPath:  string;
                          const   iFileOp:  IFileOperation);
    var
      tmDel:   TDateTime;
      lFind:   TSearchRec;
      tmFileA: TDateTime;
      tmFileW: TDateTime;
      lFileTm: TDateTimeInfoRec;
      sLog:    string;
      sDir:    string;
      sFile:   string;
      iItem:   IShellItem;

    begin  { CleanFiles }
      Assert(sPath <> '');
      gLog.LogEnter(_('Files ''%s'''), [sPath]);
      try
        tmDel := Date() - gnMinAge;
        sDir := ExtractFilePath(sPath);
        if  gbRecursively  then
          if  FindFirst(sDir + '*.*', faDirectory, lFind) = 0  then
            try
              repeat
                if  (lFind.Name = '.')
                    or (lFind.Name = '..')  then
                  Continue;
                if  (lFind.Attr and faDirectory) = 0  then
                  Continue;
                CleanFiles(sDir + lFind.Name + PathDelim + ExtractFileName(sPath),
                           iFileOp)
              until  FindNext(lFind) <> 0
            finally
              FindClose(lFind)
            end { try-finally };
        if  FindFirst(sPath, faAnyFile, lFind) = 0  then
          try
            repeat
              if  lFind.Attr and faDirectory <> 0  then
                Continue;
              sFile := sDir + lFind.Name;
              if  not FileGetDateTimeInfo(sFile, lFileTm, False)  then
                begin
                  gLog.LogMessage(_('File ''%s'' no longer exists'),
                                  [sFile]);
                  Continue
                end { not FileGetDateTimeInfo(...) };
              tmFileA := lFileTm.LastAccessTime;
              tmFileW := lFileTm.TimeStamp;
              sLog := Format(_('File ''%s'' [%s]'),
                             [sFile, DateTimeToStr(Max(tmFileW, tmFileA))]);
              if  (tmFileW > tmDel)
                  and (tmFileA > tmDel)  then
                begin
                  Inc(gnCntFilesNotDeleted);
                  gLog.LogMessage(sLog + ' ' + _('is too new or has been used recently'))
                end { Plik jest zbyt nowy lub był ostatnio używany }
              else
                begin
                  if  gbForce  then
                    FileSetAttr(sFile, 0);
                  if  iFileOp <> nil  then
                    begin  { Vista and up }
                      if  IsFailed(SHCreateItemFromParsingName(PChar(sFile), nil,
                                                               IShellItem, iItem),
                                   'SHCreateItemFromParsingName(''%s'')',
                                   [sFile])  then
                        Inc(gnCntFilesNotDeleted)
                      else if  IsFailed(iFileOp.DeleteItem(iItem, nil),
                                        'DeleteItem({%s})', [sFile])  then
                        Inc(gnCntFilesNotDeleted)
                      else
                        begin
                          Inc(gnCntFilesDeleted);
                          gLog.LogMessage(sLog + ' ' + _('has been marked for remove to the Trash'),
                                          lmWarning)
                        end { if }
                    end { Vista and up }
                  else
                    begin  { up to XP }
                      if  FileDelete(sFile, True)  then
                        begin
                          Inc(gnCntFilesDeleted);
                          gLog.LogMessage(sLog + ' ' + _('has been removed to the Trash'),
                                          lmWarning)
                        end { FileDelete(...) }
                      else
                        begin
                          Inc(gnCntFilesNotDeleted);
                          gLog.LogMessage(sLog + ' ' + _('was not removed'),
                                          lmError)
                        end { not FileDelete(...) }
                    end { up to XP }
                end { file is old enough }
            until  FindNext(lFind) <> 0
          finally
            FindClose(lFind)
          end { try-finally }
      finally
        gLog.LogExit(_('Files ''%s'''), [sPath])
      end { try-finally }
    end { CleanFiles };

  begin  { DoCleanPaths }
    Result := True;   // do not try to repeat cleaning
    if  gWindowsVersion >= wvWinVista  then
      if  IsFailed(CoInitializeEx(nil,
                                  COINIT_APARTMENTTHREADED or COINIT_DISABLE_OLE1DDE),
                   'CoInitializeEx')  then
        Exit;
    try
      iFileOp := nil;
      if  gWindowsVersion >= wvWinVista  then
        begin
          if  IsFailed(CoCreateInstance(CLSID_FileOperation, nil, CLSCTX_ALL,
                                        IFileOperation, iFileOp),
                       'CoCreateInstance')  then
            Exit;
          if  IsFailed(iFileOp.SetOperationFlags(FOF_NORECURSION or FOF_NO_UI),
                       'SetOperationFlags')  then
            Exit;
        end { gWindowsVersion >= wvWinVista };
      try
        for  nInd := 0  to  gPathList.Count - 1  do
          begin
            sPath := gPathList.Strings[nInd];
            if  (Pos('*', sPath) = 0)
                and (Pos('?', sPath) = 0)  then
              CleanDir(ExcludeTrailingPathDelimiter(gPathList.Strings[nInd]), '', iFileOp)
            else
              CleanFiles(sPath, iFileOp)
          end { for nInd }
      except
        on  EExecuteAndContinue  do
          Result := False   // repeat cleaning to remove the rest of files or folders
        else
          raise
      end { try-except };
      if  (gWindowsVersion >= wvWinVista)
          and ((gnCntFilesDeleted > 0)
               or (gnCntDirsDeleted > 0))  then
        begin
          if  IsSuccedeed(iFileOp.PerformOperations(), 'PerformOperations')  then
            gLog.LogMessage(_('All operations has been successfully performed'));
          Result := True   // do not repeat cleaning
        end { if }
    finally
      if  gWindowsVersion >= wvWinVista  then
        CoUninitialize()
    end { try-except }
  end { DoCleanPaths };

  procedure  ShowStatistics({const} nTick:  UInt64);

  var
    lInfo:  TStringList;

  begin  { ShowStatistics }
    lInfo := TStringList.Create();
    try
      lInfo.Add(_('Checked:'));
      lInfo.Add('  '
                + Format(_('- directories: %d'),
                         [gnCntDirsDeleted + gnCntDirsNotDeleted]));
      lInfo.Add('  '
                + Format(_('- files: %d'),
                         [gnCntFilesDeleted + gnCntFilesNotDeleted]));
      lInfo.Add(_('Removed:'));
      lInfo.Add('  '
                + Format(_('- directories: %d'), [gnCntDirsDeleted]));
      lInfo.Add('  '
                + Format(_('- files: %d'), [gnCntFilesDeleted]));
      lInfo.Add(_('NOT removed:'));
      lInfo.Add('  '
                + Format(_('- directories: %d'), [gnCntDirsNotDeleted]));
      lInfo.Add('  '
                + Format(_('- files: %d'), [gnCntFilesNotDeleted]));

      nTick := GetTickCount64() - nTick;
      if  nTick > 0  then   // zegar nie przekręcił się, trwało to chociaż 1 ms
        begin
          lInfo.Add('');
          lInfo.Add(Format(_('Processing time: %s'), [TicksToStr(nTick)]))
        end { nTick > 0 };
      gLog.LogValue(_('Statistics'), lInfo);
      if  not gOptions.SilentOption.TurnedOn  then
        MsgBox(lInfo.Text, 'CleanTemp', MB_OK or MB_ICONINFORMATION)
    finally
      lInfo.Free()
    end { try-except }
  end { ShowStatistics };

begin  { CleanPaths }
  nStart := GetTickCount64();
  repeat
  until  DoCleanPaths();
  ShowStatistics(nStart)
end { CleanPaths };

{$ENDREGION 'Global subroutinies'}


{$REGION 'TVerboseOption'}

function  TVerboseOption.CheckValue(const   sValue:  string)
                                    : Boolean;
begin
  if  sValue = ''  then
    begin
      fVerboseLevel := lmInformation;
      Result := True
    end { sValue = '' }
  else
    begin
      Result := False;
      fVerboseLevel := High(TVerboseLevel);
      while  fVerboseLevel > Low(TVerboseLevel)  do
        begin
          Result := SameText(sValue, gVerboseLevelString[fVerboseLevel]);
          if  Result  then
            Break;
          fVerboseLevel := Pred(fVerboseLevel)
        end { while }
    end { sValue <> '' };
  if  Result  then
    Result := inherited CheckValue(sValue)
end { CheckValue };


class function  TVerboseOption.VerboseLevelToStr(const   Level:  TVerboseLevel)
                                                 : string;
begin
  Result := gVerboseLevelString[Level]
end { VerboseLevelToStr };

{$ENDREGION 'TVerboseOption'}


{$REGION 'TOptions'}

constructor  TOptions.Create();
begin
  inherited;
  { Table of options
    ~~~~~~~~~~~~~~~~
    Short  Long    Description
    —————  ————    ———————————
      ?    help    Display program usage
      L    log     Path to the LOG file
      A    FileAge Minimal age of file (in days) to remove
      P    paths   List of directories or files to clean
      V    Verbose Verbose level: "Errors, Warnings or All"
      F    Force   Force remove read-only, system and hidden files
      S    Silent  Do not show statistics
      R    recurse Delete FILES recursively }
  {-}
  HelpOption := TGSkBoolOption.Create('?', 'Help');
  HelpOption.Explanation := _('Display program usage');
  AddOption(HelpOption);
  {-}
  LogOption := TGSkStringOption.Create('L', 'Log');
  LogOption.Explanation := _('Save log to the specified file (default extenssion: ''.log'')');
  AddOption(LogOption);
  {-}
  VerboseOption := TVerboseOption.Create('V', 'Verbose');
  VerboseOption.Explanation := _('Verbose level: ''Errors'', ''Warnings'' or ''Information'' (default: Information)');
  AddOption(VerboseOption);
  {-}
  FileAgeOption := TGSkIntegerOption.Create('A', 'FileAge');
  FileAgeOption.Explanation := Format(_('Minimal age of file (in days) to remove (default: %d)'),
                                      [gcnStdMinFileAge]);
  FileAgeOption.Value := gcnStdMinFileAge;
  AddOption(FileAgeOption);
  {-}
  PathsOption := TGSkPathListOption.Create('P', 'Paths');
  PathsOption.Explanation := _('List of directories or files to clean (default: all files from user and system temp folders)');
  AddOption(PathsOption);
  {-}
  ForceOption := TGSkBoolOption.Create('F', 'Force');
  ForceOption.Explanation := _('Force remove files with read-only, system or hidden attribute');
  AddOption(ForceOption);
  {-}
  SilentOption := TGSkBoolOption.Create('S', 'Silent');
  SilentOption.Explanation := _('Do not show statistics');
  AddOption(SilentOption);
  {-}
  RecurseOption := TGSkBoolOption.Create('R', 'Recurse');
  RecurseOption.Explanation := _('Delete FILES recursively');
  AddOption(RecurseOption)
end { Create };

{$ENDREGION 'TOptions'}


{$REGION 'EExecuteAndContinue'}

constructor  EExecuteAndContinue.Create();
begin
  inherited Create('')
end { Create };

{$ENDREGION 'EExecuteAndContinue'}


begin  { CleanTemp }
  Application.Initialize();
  Application.Title := 'Clean Temp';
  {-}
  {$IFDEF CPU64BITS}
    gnMemSizeLimit := GetAvailablePhysicalMemory() div 3 * 2;
  {$ENDIF CPI64BITS}
  {-}
  gWindowsVersion := GetWindowsVersion();
  if  gWindowsVersion = wvUnknown  then
    if  Win32MajorVersion >= 10  then
      gWindowsVersion := wvWin10;
  {-}
  gPathList := TStringList.Create();
  try
    gOptions := TOptions.Create();
    try
      ProcessCommandLine();
      if  gPathList.Count > 0  then
        begin
          SetPriority(cpBelowNormal, tpLower);
          CleanPaths()
        end { gDirList.Count > 0 }
    finally
      gOptions.Free()
    end { try-finally }
  finally
    gPathList.Free()
  end { try-finally };
  if  gLog <> nil  then
    begin
      gLog.Free();
      gLogFile.Free()
    end { gLog <> nil }
end.
