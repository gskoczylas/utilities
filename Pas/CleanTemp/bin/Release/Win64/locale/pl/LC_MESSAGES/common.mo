��          �      <      �     �     �     �     �  	   �     �       	   $     .     3  F   6  3   }  &   �     �     �          -  y  5     �     �     �  '   �     �     �       
   5     @     E  A   H  2   �  +   �  #   �  !        /     N                                                 	                                       
             %s does not exist: %s &No &Yes Action cancelled by the user Attention Driver is not connected Driver is not opened Error %s: None OK Replace either the dot or the comma with the current decimal seperator Section %s: value of the variable %s is not correct Section %s: variable %s is not defined Serial communication break Serial communication error: %s Value in words (in Polish) Warning Project-Id-Version: OPOS and CPOS common code
POT-Creation-Date: 2012-07-13 12:33
PO-Revision-Date: 2018-05-27 01:26+0200
Last-Translator: Grzegorz Skoczylas <gskoczylas+poedit@gmail.com>
Language-Team: Grzegorz Skoczylas <gskoczylas@jantar.pl>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.7
Language: pl_PL
 %s nie istnieje: %s &Nie &Tak Działanie anulowane przez użytkownika Uwaga Sterownik nie jest połączony Sterownik nie jest otwarty Błąd %s: Brak OK Zamienia kropkę lub przecinek na systemowy separator dziesiętny Sekcja %s: wartość zmiennej %s nie jest poprawna Sekcja %s: zmienna %s nie jest zdefiniowana Przerwanie połączenia szeregowego Błąd komunikacji szeregowej: %s Wartość słownie (po polsku) Ostrzeżenie 