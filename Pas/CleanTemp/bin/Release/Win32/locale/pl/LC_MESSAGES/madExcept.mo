��            )   �      �  	   �  !   �  (   �     �     �  %   �          1     ?     K     c     |     �     �     �     �  *   �  #     )   7     a     s     �  6   �     �     �               "  �  2  	   �  '   �  (   �                     ?     X  
   f     q     �     �     �  !   �     	       A   2  #   t  2   �     �     �  #   �  D   	     ]	     m	     ~	     �	     �	        
                                                                     	                                                             %appname% %appname% %appversion% bug report %appname% ver. %appversion%, %exceptMsg% &Details &OK An error occurred in the application. Connecting to server... Finalizing... Information Please wait a moment... Preparing attachments... Searching for mail server... Sending attachments... Sending bug report... Sending mail... Setting fields... Sorry, sending the bug report didn't work. The application seems to be frozen. The file "%modname%" seems to be corrupt! close application continue application error details:
%errorDetails% please find the bug report attached - module %modname% print bug report restart Application save bug report send bug report show bug report Project-Id-Version: GSkPasLib
POT-Creation-Date: 2013-09-13 12:07
PO-Revision-Date: 2013-04-04 01:43+0100
Last-Translator: Grzegorz Skoczylas <gskoczylas@gmail.com>
Language-Team: Grzegorz Skoczylas <gskoczylas@gmail.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.5
Language: Polish
X-Poedit-SourceCharset: UTF-8
 %appname% Raport błędu w %appname% %appversion% %appname% wer. %appversion%, %exceptMsg% &Szczegóły &OK Wystąpił błąd w programie. Łączenie z serwerem... Kończenie... Informacja Proszę chwilę poczekać… Proszę chwilę poczekać… Szukanie serwera pocztowego... Wysyłanie załączników... Wysyłanie raportu o błędzie... Wysyłanie listu... Definiowanie pól... Przepraszam, ale nie udało się wysłać informacji o błędzie. Aplikacja zdaje się nie działać. Plik „%modname%” wydaje się być uszkodzonym! Zakończ działanie Kontynuuj działanie Szczegóły błędu:
%errorDetails% Raport błędu (moduł: %modname%) został dołączony do tego listu Wydrukuj raport Uruchom ponownie Zapisz raport Wyślij raport Pokaż informacje 