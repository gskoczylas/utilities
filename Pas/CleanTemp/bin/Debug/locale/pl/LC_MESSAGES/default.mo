��    *      l  ;   �      �     �  (   �  1   �  R     1   i     �     �     �  .   �     �               ,     A     _          �  	   �     �     �  
   �  =   �  \   )  5   �     �     �     �     �     �  ;      
   <  :   G      �  K   �     �  '        5  
   S  $   ^     �     �  [  �     �	  *   
  4   ;
  N   p
  -   �
     �
     �
  (     1   0     b     n     �     �     �  !   �     �          $     0     C     c  L   r  �   �  B   @     �     �     �     �  
   �  H   �  
     >   *  $   i  b   �     �  )        6     Q  )   `     �     �                (         %       *       &                                                   $                !       '                 
   #                "                                	   )              %0:s version %1:s %d is not correct value of the option %s * Program will remove all files older than %d %s. * Program will remove also files with the read-only or system or hidden attribute. * The program will remove files from the folders: - directories: %d - files: %d Action '%s' failed All operations has been successfully performed Checked: Delete FILES recursively Directory '%s' Directory '%s' [%s]  Directory '%s' does not exist Directory '%s' no longer exists Display program usage Do not show statistics Examples: File '%s' [%s] File '%s' no longer exists Files '%s' Force remove files with read-only, system or hidden attribute List of directories or files to clean (default: all files from user and system temp folders) Minimal age of file (in days) to remove (default: %d) NOT removed: Options: Processing time: %s Program aborted. Removed: Save log to the specified file (default extenssion: '.log') Statistics The directory '%s' was not deleted because it is not empty Unknown command line parameters: Verbose level: 'Errors', 'Warnings' or 'Information' (default: Information) has beem removed to the Trash has been marked for remove to the Trash has been removed to the Trash is too new is too new or has been used recently was NOT removed was not removed Project-Id-Version: CleanTemp 1.5
POT-Creation-Date: 2015-09-16 14:28
PO-Revision-Date: 2017-04-29 01:35+0200
Last-Translator: Grzegorz Skoczylas <gskoczylas+poedit@gmail.com>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.1
Plural-Forms: variety;
Language: pl_PL
 %0:s wersja %1:s %d nie jest poprawną wartością opcji %s * Program usunie wszystkie pliki starsze niż %d %s. * Wymuś usunięcie plików z atrybutem tylko do odczytu, systemowy lub ukryty * Program usunie wszystkie pliki z folderów: - foldery: %d - pliki: %d Działanie „%s” zakończone błędem Wszystkie działania zostały pomyślnie wykonane Sprawdzono: Usuwaj PLIKI rekurencyjnie Folder „%s” Folder „%s” [%s] Folder „'%s” nie istnieje Folder „%s” już nie istnieje Wyświetl opcje programu Nie wyświetlaj podsumowania Przykłady: Plik „%s” [%s] Plik „%s” już nie istnieje Pliki „%s” Wymuś usunięcie plików z atrybutem tylko do odczytu, systemowy lub ukryty Lista folderów lub plików do czyszczenia (domyślnie: wszystkie pliki z folderów plików tymczasowych użytkownika i systemu) Usuwaj tylko pliki starsze niż „FileAge” dni (domyślnie: %d) NIE usunięte: Opcje: Czas działania: %s Program przerwał działanie. Usunięte: Zapisz dziennik do wskazanego pliku (domyślne rozszerzenie: „.log”) Statystyka Folder „%s” nie został usunięty ponieważ nie jest pusty Nieznane parametry wiersza poleceń: Poziom monitorowania: „Errors”, „Warnings” lub „Information” (domyślnie: Information) został usunięty do kosza został wyznaczony do usunięcia do kosza został usunięty do kosza jest zbyt nowy jest zbyt nowy lub był używany ostatnio NIE został usunięty nie został usunięty 