﻿unit  u_BaseForm;

(**********************************************
 *                                            *
 *                Job Register                *
 *     ——————————————————————————————————     *
 *  Copyright © 2008-2024 Grzegorz Skoczylas  *
 *                                            *
 *         mgr Grzegorz Skoczylas             *
 *             tel. 503 064 953               *
 *             gskoczylas@gmail.com           *
 *                                            *
 **********************************************)

{------------------------------------------------------------------------------+
| $Date:: 2016-01-23 23:42:43 +0100 (sob., 23 sty 2016)                       $|
| $Revision:: 2080                                                            $|
| $Author:: Grzegorz                                                          $|
+------------------------------------------------------------------------------}


interface


uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Registry,
  jvGnugettext;


type
  TfBaseForm = class abstract (TForm)
    procedure  FormCreate(Sender:  TObject);
    procedure  FormShow(Sender:  TObject);
  strict private
    function  GetFormKey() : string;
    function  SavePosition() : Boolean;
  strict protected
    procedure  LoadParams(const   Reg:  TRegistry);                  dynamic;
    procedure  SaveParams(const   Reg:  TRegistry);                  dynamic;
  public
    procedure  AfterConstruction();                                  override;
    procedure  BeforeDestruction();                                  override;
    procedure  ResetLanguage();
  end { TfBaseForm };


// var
//   fBaseForm:  TfBaseForm;


implementation


uses
  u_Data;


{$R *.dfm}


const
  gcsParLeft   = 'Left';
  gcsParTop    = 'Top';
  gcsParWidth  = 'Width';
  gcsParHeight = 'Height';


{$REGION 'TfBaseForm'}

procedure  TfBaseForm.AfterConstruction();

var
  lReg:  TRegistry;

begin  { AfterConstruction }
  inherited;
  lReg := TRegistry.Create();
  try
    if  lReg.OpenKey(GetFormKey(), False)  then
      try
        LoadParams(lReg)
      finally
        lReg.CloseKey()
      end { try-finally }
  finally
    lReg.Free()
  end { try-finally }
end { AfterConstruction };


procedure  TfBaseForm.BeforeDestruction();

var
  lReg:  TRegistry;

begin  { BeforeDestruction }
  inherited;
  lReg := TRegistry.Create();
  try
    if  lReg.OpenKey(GetFormKey(), True)  then
      try
        SaveParams(lReg)
      finally
        lReg.CloseKey()
      end { try-finally }
  finally
    lReg.Free()
  end { try-finally }
end { BeforeDestruction };


procedure  TfBaseForm.FormCreate(Sender:  TObject);
begin
  TranslateComponent(Self)
end { FormCreate };


procedure  TfBaseForm.FormShow(Sender:  TObject);
begin
  SetForegroundWindow(Handle);
  BringWindowToTop(Handle)
end { FormShow };


function  TfBaseForm.GetFormKey() : string;
begin
  Result := '\Software\Grzegorz Skoczylas\Job Register\' + Name
end { GetFormKey };


procedure  TfBaseForm.LoadParams(const   Reg:  TRegistry);

var
  bSizeable:  Boolean;

begin  { LoadParams }
  if  SavePosition()  then
    begin
      bSizeable := BorderStyle in [bsSizeable, bsSizeToolWin];
      { load saved position }
      with  Reg  do
        try
          Left := ReadInteger(gcsParLeft);
          Top := ReadInteger(gcsParTop);
          if  bSizeable  then
            begin
              Width := ReadInteger(gcsParWidth);
              Height := ReadInteger(gcsParHeight)
            end { if bSizeable }
        except
          { ignore exceptions }
        end { try-except; with Reg };
      { adjust horizontal  position }
      if  bSizeable  then
        if  Width > Screen.WorkAreaWidth  then
          Width := Screen.WorkAreaWidth;
      if  Left + Width > Screen.WorkAreaRect.Right  then
        Left := Screen.WorkAreaRect.Right - Width;
      if  Left < Screen.WorkAreaRect.Left  then
        Left := Screen.WorkAreaRect.Left;
      { adjust vertical position }
      if  bSizeable  then
        if  Height > Screen.WorkAreaHeight  then
          Height := Screen.WorkAreaHeight;
      if  Top + Height > Screen.WorkAreaRect.Bottom  then
        Top := Screen.WorkAreaRect.Bottom - Height;
      if  Top < Screen.WorkAreaTop  then
        Top := Screen.WorkAreaTop
    end { if }
end { LoadParams };


procedure  TfBaseForm.ResetLanguage();
begin
  UseLanguage(dmData.DefaultLanguageCode);
  RetranslateComponent(Self)
end { ResetLanguage };


procedure  TfBaseForm.SaveParams(const   Reg:  TRegistry);
begin
  if  SavePosition()  then
    with  Reg  do
      begin
        WriteInteger(gcsParLeft, Left);
        WriteInteger(gcsParTop, Top);
        if  BorderStyle in [bsSizeable, bsSizeToolWin]  then
          begin
            WriteInteger(gcsParWidth, Width);
            WriteInteger(gcsParHeight, Height)
          end { sizeable }
      end { with Reg }
end { SaveParams };


function  TfBaseForm.SavePosition() : Boolean;
begin
  Result := Position in [poDesigned, poDefault, poDefaultPosOnly, poDefaultSizeOnly]
end { SavePosition };

{$ENDREGION 'TfBaseForm'}


end.
