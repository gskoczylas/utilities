object dmData: TdmData
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 82
  Width = 189
  object dbDatabase: TIBDatabase
    DatabaseName = 'JR.FDB'
    Params.Strings = (
      'user_name=SYSDBA'
      'password=masterkey')
    LoginPrompt = False
    AllowStreamedConnected = False
    AfterConnect = dbDatabaseAfterConnect
    BeforeConnect = dbDatabaseBeforeConnect
    BeforeDisconnect = dbDatabaseBeforeDisconnect
    Left = 40
    Top = 16
  end
  object tmTimestamp: TTimer
    Enabled = False
    OnTimer = tmTimestampTimer
    Left = 128
    Top = 16
  end
end
