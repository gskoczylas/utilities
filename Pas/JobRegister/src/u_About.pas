﻿unit  u_About;

(**********************************************
 *                                            *
 *                Job Register                *
 *     ——————————————————————————————————     *
 *  Copyright © 2008-2024 Grzegorz Skoczylas  *
 *                                            *
 *         mgr Grzegorz Skoczylas             *
 *             tel. 503 064 953               *
 *             gskoczylas@gmail.com           *
 *                                            *
 **********************************************)

{------------------------------------------------------------------------------+
| $Date:: 2016-01-23 23:42:43 +0100 (sob., 23 sty 2016)                       $|
| $Revision:: 2080                                                            $|
| $Author:: Grzegorz                                                          $|
+------------------------------------------------------------------------------}


interface


uses
  Windows, SysUtils, StdCtrls, Graphics, Controls, ExtCtrls, Classes, Forms,
  ShellAPI, DateUtils,
  JvGnugettext,
  u_BaseForm;


type
  TfAboutBox = class(TfBaseForm)
    pnAbout:        TPanel;
    imProgramIcon:  TImage;
    lbProductName:  TLabel;
    lbVersion:      TLabel;
    lbCopyright:    TLabel;
    lbComments:     TLabel;
    btOK:           TButton;
    procedure  FormCreate(Sender:  TObject);
    procedure  lbCommentsClick(Sender:  TObject);
  public
    class procedure  Execute();
  end { TfAboutBox };


// var
//   fAboutBox:  TfAboutBox;


implementation


uses
    zz_GSkFileUtils;


{$R *.dfm}


{$REGION 'TfAboutBox'}

class procedure  TfAboutBox.Execute();
begin
  if  (Screen.ActiveForm <> nil)
      and (Screen.ActiveForm.ClassType() = TfAboutBox)  then
    Screen.ActiveForm.SetFocus()
  else
    with  TfAboutBox.Create(nil)  do
      try
        ShowModal()
      finally
        Free()
      end { try-finally }
end { Execute };


procedure  TfAboutBox.FormCreate(Sender:  TObject);
begin
  inherited;
  lbVersion.Caption := Format('%s %s',
                              [_('vers.'),
                               GetModuleVersion()]);
  lbCopyright.Caption := Format('© 2008-%d Grzegorz Skoczylas',
                                [YearOf(Date())])
end { FormCreate };


procedure  TfAboutBox.lbCommentsClick(Sender:  TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    if  ShellExecute(0, 'open', 'mailto://gskoczylas@gmail.com', nil, nil, SW_SHOWNORMAL) <= 32  then
      RaiseLastOSError()
  finally
    Screen.Cursor := crDefault
  end { try-finally }
end { lbCommentsClick };

{$ENDREGION 'TfAboutBox'}


end.
