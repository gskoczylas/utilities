﻿unit  u_MainForm;

(**********************************************
 *                                            *
 *                Job Register                *
 *     ——————————————————————————————————     *
 *  Copyright © 2008-2024 Grzegorz Skoczylas  *
 *                                            *
 *         mgr Grzegorz Skoczylas             *
 *             tel. 503 064 953               *
 *             gskoczylas@gmail.com           *
 *                                            *
 **********************************************)

{------------------------------------------------------------------------------+
| $Date:: 2016-01-23 23:42:43 +0100 (sob., 23 sty 2016)                       $|
| $Revision:: 2080                                                            $|
| $Author:: Grzegorz                                                          $|
+------------------------------------------------------------------------------}


interface


uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Registry, u_BaseForm, Dialogs, ExtCtrls, Menus, ImgList, ActnList, ComCtrls,
  ToolWin, StdCtrls, VirtualTrees, DateUtils, StrUtils, StdActns,
  jvGnugettext;


type
  TSortBy = (sbStartTime, sbTask);
  TShowRange = (srToday, srWeek, srMonth, srYear, srAll);

  TfJobRegister = class(TfBaseForm)
    alGeneralActions:            TActionList;
    sbStatus:                    TStatusBar;
    mnMain:                      TMainMenu;
    ilGeneralActions:            TImageList;
    aRegisterPause:              TAction;
    aRegisterResume:             TAction;
    aRegisterTerminate:          TAction;
    aRegisterSelectTask:         TAction;
    aRegisterOptions:            TAction;
    aShowToday:                  TAction;
    aShowWeek:                   TAction;
    aShowMonth:                  TAction;
    aShowYear:                   TAction;
    aShowAll:                    TAction;
    aHelpAbout:                  TAction;
    mnRegister:                  TMenuItem;
    mnFilter:                    TMenuItem;
    mnHelp:                      TMenuItem;
    aRegisterHide:               TAction;
    aRegisterShow:               TAction;
    mnRegisterHide:              TMenuItem;
    mnSep11:                     TMenuItem;
    mnRegisterPause:             TMenuItem;
    mnRegisterResume:            TMenuItem;
    mnRegisterSelectTask:        TMenuItem;
    mnSep12:                     TMenuItem;
    mnRegisterOptions:           TMenuItem;
    mnSep13:                     TMenuItem;
    mnRegisterTerminate:         TMenuItem;
    mnShowToday:                 TMenuItem;
    mnShowWeek:                  TMenuItem;
    mnShowMonth:                 TMenuItem;
    mnShowYear:                  TMenuItem;
    mnShowAll:                   TMenuItem;
    mnHelpAbout:                 TMenuItem;
    edRegister:                  TVirtualStringTree;
    ilAnimation:                 TImageList;
    mnPopUp:                     TPopupMenu;
    pmRegisterShow:              TMenuItem;
    pmRegisterHide:              TMenuItem;
    pmLine1:                     TMenuItem;
    pmRegisterPause:             TMenuItem;
    pmRegisterResume:            TMenuItem;
    pmRegisterSelectTask:        TMenuItem;
    pmLine2:                     TMenuItem;
    pmRegisterOptions:           TMenuItem;
    pmLine3:                     TMenuItem;
    pmRegisterTerminate:         TMenuItem;
    tryIcon:                     TTrayIcon;
    tmrUpdateTime:               TTimer;
    tmrPaused:                   TTimer;
    aSortStartTime:              TAction;
    aSortTaskName:               TAction;
    mnSort:                      TMenuItem;
    mnSortStartTime:             TMenuItem;
    mnSortTaskName:              TMenuItem;
    aRegisterAlertsEnable:       TAction;
    mnRegisterAlertsEnable:      TMenuItem;
    pmRegisterAlertsEnable:      TMenuItem;
    aRegisterAlertsDisable:      TAction;
    pmRegisterAlertsDisable:     TMenuItem;
    pmLine4:                     TMenuItem;
    mnRegisterAlertsDisable:     TMenuItem;
    mnSep14:                     TMenuItem;
    aRegisterCommentsAlways:     TAction;
    mnRegisterCommentsAlways:    TMenuItem;
    spComments:                  TSplitter;
    pnComments:                  TPanel;
    edComments:                  TRichEdit;
    tbComments:                  TToolBar;
    aCommentFontBold:            TAction;
    aCommentFontItalic:          TAction;
    aCommentFontUnderline:       TAction;
    aCommentJustLeft:            TAction;
    aCommentJustCenter:          TAction;
    aCommentJustRight:           TAction;
    btCommentFontBold:           TToolButton;
    btaCommentFontItalic:        TToolButton;
    btCommentFontUnderline:      TToolButton;
    btSep1:                      TToolButton;
    btCommentJustLeft:           TToolButton;
    btCommentJustCenter:         TToolButton;
    btCommentJustRight:          TToolButton;
    mnComments:                  TMenuItem;
    mnCommentFontBold:           TMenuItem;
    mnCommentFontItalic:         TMenuItem;
    mnCommentFontUnderline:      TMenuItem;
    mnSep41:                     TMenuItem;
    mnCommentJustLeft:           TMenuItem;
    mnCommentJustCenter:         TMenuItem;
    mnCommentJustRight:          TMenuItem;
    mnSep42:                     TMenuItem;
    btSep2:                      TToolButton;
    btCommentFont:               TToolButton;
    aCommentFont:                TAction;
    mnSep43:                     TMenuItem;
    mnCommentFont:               TMenuItem;
    dlgFont:                     TFontDialog;
    aGrouping2Levels:            TAction;
    aGrouping1Level:             TAction;
    mnSep31:                     TMenuItem;
    mnGrouping:                  TMenuItem;
    mnGrouping2Levels:           TMenuItem;
    mnGrouping1Level:            TMenuItem;
    aRegisterCommentsAutomatic:  TAction;
    aRegisterCommentsShow:       TAction;
    mnCommentsPanel:             TMenuItem;
    mnRegisterCommentsAutomatic: TMenuItem;
    mnSep411:                    TMenuItem;
    mnRegisterCommentsShow:      TMenuItem;
    aRegisterCommentsOnRequest:  TAction;
    mnRegisterCommentsOnRequest: TMenuItem;
    tmrAutoSave:                 TTimer;
    procedure  FormCloseQuery(Sender:  TObject;
                              var   CanClose:  Boolean);
    procedure  aRegisterTerminateExecute(Sender:  TObject);
    procedure  tryIconDblClick(Sender:  TObject);
    procedure  aRegisterShowExecute(Sender:  TObject);
    procedure  aRegisterHideExecute(Sender:  TObject);
    procedure  aShowTodayExecute(Sender:  TObject);
    procedure  aShowWeekExecute(Sender:  TObject);
    procedure  aShowMonthExecute(Sender:  TObject);
    procedure  aShowYearExecute(Sender:  TObject);
    procedure  aShowAllExecute(Sender:  TObject);
    procedure  edRegisterFreeNode(Sender:  TBaseVirtualTree;
                                  Node:  PVirtualNode);
    procedure  tmrUpdateTimeTimer(Sender:  TObject);
    procedure  aRegisterShowUpdate(Sender:  TObject);
    procedure  aRegisterHideUpdate(Sender:  TObject);
    procedure  aRegisterPauseUpdate(Sender:  TObject);
    procedure  aRegisterResumeUpdate(Sender:  TObject);
    procedure  aRegisterPauseExecute(Sender:  TObject);
    procedure  aRegisterResumeExecute(Sender:  TObject);
    procedure  tmrPausedTimer(Sender:  TObject);
    procedure  aHelpAboutExecute(Sender:  TObject);
    procedure  aRegisterOptionsExecute(Sender:  TObject);
    procedure  aSortStartTimeExecute(Sender:  TObject);
    procedure  aSortTaskNameExecute(Sender:  TObject);
    procedure  aRegisterSelectTaskExecute(Sender:  TObject);
    procedure  aRegisterSelectTaskUpdate(Sender:  TObject);
    procedure  aRegisterAlertsEnableExecute(Sender:  TObject);
    procedure  aRegisterAlertsDisableExecute(Sender:  TObject);
    procedure  edRegisterGetImageIndex(Sender:  TBaseVirtualTree;
                                       Node:  PVirtualNode;
                                       Kind:  TVTImageKind;
                                       Column:  TColumnIndex;
                                       var   Ghosted:  Boolean;
                                       var   ImageIndex:  Integer);
    procedure  aRegisterCommentsAlwaysExecute(Sender:  TObject);
    procedure  edRegisterFocusChanged(Sender:  TBaseVirtualTree;
                                      Node:  PVirtualNode;
                                      Column:  TColumnIndex);
    procedure  edRegisterFocusChanging(Sender:  TBaseVirtualTree;
                                       OldNode, NewNode:  PVirtualNode;
                                       OldColumn, NewColumn:  TColumnIndex;
                                       var   Allowed:  Boolean);
    procedure  aCommentFontBoldExecute(Sender:  TObject);
    procedure  aCommentFontBoldUpdate(Sender:  TObject);
    procedure  aCommentFontItalicExecute(Sender:  TObject);
    procedure  aCommentFontItalicUpdate(Sender:  TObject);
    procedure  aCommentFontUnderlineExecute(Sender:  TObject);
    procedure  aCommentFontUnderlineUpdate(Sender:  TObject);
    procedure  aCommentJustLeftExecute(Sender:  TObject);
    procedure  aCommentJustLeftUpdate(Sender:  TObject);
    procedure  aCommentJustCenterExecute(Sender:  TObject);
    procedure  aCommentJustCenterUpdate(Sender:  TObject);
    procedure  aCommentJustRightExecute(Sender : TObject);
    procedure  aCommentJustRightUpdate(Sender:  TObject);
    procedure  FormCreate(Sender:  TObject);
    procedure  FormHide(Sender:  TObject);
    procedure  FormClose(Sender:  TObject;
                         var   Action:  TCloseAction);
    procedure  FormDestroy(Sender:  TObject);
    procedure  aCommentFontExecute(Sender:  TObject);
    procedure  aCommentFontUpdate(Sender:  TObject);
    procedure  edRegisterGetCellIsEmpty(Sender:  TBaseVirtualTree;
                                        Node:  PVirtualNode;
                                        Column:  TColumnIndex;
                                        var   IsEmpty:  Boolean);
    procedure  aGroupingLevelsExecute(Sender:  TObject);
    procedure  aRegisterCommentsShowUpdate(Sender:  TObject);
    procedure  aRegisterCommentsAutomaticExecute(Sender:  TObject);
    procedure  aRegisterCommentsShowExecute(Sender:  TObject);
    procedure  aRegisterCommentsOnRequestExecute(Sender:  TObject);
    procedure  edCommentsChange(Sender:  TObject);
    procedure  tmrAutoSaveTimer(Sender:  TObject);
    procedure  edRegisterGetHint(Sender:  TBaseVirtualTree;
                                 Node:  PVirtualNode;
                                 Column:  TColumnIndex;
                                 var   LineBreakStyle:  TVTTooltipLineBreakStyle;
                                 var   HintText:  string);
    procedure  edRegisterGetText(Sender:  TBaseVirtualTree;
                                 Node:  PVirtualNode;
                                 Column:  TColumnIndex;
                                 TextType:  TVSTTextType;
                                 var   CellText:  string);
    procedure  edRegisterHeaderClick(Sender:  TVTHeader;
                                     Column:  TColumnIndex;
                                     Button:  TMouseButton;
                                     Shift:  TShiftState;
                                     X, Y:  Integer);
  strict private
    fbQueryEndSession:    Boolean;
    fSortBy:              TSortBy;
    fShowRange:           TShowRange;
    ftmShowFromDate:      TDateTime;
    fpCurrentTask:        PVirtualNode;
    ftmAlertAt:           TDateTime;
    fnNextAlertDelay:     Integer;
    fbInAlertBox:         Boolean;
    fbComplyNoTask:       Boolean;
    fbLoadComments:       Boolean;
    fTimeoutAlert:        TForm;
    fbResumeRegisterScr:  Boolean;
    fbResumeRegisterSes:  Boolean;
    fbResumeRegisterPwr:  Boolean;
    fbPrevScreenSaver:    Boolean;
    fbTerminate:          Boolean;
    function  NodeDurationTime(pNode:  PVirtualNode)
                               : TDateTime;
    function  HasComments(const   pNode:  PVirtualNode)
                          : Boolean;
    procedure  QueryEndSession(var   Msg:  TWMQueryEndSession); message WM_QUERYENDSESSION;
    procedure  EndSession(var   Msg:  TWMEndSession);           message WM_ENDSESSION;
    procedure  SysCommand(var   Msg:  TWMSysCommand);           message WM_SYSCOMMAND;
    procedure  PowerBroadcast(var   Msg:  TWMPower);            message WM_POWERBROADCAST;
    procedure  SessionChange(var   Msg:  TMessage);             message WM_WTSSESSION_CHANGE;
    procedure  LoadData();
    procedure  LoadComments(const   nJobID:  Int64);
    procedure  SaveComments(const   nJobID:  Int64;
                            const   pNode:  PVirtualNode);
    procedure  CheckComments(const   nJobID:  Int64;
                             const   pNode:  PVirtualNode = nil);
    procedure  ShowComments();
    procedure  HideComments();
    procedure  TimeoutAlertCreated(Sender:  TObject);
    procedure  TimeoutAlertDestroy(Sender:  TObject);
    procedure  PauseJob();
    procedure  ResumeJob();
  strict protected
    procedure  LoadParams(const   Reg:  TRegistry);                  override;
    procedure  SaveParams(const   Reg:  TRegistry);                  override;
  public
    function  ExecuteAction(Action:  TBasicAction)
                            : Boolean;                               override;
  end { TfJobRegister };


var
  fJobRegister:  TfJobRegister;


implementation


uses
  zz_GSkDialogs, zz_GSkLog, zz_GSkFileUtils,
  u_Data, u_Options, u_SelectTask, u_About;


{$R *.dfm}


const
  gcsLanguageForms = 'forms';
  {-}
  gcsParCommentsHeight = 'Comments Height';
  gcsParColumnWidth    = 'Width of Column #%d';


type
  TNodeType = (ntGroupTime, ntGroupTask, ntDetail);

  TNodeData = record
    fnJobID:   Int64;
    fNodeType: TNodeType;
    ftmStart:  TDateTime;
    ftmStop:   TDateTime;
    fnTaskID:  Int64;
    fsTask:    string;
    fbNotes:   Boolean;
  end { TNodeData };
  PNodeData = ^TNodeData;


var
  gtmStartDay:  TDateTime;
  gtmEndDay:    TDateTime;


{$REGION 'WTS'}

const
  NOTIFY_FOR_ALL_SESSIONS  = 1;
  NOTIFY_FOR_THIS_SESSIONS = 0;


function  WTSRegisterSessionNotification(hWnd:  HWND;
                                         dwFlags:  DWORD)
                                         : BOOL;                     stdcall;
  external 'wtsapi32.dll'
      name 'WTSRegisterSessionNotification';


function  WTSUnRegisterSessionNotification(hWND:  HWND)
                                           : BOOL;                   stdcall;
  external 'wtsapi32.dll'
      name 'WTSUnRegisterSessionNotification';

{$ENDREGION}


{$REGION 'TfJobRegister' }

procedure  TfJobRegister.aCommentFontBoldExecute(Sender:  TObject);
begin
  inherited;
    with  edComments.SelAttributes  do
    if  fsBold in Style  then
      Style := Style - [fsBold]
    else
      Style := Style + [fsBold]
end { aCommentFontBoldExecute };


procedure  TfJobRegister.aCommentFontBoldUpdate(Sender:  TObject);
begin
  inherited;
  aCommentFontBold.Checked := fsBold in edComments.SelAttributes.Style;
  aCommentFontBold.Enabled := pnComments.Visible
end { aCommentFontBoldUpdate };


procedure  TfJobRegister.aCommentFontExecute(Sender:  TObject);
begin
  inherited;
  dlgFont.Font.Assign(edComments.SelAttributes);
  if  dlgFont.Execute()  then
    edComments.SelAttributes.Assign(dlgFont.Font)
end { aCommentFontExecute };


procedure  TfJobRegister.aCommentFontItalicExecute(Sender:  TObject);
begin
  inherited;
  with  edComments.SelAttributes  do
    if  fsItalic in Style  then
      Style := Style - [fsItalic]
    else
      Style := Style + [fsItalic]
end { aCommentFontItalicExecute };


procedure  TfJobRegister.aCommentFontItalicUpdate(Sender:  TObject);
begin
  inherited;
  aCommentFontItalic.Checked := fsItalic in edComments.SelAttributes.Style;
  aCommentFontItalic.Enabled := pnComments.Visible
end { aCommentFontItalicUpdate };


procedure  TfJobRegister.aCommentFontUnderlineExecute(Sender:  TObject);
begin
  inherited;
  with  edComments.SelAttributes  do
    if  fsUnderline in Style  then
      Style := Style - [fsUnderline]
    else
      Style := Style + [fsUnderline]
end { aCommentFontUnderlineExecute };


procedure  TfJobRegister.aCommentFontUnderlineUpdate(Sender:  TObject);
begin
  inherited;
  aCommentFontUnderline.Checked := fsUnderline in edComments.SelAttributes.Style;
  aCommentFontUnderline.Enabled := pnComments.Visible
end { aCommentFontUnderlineUpdate };


procedure  TfJobRegister.aCommentFontUpdate(Sender:  TObject);
begin
  inherited;
  aCommentFont.Enabled := pnComments.Visible
end { aCommentFontUpdate };


procedure  TfJobRegister.aCommentJustCenterExecute(Sender:  TObject);
begin
  inherited;
  edComments.Paragraph.Alignment := taCenter
end { aCommentJustCenterExecute };


procedure  TfJobRegister.aCommentJustCenterUpdate(Sender:  TObject);
begin
  inherited;
  aCommentJustCenter.Checked := edComments.Paragraph.Alignment = taCenter;
  aCommentJustCenter.Enabled := pnComments.Visible
end { aCommentJustCenterUpdate };


procedure  TfJobRegister.aCommentJustLeftExecute(Sender:  TObject);
begin
  inherited;
  edComments.Paragraph.Alignment := taLeftJustify
end { aCommentJustLeftExecute };


procedure  TfJobRegister.aCommentJustLeftUpdate(Sender:  TObject);
begin
  inherited;
  aCommentJustLeft.Checked := edComments.Paragraph.Alignment = taLeftJustify;
  aCommentJustLeft.Enabled := pnComments.Visible
end { aCommentJustLeftUpdate };


procedure  TfJobRegister.aCommentJustRightExecute(Sender:  TObject);
begin
  inherited;
  edComments.Paragraph.Alignment := taRightJustify
end { aCommentJustRightExecute };


procedure  TfJobRegister.aCommentJustRightUpdate(Sender:  TObject);
begin
  inherited;
  aCommentJustRight.Checked := edComments.Paragraph.Alignment = taRightJustify;
  aCommentJustRight.Enabled := pnComments.Visible
end { aCommentJustRightUpdate };


procedure  TfJobRegister.aGroupingLevelsExecute(Sender:  TObject);

var
  nLevels:  Integer;

begin  { aGroupingLevelsExecute }
  inherited;
  Assert(Sender is TAction);
  nLevels := (Sender as TAction).Tag;
  Assert(nLevels > 0);
  if  dmData.GroupingLevels <> nLevels  then
    begin
      dmData.GroupingLevels := nLevels;
      LoadData()
    end { different grouping levels }
end { aGroupingLevelsExecute };


procedure  TfJobRegister.aHelpAboutExecute(Sender:  TObject);
begin
  TfAboutBox.Execute()
end { aHelpAboutExecute };


procedure  TfJobRegister.aRegisterAlertsDisableExecute(Sender:  TObject);
begin
  inherited;
  dmData.AlertsEnabled := False;
  if  fTimeoutAlert <> nil  then
    fTimeoutAlert.Close()
end { aRegisterAlertsDisableExecute };


procedure  TfJobRegister.aRegisterAlertsEnableExecute(Sender:  TObject);
begin
  inherited;
  dmData.AlertsEnabled := True
end { aRegisterAlertsEnableExecute };


procedure  TfJobRegister.aRegisterCommentsAlwaysExecute(Sender:  TObject);
begin
  inherited;
  dmData.CommentsVisible := cvAlways;
  ShowComments()
end { aRegisterCommentsExecute };


procedure  TfJobRegister.aRegisterCommentsAutomaticExecute(Sender:  TObject);
begin
  inherited;
  dmData.CommentsVisible := cvAutomatic;
  if  HasComments(edRegister.FocusedNode)  then
    ShowComments()
end { aRegisterCommentsAutomaticExecute };


procedure  TfJobRegister.aRegisterCommentsOnRequestExecute(Sender:  TObject);
begin
  inherited;
  dmData.CommentsVisible := cvOnRequest;
  if  HasComments(edRegister.FocusedNode)  then
    aRegisterCommentsShow.Execute()
end { aRegisterCommentsOnRequestExecute };


procedure  TfJobRegister.aRegisterCommentsShowExecute(Sender:  TObject);
begin
  inherited;
  if  pnComments.Visible  then
    begin
      CheckComments(PNodeData(edRegister.GetNodeData(edRegister.FocusedNode))^.fnJobID,
                    edRegister.FocusedNode);
      HideComments();
      edRegister.SetFocus()
    end { pnComments.Visible }
  else
    begin
      ShowComments();
      edComments.SetFocus()
    end { not pnComments.Visible }
end { aRegisterCommentsShowExecute };


procedure  TfJobRegister.aRegisterCommentsShowUpdate(Sender:  TObject);
begin
  inherited;
  aRegisterCommentsShow.Enabled := not aRegisterCommentsAlways.Checked
end { aRegisterCommentsShowUpdate };


procedure  TfJobRegister.aRegisterHideExecute(Sender:  TObject);
begin
  Hide()
end { aRegisterHideExecute };


procedure  TfJobRegister.aRegisterHideUpdate(Sender:  TObject);
begin
  aRegisterHide.Enabled := Visible
end { aRegisterHideUpdate };


procedure  TfJobRegister.aRegisterOptionsExecute(Sender:  TObject);
begin
  dmData.LogEnter(liProgram, 'TfJobRegister.aRegisterOptionsExecute');
  if  TfOptions.Execute()  then
    begin
      fnNextAlertDelay := 0;   // reset work-out alert
      fbComplyNoTask := dmData.WorkTimeWithNoTask;
      dmData.BeginLog();
      dmData.LogValue(liProgram, 'NextAlertDelay', fnNextAlertDelay);
      dmData.LogValue(liProgram, 'ComplyNoTask', fbComplyNoTask);
      dmData.EndLog()
    end { TfOptions.Execute() };
  dmData.LogExit(liProgram, 'TfJobRegister.aRegisterOptionsExecute');
end { aRegisterOptionsExecute };


procedure  TfJobRegister.aRegisterPauseExecute(Sender:  TObject);
begin
  PauseJob()
end { aRegisterPauseExecute };


procedure  TfJobRegister.aRegisterPauseUpdate(Sender:  TObject);
begin
  aRegisterPause.Enabled := not dmData.Paused
end { aRegisterPauseUpdate };


procedure  TfJobRegister.aRegisterResumeExecute(Sender:  TObject);
begin
  ResumeJob()
end { aRegisterResumeExecute };


procedure  TfJobRegister.aRegisterResumeUpdate(Sender:  TObject);
begin
  aRegisterResume.Enabled := dmData.Paused
end { aRegisterResumeUpdate };


procedure  TfJobRegister.aRegisterSelectTaskExecute(Sender:  TObject);

var
  nTaskID:  Int64;
  tmWhen:  TDateTime;

begin  { aRegisterSelectTaskExecute }
  dmData.LogEnter(liTask, 'TfJobRegister.aRegisterSelectTaskExecute');
  nTaskID := dmData.CurrentTaskID;
  dmData.LogValue(liProgram, 'TaskID', nTaskID);
  if  TfSelectTask.Execute(nTaskID, tmWhen, True, True, True)  then
    begin
      if  MinutesBetween(tmWhen, dmData.StartOfJob) > 0  then
        begin
          if  dmData.Paused  then
            dmData.CurrentTaskID := nTaskID
          else
            begin
              dmData.UpdateStop(dmData.StartOfJob, tmWhen);
              dmData.RegisterStart(tmWhen, nTaskID)
            end { not dmData.Paused }
        end { if TfSelectTask.Execute(...) }
      else if  nTaskID <> dmData.CurrentTaskID  then
        dmData.UpdateTask(dmData.StartOfJob, nTaskID)
      else
        MsgBox(_('Nothing changed'),
               MB_ICONWARNING or MB_OK);
      LoadData()
    end { if TfSelectTask.Execute(...) };
  dmData.LogExit(liTask, 'TfJobRegister.aRegisterSelectTaskExecute')
end { aRegisterSelectTaskExecute };


procedure  TfJobRegister.aRegisterSelectTaskUpdate(Sender:  TObject);
begin
  aRegisterSelectTask.Enabled := (Screen.ActiveForm = nil)
                                 or (Screen.ActiveForm.ClassType() <> TfSelectTask)
end { aRegisterSelectTaskUpdate };


procedure  TfJobRegister.aRegisterShowExecute(Sender:  TObject);
begin
  if  (Screen.ActiveForm <> nil)
      and (fsModal in Screen.ActiveForm.FormState)  then
    Screen.ActiveForm.BringToFront()
  else
    Show()
end { aRegisterShowExecute };


procedure  TfJobRegister.aRegisterShowUpdate(Sender:  TObject);
begin
  aRegisterShow.Enabled := not Visible
end { aRegisterShowUpdate };


procedure  TfJobRegister.aRegisterTerminateExecute(Sender:  TObject);
begin
  fbTerminate := True;
  try
    Close()
  finally
    fbTerminate := False
  end { try-finally }
end { aRegisterTerminateExecute };


procedure  TfJobRegister.aShowAllExecute(Sender:  TObject);
begin
  fShowRange := srAll;
  sbStatus.Panels[0].Text := _('All');
  LoadData()
end { aShowAllExecute };


procedure  TfJobRegister.aShowMonthExecute(Sender:  TObject);
begin
  fShowRange := srMonth;
  sbStatus.Panels[0].Text := _('Month');
  LoadData()
end { aShowMonthExecute };


procedure  TfJobRegister.aShowTodayExecute(Sender:  TObject);
begin
  fShowRange := srToday;
  sbStatus.Panels[0].Text := _('Today');
  LoadData()
end { aShowTodayExecute };


procedure  TfJobRegister.aShowWeekExecute(Sender:  TObject);
begin
  fShowRange := srWeek;
  sbStatus.Panels[0].Text := _('Week');
  LoadData()
end { aShowWeekExecute };


procedure  TfJobRegister.aShowYearExecute(Sender:  TObject);
begin
  fShowRange := srYear;
  sbStatus.Panels[0].Text := _('Year');
  LoadData()
end { aShowYearExecute };


procedure  TfJobRegister.aSortStartTimeExecute(Sender:  TObject);
begin
  fSortBy := sbStartTime;
  edRegister.Header.SortColumn := 0;
  LoadData()
end { aSortStartTimeExecute };


procedure  TfJobRegister.aSortTaskNameExecute(Sender:  TObject);
begin
  fSortBy := sbTask;
  edRegister.Header.SortColumn := 3;
  LoadData()
end { aSortTaskNameExecute };


procedure  TfJobRegister.CheckComments(const   nJobID:  Int64;
                                       const   pNode:  PVirtualNode);
begin
  if  edComments.Modified  then
    SaveComments(nJobID, pNode)
end { CheckComments };


procedure  TfJobRegister.edCommentsChange(Sender:  TObject);
begin
  inherited;
  if  not fbLoadComments  then
    tmrAutoSave.Enabled := True
end { edCommentsChange };


procedure  TfJobRegister.edRegisterFocusChanged(Sender:  TBaseVirtualTree;
                                                Node:  PVirtualNode;
                                                Column:  TColumnIndex);
begin
  inherited;
  Assert(aRegisterCommentsAlways.Checked
         xor aRegisterCommentsAutomatic.Checked
         xor aRegisterCommentsOnRequest.Checked);
  if  aRegisterCommentsAlways.Checked  then
    LoadComments(PNodeData(edRegister.GetNodeData(Node))^.fnJobID)
  else if  aRegisterCommentsAutomatic.Checked  then
    if  HasComments(edRegister.FocusedNode)  then
      ShowComments()
    else
      HideComments()
  else
    HideComments()
end { edRegisterFocusChanged };


procedure  TfJobRegister.edRegisterFocusChanging(Sender:  TBaseVirtualTree;
                                                 OldNode, NewNode:  PVirtualNode;
                                                 OldColumn, NewColumn:  TColumnIndex;
                                                 var   Allowed:  Boolean);
begin
  inherited;
  if  OldNode <> nil  then
    CheckComments(PNodeData(edRegister.GetNodeData(OldNode))^.fnJobID,
                  OldNode)
end { edRegisterFocusChanging };


procedure  TfJobRegister.edRegisterFreeNode(Sender:  TBaseVirtualTree;
                                            Node:  PVirtualNode);
begin  { edRegisterFreeNode }
  Finalize(PNodeData(edRegister.GetNodeData(Node))^)
end { edRegisterFreeNode };


procedure  TfJobRegister.edRegisterGetCellIsEmpty(Sender:  TBaseVirtualTree;
                                                  Node:  PVirtualNode;
                                                  Column:  TColumnIndex;
                                                  var   IsEmpty:  Boolean);
begin
  inherited;
  if  (Column = 4{Notes})
      and (PNodeData(edRegister.GetNodeData(Node))^.fNodeType = ntDetail)  then
    IsEmpty := False
end { edRegisterGetCellIsEmpty };


procedure  TfJobRegister.edRegisterGetHint(Sender:  TBaseVirtualTree;
                                           Node:  PVirtualNode;
                                           Column:  TColumnIndex;
                                           var   LineBreakStyle:  TVTTooltipLineBreakStyle;
                                           var   HintText:  string);
begin
  inherited;
  with  PNodeData(edRegister.GetNodeData(Node))^  do
    case  fNodeType  of
      ntGroupTask:
        HintText := fsTask;
      ntGroupTime:
        HintText := FormatDateTime('dddd, dddddd', ftmStart);
      ntDetail:
        begin
          LineBreakStyle := hlbForceMultiLine;
          HintText := Format(_('Start: %s'#13#10'End:'),
                      [FormatDateTime('dddddd t', ftmStart)]);
          if  ftmStop <> 0  then
            HintText := Format(_('%s %s'#13#10'Duration: %s'),
                               [HintText,
                                FormatDateTime('dddddd t', ftmStop),
                                DurationToStr(DurationTime(ftmStart, ftmStop),
                                              False)])
          else
            HintText := Format('%s (%s)',
                               [HintText,
                                IfThen(Node = fpCurrentTask,
                                       _('still'),            // still working
                                       _('unknown'))])        // application or computer crash
        end;
    end { case fNodeType; with }
end { edRegisterGetHint };


procedure  TfJobRegister.edRegisterGetImageIndex(Sender:  TBaseVirtualTree;
                                                 Node:  PVirtualNode;
                                                 Kind:  TVTImageKind;
                                                 Column:  TColumnIndex;
                                                 var   Ghosted:  Boolean;
                                                 var   ImageIndex:  Integer);
begin
  inherited;
  Assert(ImageIndex = -1);
  if  Column = 4{Notes}  then
    if  PNodeData(edRegister.GetNodeData(Node))^.fbNotes  then
      ImageIndex := 1
end { edRegisterGetImageIndex };


procedure  TfJobRegister.edRegisterGetText(Sender:  TBaseVirtualTree;
                                           Node:  PVirtualNode;
                                           Column:  TColumnIndex;
                                           TextType:  TVSTTextType;
                                           var   CellText:  string);
var
  pData:  PNodeData;

  function  ShowDate(const   tmStamp1, tmStamp2:  TDateTime)
                     : Boolean;
  begin
    Result := (tmStamp2 <> 0.0)
              and not SameDate(tmStamp1, tmStamp2)
  end { ShowDate };

begin  { edRegisterGetText }
  CellText := '';
  pData := edRegister.GetNodeData(Node);
  case  pData^.fNodeType  of
    ntGroupTime:
      case  Column  of
        0:  CellText := DateToStr(pData^.ftmStart)
                          + ', ' + FormatSettings.LongDayNames[DayOfWeek(pData^.ftmStart)];
        2:  CellText := DurationToStr(NodeDurationTime(Node), False)
      end { case Column; ntGroupTime };
    ntGroupTask:
      case  Column  of
        0:  begin
              CellText := pData^.fsTask;
              if  CellText = ''  then
                CellText := _('(none)')
            end { Column = 0 };
        2:  CellText := DurationToStr(NodeDurationTime(Node), False)
      end { case Column; ntGroupTask };
    ntDetail:
      case  Column  of
        0:  CellText := FormatDateTime(IfThen((fSortBy = sbTask)
                                                   and (dmData.GroupingLevels = 1)
                                                or ShowDate(pData^.ftmStart, pData^.ftmStop)
                                                or (Node = fpCurrentTask)
                                                   and (DateOf(pData^.ftmStart) <> Today()),
                                              'ddddd hh:nn',
                                              'hh:nn'),
                                       pData^.ftmStart);
        1:  if  pData^.ftmStop <> 0  then
              CellText := FormatDateTime(IfThen(ShowDate(pData^.ftmStart, pData^.ftmStop),
                                                'ddddd hh:nn',
                                                'hh:nn'),
                                        pData^.ftmStop)
            else if  Node = fpCurrentTask  then
              CellText := FormatDateTime(IfThen(pData^.ftmStart < Today(),
                                                'ddddd hh:nn:ss',
                                                'hh:nn:ss'),
                                         Now())
            else
              CellText := '?';
        2:  if  pData^.ftmStop <> 0  then
              CellText := DurationToStr(DurationTime(pData^.ftmStart, pData^.ftmStop),
                                        False)
            else if  Node = fpCurrentTask  then
              CellText := DurationToStr(DurationTime(pData^.ftmStart, Now()),
                                        True)
            else
              CellText := '?';
        3:  CellText := pData^.fsTask
      end { case Column }
  end { case pData^.fNodeType }
end { edRegisterGetText };


procedure  TfJobRegister.edRegisterHeaderClick(Sender : TVTHeader;
                                               Column:  TColumnIndex;
                                               Button:  TMouseButton;
                                               Shift:  TShiftState;
                                               X, Y: Integer);
begin
  inherited;
  if  (Button = mbLeft)
      and (Shift = [])  then
    case  Column  of
      0:  aSortStartTime.Execute();
      3:  aSortTaskName.Execute()
    end { case Column }
end { edRegisterHeaderClick };


procedure  TfJobRegister.EndSession(var   Msg:  TWMEndSession);
begin
  dmData.LogEnter(liSession, 'TfJobRegister.EndSession [WM_ENDSESSION]');
  CheckComments(PNodeData(edRegister.GetNodeData(edRegister.FocusedNode))^.fnJobID);
  with  dmData  do
    if  not Paused  then
      UpdateStop(StartOfJob, Now());
  dmData.LogExit(liSession, 'TfJobRegister.EndSession [WM_ENDSESSION]')
end { EndSession };


function  TfJobRegister.ExecuteAction(Action:  TBasicAction)
                                      : Boolean;
begin
  if  (Action is THintAction)
      and  not tmrPaused.Enabled  then   // both use the last StatusBar's panel
    begin
      sbStatus.Panels[2].Text := (Action as THintAction).Hint;
      Result := True
    end
  else
    Result := inherited ExecuteAction(Action)
end { ExecuteAction };


procedure  TfJobRegister.FormClose(Sender:  TObject;
                                   var   Action:  TCloseAction);
var
  nInd:  Integer;

begin  { FormClose }
  dmData.LogEnter(liProgram, 'TfJobRegister.FormClose');
  inherited;
  if  edRegister.FocusedNode <> nil  then
    CheckComments(PNodeData(edRegister.GetNodeData(edRegister.FocusedNode))^.fnJobID);
  if  fbTerminate  then
    for  nInd := Screen.FormCount - 1  downto  0  do
      if  Screen.Forms[nInd] <> Self  then
        if  fsModal in Screen.Forms[nInd].FormState  then
          Screen.Forms[nInd].ModalResult := mrAbort
        else
          Screen.Forms[nInd].Close();
    Application.Terminate();
  dmData.LogExit(liProgram, 'TfJobRegister.FormClose')
end { FormClose };


procedure  TfJobRegister.FormCloseQuery(Sender:  TObject;
                                        var   CanClose:  Boolean);
begin
  dmData.LogEnter(liProgram, 'TfJobRegister.FormCloseQuery');
  if  not fbQueryEndSession  then
    CanClose := MsgBox(_('Do you really want to terminate this application?'),
                       MB_ICONQUESTION or MB_YESNO or MB_DEFBUTTON2) = IDYES;
  dmData.BeginLog();
  dmData.LogValue(liProgram, 'CanClose', CanClose);
  dmData.LogExit(liProgram, 'TfJobRegister.FormCloseQuery');
  dmData.EndLog()
end { FormCloseQuery };


procedure  TfJobRegister.FormCreate(Sender:  TObject);

var
  tmNow:    TDateTime;
  nTaskID:  Int64;

begin  { FormCreate }
  dmData.LogEnter(liProgram, 'TfJobRegister.FormCreate');
  inherited;
  Caption := Format(_('%s version %s'),  // e.g. 'Job Register version 1.3.6'
                    [Caption, GetModuleVersion()]);
  {-}
  fbComplyNoTask := dmData.WorkTimeWithNoTask;
  Assert(aGrouping2Levels.Checked);
  if  dmData.GroupingLevels = 1  then
    aGrouping1Level.Checked := True;
  {-}
  tmNow := Now();
  case  dmData.StartupTaskAction  of
    gcnStartTaskUndefined:
      dmData.RegisterStart(tmNow);
    gcnStartTaskDefault:
      dmData.RegisterStart(tmNow, dmData.DefaultTaskID);
    gcnStartTaskAskFor:
      begin
        nTaskID := dmData.CurrentTaskID;
        if not TfSelectTask.Execute(nTaskID, tmNow, False, False, False)  then
          nTaskID := 0;
        dmData.RegisterStart(tmNow, nTaskID);
      end { gcnStartTaskAskFor };
    gcnStartTaskLastUsed:
      dmData.RegisterStart(tmNow, dmData.CurrentTaskID)
    else
      Assert(False, 'Unknown startup action')
  end { case dmData.StartupTaskAction };
  {-}
  aShowToday.Execute();
  {-}
  case  dmData.CommentsVisible  of
    cvAlways:
      aRegisterCommentsAlways.Execute();
    cvAutomatic:
      aRegisterCommentsAutomatic.Execute();
    cvOnRequest:
      aRegisterCommentsOnRequest.Execute()
  end { case dmData.CommentsVisible };
  {-}
  WTSRegisterSessionNotification(Handle, NOTIFY_FOR_ALL_SESSIONS);
  dmData.LogExit(liProgram, 'TfJobRegister.FormCreate')
end { FormCreate };


procedure  TfJobRegister.FormDestroy(Sender:  TObject);
begin
  WTSUnRegisterSessionNotification(Handle);
  inherited
end { FormDestroy };


procedure  TfJobRegister.FormHide(Sender:  TObject);
begin
  inherited;
  if  edRegister.FocusedNode <> nil  then
    CheckComments(PNodeData(edRegister.GetNodeData(edRegister.FocusedNode))^.fnJobID)
end { FormHide };


function  TfJobRegister.HasComments(const   pNode:  PVirtualNode)
                                    : Boolean;
begin
  if  pNode <> nil  then
    Result := PNodeData(edRegister.GetNodeData(pNode))^.fbNotes
  else
    Result := False
end { HasComments };


procedure  TfJobRegister.HideComments();
begin
  spComments.Hide();
  pnComments.Hide()
end { HideComments };


procedure  TfJobRegister.LoadComments(const   nJobID:  Int64);

var
  lBuf:  TStringStream;

begin  { LoadComments }
  dmData.LogEnter(liProgram, 'TfJobRegister.LoadComments(%d)', [nJobID]);
  fbLoadComments := True;
  try
    with  dmData  do
      try
        with  ExecQuery('SELECT bNotes FROM t_Jobs WHERE nJobID = %d', [nJobID])  do
          try
            lBuf := TStringStream.Create(Fields[0].AsString);
            try
              lBuf.Seek(0, soFromBeginning);
              edComments.Lines.LoadFromStream(lBuf);
              edComments.Modified := False
            finally
              lBuf.Free()
            end { try-finally }
          finally
            Free()
          end { try-finally }
      except
        on  eErr : Exception  do
          Error(_('Reading comments from database'), eErr)
      end { try-except }
  finally
    fbLoadComments := False
  end { LoadComments };
  dmData.LogExit(liProgram, 'TfJobRegister.LoadComments')
end { LoadComments };


procedure  TfJobRegister.LoadData();

const
  cnFldJobID  = 0;   // \
  cnFldStart  = 1;   //  \
  cnFldStop   = 2;   //   > see SQL SELECT statement below
  cnFldTaskID = 3;   //  /
  cnFldTask   = 4;   // /
  cnFldNotes  = 5;   ///

var
  sStmt:   string;
  sTask:   string;
  pNode:   PVirtualNode;
  pData:   PNodeData;
  pLev1:   PVirtualNode;
  pLev2:   PVirtualNode;
  tmDate:  TDateTime;
  tmLast:  TDateTime;

begin  { LoadData }
  dmData.LogEnter(liProgram, 'TfJobRegister.LoadData');
  // Order of fields below have to be compatible with 'cnFld...' constants above
  sStmt := 'SELECT nJobID, tmStart, tmStop, nTaskID, sTaskName, bNotes FROM v_Jobs';
  if  fShowRange <> srAll  then
    begin
      ftmShowFromDate := Today();
      case  fShowRange  of
        srToday:
          { OK };
        srWeek:
          ftmShowFromDate := StartOfTheWeek(ftmShowFromDate);
        srMonth:
          ftmShowFromDate := StartOfTheMonth(ftmShowFromDate);
        srYear:
          ftmShowFromDate := StartOfTheYear(ftmShowFromDate);
        srAll:
          { OK }
      end { case fShowRange };
      sStmt := Format('%0:s WHERE tmStart >= ''%1:s'' OR tmStop >= ''%1:s'' OR tmStart >= ''YESTERDAY'' AND tmStop IS NULL',
                      [sStmt, FormatDateTime('yyyy-mm-dd', ftmShowFromDate)])
    end { fShowRange <> srAll };
  case  fSortBy  of
    sbStartTime:
      if  dmData.GroupingLevels = 1  then
        sStmt := sStmt + ' ORDER BY tmStart'
      else
        sStmt := sStmt + ' ORDER BY Cast(tmStart AS DATE), sTaskName, tmStart';
    sbTask:
      sStmt := sStmt + ' ORDER BY sTaskName, tmStart'
  end { case fSortBy };
  edRegister.BeginUpdate();
  try
    fpCurrentTask := nil;
    edRegister.Clear();
    edRegister.NodeDataSize := SizeOf(TNodeData);
    tmDate := 0;
    tmLast := 0;
    sTask := #0;
    pLev1 := nil;
    pLev2 := nil;
    // pNode := nil;
    with  dmData.ExecQuery(sStmt)  do
      try
        while  not Eof  do
          begin
            case  fSortBy  of
              sbStartTime:
                begin
                  if  tmDate <> DateOf(Fields[cnFldStart].AsDateTime)  then
                    begin
                      tmDate := DateOf(Fields[cnFldStart].AsDateTime);
                      pLev1 := edRegister.AddChild(nil);
                      pData := edRegister.GetNodeData(pLev1);
                      Initialize(pData^);
                      pData^.fNodeType := ntGroupTime;
                      pData^.ftmStart := tmDate;
                      sTask := #0   // reset Task
                    end { new day };
                  if  dmData.GroupingLevels = 1  then
                    pLev2 := pLev1
                  else  { GroupingLevels = 2 }
                    if  sTask <> Fields[cnFldTask].AsString  then
                      begin
                        sTask := Fields[cnFldTask].AsString;
                        pLev2 := edRegister.AddChild(pLev1);
                        pData := edRegister.GetNodeData(pLev2);
                        Initialize(pData^);
                        pData^.fNodeType := ntGroupTask;
                        pData^.ftmStart := tmDate;
                        pData^.fsTask := sTask
                      end { new task };
                  pNode := edRegister.AddChild(pLev2);
                  pData := edRegister.GetNodeData(pNode);
                  Initialize(pData^);
                  pData^.fNodeType := ntDetail;
                  pData^.fnJobID := Fields[cnFldJobID].AsInt64;
                  pData^.ftmStart := Fields[cnFldStart].AsDateTime;
                  pData^.ftmStop := Fields[cnFldStop].AsDateTime;
                  pData^.fnTaskID := Fields[cnFldTaskID].AsInt64;
                  pData^.fsTask := Fields[cnFldTask].AsString;
                  pData^.fbNotes := not Fields[cnFldNotes].IsNull;
                  if  (pData^.ftmStop = 0)
                      and (pData^.fnTaskID = dmData.CurrentTaskID)
                      and (pData^.ftmStart > tmLast)
                      and not dmData.Paused  then
                    begin
                      fpCurrentTask := pNode;
                      tmLast := pData^.ftmStart
                    end { if }
                end { sbStartTime };
              sbTask:
                begin
                  if  sTask <> Fields[cnFldTask].AsString  then
                    begin
                      sTask := Fields[cnFldTask].AsString;
                      pLev1 := edRegister.AddChild(nil);
                      pData := edRegister.GetNodeData(pLev1);
                      Initialize(pData^);
                      pData^.fNodeType := ntGroupTask;
                      pData^.fsTask := sTask;
                      tmDate := 0   // reset StartTime
                    end { new Task };
                  if  dmData.GroupingLevels = 1  then
                    pLev2 := pLev1
                  else  { GroupingLevels = 2 }
                    if  tmDate <> DateOf(Fields[cnFldStart].AsDate)  then
                      begin
                        tmDate := DateOf(Fields[cnFldStart].AsDate);
                        pLev2 := edRegister.AddChild(pLev1);
                        pData := edRegister.GetNodeData(pLev2);
                        Initialize(pData^);
                        pData^.fNodeType := ntGroupTime;
                        pData^.fsTask := sTask;
                        pData^.ftmStart := tmDate
                      end { new day };
                  pNode := edRegister.AddChild(pLev2);
                  pData := edRegister.GetNodeData(pNode);
                  Initialize(pData^);
                  pData^.fNodeType := ntDetail;
                  pData^.fnJobID := Fields[cnFldJobID].AsInt64;
                  pData^.ftmStart := Fields[cnFldStart].AsDateTime;
                  pData^.ftmStop := Fields[cnFldStop].AsDateTime;
                  pData^.fnTaskID := Fields[cnFldTaskID].AsInt64;
                  pData^.fsTask := Fields[cnFldTask].AsString;
                  pData^.fbNotes := not Fields[cnFldNotes].IsNull;
                  if  (pData^.ftmStop = 0)
                      and (pData^.fnTaskID = dmData.CurrentTaskID)
                      and (pData^.ftmStart > tmLast)
                      and not dmData.Paused  then
                    begin
                      fpCurrentTask := pNode;
                      tmLast := pData^.ftmStart
                    end { if }
                end { sbTask }
            end { case fSortBy };
            Next()
          end { while not Eof };
      finally
        Free()
      end { try-finally }
  finally
    edRegister.EndUpdate();
    edRegister.FocusedNode := fpCurrentTask;
    if  fpCurrentTask <> nil  then
      begin
        edRegister.Selected[fpCurrentTask] := True;
        edRegister.ScrollIntoView(fpCurrentTask, True)
      end { fpCurrentTask <> nil }
  end { try-finally };
  dmData.LogExit(liProgram, 'TfJobRegister.LoadData')
end { LoadData };


procedure  TfJobRegister.LoadParams(const   Reg:  TRegistry);

var
  nInd:  Integer;

begin  { LoadParams }
  inherited;
  try
    pnComments.Height := Reg.ReadInteger(gcsParCommentsHeight);
    with  edRegister.Header.Columns  do
      for  nInd := Count - 1  downto  0  do
        Items[nInd].Width := Reg.ReadInteger(Format(gcsParColumnWidth, [nInd]))
  except
    { ignore exceptions }
  end { try-except }
end { LoadParams };


function  TfJobRegister.NodeDurationTime(pNode:  PVirtualNode)
                                         : TDateTime;
var
  pData:  PNodeData;

begin  { NodeDurationTime }
  if  pNode^.ChildCount = 0  then
    begin
      pData := edRegister.GetNodeData(pNode);
      Assert(pData^.fNodeType = ntDetail);
      if  pData^.ftmStop <> 0  then
        Result := DurationTime(pData^.ftmStart, pData^.ftmStop)
      else if  pNode = fpCurrentTask  then
        Result := DurationTime(pData^.ftmStart, Now())
      else
        Result := 0
    end { pNode^.ChildCount = 0 }
  else
    begin
      Assert(pNode^.ChildCount > 0);
      Result := 0;
      pNode := pNode^.FirstChild;
      while  pNode <> nil  do
        begin
          Result := Result + NodeDurationTime(pNode);
          pNode := pNode^.NextSibling
        end { while pNode <> nil }
    end { pData^.fNodeType <> ntDetail }
end { NodeDurationTime f };


procedure  TfJobRegister.PauseJob();
begin
  dmData.LogEnter(liProgram, 'TfJobRegister.PauseJob');
  dmData.PauseJob();
  tryIcon.IconIndex := 0;
  tryIcon.Icons := ilAnimation;
  tryIcon.Animate := True;
  tmrPaused.Enabled := True;
  LoadData();
  dmData.LogExit(liProgram, 'TfJobRegister.PauseJob');
end { PauseJob };


procedure  TfJobRegister.PowerBroadcast(var   Msg:  TWMPower);

const
  PBT_APMSUSPEND         = $04;
  PBT_APMRESUMESUSPEND   = $07;
  PBT_APMRESUMEAUTOMATIC = $12;

begin  { PowerBroadcast }
  if  dmData.tmTimestamp.Enabled  then
    case  Msg.PowerEvt  of
      PBT_APMSUSPEND:
        begin
          dmData.BeginLog();
          try
            dmData.LogEnter(liSession, 'WM_POWERBROADCAST');
            dmData.LogMessage(liSession, 'PowerEvt = PBT_APMSUSPEND');
            dmData.LogMessage(liSession, 'Data registration is '
                                           + IfThen(dmData.Paused, 'PAUSED', 'active'))
          finally
            dmData.EndLog()
          end { try-finally };
          fbResumeRegisterPwr := not dmData.Paused;
          if  fbResumeRegisterPwr  then
            PauseJob();
          dmData.LogExit(liSession, 'WM_POWERBROADCAST')
        end { PBT_APMSUSPEND };
      PBT_APMRESUMEAUTOMATIC:
        begin
          dmData.BeginLog();
          try
            dmData.LogEnter(liSession, 'WM_POWERBROADCAST');
            dmData.LogMessage(liSession, 'WM_POWERBROADCAST: PowerEvt = PBT_APMRESUMEAUTOMATIC');
          finally
            dmData.EndLog()
          end { try-finally };
          if  fbResumeRegisterPwr  then
            ResumeJob();
           dmData.LogExit(liSession, 'WM_POWERBROADCAST')
        end { PBT_APMRESUMEAUTOMATIC }
      else
        dmData.LogMessage(liSession, 'WM_POWERBROADCAST: PowerEvt = %0:d ($%0:x)',
                          [Msg.PowerEvt])
    end { Msg.PowerEvt }
end { PowerBroadcast };


procedure  TfJobRegister.QueryEndSession(var   Msg:  TWMQueryEndSession);
begin
  dmData.LogEnter(liProgram, 'TfJobRegister.QueryEndSession [WM_QUERYENDSESSION]');
  fbQueryEndSession := True;
  try
    inherited   // generate OnCloseQuery event
  finally
    fbQueryEndSession := False
  end { try-finally };
  dmData.LogExit(liProgram, 'TfJobRegister.QueryEndSession [WM_QUERYENDSESSION]')
end { QueryEndSession };


procedure  TfJobRegister.ResumeJob();
begin
  dmData.LogEnter(liProgram, 'TfJobRegister.ResumeJob');
  dmData.ResumeJob();
  tryIcon.Icons := nil;
  tryIcon.Animate := False;
  tmrPaused.Enabled := False;
  sbStatus.Panels[2].Text := '';
  LoadData();
  dmData.LogExit(liProgram, 'TfJobRegister.ResumeJob')
end { ResumeJob };


procedure  TfJobRegister.SaveComments(const   nJobID:  Int64;
                                      const   pNode:  PVirtualNode);
var
  lTmp:  TMemoryStream;
  pDta:  PNodeData;

begin  { SaveComments };
  dmData.LogEnter(liProgram, 'TfJobRegister.SaveComments(%d, ...)', [nJobID]);
  tmrAutoSave.Enabled := False;
  edComments.Modified := False;
  {-}
  if  pNode <> nil  then
    pDta := edRegister.GetNodeData(pNode)
  else
    pDta := nil;
  lTmp := TMemoryStream.Create();
  try
    edComments.Lines.SaveToStream(lTmp);
    with  dmData  do
      if  edComments.Lines.Text = ''  then
        begin
          ExecSQL('UPDATE t_Jobs SET bNotes = NULL WHERE nJobID = %d',
                  [nJobID]);
          if  pDta <> nil  then
            pDta^.fbNotes := False
        end { edComments.Lines.Text = '' }
      else
        begin
          with  ExecQuery('UPDATE t_Jobs SET bNotes = :bNotes WHERE nJobID = %d',
                          [nJobID])  do
            try
              lTmp.Seek(0, soFromBeginning);
              Params[0].LoadFromStream(lTmp);
              ExecQuery()
            finally
              Free()
            end { try-finally };
          if  pDta <> nil  then
            pDta^.fbNotes := True
        end { edComments.Lines.Text <> '' }
  finally
    lTmp.Free()
  end { try-finally };
  dmData.LogExit(liProgram, 'TfJobRegister.SaveComments')
end { SaveComments };


procedure  TfJobRegister.SaveParams(const   Reg:  TRegistry);

var
  nInd:  Integer;

begin  { SaveParams }
  inherited;
  Reg.WriteInteger(gcsParCommentsHeight, pnComments.Height);
  with  edRegister.Header.Columns  do
    for  nInd := Count - 1  downto  0  do
      Reg.WriteInteger(Format(gcsParColumnWidth, [nInd]),
                       Items[nInd].Width)
end { SaveParams };


procedure  TfJobRegister.SessionChange(var   Msg:  TMessage);
begin
  dmData.BeginLog();
  try
    dmData.LogEnter(liSession, 'TfJobRegister.SessionChange');
    case  Msg.wParam  of
      WTS_SESSION_LOCK:
        dmData.LogMessage(liSession, 'wParam = WTS_SESSION_LOCK');
      WTS_SESSION_UNLOCK:
        dmData.LogMessage(liSession, 'wParam = WTS_SESSION_UNLOCK');
      else
        dmData.LogValue(liSession, 'Msg.wParam', Msg.wParam, vtHexDec);
    end { case Msg.wParam };
    dmData.LogMessage(liSession, 'Data registration is '
                                   + IfThen(dmData.Paused, 'PAUSED', 'active'));
  finally
    dmData.EndLog()
  end { try-finally };
  Assert(Msg.Msg = WM_WTSSESSION_CHANGE);
  case  Msg.wParam  of
    WTS_SESSION_LOCK:
      begin
        fbResumeRegisterSes := dmData.DetectScreenSaver
                               and not dmData.Paused;
        if  fbResumeRegisterSes  then
          PauseJob()
      end { WTS_SESSION_LOCK };
    WTS_SESSION_UNLOCK:
      if  fbResumeRegisterSes  then
        ResumeJob()
  end { case Msg.wParam };
  dmData.LogExit(liSession, 'TfJobRegister.SessionChange')
end { SessionChange };


procedure  TfJobRegister.ShowComments();
begin
  LoadComments(PNodeData(edRegister.GetNodeData(edRegister.FocusedNode))^.fnJobID);
  {-}
  if  not pnComments.Visible  then
    begin
      edRegister.Align := alTop;
      spComments.Top := edRegister.Height + 1;
      spComments.Show();
      pnComments.Top := spComments.Top + 1;
      pnComments.Show();
      sbStatus.Top := pnComments.Top + pnComments.Height + 1;
      edRegister.Align := alClient
    end { not pnComments.Visible }
end { ShowComments };


procedure  TfJobRegister.SysCommand(var   Msg:  TWMSysCommand);
begin
  case  Msg.CmdType and $FFF0  of
    SC_MINIMIZE:
      begin
        aRegisterHide.Execute();
        dmData.LogMessage(liProgram, 'WM_SYSCOMMAND: Msg.CmdType = SC_MINIMIZE');
        Exit
      end { SC_MINIMIZE };
    SC_CLOSE:
      begin
        { TODO : It should be controlled by option              }  // <-- check it!
        {        Either hide this form or terminate application }
        aRegisterHide.Execute();
        dmData.LogMessage(liProgram, 'WM_SYSCOMMAND: Msg.CmdType = SC_CLOSE');
        Exit
      end { SC_CLOSE };
    SC_SIZE:
      dmData.LogMessage(liProgram, 'WM_SYSCOMMAND: Msg.CmdType = SC_SIZE');
    SC_MOVE:
      dmData.LogMessage(liProgram, 'WM_SYSCOMMAND: Msg.CmdType = SC_MOVE');
    SC_MAXIMIZE:
      dmData.LogMessage(liProgram, 'WM_SYSCOMMAND: Msg.CmdType = SC_MAXIMIZE');
    SC_NEXTWINDOW:
      dmData.LogMessage(liProgram, 'WM_SYSCOMMAND: Msg.CmdType = SC_NEXTWINDOW');
    SC_PREVWINDOW:
      dmData.LogMessage(liProgram, 'WM_SYSCOMMAND: Msg.CmdType = SC_PREVWINDOW');
    SC_VSCROLL:
      dmData.LogMessage(liProgram, 'WM_SYSCOMMAND: Msg.CmdType = SC_VSCROLL');
    SC_HSCROLL:
      dmData.LogMessage(liProgram, 'WM_SYSCOMMAND: Msg.CmdType = SC_HSCROLL');
    SC_MOUSEMENU:
      dmData.LogMessage(liProgram, 'WM_SYSCOMMAND: Msg.CmdType = SC_MOUSEMENU');
    SC_KEYMENU:
      dmData.LogMessage(liProgram, 'WM_SYSCOMMAND: Msg.CmdType = SC_KEYMENU');
    SC_ARRANGE:
      dmData.LogMessage(liProgram, 'WM_SYSCOMMAND: Msg.CmdType = SC_ARRANGE');
    SC_RESTORE:
      dmData.LogMessage(liProgram, 'WM_SYSCOMMAND: Msg.CmdType = SC_RESTORE');
    SC_TASKLIST:
      dmData.LogMessage(liProgram, 'WM_SYSCOMMAND: Msg.CmdType = SC_TASKLIST');
    SC_SCREENSAVE:
      dmData.LogMessage(liProgram, 'WM_SYSCOMMAND: Msg.CmdType = SC_SCREENSAVE');
    SC_HOTKEY:
      dmData.LogMessage(liProgram, 'WM_SYSCOMMAND: Msg.CmdType = SC_HOTKEY');
    SC_DEFAULT:
      dmData.LogMessage(liProgram, 'WM_SYSCOMMAND: Msg.CmdType = SC_DEFAULT');
    SC_MONITORPOWER:
      dmData.LogMessage(liProgram, 'WM_SYSCOMMAND: Msg.CmdType = SC_MONITORPOWER');
    SC_CONTEXTHELP:
      dmData.LogMessage(liProgram, 'WM_SYSCOMMAND: Msg.CmdType = SC_CONTEXTHELP');
    SC_SEPARATOR:
      dmData.LogMessage(liProgram, 'WM_SYSCOMMAND: Msg.CmdType = SC_SEPARATPR')
    else
      dmData.LogMessage(liProgram, 'WM_SYSCOMMAND: Msg.CmdType = $%x ($%x)',
                        [Msg.CmdType, Msg.CmdType and $FFF0])
  end { case };
  inherited
end { SysCommand };


procedure  TfJobRegister.TimeoutAlertCreated(Sender:  TObject);
begin
  fTimeoutAlert := Sender as TForm
end { TimeoutAlertCreated };


procedure  TfJobRegister.TimeoutAlertDestroy(Sender:  TObject);
begin
  fTimeoutAlert := nil
end { TimeoutAlertDestroy };


procedure  TfJobRegister.tmrAutoSaveTimer(Sender:  TObject);
begin
  inherited;
  SaveComments(PNodeData(edRegister.GetNodeData(edRegister.FocusedNode))^.fnJobID,
               edRegister.FocusedNode)
end { tmAutoSaveTimer };


procedure  TfJobRegister.tmrPausedTimer(Sender:  TObject);
begin
  tmrPaused.Tag := (tmrPaused.Tag + 1) mod 3;
  case  tmrPaused.Tag  of
    0:  sbStatus.Panels[2].Text := ' Paused';
    2:  sbStatus.Panels[2].Text := ''
  end { case }
end { tmrPausedTimer };


procedure  TfJobRegister.tmrUpdateTimeTimer(Sender:  TObject);

var
  pNode:       PVirtualNode;
  tmYesterday: TDateTime;
  tmDuration:  TDateTime;   // total
  tmDurationT: TDateTime;   // only registrations with assigned task
  tmStart:     TDateTime;
  tmStop:      TDateTime;
  tmAlert:     TDateTime;   // total
  tmAlertT:    TDateTime;   // only registrations with assigned task
  tmBuf:       TDateTime;

  procedure  CheckAlert(const   tmAlertTotal, tmAlertTasks:  TDateTime);

  var
    sCaption:  string;
    sMessage:  string;
    tmAlert:  TDateTime;

    function  FmtTimeoutStr(const   tmDuration:  TDateTime)
                            : string;
    var
      nD, nH, nM, nS, nT:  Word;

      procedure  AddText(var   sBuf:  string;
                         const   sAdd:  string);                       overload;
      begin
        if  sBuf <> ''  then
          sBuf := sBuf + ' ' + sAdd
        else
          sBuf := sAdd
      end { AddText };

      procedure  AddText(var   sBuf:  string;
                         const   sFmt:  string;
                         const   Args:  array of const);               overload;
      begin
        AddText(sBuf, Format(sFmt, Args))
      end { AddText };

    begin  { FmtTimeoutStr }
      nD := Trunc(tmDuration);
      DecodeTime(tmDuration, nH, nM, nS, nT);
      Result := '';
      if  nD <> 0  then
        AddText(Result,
                '%d %s',
                [nD, dngettext(gcsLanguageForms, 'day', 'days', nD)]);
      if  (nH <> 0) or (nD <> 0)  then
        begin
          if  (nM = 0)
              and (nD <> 0)  then
            AddText(Result, _('and'));   // e.g. 'H hours and M minutes'
          AddText(Result,
                  '%d %s',
                  [nH, dngettext(gcsLanguageForms, 'hour', 'hours', nH)])
        end ;
      if  (nM <> 0)
          or (nH = 0)
             and (nD = 0)  then
        begin
          if  Result <> ''  then
            AddText(Result, _('and'));
          AddText(Result,
                  '%d %s',
                  [nM, dngettext(gcsLanguageForms, 'minute', 'minutes', nM)])
        end
    end { FmtTimeoutStr };

    procedure  AddTotalTime(var   sText:  string;
                            const   tmTask, tmTotal:  TDateTime);
    begin
      if  not fbComplyNoTask  then
        if  MinutesBetween(tmTask, tmTotal) <> 0  then
          sText := Format(_('%s'#13#10'(total: %s)'),
                          [sText, FmtTimeoutStr(tmTotal)])
    end { AddTotalTime };

  begin  { CheckAlert }
    if  fbComplyNoTask  then
      tmAlert := tmAlertTotal
    else
      tmAlert := tmAlertTasks;
    if  (tmAlert >= dmData.CurrentNominalWorkTime)
        and ((fnNextAlertDelay = 0)
             or (SecondsBetween(ftmAlertAt, Now()) = 0))  then
      begin
        sCaption := _('Time-out');
        {-}
        sMessage := FmtTimeoutStr(tmAlert);
        {-}
        tryIcon.Hint := Format(_('%s worked out'), [sMessage]);
        if  Trunc(tmAlert) <> 0  then
          sMessage := Format(_('You have worked out already %s'), [sMessage])
        else
          sMessage := Format(_('Today you have worked out already %s'), [sMessage]);
        AddTotalTime(sMessage, tmAlertTasks, tmAlertTotal);
        {-}
        tryIcon.BalloonFlags := bfInfo;
        tryIcon.BalloonTimeout := 5000;   { TODO -cImprove : BalloonTimeout option (general options?) }
        tryIcon.BalloonTimeout := fnNextAlertDelay * 1000;
        tryIcon.BalloonTitle := sCaption;
        tryIcon.BalloonHint := sMessage;
        tryIcon.ShowBalloonHint();
        {-}
        if  fnNextAlertDelay = 0  then
          fnNextAlertDelay := 21;
        if  fnNextAlertDelay > 5  then
          Dec(fnNextAlertDelay, 2);
        ftmAlertAt := IncMinute(Now(), fnNextAlertDelay);
        {-}
        if  dmData.AlertsEnabled  then
          if  not fbInAlertBox  then
            try
              fbInAlertBox := True;
              // MsgBox(sCaption, sMessage, MB_ICONINFORMATION or MB_OK)
              MsgDlgEx(sCaption, sMessage, mtInformation, gsOK, gnOK, nil,
                       180, gcnDefaultTimeMin, 0, 1, -1,
                       TimeoutAlertCreated, TimeoutAlertDestroy)
            finally
              fbInAlertBox := False
            end { try-finally };
      end { display work-out alert }
    else  { update tray icon's hint only }
      begin
        sMessage := Format(_('%s worked out'), [FmtTimeoutStr(tmAlert)]);
        AddTotalTime(sMessage, tmAlertTasks, tmAlertTotal);
        tryIcon.Hint := sMessage
      end { update tray icon's hint only }
  end { CheckAlert };

  procedure  CheckMidnight();
  begin
    if  Now() > gtmEndDay  then
      begin
        gtmStartDay := StartOfTheDay(Today());
        gtmEndDay := EndOfTheDay(Today());
        LoadData()
      end { just after midnight }
  end { CheckMidnight };

  procedure  CheckScreenSaver();

  var
    bScreenSaverIsRunning:  Windows.BOOL;  // LongBool

  begin  { CheckScreenSaver }
    if  dmData.tmTimestamp.Enabled  then
      if  dmData.DetectScreenSaver  then
        if SystemParametersInfo(SPI_GETSCREENSAVERRUNNING, 0,
                                Addr(bScreenSaverIsRunning), 0)  then
          if  fbPrevScreenSaver <> bScreenSaverIsRunning  then
            begin
              dmData.BeginLog();
              try
                dmData.LogEnter(liSession, 'CheckScreenSaver');
                dmData.LogMessage(liSession, 'ScreenSaver is '
                                               + IfThen(bScreenSaverIsRunning, '', 'NOT ')
                                               + 'running now');
                dmData.LogMessage(liSession, 'Data registration is '
                                               + IfThen(dmData.Paused, 'PAUSED', 'active'))

              finally
                dmData.EndLog()
              end { try-finally };
              if  bScreenSaverIsRunning  then
                begin
                  fbResumeRegisterScr := not dmData.Paused;
                  if  fbResumeRegisterScr  then
                    PauseJob()
                end { if bScreenSaverIsRunning }
              else
                begin
                  if  fbResumeRegisterScr  then
                    ResumeJob()
                end { if not bScreenSaverIsRunning };
              fbPrevScreenSaver := bScreenSaverIsRunning;
              dmData.LogExit(liSession, 'CheckScreenSaver')
            end
  end { CheckScreenSaver };

begin  { tmrUpdateTimeTimer }
//   {$WARN SYMBOL_PLATFORM OFF}
//     if  DebugHook <> 0  then
//       Exit;
//   {$WARN SYMBOL_PLATFORM ON}
  if  fpCurrentTask <> nil  then
    begin
      pNode := fpCurrentTask;
      repeat
        if  pNode = edRegister.RootNode  then
          Break;
        edRegister.RepaintNode(pNode);
        pNode := pNode^.Parent
      until  pNode = nil
    end { fpCurrentTask <> nil };
  {-}
  tmYesterday := Yesterday();
  tmDuration := 0;
  tmDurationT := 0;
  tmAlert := 0;
  tmAlertT := 0;
  pNode := edRegister.GetFirst();
  while  pNode <> nil  do
    begin
      with  PNodeData(edRegister.GetNodeData(pNode))^  do
        if  (fNodeType = ntDetail)
            and ((ftmStop <> 0)
                 or (ftmStop = 0) and (ftmStart >= tmYesterday))  then
          begin
            tmStart := ftmStart;
            tmStop := ftmStop;
            if  (tmStop = 0)
                and (pNode = fpCurrentTask)  then
              tmStop := Now();
            if  tmStop <> 0  then
              begin
                tmBuf := DurationTime(tmStart, tmStop, ftmShowFromDate, gtmEndDay);
                tmDuration := tmDuration + tmBuf;
                if  fnTaskID <> 0  then
                  tmDurationT := tmDurationT + tmBuf;
                tmBuf := DurationTime(tmStart, tmStop, gtmStartDay, gtmEndDay);
                tmAlert := tmAlert + tmBuf;
                if  fnTaskID <> 0  then
                  tmAlertT := tmAlertT + tmBuf
              end { tmStop <> 0 }
          end { if };
      pNode := edRegister.GetNext(pNode)
    end { while pNode <> nil };
  {-}
  if  fbComplyNoTask  then
    tmBuf := tmDuration
  else
    tmBuf := tmDurationT;
  sbStatus.Panels[1].Text := DurationToStr(tmBuf, False);
  if  fbComplyNoTask
      or (MinutesBetween(tmDuration, tmDurationT) = 0)  then
    sbStatus.Hint := ''
  else
    sbStatus.Hint := Format(_('Total time: %s'),
                            [DurationToStr(tmDuration, False)]);
  {-}
  CheckAlert(tmAlert, tmAlertT);
  CheckMidnight();
  CheckScreenSaver()
end { tmrUpdateTimeTimer };


procedure  TfJobRegister.tryIconDblClick(Sender:  TObject);
begin
  Visible := not Visible
end { tryIconDblClick };

{$ENDREGION}


initialization
  gtmStartDay := StartOfTheDay(Today());
  gtmEndDay := EndOfTheDay(Today());
  {-}
  FormatSettings.LongDayNames[1] := _('Sunday');
  FormatSettings.LongDayNames[2] := _('Monday');
  FormatSettings.LongDayNames[3] := _('Tuesday');
  FormatSettings.LongDayNames[4] := _('Wednesday');
  FormatSettings.LongDayNames[5] := _('Thursday');
  FormatSettings.LongDayNames[6] := _('Friday');
  FormatSettings.LongDayNames[7] := _('Saturday');
  FormatSettings.LongMonthNames[ 1] := _('January');
  FormatSettings.LongMonthNames[ 2] := _('February');
  FormatSettings.LongMonthNames[ 3] := _('March');
  FormatSettings.LongMonthNames[ 4] := _('April');
  FormatSettings.LongMonthNames[ 5] := _('May');
  FormatSettings.LongMonthNames[ 6] := _('June');
  FormatSettings.LongMonthNames[ 7] := _('July');
  FormatSettings.LongMonthNames[ 8] := _('August');
  FormatSettings.LongMonthNames[ 9] := _('September');
  FormatSettings.LongMonthNames[10] := _('October');
  FormatSettings.LongMonthNames[11] := _('November');
  FormatSettings.LongMonthNames[12] := _('December');


end.
