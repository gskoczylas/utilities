﻿unit  u_Options;

(**********************************************
 *                                            *
 *                Job Register                *
 *     ——————————————————————————————————     *
 *  Copyright © 2008-2024 Grzegorz Skoczylas  *
 *                                            *
 *         mgr Grzegorz Skoczylas             *
 *             tel. 503 064 953               *
 *             gskoczylas@gmail.com           *
 *                                            *
 **********************************************)

{------------------------------------------------------------------------------+
| $Date:: 2016-01-23 23:42:43 +0100 (sob., 23 sty 2016)                       $|
| $Revision:: 2080                                                            $|
| $Author:: Grzegorz                                                          $|
+------------------------------------------------------------------------------}


interface


uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, Buttons, ExtCtrls, Spin, ActnList, ImgList,
  DateUtils, u_BaseForm,
  jvGnugettext;


const
  { ItemIndex of edStartupTask component in fOptions form }
  gcnStartTaskUndefined = 0;
  gcnStartTaskDefault   = 1;
  gcnStartTaskAskFor    = 2;
  gcnStartTaskLastUsed  = 3;


type
  TfOptions = class(TfBaseForm)
    pcOptions:                  TPageControl;
    tsTimes:                    TTabSheet;
    lbTimestamp:                TLabel;
    edTimestamp:                TSpinEdit;
    tsTasks:                    TTabSheet;
    tsStartup:                  TTabSheet;
    edStartupTask:              TRadioGroup;
    edDefaultTask:              TComboBox;
    pnActions:                  TPanel;
    btCancel:                   TBitBtn;
    btOk:                       TBitBtn;
    edTasks:                    TListView;
    ilActions:                  TImageList;
    alActions:                  TActionList;
    aTaskAdd:                   TAction;
    aTaskDelete:                TAction;
    aTaskEdit:                  TAction;
    pnEdit:                     TPanel;
    btTaskAdd:                  TBitBtn;
    btTaskEdit:                 TBitBtn;
    btTaskDelete:               TBitBtn;
    lbWorkingDays:              TLabel;
    edWorkingDays:              TDateTimePicker;
    edSaturdays:                TDateTimePicker;
    lbSaturdays:                TLabel;
    edSundays:                  TDateTimePicker;
    lbSundays:                  TLabel;
    gbNominalWorkingTime:       TGroupBox;
    edComplyNoneTask:           TCheckBox;
    lbLanguage:                 TLabel;
    edLanguage:                 TComboBox;
    edContinuePrevTask:         TCheckBox;
    edContinePrevTaskBreak:     TSpinEdit;
    lbContinuePrevTaskMinutes:  TLabel;
    edDetectScreenSaver:        TCheckBox;
    edShowMainForm:             TCheckBox;
    procedure  FormCreate(Sender:  TObject);
    procedure  btOkClick(Sender:  TObject);
    procedure  edStartupTaskClick(Sender:  TObject);
    procedure  FormShow(Sender:  TObject);
    procedure  pcOptionsChange(Sender:  TObject);
    procedure  edTasksDeletion(Sender:  TObject;
                               Item:  TListItem);
    procedure  aTaskDeleteUpdate(Sender:  TObject);
    procedure  aTaskAddExecute(Sender:  TObject);
    procedure  aTaskDeleteExecute(Sender:  TObject);
    procedure  aTaskEditUpdate(Sender:  TObject);
    procedure  aTaskEditExecute(Sender:  TObject);
    procedure  edTasksInfoTip(Sender:  TObject;
                              Item:  TListItem;
                              var   InfoTip:  string);
    procedure  edContinuePrevTaskClick(Sender:  TObject);
    procedure  edContinePrevTaskBreakChange(Sender:  TObject);
  strict private
    fbTasksChanged:  Boolean;
    procedure  FillDefaultTasks();
  public
    class function  Execute() : Boolean;
  end { TfOptions };


// var
//   fOptions:  TfOptions;


implementation


uses
  zz_GSkDialogs,
  u_Data;


{$R *.dfm}


{$REGION 'TfOptions'}

procedure  TfOptions.aTaskAddExecute(Sender:  TObject);

label
  AskAgain;

var
  sNewTask:  string;
  nInd:  Integer;

begin  { aTaskAddExecute }
  if  edTasks.ItemFocused <> nil  then
    sNewTask := edTasks.ItemFocused.Caption
  else
    sNewTask := '';
AskAgain:
  sNewTask := Trim(sNewTask);
  if  InputQuery(_('Add new task'),
                 _('Name for the task:'),
                 sNewTask)  then
    begin
      sNewTask := Trim(sNewTask);
      if  Trim(sNewTask) = ''  then
        begin
          MsgBox(_('The name for the task cannot be empty'), MB_ICONERROR);
          goto  AskAgain
        end { empty task's name };
      if  Length(sNewTask) > gcnMaxTaskNameLength  then
        begin
          MsgBox(_('The name for the task is too long'), MB_ICONERROR);
          goto  AskAgain
        end { task's name too long };
      sNewTask := ' ' + sNewTask + ' ';
      for  nInd := edTasks.Items.Count - 1  downto  0  do
        begin
          if  SameText(sNewTask, edTasks.Items.Item[nInd].Caption)  then
            begin
              MsgBox(_('The different task already has the same name'),
                     MB_ICONERROR);
              goto  AskAgain
            end { task's name isn't unique }
        end { for nInd };
      {-}
      edTasks.ItemFocused := edTasks.Items.Add();
      edTasks.ItemFocused.Caption := sNewTask;
      fbTasksChanged := True
    end { if InputQuery(...) }
end { aTaskAddExecute };


procedure  TfOptions.aTaskDeleteExecute(Sender:  TObject);
begin
  Assert(edTasks.ItemIndex >= 0);
  Assert(edTasks.ItemFocused <> nil);
  if  MsgBox(_('Are you sure that you want to delete task ''%s''?'),
             [edTasks.ItemFocused.Caption],
             MB_ICONQUESTION or MB_YESNO or MB_DEFBUTTON2) = IDYES  then
    begin
      edTasks.Items.Delete(edTasks.ItemIndex);
      fbTasksChanged := True
    end { if MsgBox(...) = IDYES }
end { aTaskDeleteExecute };


procedure  TfOptions.aTaskDeleteUpdate(Sender:  TObject);
begin
  aTaskDelete.Enabled := edTasks.ItemIndex >= 0
end { aTaskDeleteUpdate };


procedure  TfOptions.aTaskEditExecute(Sender:  TObject);

label
  AskAgain;

var
  sTask:  string;
  nInd:  Integer;

begin  { aTaskEditExecute }
  Assert(edTasks.ItemFocused <> nil);
  sTask := edTasks.ItemFocused.Caption;
AskAgain:
  sTask := Trim(sTask);
  if  InputQuery(_('Edit task'), _('Name for the task:'), sTask)  then
    begin
      sTask := Trim(sTask);
      if  sTask = edTasks.ItemFocused.Caption  then
        begin
          MsgBox(_('The name for the task didn''t change'), MB_ICONWARNING);
          Exit;
        end { task name isn't changed };
      if  Trim(sTask) = ''  then
        begin
          MsgBox(_('The name for the task cannot be empty'), MB_ICONERROR);
          goto  AskAgain
        end { empty task's name };
      if  Length(sTask) > gcnMaxTaskNameLength  then
        begin
          MsgBox(_('The name for the task is too long'), MB_ICONERROR);
          goto  AskAgain
        end { task's name too long };
      sTask := ' ' + sTask + ' ';
      for  nInd := edTasks.Items.Count - 1  downto  0  do
        begin
          if  edTasks.Items.Item[nInd] <> edTasks.ItemFocused  then
            if  SameText(sTask, edTasks.Items.Item[nInd].Caption)  then
              begin
                MsgBox(_('The different task already has the same name'),
                       MB_ICONERROR);
                goto  AskAgain
              end { task's name isn't unique }
        end { for nInd };
      {-}
      edTasks.ItemFocused.Caption := sTask;
      fbTasksChanged := True
    end { if InputQuery(...) }
end { aTaskEditExecute };


procedure  TfOptions.aTaskEditUpdate(Sender:  TObject);
begin
  aTaskEdit.Enabled := edTasks.ItemIndex >= 0
end { aTaskEditUpdate };


procedure  TfOptions.btOkClick(Sender:  TObject);

var
  lTaskList:  TStringList;
  nInd:  Integer;

begin  { btOkClick }
  dmData.NominalWorkTime[dkWorkingDay] := TimeOf(edWorkingDays.Time);
  dmData.NominalWorkTime[dkSaturday] := TimeOf(edSaturdays.Time);
  dmData.NominalWorkTime[dkSunday] := TimeOf(edSundays.Time);
  dmData.WorkTimeWithNoTask := edComplyNoneTask.Checked;
  dmData.DetectScreenSaver := edDetectScreenSaver.Checked;
  dmData.ShowMainFormOnStarup := edShowMainForm.Checked;
  dmData.TimestampEvery := edTimestamp.Value;
  {-}
  lTaskList := TStringList.Create();
  try
    with  edTasks.Items  do
      for  nInd := Count - 1  downto  0  do
        with  Item[nInd]  do
          lTaskList.AddObject(Trim(Caption), TObject(Data));
    dmData.UpdateTasks(lTaskList)
  finally
    lTaskList.Free()
  end { try-finally };
  {-}
  dmData.StartupTaskAction := edStartupTask.ItemIndex;
  if  edDefaultTask.ItemIndex >= 0  then
    dmData.SetDefaultTask(edDefaultTask.Text)
  else
    dmData.DefaultTaskID := 0;
  {-}
  if  edLanguage.ItemIndex = 0  then
    dmData.DefaultLanguageCode := ''
  else
    dmData.DefaultLanguageCode := dmData.LanguageCode[Integer(edLanguage.Items.Objects[edLanguage.ItemIndex])];
  {-}
  dmData.ContinuePreviousTask := edContinuePrevTask.Checked;
  dmData.ContinuePreviousTaskBreak := edContinePrevTaskBreak.Value;
  {-}
  ModalResult := mrOk
end { btOkClick };


procedure  TfOptions.edContinePrevTaskBreakChange(Sender:  TObject);
begin
  inherited;
  lbContinuePrevTaskMinutes.Caption := dngettext('forms',
                                                 'minute', 'minutes',
                                                 edContinePrevTaskBreak.Value)

end { edContinePrevTaskBreakChange };


procedure  TfOptions.edContinuePrevTaskClick(Sender:  TObject);

var
  bActive:  Boolean;

begin  { edContinuePrevTaskClick }
  inherited;
  bActive := edContinuePrevTask.Checked;
  edContinePrevTaskBreak.Enabled := bActive;
  lbContinuePrevTaskMinutes.Enabled := bActive
end { edContinuePrevTaskClick };


procedure  TfOptions.edStartupTaskClick(Sender:  TObject);
begin
  if  edStartupTask.ItemIndex = gcnStartTaskDefault  then
    edDefaultTask.SetFocus()
end { edStartupTaskClick };


procedure  TfOptions.edTasksDeletion(Sender:  TObject;
                                     Item:  TListItem);
begin
  if  Item.Data <> nil  then
    Dispose(PInt64(Item.Data))
end { edTasksDeletion };


procedure  TfOptions.edTasksInfoTip(Sender:  TObject;
                                    Item:  TListItem;
                                    var   InfoTip:  string);
begin
  inherited;
  InfoTip := Item.Caption
end { edTasksInfoTip };


class function  TfOptions.Execute() : Boolean;
begin
  Result := False;
  if  (Screen.ActiveForm <> nil)
      and (Screen.ActiveForm.ClassType() = TfOptions)  then
    Screen.ActiveForm.SetFocus()
  else
    with  TfOptions.Create(nil)  do
      try
        Result := ShowModal() = mrOk
      finally
        Free()
      end { try-finally }
end { Execute };


procedure  TfOptions.FillDefaultTasks();

var
  nInd:  Integer;

begin  { FillDefaultTasks }
  edDefaultTask.Clear();
  with  edTasks.Items  do
    for  nInd := 0  to  Count - 1  do
      with  Item[nInd]  do
        edDefaultTask.AddItem(Trim(Caption), TObject(Data));
  edDefaultTask.ItemIndex := 0
end { FillDefaultTasks };


procedure  TfOptions.FormCreate(Sender:  TObject);
begin
  inherited;
  pcOptions.ActivePageIndex := 0
end { FormCreate };


procedure  TfOptions.FormShow(Sender:  TObject);

var
  nInd:  Integer;
  pnTaskID:  PInt64;

begin  { FormShow }
  inherited;
  edContinePrevTaskBreak.Value := dmData.ContinuePreviousTaskBreak;
  edContinuePrevTask.Checked := dmData.ContinuePreviousTask;
  {-}
  edLanguage.Clear();
  edLanguage.AddItem(_('* Default *'), TObject(-1));
  for  nInd := 0  to  dmData.LanguageCount - 1  do
    begin
      edLanguage.AddItem(dmData.LanguageName[nInd], TObject(nInd));
      if  dmData.LanguageCode[nInd] = dmData.DefaultLanguageCode  then
        edLanguage.ItemIndex := nInd
    end { for nInd };
  if  edLanguage.ItemIndex < 0  then
    edLanguage.ItemIndex := 0;
  {-}
  edWorkingDays.Time := dmData.NominalWorkTime[dkWorkingDay];
  edSaturdays.Time := dmData.NominalWorkTime[dkSaturday];
  edSundays.Time := dmData.NominalWorkTime[dkSunday];
  edComplyNoneTask.Checked := dmData.WorkTimeWithNoTask;
  edDetectScreenSaver.Checked := dmData.DetectScreenSaver;
  edShowMainForm.Checked := dmData.ShowMainFormOnStarup;
  edTimestamp.Value := dmData.TimestampEvery;
  {-}
  Assert(edTasks.Items.Count = 0);   // if not --> clear
  for  nInd := 0  to  dmData.TaskCount - 1  do
    with  edTasks.Items.Add()  do
      begin
        Caption := ' ' + dmData.Task[nInd] + ' ';
        New(pnTaskID);
        pnTaskID^ := dmData.TaskID[nInd];
        Data := pnTaskID
      end { with };
  {-}
  edStartupTask.ItemIndex := dmData.StartupTaskAction;
  {-}
  FillDefaultTasks();
  edDefaultTask.ItemIndex := edDefaultTask.Items.Count - 1;
  while  (edDefaultTask.ItemIndex > 0)
         and (PInt64(edDefaultTask.Items.Objects[edDefaultTask.ItemIndex])^ <> dmData.DefaultTaskID)  do
    edDefaultTask.ItemIndex := edDefaultTask.ItemIndex - 1
end { FormShow };


procedure  TfOptions.pcOptionsChange(Sender:  TObject);

var
  sSelTask:  string;

begin  { pcOptionsChange }
  if  fbTasksChanged
      and (pcOptions.ActivePage = tsStartup)  then
    begin
      if  edTasks.Items.Count = 0  then
        begin
          (edStartupTask.Components[gcnStartTaskDefault] as TRadioButton).Enabled := False;
          edDefaultTask.Enabled := False;
          edDefaultTask.Clear()
        end { no tasks }
      else
        begin
          (edStartupTask.Components[gcnStartTaskDefault] as TRadioButton).Enabled := True;
          edDefaultTask.Enabled := True;
          sSelTask := edDefaultTask.Text;   // name of tasks are unique
          FillDefaultTasks();
          edDefaultTask.ItemIndex := edDefaultTask.Items.IndexOf(sSelTask)
        end { tasks are defined };
      fbTasksChanged := False
    end { fbTasksChanged }
end { pcOptionsChange };

{$ENDREGION 'TfOptions'}


end.
