﻿unit  u_SelectTask;

(**********************************************
 *                                            *
 *                Job Register                *
 *     ——————————————————————————————————     *
 *  Copyright © 2008-2024 Grzegorz Skoczylas  *
 *                                            *
 *         mgr Grzegorz Skoczylas             *
 *             tel. 503 064 953               *
 *             gskoczylas@gmail.com           *
 *                                            *
 **********************************************)

{------------------------------------------------------------------------------+
| $Date:: 2016-01-23 23:42:43 +0100 (sob., 23 sty 2016)                       $|
| $Revision:: 2080                                                            $|
| $Author:: Grzegorz                                                          $|
+------------------------------------------------------------------------------}


interface


uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, ExtCtrls, StdCtrls, Buttons, DB, DBClient, StrUtils,
  ComCtrls, DateUtils, u_BaseForm,
  JvGnugettext;


type
  TfSelectTask = class(TfBaseForm)
    pnSearch:   TPanel;
    pnActions:  TPanel;
    lbSearch:   TLabel;
    edSearch:   TEdit;
    pnButtons:  TPanel;
    btSelect:   TBitBtn;
    btCancel:   TBitBtn;
    sbStatus:   TStatusBar;
    btNone:     TBitBtn;
    pnWhen:     TPanel;
    lbWhen:     TLabel;
    edWhen:     TDateTimePicker;
    edNow:      TCheckBox;
    tmNow:      TTimer;
    lbNow:      TLabel;
    edTasks:    TListView;
    tmWakeUp:   TTimer;
    procedure  edSearchChange(Sender:  TObject);
    procedure  btSelectClick(Sender:  TObject);
    procedure  btNoneClick(Sender:  TObject);
    procedure  edWhenChange(Sender:  TObject);
    procedure  edNowClick(Sender:  TObject);
    procedure  tmNowTimer(Sender:  TObject);
    procedure  FormShow(Sender:  TObject);
    procedure  edTasksDeletion(Sender:  TObject;
                               Item:  TListItem);
    procedure  FormCloseQuery(Sender:  TObject;
                              var   CanClose:  Boolean);
    procedure  edTasksKeyPress(Sender:  TObject;
                               var   Key:  Char);
    procedure  edSearchKeyDown(Sender:  TObject;
                               var   Key:  Word;
                               Shift:  TShiftState);
    procedure  edSearchKeyUp(Sender:  TObject;
                             var   Key:  Word;
                             Shift:  TShiftState);
    procedure  tmWakeUpTimer(Sender:  TObject);
  strict private
    fnSelected:  Int64;
    function  GetWhen() : TDateTime;
    procedure  CheckWhen();
  private
    fbConfirm:   Boolean;
    ftmWhen:     TDateTime;
  public
    class function  Execute(var   nTaskID:  Int64;
                            var   tmWhen:  TDateTime;
                            const   bAskWhen, bAllowCancel, bConfirm:  Boolean)
                            : Boolean;
    property  Selected:  Int64
              read  fnSelected;
    property  When:  TDateTime
              read  GetWhen;
  end { TfSelectTask };


// var
//   fSelectTask:  TfSelectTask;


implementation


uses
  zz_GSkDialogs,
  u_Data, u_MainForm;


{$R *.dfm}


{$REGION 'TfSelectTask'}

procedure  TfSelectTask.btNoneClick(Sender:  TObject);
begin
  dmData.LogEnter(liTask, 'TfSelectTask.btNoneClick');
  CheckWhen();
  if  (edTasks.Items.Count = 0)
      or not fbConfirm  then
    ModalResult := mrNo
  else
    if  MsgBox(_('Are you sure that you want to switch over for no-task?'),
               MB_ICONQUESTION or MB_YESNO) = IDYES  then
      ModalResult := mrNo;
  dmData.LogExit(liTask, 'TfSelectTask.btNoneClick')
end { btNoneClick };


procedure  TfSelectTask.btSelectClick(Sender:  TObject);

var
  lItem:  TListItem;

begin  { btSelectClick }
  dmData.LogEnter(liTask, 'TfSelectTask.btSelectClick');
  Assert(edTasks.ItemIndex >= 0);
  CheckWhen();
  with  edTasks  do
    lItem := Items.Item[ItemIndex];
  if  not fbConfirm
      or (MsgBox(_('Are you sure that you want to switch over for the ''%s'' task?'),
                 [Trim(lItem.Caption)],
                 MB_ICONQUESTION or MB_YESNO) = IDYES)  then
    begin
      fnSelected := PInt64(lItem.Data)^;
      ModalResult := mrOk
    end { IDYES }
  else
    Abort();
  dmData.LogExit(liTask, 'TfSelectTask.btSelectClick')
end { btOkClick };


procedure  TfSelectTask.CheckWhen();

var
  tmWhen:  TDateTime;

begin  { CheckWhen }
  if  not edNow.Checked  then
    begin
      tmWhen := When;
      if  (tmWhen < dmData.StartOfJob)
          and (MinutesBetween(tmWhen, dmData.StartOfJob) <> 0)  then
        begin
          dmData.LogMessage(liTask, 'When = '
                                      + FormatDateTime('yyyy-mm-dd hh:nn:ss.zzz',
                                                       tmWhen));
          dmData.LogMessage(liTask, 'StartOfJob = '
                                      + FormatDateTime('yyyy-mm-dd hh:nn:ss.zzz',
                                                       dmData.StartOfJob));
          MsgBox(_('Start time can''t be earlier than %s'),
                 [FormatDateTime('h:nn', dmData.StartOfJob)],
                 MB_ICONERROR or MB_OK);
          Abort()
        end { tmWhen < dmAppData.StartTime };
      if  tmWhen > Now()  then
        if  MsgBox(_('You selected time after now!'),
                   MB_ICONWARNING or MB_OKCANCEL or MB_DEFBUTTON2) <> IDOK  then
          Abort()
    end { not edNow.Checked }
end { CheckWhen };


procedure  TfSelectTask.edNowClick(Sender:  TObject);
begin
  if  not edNow.Checked  then
    if  pnWhen.Visible  then
      edWhen.SetFocus()
end { edNowClick };


procedure  TfSelectTask.edSearchChange(Sender:  TObject);

var
  lItem:  TListItem;
  nInd:   Integer;
  sFind:  string;
  nLen:   Integer;
  bFound: Boolean;

begin  { edSearchChange }
  sFind := ' ' + Trim(edSearch.Text);
  nLen := Length(sFind);
  bFound := False;
  lItem := nil;
  with  edTasks.Items  do
    for  nInd := 0  to  Count - 1  do    // ascending!
      begin
        lItem := Item[nInd];
        bFound := AnsiSameText(sFind, Copy(lItem.Caption, 1, nLen));
        if  bFound  then
          Break;
      end { for nInd };
  if  bFound  then
    begin
      sbStatus.SimpleText := '';
      edTasks.ItemFocused := lItem;
      edTasks.ItemIndex := lItem.Index
    end { bFound }
  else
    begin
      sbStatus.SimpleText := _('Not found');   // task not found
      edTasks.ItemFocused := nil;
      edTasks.ItemIndex := -1;
      MessageBeep(MB_ICONINFORMATION)
    end { not bFound }
end { edSearchChange };


procedure  TfSelectTask.edSearchKeyDown(Sender:  TObject;
                                        var   Key:  Word;
                                        Shift:  TShiftState);
begin
  inherited;
  if  Shift = []  then
    if  (Key = VK_DOWN)
        or (Key = VK_UP)  then
      begin
        edTasks.Perform(WM_KEYDOWN, Key, 0);
        Key := 0
      end { [Up], [Down] }
end { edSearchKeyDown };


procedure  TfSelectTask.edSearchKeyUp(Sender:  TObject;
                                      var   Key:  Word;
                                      Shift:  TShiftState);
begin
  inherited;
  if  Shift = []  then
    if  (Key = VK_DOWN)
        or (Key = VK_UP)  then
      begin
        edTasks.Perform(WM_KEYUP, Key, 0);
        Key := 0;
        edTasks.SetFocus()
      end { [Up], [Down] }
end { edSearchKeyUp };


procedure  TfSelectTask.edTasksDeletion(Sender:  TObject;
                                        Item:  TListItem);
begin
  Dispose(PInt64(Item.Data))
end { edTasksDeletion };


procedure  TfSelectTask.edTasksKeyPress(Sender:  TObject;
                                        var   Key:  Char);
begin
  inherited;
  edSearch.Clear();
  edSearch.SetFocus();
  edSearch.Perform(WM_CHAR, Ord(Key), 0);
  Key := #0
end { edTasksKeyPress };


procedure  TfSelectTask.edWhenChange(Sender:  TObject);
begin
  edNow.Checked := False
end { edWhenChange };


class function  TfSelectTask.Execute(var   nTaskID:  Int64;
                                     var   tmWhen:  TDateTime;
                                     const   bAskWhen, bAllowCancel, bConfirm:  Boolean)
                                     : Boolean;
begin
  with  TfSelectTask.Create(Application)  do
    try
      pnWhen.Visible := bAskWhen;
      ftmWhen := tmWhen;
      btCancel.Enabled := bAllowCancel;
      fbConfirm := bConfirm;
      {-}
      case  ShowModal()  of
        mrNo:
          begin
            nTaskID := 0;
            Result := True;
          end { mrNo };
        mrOk:
          begin
            nTaskID := Selected;
            Result := True
          end { mrOk };
        else
          Result := False
      end { case ShowModal() };
      tmWhen := When
    finally
      Free()
    end { try-finally }
end { Execute };


procedure  TfSelectTask.FormCloseQuery(Sender:  TObject;
                                       var   CanClose:  Boolean);
begin
  CanClose := (ModalResult <> mrCancel)
              or btCancel.Enabled
end { FormCloseQuery };


procedure  TfSelectTask.FormShow(Sender:  TObject);

var
  pnTaskID:  PInt64;
  lItem:     TListItem;
  nInd:  Integer;

begin  { FormShow }
  inherited;
  edWhen.DateTime := ftmWhen;
  edNow.Checked := pnWhen.Visible;
  {-}
  edTasks.Items.BeginUpdate();
  try
    edTasks.Clear();
    for  nInd := dmData.TaskCount - 1  downto  0  do
      begin
        lItem := edTasks.Items.Add();
        lItem.Caption := ' ' + dmData.Task[nInd];
        New(pnTaskID);
        pnTaskID^ := dmData.TaskID[nInd];
        lItem.Data := pnTaskID;
        {-}
        if  pnTaskID^ = dmData.CurrentTaskID  then
          edTasks.ItemFocused := lItem
      end { for nInd }
  finally
    edTasks.Items.EndUpdate()
  end { try-finally };
  {-}
  if  edTasks.ItemFocused <> nil  then
    edTasks.ItemIndex := edTasks.ItemFocused.Index
  else if  edTasks.ItemIndex < 0  then
    edTasks.ItemIndex := 0;
  btSelect.Enabled := edTasks.Items.Count > 0;
  tmNowTimer(nil)
end { FormShow };


function  TfSelectTask.GetWhen() : TDateTime;
begin
  Result := Today() + TimeOf(edWhen.Time)
end { GetWhen };


procedure  TfSelectTask.tmNowTimer(Sender:  TObject);

var
  tmNow:  TDateTime;

begin  { tmNowTimer }
  tmNow := Time();
  lbNow.Caption := TimeToStr(tmNow);
  if  edNow.Checked  then
    edWhen.Time := tmNow
end { tmNowTimer };


procedure  TfSelectTask.tmWakeUpTimer(Sender:  TObject);
begin
  inherited;
  SetForegroundWindow(Handle);
  BringWindowToTop(Handle)
end { tmWakeUpTimer };

{$ENDREGION 'TfSelectTask'}


end.
