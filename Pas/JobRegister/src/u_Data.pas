﻿unit  u_Data;

(**********************************************
 *                                            *
 *                Job Register                *
 *     ——————————————————————————————————     *
 *  Copyright © 2008-2024 Grzegorz Skoczylas  *
 *                                            *
 *         mgr Grzegorz Skoczylas             *
 *             tel. 503 064 953               *
 *             gskoczylas@gmail.com           *
 *                                            *
 **********************************************)

{------------------------------------------------------------------------------+
| $Date:: 2016-01-23 23:42:43 +0100 (sob., 23 sty 2016)                       $|
| $Revision:: 2080                                                            $|
| $Author:: Grzegorz                                                          $|
+------------------------------------------------------------------------------}


interface


uses
  SysUtils, Classes, Forms, Windows, IBDatabase, DB, IBSQL, DateUtils,
  ExtCtrls, IniFiles, Math, StrUtils,
  madExcept,
  JvGnugettext,
  zz_GSkLog, zz_GSkLogFile, zz_GSkLanguageCodes;


const
  gcnMSecsPerSecond = Int64(1000);
  gcnMSecsPerMinute = gcnMSecsPerSecond * 60{ seconds per minute };
  gcnMSecsPerHour   = gcnMSecsPerMinute * 60{ minutes per hour   };
  gcnMSecsPerDay    = gcnMSecsPerHour   * 24{ hours per day      };
  { ItemIndex of edStartupTask component in fOptions form }
  gcnStartTaskUndefined = 0;
  gcnStartTaskDefault   = 1;
  gcnStartTaskAskFor    = 2;
  gcnStartTaskLastUsed  = 3;
  {-}
  gcnMaxTaskNameLength = 250;   // database field's size


type
  TDayKind = (dkWorkingDay, dkSaturday, dkSunday);
  TCommentsVisible = (cvAlways, cvAutomatic, cvOnRequest);
  TLogInfo = (liProgram, liSQL, liTask, liSession, liOptions);
  TLogInfos = set of  TLogInfo;

  TQuerySQL = class(TIBSQL)
  public
    constructor  Create(AOwner:  TComponent);                        override;
    destructor  Destroy();                                           override;
    {-}
    function  InTransaction() : Boolean;
    procedure  Rollback();
  end { TQuerySQL };

  TdmData = class(TDataModule)
    dbDatabase:   TIBDatabase;
    tmTimestamp:  TTimer;
    procedure  DataModuleCreate(Sender:  TObject);
    procedure  DataModuleDestroy(Sender:  TObject);
    procedure  tmTimestampTimer(Sender:  TObject);
    procedure  dbDatabaseAfterConnect(Sender:  TObject);
    procedure  dbDatabaseBeforeDisconnect(Sender:  TObject);
    procedure  dbDatabaseBeforeConnect(Sender:  TObject);
  private
    var
      fLog:           TGSkLog;
  strict private
    var
      ftmStart:       TDateTime;
      fnTaskID:       Int64;
      fbPaused:       Boolean;
      fnTimeStamp:    Integer;
      fTasks:         TStringList;
      fnStartTskAct:  Integer;
      fnDefTaskID:    Int64;
      fbAlertEnbl:    Boolean;
      fbFirstTime:    Boolean;
      fnGrpLevels:    Integer;
      fNominalWorkTm: array[TDayKind] of  TDateTime;
      fComments:      TCommentsVisible;
      fbWithNoTask:   Boolean;
      fbDetectScrSvr: Boolean;
      fbShowMainForm: Boolean;
      fLangCodes:     TStringList;
      fLangNames:     TStringList;
      fsDefLangCode:  string;
      fLogInfo:       TLogInfos;
    function  TaskIdToStr(const   nTaskID:  Int64)
                          : string;
    function  DateTimeToStr(const   tmValue:  TDateTime)
                            : string;
    function  GetParamStr(const   sParamName:  string;
                          const   sDefault:  string = '')
                          : string;
    function  GetParamInt(const   sParamName:  string;
                          const   nDefault:  Int64 = 0)
                          : Int64;
    function  GetParamTime(const   sParamName:  string;
                           const   tmDefault:  TDateTime = 0.0)
                           : TDateTime;
    function  GetParamBool(const   sParamName:  string;
                           const   bDefault:  Boolean = False)
                           : Boolean;
    function  GetTaskCount() : Integer;
    function  GetTask(const   nIndex:  Integer)
                      : string;
    function  GetTaskID(const   nIndex:  Integer)
                        : Int64;
    function  GetNominalWorkTime(const   Index:  TDayKind)
                                 : TDateTime;
    function  GetCurrentNominalWorkTime() : TDateTime;
    function  GetLanguageCount() : Integer;
    function  GetLanguageCode(const   nIndex:  Integer)
                              : string;
    function  GetLanguageName(const   nIndex:  Integer)
                              : string;
    function  GetContinuePreviousTask() : Boolean;
    function  GetContinuePreviousTaskBreak() : Integer;
    procedure  SetParamStr(const   sParamName, sParamValue:  string);
    procedure  SetParamInt(const   sParamName:  string;
                           const   nParamValue:  Int64);
    procedure  SetParamTime(const   sParamName:  string;
                            const   tmParamValue:  TDateTime);
    procedure  SetParamBool(const   sParamName:  string;
                            const   bParamValue:  Boolean);
    procedure  SetNominalWorkTime(const   Index:  TDayKind;
                                  const   tmTime:  TDateTime);
    procedure  SetTimestampEvery(const   nMinutes:  Integer);
    procedure  SetCurrentTaskID(const   nTaskID:  Int64);
    procedure  SetStartupTaskAction(const   nActionNo:  Integer);
    procedure  SetDefTaskID(const   nTaskID:  Int64);
    procedure  SetGroupingLevels(const   nLevels:  Integer);
    procedure  SetCommentsVisible(const   Value:  TCommentsVisible);
    procedure  SetWithNoTask(const   Value:  Boolean);
    procedure  SetDefLangCode(const   Value:  string);
    procedure  SetContinuePreviousTask(const   Value:  Boolean);
    procedure  SetContinuePreviousTaskBreak(const   Value:  Integer);
    procedure  SetDetectScreenSaver(const   Value:  Boolean);
    procedure  SetShowMainFormOnStartup(const   Value:  Boolean);
    procedure  DeleteTaks(const   nIndex:  Integer;
                          const   bUpdateDatabase:  Boolean);
    procedure  ClearTasks();
    procedure  LoadTasks();
  private
    function  StartTransaction() : TIBTransaction;
  public
    function  ExecQuery(const   sStatement:  string)
                        : TQuerySQL;                                 overload;
    function  ExecQuery(const   sStmtFmt:  string;
                        const   StmtArgs:  array of const)
                        : TQuerySQL;                                 overload;
    procedure  ExecSQL(const   sStatement:  string);                 overload;
    procedure  ExecSQL(const   sStmtFmt:  string;
                       const   StmsArgs:  array of const);           overload;
    procedure  RegisterStart(const   tmStart:  TDateTime;
                             const   nTaskID:  Int64 = 0);
    procedure  UpdateStop(const   tmStart, tmStop:  TDateTime);
    procedure  UpdateTask(const   tmStart:  TDateTime;
                          const   nTaskID:  Int64);
    procedure  UpdateTasks(const   TaskList:  TStringList);
    procedure  SetDefaultTask(const   sTaskName:  string);
    procedure  PauseJob();
    procedure  ResumeJob();
    procedure  ResetTimestamp();
    {-}
    procedure  BeginLog();
    procedure  EndLog();
    procedure  AbandonLog();
    procedure  LogEnter(const   liCategory:  TLogInfo;
                        const   sInfo:  string;
                        const   lmMsgType:  TGSkLogMessageType = lmInformation); overload;
    procedure  LogEnter(const   liCategory:  TLogInfo;
                        const   sInfoFmt:  string;
                        const   InfoArgs:  array of const;
                        const   lmMsgType:  TGSkLogMessageType = lmInformation); overload;
    procedure  LogExit(const   liCategory:  TLogInfo;
                       const   sInfo:  string;
                       const   lmMsgType:  TGSkLogMessageType = lmInformation);
    procedure  LogValue(const   liCategory:  TLogInfo;
                        const   sName:  string;
                        const   vValue:  Variant;
                        const   lmMsgType:  TGSkLogMessageType = lmInformation); overload;
    procedure  LogValue(const   liCategory:  TLogInfo;
                        const   sName:  string;
                        const   nValue:  Int64;
                        const   ValType:  TGSkLogValueNumType = vtDec;
                        const   lmMsgType:  TGSkLogMessageType = lmInformation); overload;
    procedure  LogMessage(const   liCategory:  TLogInfo;
                          const   sMsg:  string;
                          const   lmMsgType:  TGSkLogMessageType = lmInformation); overload;
    procedure  LogMessage(const   liCategory:  TLogInfo;
                          const   sMsgFmt:  string;
                          const   MsgArgs:  array of const;
                          const   lmMsgType:  TGSkLogMessageType = lmInformation); overload;
    {-}
    property  StartOfJob:  TDateTime
              read  ftmStart;
    property  CurrentTaskID:  Int64
              read  fnTaskID
              write SetCurrentTaskID;
    property  Paused:  Boolean
              read  fbPaused;
    property  NominalWorkTime[const   dkIndex:  TDayKind] : TDateTime
              read  GetNominalWorkTime
              write SetNominalWorkTime;
    property  CurrentNominalWorkTime:  TDateTime
              read  GetCurrentNominalWorkTime;
    property  TimestampEvery:  Integer
              read  fnTimeStamp
              write SetTimestampEvery;
    property  TaskCount:  Integer
              read  GetTaskCount;
    property  Task[const   nIndex:  Integer] : string
              read  GetTask;
    property  TaskID[const   nIndex:  Integer] : Int64
              read  GetTaskID;
    property  TaskList:  TStringList
              read  fTasks;
    property  StartupTaskAction:  Integer
              read  fnStartTskAct
              write SetStartupTaskAction;
    property  DefaultTaskID:  Int64
              read  fnDefTaskID
              write SetDefTaskID;
    property  AlertsEnabled:  Boolean
              read  fbAlertEnbl
              write fbAlertEnbl;
    property  GroupingLevels:  Integer
              read  fnGrpLevels
              write SetGroupingLevels;
    property  CommentsVisible:  TCommentsVisible
              read  fComments
              write SetCommentsVisible;
    property  WorkTimeWithNoTask:  Boolean
              read  fbWithNoTask
              write SetWithNoTask;
    property  DetectScreenSaver:  Boolean
              read  fbDetectScrSvr
              write SetDetectScreenSaver;
    property  ShowMainFormOnStarup:  Boolean
              read  fbShowMainForm
              write SetShowMainFormOnStartup;
    property  LanguageCount:  Integer
              read  GetLanguageCount;
    property  LanguageCode[const   nIndex:  Integer] : string
              read  GetLanguageCode;
    property  LanguageName[const   nIndex:  Integer] : string
              read  GetLanguageName;
    property  DefaultLanguageCode:  string
              read  fsDefLangCode
              write SetDefLangCode;
    property  ContinuePreviousTask:  Boolean
              read  GetContinuePreviousTask
              write SetContinuePreviousTask;
    property  ContinuePreviousTaskBreak:  Integer
              read  GetContinuePreviousTaskBreak
              write SetContinuePreviousTaskBreak;
  end { TdmData };


var
  dmData:  TdmData;
  {-}
  gsOK:  array[0..0] of  string;
  gnOK:  array[0..0] of  Integer;


function  DurationTime(const   tmStart, tmStop:  TDateTime)
                       : TDateTime;                                  overload;

function  DurationTime({const} tmStart, tmStop:  TDateTime;
                       const   tmRangeBegin, tmRangeEnd:  TDateTime)
                       : TDateTime;                                  overload;

function  DurationToStr(const   tmDuration:  TDateTime;
                        const   bWithSeconds:  Boolean)
                        : string;

procedure  Error(const   sAction:  string;
                 const   eError:  Exception);


implementation


uses
  zz_GSkDialogs,
  u_BaseForm, u_MainForm;


{$R *.dfm}


const
  gcsParNominalWOrkTime:  array[TDayKind] of  string = (
      'Work Time Working Days',   // dkWorkingDay
      'Work Time Saturdays',      // dkSaturday
      'Work Time Sundays');       // dkSunday
  gcsParTimestampEvery      = 'Timestamp Every';
  gcsParTimestamp           = 'Timestamp';
  gcsParTaskID              = 'Task ID';
  gcsParTaskStart           = 'Task Start';
  gcsParStartupTaskAct      = 'Startup Task Action';
  gcsParDefaultTask         = 'Default Task';
  gcsParGroupingLevels      = 'Grouping Levels';
  gcsParCommentsVisible     = 'Comments Visible';
  gcsParWorkWithoutTask     = 'Inc. Reg. Without Task';
  gcsParDetectScreenSaver   = 'Detect Screen Saver';
  gcsParDefaultLanguage     = 'Default Language';
  gcsParContPrevTask        = 'Continue Previous Task';
  gcsParContPrevTaskBrak    = 'Continue Task Break';
  gcsParStartupShowMainForm = 'Show Main Form';
  {-}
  gcnStdTimestampEvery    = 10;   // minutes
  gcnStdGroupLevels       = 2;
  gcnStdStarupTaskAction  = gcnStartTaskLastUsed;
  gcbStdWorkWithoutTask   = True;
  gcbStdDetectScreenSaver = True;
  gcbStdContPrevTask      = False;
  gcbStdStartShowMainForm = False;
  gcnStdContPrevTaskBreak = 5;
  gcStdCommentsStatus     = cvAutomatic;


{$IfOpt C+ }

var
  gAssertErrorProc:  TAssertErrorProc;


procedure  AssertError(const   sMessage, sFilename:  string;
                       nLineNumber:  Integer;
                       pErrorAddr:  Pointer);
begin
  if  dmData <> nil  then
    if  dmData.fLog <> nil  then
      dmData.fLog.LogMessage('BŁĄD NIEZMIENNIKA w %s, wiersz %d: %s',
                             [sFilename, nLineNumber, sMessage],
                             lmError);
  gAssertErrorProc(sMessage, sFilename, nLineNumber, pErrorAddr)
end { AssertError };

{$EndIf C+ }


procedure  madExceptHandler(const   iExcept:  IMEException;
                            var   bHandled:  Boolean);
begin
  if  dmData <> nil  then
    if  dmData.fLog <> nil  then
      dmData.fLog.LogMemo(iExcept.BugReport, lmError)
end { madExceptHandler };


procedure  Error(const   sAction:  string;
                 const   eError:  Exception);
begin
  MsgBox(Format(_('Action: %s'#13#10#13#10'Exception ''%s'':'#13#10'%s'),
                [sAction, eError.ClassName(), eError.Message]),
         MB_ICONERROR or MB_OK)
end { Error };


function  DurationTime(const   tmStart, tmStop:  TDateTime)
                       : TDateTime;
var
  nDays, nHours, nMinutes, nSeconds:  Word;
  nTmp:  Int64;

begin  { DurationTime }
  nTmp := SecondsBetween(tmStart, tmStop);
  nSeconds := nTmp mod 60;   nTmp := nTmp div 60;
  nMinutes := nTmp mod 60;   nTmp := nTmp div 60;
  nHours := nTmp mod 24;
  nDays := nTmp div 24;
  Result := nDays
            + EncodeTime(nHours, nMinutes, nSeconds, 0)
end { DurationTime };


function  DurationTime({const} tmStart, tmStop:  TDateTime;
                       const   tmRangeBegin, tmRangeEnd:  TDateTime)
                       : TDateTime;
begin
  try
    dmData.BeginLog();
    dmData.LogEnter(liSession,
                    'DurationTime([%s], [%s], [%s], [%s])',
                    [DateTimeToStr(tmStart), DateTimeToStr(tmStop),
                     DateTimeToStr(tmRangeBegin), DateTimeToStr(tmRangeEnd)]);
    if  (tmStop < tmRangeBegin)
        or (tmStart > tmRangeEnd)  then
      Result := 0
    else
      begin
        if  tmStart < tmRangeBegin  then
          tmStart := tmRangeBegin;
        if  tmStop > tmRangeEnd  then
          tmStop := tmRangeEnd;
        dmData.LogValue(liSession, 'tmStart', tmStart);
        dmData.LogValue(liSession, 'tmStop', tmStop);
        Assert(tmStart <= tmStop);
        Result := DurationTime(tmStart, tmStop);
        dmData.LogValue(liSession, 'Result', Result);
        dmData.LogExit(liSession, 'DurationTime');
        dmData.AbandonLog()   // ta procedura jest wywoływana często; nie rejstruję jeżeli nie ma błędu
      end { in range };
  except
    dmData.LogExit(liSession, 'DurationTime');
    dmData.EndLog();
    raise
  end { try-except }
end { DurationTime };


function  DurationToStr(const   tmDuration:  TDateTime;
                        const   bWithSeconds:  Boolean)
                        : string;
var
  nDays, nHours, nMinutes, nSeconds, nMilliseconds:  Word;

begin  { DurationToStr }
  nDays := Trunc(tmDuration);
  DecodeTime(tmDuration, nHours, nMinutes, nSeconds, nMilliseconds);
  Inc(nHours, nDays * 24);
  { Format result }
  if  bWithSeconds
      or (nHours = 0)
         and (nMinutes = 0)  then
    Result := Format('%2.2d:%2.2d:%2.2d',
                     [nHours, nMinutes, nSeconds])
  else
    Result := Format('%2.2d:%2.2d', [nHours, nMinutes])
end { DurationToStr };


{ TQuerySQL }

constructor  TQuerySQL.Create(AOwner:  TComponent);
begin
  inherited;
  Database := dmData.dbDatabase;
  Transaction := dmData.StartTransaction()
end { Create };


destructor  TQuerySQL.Destroy();
begin
  if  Transaction.InTransaction  then
    Transaction.Commit();
  Transaction.Free();
  inherited;
end { Destroy };


function  TQuerySQL.InTransaction() : Boolean;
begin
  Result := Transaction.InTransaction
end { InTransaction };


procedure  TQuerySQL.Rollback();
begin
  Transaction.Rollback()
end { Rollback };


{ TdmData }

procedure  TdmData.AbandonLog();
begin
  fLog.AbandonLog()
end { AbandonLog };


procedure  TdmData.BeginLog();
begin
  fLog.BeginLog()
end { BeginLog };


procedure  TdmData.ClearTasks();
begin
  while  fTasks.Count > 0  do
    DeleteTaks(0, False)
end { ClearTasks };


procedure  TdmData.DataModuleCreate(Sender:  TObject);

var
  lLogFile:  TGSkLogFile;
  sLogFile:  string;

  procedure  InitLanguages();

  var
    sCurrLang:  string;
    nInd:       Integer;
    sLangCode:  string;

    function  LangName(const   sLangCode:  string)
                       : string;
    begin
      Result := zz_GSkLanguageCodes.GetLanguageName(sLangCode);
      if  Result = ''  then
        Result := sLangCode;
      Result := dgettext('languages', Result)
    end { LangName };

  begin  { InitLanguages }
    { Get languages }
    DefaultInstance.GetListOfLanguages('default', fLangCodes);
    if  fLangCodes.IndexOf('en') < 0  then
      fLangCodes.Add('en');
    fLangCodes.Sort();
    sCurrLang := GetCurrentLanguage();
    if  fLangCodes.IndexOf(sCurrLang) < 0  then
      sCurrLang := LeftStr(sCurrLang, 2);
    for  nInd := 0  to  fLangCodes.Count - 1  do
      begin
        sLangCode := fLangCodes[nInd];
        fLangNames.Add(dgettext('languages', LangName(sLangCode)))
      end { for nInd }
  end { InitLanguages };

begin  { DataModuleCreate }
  fLog := TGSkLog.Create(Self);
  lLogFile := TGSkLogFile.Create(fLog);
  {-}
  fbFirstTime := True;
  fbAlertEnbl := True;
  fTasks := TStringList.Create();
  fTasks.Sorted := True;
  Assert(not fTasks.CaseSensitive);   // should not be case sensitive
  {-}
  fLangCodes := TStringList.Create();
  fLangNames := TStringList.Create();
  InitLanguages();
  {-}
  try
    with  TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'))  do
      try
        if  ReadBool('Log', 'Monitor', False)  then
          begin
            {$if Declared(TGSkLogTraceTool)}
              TGSkLogTraceTool.Create(fLog);
            {$ifEnd}
            {$if Declared(TGSkLogGExperts)}
              TGSkLogGExperts.Create(fLog);
            {$ifEnd}
          end { if ReadBool('Monitor') };
        {-}
        dbDatabase.DatabaseName := ReadString('Parameters', 'DbPath',
                                              dbDatabase.DatabaseName);
        sLogFile := ReadString('Log', 'File', '');
        if  sLogFile <> ''  then
          lLogFile.FileName := sLogFile;
        {-}
        fLogInfo := [];
        if  ReadBool('Log', 'Program', False)  then
          Include(fLogInfo, liProgram);
        if  ReadBool('Log', 'SQL', False)  then
          Include(fLogInfo, liSQL);
        if  ReadBool('Log', 'Task', False)  then
          Include(fLogInfo, liTask);
        if  ReadBool('Log', 'Session', False)  then
          Include(fLogInfo, liSession);
        if  ReadBool('Log', 'Options', False)  then
          Include(fLogInfo, liOptions)
      finally
        Free()
      end { try-finally }
  except
    { OK }
  end { try-except };
  {-}
  try
    dbDatabase.Open()
  except
    on  eErr : Exception  do
      begin
        fLog.LogException(eErr, 'Opening database ' + dbDatabase.DatabaseName);
        Error(Format(_('Opening database %s'),
                     [dbDatabase.DatabaseName]),
              eErr);
        Application.Terminate();
        Abort()
      end { on Exception }
  end { try-except }
end { DataModuleCreate };


procedure  TdmData.DataModuleDestroy(Sender:  TObject);
begin
  LogEnter(liProgram, 'TdmData.DataModuleDestroy');
  if  dbDatabase.Connected  then
    begin
      if  not Paused  then
        UpdateStop(StartOfJob, Now());
      dbDatabase.Close();
      ClearTasks()
    end { dbDatabase.Connected };
  fTasks.Free();
  fLangCodes.Free();
  fLangNames.Free();
  LogExit(liProgram, 'TdmData.DataModuleDestroy')
end { DataModuleDestroy };


function  TdmData.DateTimeToStr(const   tmValue:  TDateTime)
                                : string;
begin
  // Result := FormatDateTime('yyyy-mm-dd hh:nn:ss.zzz', tmValue)
  Result := FormatDateTime('yyyy-mm-dd hh:nn:ss', tmValue)
end { DateTimeToStr };


procedure  TdmData.dbDatabaseAfterConnect(Sender:  TObject);
begin
  LogMessage(liProgram, 'Database connected.');
  LogEnter(liProgram, 'TdmData.dbDatabaseAfterConnect');
  {-}
  fNominalWorkTm[dkWorkingDay] := GetParamTime(gcsParNominalWorkTime[dkWorkingDay],
                                               EncodeTime(8, 0, 0, 0));
  fNominalWorkTm[dkSaturday] := GetParamTime(gcsParNominalWorkTime[dkSaturday],
                                             EncodeTime(6, 0, 0, 0));
  fNominalWorkTm[dkSunday] := GetParamTime(gcsParNominalWorkTime[dkSunday],
                                           EncodeTime(1, 0, 0, 0));
  fnTimeStamp := GetParamInt(gcsParTimestampEvery, gcnStdTimestampEvery);
  fnTaskID := GetParamInt(gcsParTaskID);
  fnStartTskAct := GetParamInt(gcsParStartupTaskAct, gcnStdStarupTaskAction);
  fnDefTaskID := GetParamInt(gcsParDefaultTask);
  fnGrpLevels := GetParamInt(gcsParGroupingLevels, gcnStdGroupLevels);
  fComments := TCommentsVisible(EnsureRange(GetParamInt(gcsParCommentsVisible,
                                                        Ord(gcStdCommentsStatus)),
                                            Ord(Low(TCommentsVisible)),
                                            Ord(High(TCommentsVisible))));
  fbWithNoTask := GetParamBool(gcsParWorkWithoutTask, gcbStdWorkWithoutTask);
  fbDetectScrSvr := GetParamBool(gcsParDetectScreenSaver, gcbStdDetectScreenSaver);
  fbShowMainForm := GetParamBool(gcsParStartupShowMainForm, gcbStdStartShowMainForm);
  fsDefLangCode := GetParamStr(gcsParDefaultLanguage);
  {-}
  LoadTasks();
  {-}
  tmTimestamp.Interval := TimestampEvery * gcnMSecsPerMinute;
  // tmTimestamp.Enabled := True;
     Assert(not tmTimestamp.Enabled);
  {-}
  LogExit(liProgram, 'TdmData.dbDatabaseAfterConnect')
end { dbDatabaseAfterConnect };


procedure  TdmData.dbDatabaseBeforeConnect(Sender:  TObject);
begin
  LogMessage(liProgram, 'Connecting database ' + dbDatabase.DatabaseName + '...')
end { dbDatabaseBeforeConnect };


procedure  TdmData.dbDatabaseBeforeDisconnect(Sender:  TObject);
begin
  LogMessage(liProgram, 'TdmData.dbDatabaseBeforeDisconnect');
  tmTimestamp.Enabled := False
end { dbDatabaseBeforeDisconnect };


procedure  TdmData.DeleteTaks(const   nIndex:  Integer;
                              const   bUpdateDatabase:  Boolean);
begin
  LogEnter(liTask, 'TdmData.DeleteTaks(%d, %s)',
           [nIndex, BoolToStr(bUpdateDatabase, True)]);
  if  bUpdateDatabase  then
    ExecSQL('DELETE FROM t_Tasks WHERE nTaskID = %d',
            [PInt64(fTasks.Objects[nIndex])^]);
  Dispose(PInt64(fTasks.Objects[nIndex]));
  fTasks.Delete(nIndex);
  LogExit(liTask, 'TdmData.DeleteTaks')
end { DeleteTaks };


function  TdmData.ExecQuery(const   sStatement:  string)
                            : TQuerySQL;
var
  nInd:  Integer;

begin  { ExecQuery }
  Result := TQuerySQL.Create(Self);
  Result.SQL.Text := sStatement;
    begin
      LogEnter(liSQL, 'SQL: ' + sStatement);
      try
        try
          Result.ExecQuery();
          {-}
          BeginLog();
          try
            for  nInd := 0  to  Result.FieldCount - 1  do
              with  Result.Fields[nInd]  do
                LogValue(liSQL, Name, Value);
            for  nInd := 0  to  Result.Params.Count - 1  do
              with  Result.Params.Vars[nInd]  do
                LogValue(liSQL, 'Par. „' + Name + '”', Value)
          finally
            EndLog()
          end { try-finally }
        except
          on  eErr : Exception  do
            begin
              Error(_('Execution of SQL statement:') + sLineBreak + sStatement, eErr);
              if  Result.InTransaction()  then
                Result.Rollback();
              raise
            end { on Exception }
        end { try-except };
      finally
        LogExit(liSQL, 'SQL: ' + sStatement)
      end { try-finally }
    end { with dmData.LogObj[ldSQL] }
end { ExecQuery };


procedure  TdmData.EndLog();
begin
  fLog.EndLog()
end { EndLog };


function  TdmData.ExecQuery(const   sStmtFmt:  string;
                            const   StmtArgs:  array of const)
                            : TQuerySQL;
begin
  Result := ExecQuery(Format(sStmtFmt, StmtArgs))
end { ExecQuery };


procedure  TdmData.ExecSQL(const   sStmtFmt:  string;
                           const   StmsArgs:  array of const);
begin
  ExecSQL(Format(sStmtFmt, StmsArgs))
end { ExecSQL };


procedure  TdmData.ExecSQL(const   sStatement:  string);
begin
  ExecQuery(sStatement).Free()
end { ExecSQL };


function  TdmData.GetContinuePreviousTask() : Boolean;
begin
  Result := GetParamBool(gcsParContPrevTask, gcbStdContPrevTask)
end { GetContinuePreviousTask };


function  TdmData.GetContinuePreviousTaskBreak() : Integer;
begin
  Result := GetParamInt(gcsParContPrevTaskBrak, gcnStdContPrevTaskBreak)
end { GetContinuePreviousTaskBreak };


function  TdmData.GetCurrentNominalWorkTime() : TDateTime;
begin
  case  DayOfTheWeek(Today())  of
    6:  Result := fNominalWorkTm[dkSaturday];
    7:  Result := fNominalWorkTm[dkSunday]
   else Result := fNominalWorkTm[dkWorkingDay]
  end { case DayOfWeek(Date()) }
end { GetCurrentNominalWorkTime };


function  TdmData.GetLanguageCode(const   nIndex:  Integer)
                                  : string;
begin
  Result := fLangCodes.Strings[nIndex]
end { GetLanguageCode };


function  TdmData.GetLanguageCount() : Integer;
begin
  Result := fLangCodes.Count
end { GetLanguageCount };


function  TdmData.GetLanguageName(const   nIndex:  Integer)
                                  : string;
begin
  Result := fLangNames.Strings[nIndex]
end { GetLanguageName };


function  TdmData.GetNominalWorkTime(const   Index:  TDayKind)
                                     : TDateTime;
begin
  Result := fNominalWorkTm[Index]
end { GetNominalWorkTime };


function  TdmData.GetParamBool(const   sParamName:  string;
                               const   bDefault:  Boolean)
                               : Boolean;
begin
  Result := StrToBool(GetParamStr(sParamName,
                                  BoolToStr(bDefault)))
end { GetParamBool };


function  TdmData.GetParamInt(const   sParamName:  string;
                              const   nDefault:  Int64)
                              : Int64;
begin
  Result := StrToInt64(GetParamStr(sParamName, IntToStr(nDefault)))
end { GetParamInt };


function  TdmData.GetParamStr(const   sParamName, sDefault:  string)
                              : string;
begin
  LogEnter(liOptions, 'TdmData.GetParam("%s", {%s})',
           [sParamName, sDefault]);
  with  ExecQuery('SELECT sParamValue FROM t_Params WHERE sParamName = ''%s''',
                  [sParamName])  do
    try
      if  Eof
          or Fields[0].IsNull  then
        Result := sDefault
      else
        Result := Fields[0].AsString
    finally
      Free()
    end { try-finally };
  LogValue(liOptions, 'Result', Result);
  LogExit(liOptions, 'TdmData.GetParam')
end { GetParamStr };


function  TdmData.GetParamTime(const   sParamName:  string;
                               const   tmDefault:  TDateTime)
                               : TDateTime;
var
  sTime:  string;
  nYr, nMn, nDy:  Word;
  nH, nM, nS:  Word;

begin  { GetParamTime }
  sTime := GetParamStr(sParamName, DateTimeToStr(tmDefault));
  { YYYY-MM-DD HH:MM:SS }
  { 123456789o123456789 }
  nYr := StrToInt(Copy(sTime, 1, 4));
  nMn := StrToInt(Copy(sTime, 6, 2));
  nDy := StrToInt(Copy(sTime, 9, 2));
  nH := StrToInt(Copy(sTime, 12, 2));
  nM := StrToInt(Copy(sTime, 15, 2));
  nS := StrToInt(Copy(sTime, 18, 2));
  Result := EncodeDateTime(nYr, nMn, nDy, nH, nM, nS, 0)
end { GetParamTime };


function  TdmData.GetTask(const   nIndex:  Integer)
                          : string;
begin
  Result := fTasks.Strings[nIndex]
end { GetTask };


function  TdmData.GetTaskCount() : Integer;
begin
  Result := fTasks.Count
end { GetTaskCount };


function  TdmData.GetTaskID(const   nIndex:  Integer)
                            : Int64;
begin
  Result := PInt64(fTasks.Objects[nIndex])^
end { GetTaskID };


procedure  TdmData.LoadTasks();

var
  pTaskID:  PInt64;

begin  { LoadTasks }
  LogEnter(liProgram, 'TdmData.LoadTasks');
  fTasks.BeginUpdate();
  try
    ClearTasks();
    with  ExecQuery('SELECT nTaskID, sTaskName FROM t_Tasks')  do
      try
        while  not Eof  do
          begin
            New(pTaskID);
            pTaskID^ := Fields[0].AsInt64;
            fTasks.AddObject(Fields[1].AsString, TObject(pTaskID));
            Next()
          end { while not Eof }
      finally
        Free()
      end { try-finally }
  finally
    fTasks.EndUpdate()
  end { try-finally };
  LogExit(liProgram, 'TdmData.LoadTasks')
end { LoadTasks };


procedure  TdmData.LogEnter(const   liCategory:  TLogInfo;
                            const   sInfo:  string;
                            const   lmMsgType:  TGSkLogMessageType);
begin
  if  liCategory in fLogInfo  then
    fLog.LogEnter(sInfo, lmMsgType)
end { LogEnter };


procedure  TdmData.LogEnter(const   liCategory:  TLogInfo;
                            const   sInfoFmt:  string;
                            const   InfoArgs:  array of const;
                            const   lmMsgType:  TGSkLogMessageType);
begin
  if  liCategory in fLogInfo  then
    fLog.LogEnter(sInfoFmt, InfoArgs, lmMsgType)
end { LogEnter };


procedure  TdmData.LogExit(const   liCategory:  TLogInfo;
                           const   sInfo:  string;
                           const   lmMsgType:  TGSkLogMessageType);
begin
  if  liCategory in fLogInfo  then
    fLog.LogExit(sInfo, lmMsgType)
end { LogExit };


procedure  TdmData.LogMessage(const   liCategory:  TLogInfo;
                              const   sMsg:  string;
                              const   lmMsgType:  TGSkLogMessageType);
begin
  if  liCategory in fLogInfo  then
    fLog.LogMessage(sMsg, lmMsgType)
end { LogMessage };


procedure  TdmData.LogMessage(const   liCategory:  TLogInfo;
                              const   sMsgFmt:  string;
                              const   MsgArgs:  array of const;
                              const   lmMsgType:  TGSkLogMessageType);
begin
  if  liCategory in fLogInfo  then
    fLog.LogMessage(sMsgFmt, MsgArgs, lmMsgType);
end { LogMessage };


procedure  TdmData.LogValue(const   liCategory:  TLogInfo;
                            const   sName:  string;
                            const   vValue:  Variant;
                            const   lmMsgType:  TGSkLogMessageType);
begin
  if  liCategory in fLogInfo  then
    fLog.LogValue(sName, vValue, lmMsgType)
end { LogValue };


procedure  TdmData.LogValue(const   liCategory:  TLogInfo;
                            const   sName:  string;
                            const   nValue:  Int64;
                            const   ValType:  TGSkLogValueNumType;
                            const   lmMsgType:  TGSkLogMessageType);
begin
  if  liCategory in fLogInfo  then
    fLog.LogValue(sName, nValue, lmMsgType)
end { LogValue };


procedure  TdmData.PauseJob();
begin
  LogEnter(liTask, 'TdmData.PauseJob');
  UpdateStop(ftmStart, Now());
  fbPaused := True;
  LogExit(liTask, 'TdmData.PauseJob')
end { PauseJob };


procedure  TdmData.RegisterStart(const   tmStart:  TDateTime;
                                 const   nTaskID:  Int64);
const
  cnFldStart = 0;   // \
  cnFldStop  = 1;   //  > See SQL SELECT statement below
  cnFldTask  = 2;   // /
  cnFldJobID = 3;   ///

var
  bRegister:  Boolean;

begin  { RegisterStart }
  BeginLog();
  try
    LogEnter(liTask, 'TdmData.RegisterStart({%s}, %d)',
             [DateTimeToStr(tmStart), nTaskID]);
    LogValue(liTask, 'fbFirstTime', fbFirstTime)
  finally
    EndLog()
  end { try-finally };
  bRegister := True;
  if  fbFirstTime  then
    try
      with  ExecQuery('SELECT tmStart, tmStop, nTaskID, nJobID FROM t_Jobs WHERE nJobID = (SELECT Max(nJobID) FROM t_Jobs)')  do
        try
          { Continue last registration if:
                1. 'tmStop' IS NULL --> last registration ended with crash
                2. current and previous task are the same,
                3. current time is not later than last timestamp + 5 minutes
                4. start of task is the same like registered in Params table
              OR
                1. option for continue previous task is set
                2. registered task is the same like previous one
                3. registration break is not too long.
            Otherwise register end of task (time from last timestamp).     }
          BeginLog();
          try
            LogValue(liTask, 'Fields[cnFldStop].IsNull', Fields[cnFldStop].IsNull);
            LogValue(liTask, 'Fields[cnFldTask].AsInt64', Fields[cnFldTask].AsInt64);
            LogValue(liTask, 'Fields[cnFldTask].IsNull', Fields[cnFldTask].IsNull);
            LogValue(liTask, 'nTaskID', nTaskID);
            if  Fields[cnFldStop].IsNull  then
              if  ((Fields[cnFldTask].AsInt64 = nTaskID)
                    or (Fields[cnFldTask].IsNull
                        and (nTaskID = 0)))
                   and (SecondsBetween(Fields[cnFldStart].AsDateTime,
                                       GetParamTime(gcsParTaskStart)) = 0)
                   and (MinutesBetween(Fields[cnFldStart].AsDateTime,
                                       Now()) <= (TimestampEvery + 5))  then
                begin
                  LogMessage(liTask, '--> #1');
                  ftmStart := Fields[cnFldStart].AsDateTime;
                  bRegister := False
                end { restart }
              else
                UpdateStop(Fields[cnFldStart].AsDateTime,
                           GetParamTime(gcsParTimestamp, Now()))
            else  { not Fields[cnFldStop].IsNull }
              if  ContinuePreviousTask
                  and ((Fields[cnFldTask].AsInt64 = nTaskID)
                       or (Fields[cnFldTask].IsNull
                           and (nTaskID = 0)))
                  and (MinutesBetween(Fields[cnFldStart].AsDateTime, Now()) <= ContinuePreviousTaskBreak)  then
                begin
                  LogMessage(liTask, '--> #2');
                  ExecSQL('UPDATE t_Jobs SET tmStop = NULL WHERE nJobID = %d',
                          [Fields[cnFldJobID].AsInt64]);
                  ftmStart := Fields[cnFldStart].AsDateTime;
                  bRegister := False
                end { Continue previous task }
          finally
            EndLog()
          end
        finally
          Free()
        end { try-finally }
    finally
      fbFirstTime := False
    end { try-finally };
  LogValue(liTask, 'bRegister', bRegister);
  if  bRegister  then
    begin
      ExecSQL('INSERT INTO t_Jobs(tmStart, nTaskID) VALUES (''%s'', %s)',
              [DateTimeToStr(tmStart), TaskIdToStr(nTaskID)]);
      ftmStart := tmStart;
      SetParamTime(gcsParTaskStart, tmStart)
    end { if bNotRestart };
  CurrentTaskID := nTaskID;
  ResetTimestamp();
  LogValue(liTask, 'ftmStart', ftmStart);
  LogExit(liTask, 'TdmData.RegisterStart')
end { RegisterStart };


procedure  TdmData.ResetTimestamp();
begin
  LogEnter(liTask, 'TdmData.ResetTimestamp');
  tmTimestamp.Enabled := False;
  tmTimestampTimer(nil);
  tmTimestamp.Enabled := True;
  LogExit(liTask, 'TdmData.ResetTimestamp')
end { ResetTimestamp };


procedure  TdmData.ResumeJob();
begin
  LogEnter(liTask, 'TdmData.ResumeJob');
  RegisterStart(Now(), fnTaskID);
  fbPaused := False;
  LogExit(liTask, 'TdmData.ResumeJob')
end { ResumeJob };


procedure  TdmData.SetParamBool(const   sParamName:  string;
                                const   bParamValue:  Boolean);
begin
  SetParamStr(sParamName, BoolToStr(bParamValue, True))
end { SetParamBool };


procedure  TdmData.SetParamInt(const   sParamName:  string;
                               const   nParamValue:  Int64);
begin
  SetParamStr(sParamName, IntToStr(nParamValue))
end { SetParamInt };


procedure  TdmData.SetParamStr(const   sParamName, sParamValue:  string);
begin
  LogEnter(liOptions, 'TdmData.SetParam("%s", {%s})', [sParamName, sParamValue]);
  ExecSQL('UPDATE OR INSERT INTO t_Params(sParamName, sParamValue) VALUES (''%s'', ''%s'') MATCHING (sParamName)',
          [sParamName, sParamValue]);
  LogExit(liOptions, 'TdmData.SetParam')
end { SetParamStr };


procedure  TdmData.SetParamTime(const   sParamName:  string;
                                const   tmParamValue:  TDateTime);
begin
  SetParamStr(sParamName, DateTimeToStr(tmParamValue))
end { SetParamTime };


procedure  TdmData.SetShowMainFormOnStartup(const   Value:  Boolean);
begin
  if  Value <> fbShowMainForm  then
    begin
      fbShowMainForm := Value;
      SetParamBool(gcsParStartupShowMainForm, fbShowMainForm)
    end { if Value <> fbDetectScrSvr }
end { SetShowMainFormOnStartup };


procedure  TdmData.SetStartupTaskAction(const   nActionNo:  Integer);
begin
  if  fnStartTskAct <> nActionNo  then
    begin
      fnStartTskAct := nActionNo;
      SetParamInt(gcsParStartupTaskAct, nActionNo)
    end { fnStartTskAct <> nActionNo }
end { SetStartupTaskAction };


procedure  TdmData.SetCommentsVisible(const   Value:  TCommentsVisible);
begin
  if  Value <> fComments  then
    begin
      fComments := Value;
      SetParamInt(gcsParCommentsVisible, Ord(Value))
    end { Value <> fComments }
end { SetCommentsVisible };


procedure  TdmData.SetContinuePreviousTask(const   Value:  Boolean);
begin
  SetParamBool(gcsParContPrevTask, Value)
end { SetContinuePreviousTask };


procedure  TdmData.SetContinuePreviousTaskBreak(const   Value:  Integer);
begin
  SetParamInt(gcsParContPrevTaskBrak, Value)
end { SetContinuePreviousTaskBreak };


procedure  TdmData.SetCurrentTaskID(const   nTaskID:  Int64);
begin
  if  fnTaskID <> nTaskID  then
    begin
      fnTaskID := nTaskID;
      SetParamInt(gcsParTaskID, fnTaskID)
    end { fnTaskID <> nTaskID }
end { SetCurrentTaskID };


procedure  TdmData.SetDefaultTask(const   sTaskName:  string);

var
  nInd:  Integer;

begin  { SetDefaultTask }
  nInd := fTasks.IndexOf(sTaskName);
  if  nInd >= 0  then
    DefaultTaskID := PInt64(fTasks.Objects[nInd])^
  else
    DefaultTaskID := 0;
  BeginLog();
  try
    LogMessage(liOptions, 'TdmData.SetDefaultTask("%s")', [sTaskName]);
    LogValue(liOptions, 'DefaultTaskID', DefaultTaskID)
  finally
    EndLog()
  end { try-finally }
end { SetDefaultTask };


procedure  TdmData.SetDefLangCode(const   Value:  string);

var
  nInd:  Integer;

begin  { SetDefLangCode }
  if  Value <> fsDefLangCode  then
    begin
      fsDefLangCode := Value;
      SetParamStr(gcsParDefaultLanguage, Value);
      for  nInd := Screen.FormCount - 1  downto  0  do
        if  Screen.Forms[nInd] is TfBaseForm  then
          (Screen.Forms[nInd] as TfBaseForm).ResetLanguage()
    end { Value <> fsDefLangCode }
end { SetDefLangCode };


procedure  TdmData.SetDefTaskID(const   nTaskID:  Int64);
begin
  if  fnDefTaskID = nTaskID  then
    begin
      fnDefTaskID := nTaskID;
      SetParamInt(gcsParDefaultTask, nTaskID)
    end { fnDefTaskID = nTaskID }
end { SetDefTaskID };


procedure  TdmData.SetDetectScreenSaver(const   Value:  Boolean);
begin
  if  Value <> fbDetectScrSvr  then
    begin
      fbDetectScrSvr := Value;
      SetParamBool(gcsParDetectScreenSaver, fbDetectScrSvr)
    end { if Value <> fbDetectScrSvr }
end { SetDetectScreenSaver };


procedure  TdmData.SetGroupingLevels(const   nLevels:  Integer);
begin
  if  fnGrpLevels <> nLevels  then
    begin
      fnGrpLevels := nLevels;
      SetParamInt(gcsParGroupingLevels, nLevels)
    end { fnGrpLevels <> nLevels }
end { SetGroupingLevels };


procedure  TdmData.SetNominalWorkTime(const   Index:  TDayKind;
                                      const   tmTime:  TDateTime);
begin
  if  fNominalWorkTm[Index] <> tmTime  then
    begin
      fNominalWorkTm[Index] := tmTime;
      SetParamTime(gcsParNominalWorkTime[Index], tmTime)
    end { fNominalWorkTm[Index] <> tmTime }
end { SetNominalWorkTime };


procedure  TdmData.SetTimestampEvery(const   nMinutes:  Integer);
begin
  if  fnTimeStamp <> nMinutes  then
    begin
      fnTimeStamp := nMinutes;
      tmTimestamp.Enabled := False;
      tmTimestamp.Interval := fnTimeStamp;
      ResetTimestamp();
      SetParamInt(gcsParTimestampEvery, nMinutes)
    end { fnTimeStamp <> nMinutes }
  else
    ResetTimestamp()
end { SetTimestampEvery };


procedure  TdmData.SetWithNoTask(const   Value:  Boolean);
begin
  if  Value <> fbWithNoTask  then
    begin
      fbWithNoTask := Value;
      SetParamBool(gcsParWorkWithoutTask, Value)
    end { Value <> fbWithNoTask }
end { SetWithNoTask };


function  TdmData.StartTransaction() : TIBTransaction;
begin
  Result := TIBTransaction.Create(Self);
  Result.DefaultDatabase := dbDatabase;
  Result.StartTransaction()
end { StartTransaction };


function  TdmData.TaskIdToStr(const   nTaskID:  Int64)
                              : string;
begin
  if  nTaskID = 0  then
    Result := 'NULL'
  else
    Result := IntToStr(nTaskID)
end { TaskIdToStr };


procedure  TdmData.tmTimestampTimer(Sender:  TObject);
begin
  SetParamTime(gcsParTimestamp, Now());
  SetParamInt(gcsParTaskID, fnTaskID);
end { tmTimestampTimer };


procedure  TdmData.UpdateStop(const   tmStart, tmStop:  TDateTime);
begin
  LogEnter(liTask, 'TdmData.UpdateStop({%s}, {%s})',
           [DateTimeToStr(tmStart), DateTimeToStr(tmStop)]);
  ExecSQL('UPDATE t_Jobs SET tmStop = ''%s'' WHERE tmStart = ''%s''',
          [DateTimeToStr(tmStop), DateTimeToStr(tmStart)]);
  ResetTimestamp();
  LogExit(liTask, 'TdmData.UpdateStop')
end { UpdateStop };


procedure  TdmData.UpdateTask(const   tmStart:  TDateTime;
                              const   nTaskID:  Int64);
begin
  LogEnter(liTask, 'TdmData.UpdateTask([%s], %d)',
           [DateTimeToStr(tmStart), nTaskID]);
  ExecSQL('UPDATE t_Jobs SET nTaskID = %s WHERE tmStart = ''%s''',
          [TaskIdToStr(nTaskID), DateTimeToStr(tmStart)]);
  CurrentTaskID := nTaskID;
  ResetTimestamp();
  LogExit(liTask, 'TdmData.UpdateTask')
end { UpdateTask };


procedure  TdmData.UpdateTasks(const   TaskList:  TStringList);

var
  nInd:  Integer;
  bLoad:  Boolean;

begin  { UpdateTasks }
  LogEnter(liProgram, 'TdmData.UpdateTasks');
  bLoad := False;
  { Update renamed tasks }
  for  nInd := TaskList.Count - 1  downto  0  do
    if  (TaskList.Objects[nInd] <> nil)
        and (PInt64(TaskList.Objects[nInd])^ <> 0)  then
      if  fTasks.IndexOf(TaskList.Strings[nInd]) < 0  then
        begin
          ExecSQL('UPDATE t_Tasks SET sTaskName = ''%s'' WHERE nTaskID = %d',
                  [TaskList.Strings[nInd],
                   PInt64(TaskList.Objects[nInd])^]);
          bLoad := True
        end ;
  if  bLoad  then
    begin
      LoadTasks();
      bLoad := False
    end { if bLoad };
  { Delete removed tasks }
  for  nInd := fTasks.Count - 1  downto  0  do
    if  TaskList.IndexOf(fTasks.Strings[nInd]) < 0  then
      DeleteTaks(nInd, True);
  { Insert new tasks }
  for  nInd := TaskList.Count - 1  downto  0  do
    if  fTasks.IndexOf(TaskList.Strings[nInd]) < 0  then
      begin
        ExecSQL('INSERT INTO t_Tasks(sTaskName) VALUES (''%s'')',
                [TaskList.Strings[nInd]]);
        bLoad := True
      end ;
  { Reload all tasks }
  if  bLoad  then
    LoadTasks();
  LogExit(liProgram, 'TdmData.UpdateTasks')
end { UpdateTasks };


initialization
  gsOK[0] := _('OK');
  gnOK[0] := ID_OK;
  {-}
  {$IfOpt C+ }
    gAssertErrorProc := AssertErrorProc;
    AssertErrorProc := AssertError;
  {$EndIf C+ }
  RegisterExceptionHandler(madExceptHandler, stTrySyncCallAlways, epCompleteReport)


end.
