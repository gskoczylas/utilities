﻿program  JobRegister;

(**********************************************
 *                                            *
 *                Job Register                *
 *     ——————————————————————————————————     *
 *  Copyright © 2008-2024 Grzegorz Skoczylas  *
 *                                            *
 *         mgr Grzegorz Skoczylas             *
 *             tel. 503 064 953               *
 *             gskoczylas@gmail.com           *
 *                                            *
 **********************************************)


(*
  madExcept, madLinkDisAsm, madListHardware, madListProcesses, madListModules,
  Forms,
  zz_GSkMadExcept in 'p:\GSkPasLib\zz_GSkMadExcept.pas',
  zz_GSkLog in 'p:\GSkPasLib\zz_GSkLog.pas',
  zz_GSkLogFile in 'p:\GSkPasLib\zz_GSkLogFile.pas',
  zz_GSkFileUtils in 'p:\GSkPasLib\zz_GSkFileUtils.pas',
  zz_GSkLanguageCodes in 'p:\GSkPasLib\zz_GSkLanguageCodes.pas',
  zz_GSkDialogs in 'p:\GSkPasLib\zz_GSkDialogs.pas',
*)
uses
  madExcept, madLinkDisAsm, madListHardware, madListProcesses, madListModules,
  Forms,
  zz_GSkMadExcept in 'p:\GSkPasLib\zz_GSkMadExcept.pas',
  zz_GSkLog in 'p:\GSkPasLib\zz_GSkLog.pas',
  zz_GSkLogFile in 'p:\GSkPasLib\zz_GSkLogFile.pas',
  zz_GSkFileUtils in 'p:\GSkPasLib\zz_GSkFileUtils.pas',
  zz_GSkLanguageCodes in 'p:\GSkPasLib\zz_GSkLanguageCodes.pas',
  zz_GSkDialogs in 'p:\GSkPasLib\zz_GSkDialogs.pas',
  u_About in 'u_About.pas' {fAboutBox},
  u_BaseForm in 'u_BaseForm.pas' {fBaseForm},
  u_Data in 'u_Data.pas' {dmData: TDataModule},
  u_MainForm in 'u_MainForm.pas' {fJobRegister},
  u_Options in 'u_Options.pas' {fOptions},
  u_SelectTask in 'u_SelectTask.pas' {fSelectTask};

{$R *.res}


begin
  Application.Initialize();
  Application.MainFormOnTaskbar := True;
  Application.Title := 'Job Register';
  Application.CreateForm(TdmData, dmData);
  if  dmData.dbDatabase.Connected  then
    begin
      Application.CreateForm(TfJobRegister, fJobRegister);
      Application.ShowMainForm := dmData.ShowMainFormOnStarup
    end { dbDatabase.Connected }
  else
    Application.ShowMainForm := False;
  Application.Run()
end.
