object fSelectTask: TfSelectTask
  Left = 0
  Top = 0
  ActiveControl = edTasks
  Caption = 'Select Task'
  ClientHeight = 286
  ClientWidth = 437
  Color = clBtnFace
  Constraints.MinWidth = 270
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnSearch: TPanel
    Left = 0
    Top = 0
    Width = 437
    Height = 41
    Align = alTop
    TabOrder = 0
    DesignSize = (
      437
      41)
    object lbSearch: TLabel
      Left = 11
      Top = 11
      Width = 37
      Height = 13
      Caption = '&Search:'
      FocusControl = edSearch
    end
    object edSearch: TEdit
      Left = 70
      Top = 8
      Width = 353
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 0
      OnChange = edSearchChange
      OnKeyDown = edSearchKeyDown
      OnKeyUp = edSearchKeyUp
    end
  end
  object pnActions: TPanel
    Left = 0
    Top = 226
    Width = 437
    Height = 41
    Align = alBottom
    TabOrder = 3
    object pnButtons: TPanel
      Left = 187
      Top = 1
      Width = 249
      Height = 39
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object btSelect: TBitBtn
        Left = 1
        Top = 7
        Width = 75
        Height = 25
        Caption = 'Select'
        Default = True
        Glyph.Data = {
          DE010000424DDE01000000000000760000002800000024000000120000000100
          0400000000006801000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333330000333333333333333333333333F33333333333
          00003333344333333333333333388F3333333333000033334224333333333333
          338338F3333333330000333422224333333333333833338F3333333300003342
          222224333333333383333338F3333333000034222A22224333333338F338F333
          8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
          33333338F83338F338F33333000033A33333A222433333338333338F338F3333
          0000333333333A222433333333333338F338F33300003333333333A222433333
          333333338F338F33000033333333333A222433333333333338F338F300003333
          33333333A222433333333333338F338F00003333333333333A22433333333333
          3338F38F000033333333333333A223333333333333338F830000333333333333
          333A333333333333333338330000333333333333333333333333333333333333
          0000}
        NumGlyphs = 2
        TabOrder = 0
        OnClick = btSelectClick
      end
      object btCancel: TBitBtn
        Left = 163
        Top = 7
        Width = 75
        Height = 25
        Cancel = True
        Caption = 'Cancel'
        Glyph.Data = {
          DE010000424DDE01000000000000760000002800000024000000120000000100
          0400000000006801000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333333333333333333333000033338833333333333333333F333333333333
          0000333911833333983333333388F333333F3333000033391118333911833333
          38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
          911118111118333338F3338F833338F3000033333911111111833333338F3338
          3333F8330000333333911111183333333338F333333F83330000333333311111
          8333333333338F3333383333000033333339111183333333333338F333833333
          00003333339111118333333333333833338F3333000033333911181118333333
          33338333338F333300003333911183911183333333383338F338F33300003333
          9118333911183333338F33838F338F33000033333913333391113333338FF833
          38F338F300003333333333333919333333388333338FFF830000333333333333
          3333333333333333333888330000333333333333333333333333333333333333
          0000}
        ModalResult = 2
        NumGlyphs = 2
        TabOrder = 2
      end
      object btNone: TBitBtn
        Left = 82
        Top = 7
        Width = 75
        Height = 25
        Caption = '&None'
        Glyph.Data = {
          DE010000424DDE01000000000000760000002800000024000000120000000100
          0400000000006801000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333FFFFF333333000033333388888833333333333F888888FFF333
          000033338811111188333333338833FFF388FF33000033381119999111833333
          38F338888F338FF30000339119933331111833338F388333383338F300003391
          13333381111833338F8F3333833F38F3000039118333381119118338F38F3338
          33F8F38F000039183333811193918338F8F333833F838F8F0000391833381119
          33918338F8F33833F8338F8F000039183381119333918338F8F3833F83338F8F
          000039183811193333918338F8F833F83333838F000039118111933339118338
          F3833F83333833830000339111193333391833338F33F8333FF838F300003391
          11833338111833338F338FFFF883F83300003339111888811183333338FF3888
          83FF83330000333399111111993333333388FFFFFF8833330000333333999999
          3333333333338888883333330000333333333333333333333333333333333333
          0000}
        NumGlyphs = 2
        TabOrder = 1
        OnClick = btNoneClick
      end
    end
  end
  object sbStatus: TStatusBar
    Left = 0
    Top = 267
    Width = 437
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object pnWhen: TPanel
    Left = 0
    Top = 185
    Width = 437
    Height = 41
    Align = alBottom
    TabOrder = 2
    object lbWhen: TLabel
      Left = 11
      Top = 17
      Width = 32
      Height = 13
      Caption = '&When:'
      FocusControl = edWhen
    end
    object lbNow: TLabel
      Left = 176
      Top = 18
      Width = 4
      Height = 13
      Caption = '-'
    end
    object edWhen: TDateTimePicker
      Left = 49
      Top = 14
      Width = 59
      Height = 21
      Date = 39470.496152037040000000
      Format = ' HH:mm'
      Time = 39470.496152037040000000
      Kind = dtkTime
      TabOrder = 0
      OnChange = edWhenChange
    end
    object edNow: TCheckBox
      Left = 120
      Top = 16
      Width = 57
      Height = 17
      Caption = 'Now'
      Checked = True
      State = cbChecked
      TabOrder = 1
      OnClick = edNowClick
    end
  end
  object edTasks: TListView
    AlignWithMargins = True
    Left = 3
    Top = 41
    Width = 431
    Height = 144
    Margins.Top = 0
    Margins.Bottom = 0
    Align = alClient
    Columns = <>
    HideSelection = False
    IconOptions.AutoArrange = True
    ReadOnly = True
    SortType = stText
    TabOrder = 1
    ViewStyle = vsList
    OnDeletion = edTasksDeletion
    OnKeyPress = edTasksKeyPress
  end
  object tmNow: TTimer
    OnTimer = tmNowTimer
    Left = 368
    Top = 192
  end
  object tmWakeUp: TTimer
    Interval = 180000
    OnTimer = tmWakeUpTimer
    Left = 400
    Top = 192
  end
end
