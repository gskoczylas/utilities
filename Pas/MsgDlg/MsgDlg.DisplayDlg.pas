﻿unit  MsgDlg.DisplayDlg;

(**********************************************
 *                                            *
 *            Show Message Dialog             *
 *     ——————————————————————————————————     *
 *  Copyright © 2019-2024 Grzegorz Skoczylas  *
 *                                            *
 *    Zakład Usług Informatycznych PROGRAM    *
 *         mgr Grzegorz Skoczylas             *
 *             ul. Grażyny 13/15              *
 *             43-300 Bielsko-Biała           *
 *             tel. +48 503 064 953           *
 *             gskoczylas@gmail.com           *
 *                                            *
 **********************************************)

interface


uses
  System.SysUtils, System.StrUtils, System.Classes,
  WinAPI.Windows, System.Win.Registry,
  VCL.Forms;


type
  TDialogType = (dtWarning, dtError, dtInformation, dtQuestion, dtCustom);
  { Uwaga! Kolejność elementów musi być zgodna z kolejnością elementów
           w standardowym typie "TMsgDlgType" }

  TDialogPosition = (dpUnknown,
                     dpTopLeft, dpTopCenter, dpTopRight,
                     dpMiddleLeft, dpCenter, dpMiddleRight,
                     dpBottomLeft, dpBottomCenter, dpBottomRight,
                     dpPrevious);


procedure  DisplayDlg(const   sCaption, sMessage:  string;
                      const   DialogType:  TDialogType;
                      const   Buttons:  TStringList;
                      const   sDefaultBtn:  string;
                      const   nTimeout:  Integer;
                      const   Postion:  TDialogPosition;
                      const   sPositionId:  string);


implementation


uses
  GSkPasLib.Dialogs, GSkPasLib.TimeUtils;


type
  THack = class
  private
    const
      csRegKey = 'Software\Grzegorz Skoczylas\MsgDlg';
    class var
      fDialog:      TForm;
      fbHide:       Boolean;
      fnTimeout:    Integer;
      fnStart:      Int64;
      fPosition:    TDialogPosition;
      fPositionId:  string;
    class function  RegKey() : string;
  public
    class property  PositionId:  string
                    read fPositionId  write fPositionId;
    class procedure  OnCreate(Sender:  TObject);
    class procedure  OnActivate(Sender:  TObject);
    class procedure  OnDestroy(Sender:  TObject);
  end { THack };


procedure  TimerCallBack(hWnd:    HWND;
                         Msg:     Word;
                         TimerID: Word;
                         SysTime: LongInt);                          stdcall;

  function  ForceShowWindow() : Boolean;

  var
    nWorksSecs:  Integer;
    nBeforeEnd:  Integer;

  begin
    nWorksSecs := (GetTickCount64() - THack.fnStart) div 1000;
    if  THack.fnTimeout = 0  then
      if  nWorksSecs <= 60  then             // if works no longer than minute
        Result := nWorksSecs mod 10 = 0      //      show dialogue every 10 seconds
      else if  nWorksSecs <= 120  then       // if works not longer than two minutes
        Result := nWorksSecs mod 15 = 0      //      show dialogue every 15 seconds
      else if  nWorksSecs <= 180  then       // if works not longer than three munutes
        Result := nWorksSecs mod 20 = 0      //      show dialogue every 20 seconds
      else                                   // else
        Result := nWorksSecs mod 30 = 0      //      show dialogue every half minute
    else
      begin
        nBeforeEnd := THack.fnTimeout - nWorksSecs;
        if  nBeforeEnd >= THack.fnTimeout - 1  then
          Result := True
        else if  nBeforeEnd >= 180  then    // if >= 3 minutes to deadline
          Result := nBeforeEnd mod 90 = 0   //       show dialogue every 90 seconds
        else if  nBeforeEnd >= 60  then     // if >= 1 minute to deadline
          Result := nBeforeEnd mod 30 = 0   //       show dialogue every 30 seconds
        else if  nBeforeEnd > 10  then      // if >= 10 seconds to deadline
          Result := nBeforeEnd mod 15 = 0   //       show dialogue every 15 seconds
        else                                // else
          Result := True                    //       show dialogue every second
      end { Timeout <> 0 }
  end { AdjustTimerInterval };

begin  { TimerCallBack }
  if  ForceShowWindow()  then
    SetWindowPos(THack.fDialog.Handle, HWND_TOPMOST, 0, 0, 0, 0,
                 SWP_NOSIZE or SWP_NOMOVE)
end { TimerCallBack };


procedure  DisplayDlg(const   sCaption, sMessage:  string;
                      const   DialogType:  TDialogType;
                      const   Buttons:  TStringList;
                      const   sDefaultBtn:  string;
                      const   nTimeout:  Integer;
                      const   Postion:  TDialogPosition;
                      const   sPositionId:  string);
const
  csMarkerPrefix = '\';
  csTimeMarker   = 't';
  csNewLineMaker = 'n';
  {-}
  csetCaptionMarkers = [csMarkerPrefix, csTimeMarker];
  csetMessageMarkers = [csMarkerPrefix, csTimeMarker, csNewLineMaker];


var
  lButtons:  TArray<string>;
  lResults:  TArray<Integer>;
  nInd:      Integer;
  sTitle:    string;

  function  ProcessMarkers(const   sSrc:  string;
                           const   setMarkers:  TSysCharSet)
                           : string;
  var
    nInd:  Integer;
    nLen:  Integer;

  begin  { ProcessMarkers }
    Result := sSrc;
    nLen := Length(Result);
    nInd := 1;
    while  nInd < nLen  do
      begin
        if  Result[nInd] = csMarkerPrefix  then
          if  nInd < Length(Result)  then
            if  CharInSet(Result[nInd + 1], setMarkers)  then
              begin
                case  Result[nInd + 1]  of
                  csMarkerPrefix:
                    Delete(Result, nInd, 1);   // "\\" --> "\"
                  csTimeMarker:   // "\t" --> "h:mm" (current time)
                    Result := StuffString(Result, nInd, 2,
                                          FormatDateTime('h:nn', Time()));
                  csNewLineMaker:  // "\n" --> sLineBreak
                    Result := StuffString(Result, nInd, 2, sLineBreak)
                end { case Result[nInd + 1] };
                nLen := Length(Result)
              end { Result[nInd + 1] in setMarkers };
        Inc(nInd)
      end { while nInd < nLen }
  end { ProcessMarkers };

begin  { DisplayDlg }
  if  Buttons.Count <> 0  then
    begin
      SetLength(lButtons, Buttons.Count);
      for  nInd := Buttons.Count - 1  downto  0  do
        lButtons[nInd] := Buttons.Strings[nInd]
    end { Buttons.Count <> 0 }
  else
    begin
      SetLength(lButtons, 1);
      lButtons[0] := 'OK'
    end { Buttons.Count = 0 };
  {-}
  SetLength(lResults, Length(lButtons));
  for  nInd := Low(lResults)  to  High(lResults)  do
    lResults[nInd] := 11 + nInd;
  {-}
  sTitle := ProcessMarkers(sCaption, csetCaptionMarkers);
  Application.Title := sTitle;
  THack.fnTimeout := nTimeout;
  THack.fbHide := Trim(sTitle) = '';
  THack.fPosition := Postion;
  THack.PositionId := sPositionId;
  ExitCode := MsgDlgEx(sTitle,
                       ProcessMarkers(sMessage, csetMessageMarkers),
                       TMsgDlgType(DialogType), lButtons, lResults,
                       nil, nTimeout, 500, Buttons.IndexOf(sDefaultBtn) + 1,
                       1, -1,
                       THack.OnCreate, THack.OnDestroy, THack.OnActivate);
  if  ExitCode >= 11  then
    Dec(ExitCode, 10)
  else
    ExitCode := 0
end { DisplayDlg };


{$REGION 'THack'}

class procedure  THack.OnActivate(Sender:  TObject);
begin
  if  fbHide  then
    ShowWindow(Application.Handle, SW_HIDE);
  {-}
  fnStart := GetTickCount64();
  TimerCallBack(fDialog.Handle, 0, 0, 0);
  SetTimer(0, 0, 1000, Addr(TimerCallBack))
end { OnActivate };


class procedure  THack.OnCreate(Sender:  TObject);

  function  GetLeft() : Integer;
  begin
    Result := fDialog.Monitor.WorkareaRect.Left
  end { GetTop };

  function  GetCenter() : Integer;
  begin
    Result := fDialog.Monitor.WorkareaRect.Left
                 + fDialog.Monitor.WorkareaRect.Width div 2
                 - fDialog.Width div 2
  end { GetCenter };

  function  GetRight() : Integer;
  begin
    Result := fDialog.Monitor.WorkareaRect.Right
                - fDialog.Width
  end { GetRight };

  function  GetTop() : Integer;
  begin
    Result := fDialog.Monitor.WorkareaRect.Top
  end { GetTop };

  function  GetMiddle() : Integer;
  begin
    Result := fDialog.Monitor.WorkareaRect.Top
                + fDialog.Monitor.WorkareaRect.Height div 2
                - fDialog.Height div 2
  end { GetMiddle };

  function  GetBottom() : Integer;
  begin
    Result := fDialog.Monitor.WorkareaRect.Bottom
                - fDialog.Height
  end { GetBottom };

  procedure  SetPos(const   nLeft, nTop:  Integer);
  begin
    with  fDialog  do
      SetBounds(nLeft, nTop, Width, Height)
  end { SetPos };

begin  { OnCreate }
  fDialog := Sender as TForm;
  {-}
  case  fPosition  of
    dpTopLeft:
      SetPos(GetLeft(), GetTop());
    dpTopCenter:
      SetPos(GetCenter(), GetTop());
    dpTopRight:
      SetPos(GetRight(), GetTop());
    dpMiddleLeft:
      SetPos(GetLeft(), GetMiddle());
    dpCenter:
      { OK };
    dpMiddleRight:
      SetPos(GetRight(), GetMiddle());
    dpBottomLeft:
      SetPos(GetLeft(), GetBottom());
    dpBottomCenter:
      SetPos(GetCenter(), GetBottom());
    dpBottomRight:
      SetPos(GetRight(), GetBottom());
    dpPrevious:
      with  TRegistry.Create()  do
        try
          if  OpenKeyReadOnly(RegKey())  then
            try
              SetPos(ReadInteger('Left'), ReadInteger('Top'))
            finally
              CloseKey()
            end { try-finally }
        finally
          Free()
        end { try-finally }
  end { case fPosition };
  fDialog.MakeFullyVisible();
end { OnCreate };


class procedure  THack.OnDestroy(Sender:  TObject);
begin
  KillTimer(fDialog.Handle, 0);
  with  TRegistry.Create()  do
    try
      if  OpenKey(RegKey(), True)  then
        try
          WriteInteger('Left', fDialog.Left);
          WriteInteger('Top', fDialog.Top)
        finally
          CloseKey()
        end { try-finally }
    finally
      Free()
    end { try-finally }
end { OnDestroy };


class function  THack.RegKey() : string;
begin
  Result := csRegKey;
  if  PositionId <> ''  then
    Result := Result + PathDelim + PositionId
end { RegKey };

{$ENDREGION 'THack'}


end.

