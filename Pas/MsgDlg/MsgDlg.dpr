﻿program  MsgDlg;

(**********************************************
 *                                            *
 *            Show Message Dialog             *
 *     ——————————————————————————————————     *
 *  Copyright © 2019-2024 Grzegorz Skoczylas  *
 *                                            *
 *    Zakład Usług Informatycznych PROGRAM    *
 *         mgr Grzegorz Skoczylas             *
 *             ul. Grażyny 13/15              *
 *             43-300 Bielsko-Biała           *
 *             tel. +48 503 064 953           *
 *             gskoczylas@gmail.com           *
 *                                            *
 **********************************************)

uses
  GSkPasLib.CmdLineOptionParser in 'p:\GSkPasLib\GSkPasLib.CmdLineOptionParser.pas',
  GSkPasLib.Dialogs in 'p:\GSkPasLib\GSkPasLib.Dialogs.pas',
  GSkPasLib.FileUtils in 'p:\GSkPasLib\GSkPasLib.FileUtils.pas',
  JvGnugettext,
  MsgDlg.Options in 'MsgDlg.Options.pas',
  MsgDlg.DisplayDlg in 'MsgDlg.DisplayDlg.pas';

{$R *.res}


begin
  UseLanguage('');
  TOptions.ProcessCommandLine()
end.

