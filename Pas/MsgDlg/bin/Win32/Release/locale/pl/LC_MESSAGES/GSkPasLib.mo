��    A      $  Y   ,      �     �     �     �     �     �     �     �     �     �     �     �     �  )   �           '     <     D  (   L  *   u  *   �     �     �     �     �     
  @     >   `     �     �     �  #   �               9     S  %   n     �     �  )   �     �  
   �     �     �  )   �  2   $	  .   W	     �	      �	      �	  %   �	  3   �	  (   1
  $   Z
  #   
  +   �
  *   �
      �
       $   $     I     `     x     �     �  �  �  	     	        )     1     :     A     J  
   O     Z  	   _  
   i  %   t  4   �     �     �  
   �  	     =     4   I  3   ~  4   �     �     �     �       @     D   `     �     �     �  +   �  "     "   7     Z     w     �     �  
   �  5   �       
                5   #  A   Y  1   �  
   �      �     �  (     4   B  1   w  -   �  ,   �  '     3   ,  +   `     �  '   �     �      �            
   !        $   0   ,   %   (             6   <   9       ;   -                 @                        
   5   :              8      /       #      '             =       +             *            "                    !   &              .   1                         2   )          7      	          >      ?   4   3       A        &Abort &All &Cancel &Close &Help &Ignore &No &Retry &Yes (today) Accept Can not add *nil* as adapter Can not trace - none adapter is connected Cancel Configure toolbar... Confirm Correct Creating file %s raised exception %s: %s Creating the file '%s' raised error %u:
%s Deleting the file '%s' raised error %u:
%s Do you really want to exit? Error Events of %s Fill in the %s. Fill in the date %s. Function can not be used in this version of the operating system In the node '%s' the attribute '%s' has incorrect value: %s
%s Incorrect date and time: %s Incorrect date. Incorrect date: %s Incorrect file structure in line %d Incorrect float number: %s Incorrect logical value: %s Incorrect order of dates. Incorrect order of the %s. Incorrect seperator of the CSV fields Incorrect time: %s Information Locking the file '%s' raised error %u:
%s Missing %s. N&o to All No OK Opening the file '%s' raised error %u:
%s Renaming the file '%s'
to '%s'
raised error %u:
%s Required system library is missing - error $%x Select the %s. Serial port is already connected Serial port is not connected yet The date %s can not be later than %s. The node '%s' does not have required attribute '%s' The node '%s' has incorrect value: %s
%s This date may not be ealier than %s. This date may not be later than %s. This date must be the period from %s to %s. This dialog is closing in %d %s and %d %s. This dialog is closing in %d %s. Time-out Unexpected character #$%x in line %d Unexpected end of file Update system libraries Warning Yes Yes to &All Project-Id-Version: GSkPasLib
POT-Creation-Date: 2013-09-13 12:07
PO-Revision-Date: 2018-05-27 01:14+0200
Last-Translator: Grzegorz Skoczylas <gskoczylas+poedit@gmail.com>
Language-Team: Grzegorz Skoczylas <gskoczylas@gmail.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.7
Language: pl
X-Poedit-SourceCharset: UTF-8
 &Przerwij &Wszystko &Anuluj &Zamknij Pomo&c &Ignoruj &Nie &P&owtórz &Tak (dzisiaj) Akceptuję Nie można dodać *nil* jako adaptera Nie można monitorować — nie ma żadnego adaptera Anuluj Konfiguruj pasek przycisków... Potwierdź Poprawiam Tworzenie pliku %s spowodowało wygenerowanie wyjątku %s: %s Tworzenie pliku „%s” wygenerowało błąd %u:
%s Usuwanie pliku „%s” wygenerowało błąd %u:
%s Czy na pewno chcesz zakończyć działanie programu? Błąd Zdarzenia w %s Wypełnij %s. Wypełnij datę %s. Funkcja nie  może być użyta w tej wersji systemu operacyjnego W węźle „%s” atrybut „%s” ma niepoprawną wartość: %s
%s Niepoprawna data i czas: %s Niepoprawna data. Niepoprawna data: %s Niepoprawna struktura pliku w wierszu nr %d Niepoprawna liczba rzeczywista: %s Niepoprawna wartość logiczna: %s Niepoprawna kolejność dat. Niepoprawna kolejność %s. Niepoprawny seperator pól CSV Niepoprawny czas: %s Informacja Blokowanie pliku „%s” wygenerowało błąd %u:
%s Brak %s. Zawsze NIE Nie OK Otwieranie pliku „%s” wygenerowało błąd %u:
%s Zmiana nazwy pliku „%s”
na „%s'
wygenerowało błąd %u:
%s Brak wymaganej biblioteki systemowej - błąd $%x Wskaż %s. Port szeregowy jest już otwarty Port szeregowy nie jest otwarty Data nie może być poźniejsza niż %s. Węzeł „%s” nie ma wymaganego atrybutu „%s” Węzeł „%s” ma niepoprawną wartość: %s
%s Ta data nie może być wcześniejsza niż %s. Ta data nie może być późniejsza niż %s. Ta data musi być z okresu od %s do %s. Ten komunikat zostanie zamknięty za %d %s i %d %s. Ten komunikat zostanie zamknięty za %d %s. Przekroczony czas oczekiwania Nieoczekiwany znak #$%x w wierszu nr %d Niespodziewany koniec pliku. Zaktualizuj biblioteki systemowe Ostrzeżenie Tak Zawsze TAK 