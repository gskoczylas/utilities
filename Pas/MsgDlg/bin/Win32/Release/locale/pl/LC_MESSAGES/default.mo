��            )         �     �  -   �     �               &     2  	   8  !   B     d     p  G   �     �     �  W        Z     m  )   v     �     �     �  #   �            /   3  !   c     �     �     �     �    �     �  E   �     8     J     Y     l     x       +   �  
   �     �  W   �     2     O  _   m     �     �  L   �     4	      D	     e	  (   �	     �	     �	  H   �	  ,   
     L
     \
     i
     v
                  	          
                                                                                                            %0:s version %1:s After time-out (in seconds) use default value Button captions Caption of the dialog Default button Dialog type Error Examples: Incorrect value of default button Information Message text is missing Message text is obligatory but both "--%2:s" and "-%12:s" are optional. Message to display Negative time-out is not valid Position ID (it applies only if the “position” option has the value “previous”) Possible position: Question Run with option "--help" or "-?" for help Show this help This is sample message This is sample question Unrecognised command line fragments Unrecognized dialogue type Usage: %0:s [options] Use marker "\n" for new line (in message only). Use marker "\t" for current time. Valid options are: Warning We have a problem Where to display the dialogue Project-Id-Version: 
PO-Revision-Date: 2020-06-18 21:59+0200
Last-Translator: Grzegorz Skoczylas <gskoczylas+poedit@gmail.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.3.1
Language-Team: 
Language: pl
 %0:s wersja %1:s Po upływie wskazanego czasu (w sekundach) użyj wartość domyślną Opisy przycisków Tytuł dialogu Przycisk domyślny Typ dialogu Błąd Przykłady: Niepoprawna wartość domyślnego przycisku Informacja Brak tekstu wiadomości Tekst wiadomości jest obowiązkowy, ale „--%2:s” oraz „-%12:s” są opcjonalne. Wiadomość do wyświetlenia Ujemny czas nie jest poprawny Id. pozycji (ma zastosowanie tylko wtedy, gdy opcja „position” ma wartość „previous”) Poprawne pozycje: Pytanie Uruchom z opcją „--help” lub „-?” aby wyświetlić opis parametrów Pokaż ten opis To jest przykładowa wiadomość To jest przykładowe pytanie Nierozpoznane fragmenty wiersza poleceń Nieznany typ dialogu Użycie: %0:s [opcje] Użyj znacznik „\n” do zmiany wiersza (tylko w treści wiadomości). Użyj znacznik „\t” jako bieżący czas. Poprawne opcje: Ostrzeżenie Mamy problem Gdzie wyświetlić dialog 