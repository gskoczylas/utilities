﻿unit  MsgDlg.Options;

(**********************************************
 *                                            *
 *            Show Message Dialog             *
 *     ——————————————————————————————————     *
 *  Copyright © 2019-2024 Grzegorz Skoczylas  *
 *                                            *
 *    Zakład Usług Informatycznych PROGRAM    *
 *         mgr Grzegorz Skoczylas             *
 *             ul. Grażyny 13/15              *
 *             43-300 Bielsko-Biała           *
 *             tel. +48 503 064 953           *
 *             gskoczylas@gmail.com           *
 *                                            *
 **********************************************)

interface


{-------------------------------------------------------------------------------

Options:
  --help -?
    Show available options.
  --message -M
    Message to display.
  --caption -C
    Caption of the dialog.
  --button -B
    List of buttons (answers).
  --default -D
    Default button(default answer).
  --type -T
    Dialog type (Information, Question, Warning, Error, custom).
  --timeout -X
    After specified amount of milliseconds use default value.
  --position -P
    Where to display the dialogue (default: center).
    Possible values: top-left top-center top-right
                     middle-left center middle-right
                     bottom-left bottom-center bottom-right
                     previous
  --id -I
    Position ID.
    The option only applies if the "position" option has the value "previous".

-------------------------------------------------------------------------------}


uses
  System.SysUtils, System.StrUtils, System.Classes,
  JvGnugettext,
  GSkPasLib.CmdLineOptionParser,
  MsgDlg.DisplayDlg;


const
  gcnErrParam = 101;


type
  TPositionOption = class;

  TOptions = class(TGSkOptionParser)
  strict private
    var
      fHelpOption:       TGSkBoolOption;
      fMessageOption:    TGSkStringOption;
      fCaptionOption:    TGSkStringOption;
      fButtonsOption:    TGSkStringListOption;
      fDefaultBtnOption: TGSkStringOption;
      fDialogTypeOption: TGSkStringOption;
      fTimeoutOption:    TGSkIntegerOption;
      fPositionOption:   TPositionOption;
      fPositionIdOption: TGSkStringOption;
      {-}
      fDialogType:       TDialogType;
    function  GetIsHelpRequested() : Boolean;
    function  GetMessage() : string;
    function  GetCaption() : string;
    function  GetButtonList() : TStringList;
    function  GetDefaultButton() : string;
    function  GetDialogType() : TDialogType;
    function  GetTimeout() : Integer;
    function  GetPosition() : TDialogPosition;
    function  GetPositionId() : string;
  public
    constructor  Create();                                           override;
    {-}
    class procedure  ProcessCommandLine();
    procedure  CheckOptions();
    procedure  ShowHelp();
    {-}
    property  IsHelpRequested:  Boolean
              read  GetIsHelpRequested;
    property  Message:  string
              read  GetMessage;
    property  Caption:  string
              read  GetCaption;
    property  ButtonList:  TStringList
              read  GetButtonList;
    property  DefaultButton:  string
              read  GetDefaultButton;
    property  DialogType:  TDialogType
              read  GetDialogType;
    property  Timeout:  Integer
              read  GetTimeout;
    property  Position:  TDialogPosition
              read  GetPosition;
    property  PositionId:  string
              read  GetPositionId;
  end { TOptions };

  TPositionOption = class(TGSkStringOption)
  strict private
    var
      fPosition:  TDialogPosition;
  strict protected
    function  CheckValue(const   sString:  string)
                         : Boolean;                                  override;
  public
    class function  PositionToStr(const   Position:  TDialogPosition)
                                : string;
    property  Position:  TDialogPosition
              read  fPosition;
  end { TPositionOption };


implementation


uses
  GSkPasLib.Dialogs, GSkPasLib.FileUtils;


const
  csDialogType:  array[TDialogType] of  string = (
                   'Warning', 'Error', 'Information', 'Question', '');
  csDialogPosition:  array[TDialogPosition] of  string = (
                      '(?)',
                      'top-left', 'top-center', 'top-right',
                      'middle-left', 'center', 'middle-right',
                      'bottom-left', 'bottom-center', 'bottom-right',
                      'previous');


{$REGION 'TPositionOption'}

function  TPositionOption.CheckValue(const   sString:  string)
                                     : Boolean;
begin
  Assert(dpUnknown = Low(TDialogPosition));
  fPosition := High(TDialogPosition);
  while  fPosition > dpUnknown  do
    begin
      if  SameText(sString, csDialogPosition[fPosition])  then
        Break;
      fPosition := Pred(fPosition)
    end { while };
  if  fPosition = dpUnknown  then
    Result := False
  else
    Result := inherited CheckValue(sString)
end { CheckValue };


class function  TPositionOption.PositionToStr(const   Position:  TDialogPosition)
                                              : string;
begin
  Result := csDialogPosition[Position]
end { PositionToStr };

{$ENDREGION 'TPositionOption'}


{$REGION 'TOptions'}

procedure  TOptions.CheckOptions();

var
  nInd:  Integer;
  sMsg:  string;

  procedure  Error(const   sErrMsg:  string);
  begin
    MsgDlgEx(sErrMsg + sLineBreak
             + sLineBreak + _('Run with option "--help" or "-?" for help'),
             mtError, ['OK'], [1]);
    Halt(gcnErrParam)
  end { Error };

begin  { CheckOptions }
  nInd := IndexText(fDialogTypeOption.Value, csDialogType);
  if  nInd < 0  then
    Error(_('Unrecognized dialogue type'));
  fDialogType := TDialogType(nInd);
  {-}
  if  Trim(fMessageOption.Value) <> ''  then
    begin
      if  LeftList.Count <> 0  then
        begin
          for  nInd := LeftList.Count - 1  downto  0  do
            LeftList.Strings[nInd] := '>> ' + LeftList.Strings[nInd];
          MsgBox(LeftList.Text, _('Unrecognised command line fragments'),
                 MB_ICONERROR);
          Halt(gcnErrParam);
        end { lOptions.LeftList.Count <> 0 }
    end { fMessageOption.Value <> '' }
  else
    begin
      sMsg := '';
      for  nInd := 0  to  LeftList.Count - 1  do
        sMsg := sMsg + ' ' + LeftList.Strings[nInd];
      sMsg := Trim(sMsg);
      if  sMsg <> ''  then
        fMessageOption.Value := sMsg
      else
        Error(Format(_('Message text is missing'),
                     [fMessageOption.LongForm, fMessageOption.ShortForm]));
    end { fMessageOption.Value = '' };
  {-}
  if  fDefaultBtnOption.WasSpecified  then
    if  fButtonsOption.Value.IndexOf(fDefaultBtnOption.Value) < 0  then
      Error(_('Incorrect value of default button'));
  {-}
  if  fTimeoutOption.Value < 0  then
    Error(_('Negative time-out is not valid'))
end { CheckOptions };


constructor  TOptions.Create();
begin
  inherited;
  {-}
  fHelpOption := TGSkBoolOption.Create('?', 'help',
                                       _('Show this help'));
  AddOption(fHelpOption);
  {-}
  fMessageOption := TGSkStringOption.Create('M', 'message',
                                            _('Message to display'));
  AddOption(fMessageOption);
  {-}
  fCaptionOption := TGSkStringOption.Create('C', 'caption',
                                            _('Caption of the dialog'));
  AddOption(fCaptionOption);
  {-}
  fButtonsOption := TGSkStringListOption.Create('B', 'button',
                                                _('Button captions'));
  AddOption(fButtonsOption);
  {-}
  fDefaultBtnOption := TGSkStringOption.Create('D', 'default',
                                               _('Default button'));
  AddOption(fDefaultBtnOption);
  {-}
  fDialogTypeOption := TGSkStringOption.Create('T', 'type',
                                               _('Dialog type'));
  AddOption(fDialogTypeOption);
  {-}
  fTimeoutOption := TGSkIntegerOption.Create('X', 'timeout',
                                             _('After time-out (in seconds) use default value'));
  AddOption(fTimeoutOption);
  {-}
  fPositionOption := TPositionOption.Create('P', 'position',
                                            _('Where to display the dialogue'));
  AddOption(fPositionOption);
  {-}
  fPositionIdOption := TGSkStringOption.Create('I', 'id',
                                               _('Position ID (it applies only if the “position” option has the value “previous”)'));
  AddOption(fPositionIdOption)
end { Create };


function  TOptions.GetButtonList() : TStringList;
begin
  Result := fButtonsOption.Value
end { GetButtonList };


function  TOptions.GetCaption() : string;
begin
  if  fCaptionOption.WasSpecified  then
    Result := fCaptionOption.Value
  else
    case  fDialogType  of
      dtCustom:
        Result := ' ';
      dtInformation:
        Result := _('Information');
      dtQuestion:
        Result := _('Question');
      dtWarning:
        Result := _('Warning');
      dtError:
        Result := _('Error')
    end { case fDialogType }
end { GetCaption };


function  TOptions.GetDefaultButton() : string;
begin
  Result := fDefaultBtnOption.Value
end { GetDefaultButton };


function  TOptions.GetDialogType() : TDialogType;
begin
  Result := TDialogType(IndexText(fDialogTypeOption.Value, csDialogType))
end { GetDialogType };


function  TOptions.GetIsHelpRequested() : Boolean;
begin
  Result := fHelpOption.WasSpecified
end { GetIsHelpRequested };


function  TOptions.GetMessage() : string;
begin
  Result := fMessageOption.Value
end { GetMessage };


function  TOptions.GetPosition() : TDialogPosition;
begin
  Result := fPositionOption.Position
end { GetPosition };


function  TOptions.GetPositionId() : string;
begin
  Result := fPositionIdOption.Value
end { GetPositionId };


function  TOptions.GetTimeout() : Integer;
begin
  Result := fTimeoutOption.Value
end { GetTimeout };


class procedure  TOptions.ProcessCommandLine();

var
  lOptions:  TOptions;

begin  { ProcessCommandLine }
  lOptions := TOptions.Create();
  try
    lOptions.ParseOptions();
    {-}
    if  lOptions.IsHelpRequested  then
      lOptions.ShowHelp();
    {-}
    lOptions.CheckOptions();
    {-}
    with  lOptions  do
      DisplayDlg(Caption, Message, DialogType, ButtonList, DefaultButton,
                 Timeout, Position, PositionId)
  finally
    lOptions.Free()
  end { try-finally }
end { ProcessCommandLine };


procedure  TOptions.ShowHelp();

  function  GetPositionList() : string;

  const
    cnOffset = 8;

  var
    lPos:  TDialogPosition;

  begin  { GetPositionList }
    Result := StringOfChar(' ', cnOffset);
    Assert(Low(TDialogPosition) = dpUnknown);
    for  lPos := Succ(Low(TDialogPosition))  to  Pred(High(TDialogPosition))  do
      begin
        Result := Result + csDialogPosition[lPos] + ',';
        if  Ord(lPos) mod 3 <> 0  then
          Result := Result + ' '
        else
          Result := Result + sLineBreak + StringOfChar(' ', cnOffset)
      end { for lPos };
    Result := Result + csDialogPosition[High(TDialogPosition)] + sLineBreak
  end { GetPositionList };

begin  { ShowHelp }
  MsgDlgEx(Format(_('%0:s version %1:s') + sLineBreak
                    + sLineBreak
                    + _('Usage: %0:s [options]') + sLineBreak
                    + sLineBreak
                    + _('Valid options are:') + sLineBreak
                    + GetExplanations() + sLineBreak
                    + _('Possible position:') + sLineBreak
                    + GetPositionList() + sLineBreak
                    + _('Use marker "\t" for current time.') + sLineBreak
                    + _('Use marker "\n" for new line (in message only).') + sLineBreak
                    + _('Message text is obligatory but both "--%2:s" and "-%12:s" are optional.') + sLineBreak
                    + sLineBreak
                    + _('Examples:')
                    + sLineBreak
                    + '  >%0:s  "%3:s"' + sLineBreak
                    + '  >%0:s  "%5:s"  --%4:s Question  --%6:s Yes  --%6:s No  --%7:s No' + sLineBreak
                    + '  >%0:s  --%2:s "%3:s"' + sLineBreak
                    + '  >%0:s  --%2:s "%s:s"  --%4:s Information' + sLineBreak
                    + '  >%0:s  --%4:s Question  --%2:s "%5:s  --%6:s Yes  --%6:s No' + sLineBreak
                    + '  >%0:s  --%4:s Question  --%2:s "%5:s"  --%6:s Yes  --%6:s No  --%7:s No' + sLineBreak
                    + '  >%0:s  --%8:s "%9:s"  --%4:s Question  --%2:s "%5:s"  --%6:s Yes  --%6:s No  --%7:s No' + sLineBreak
                    + '  >%0:s  -%10:s "%9:s"  -%11:s Question  -%12:s "%5:s"  -%13:s Yes  -%13:s No  -%14:s No' + sLineBreak
                    + '  >%0:s  --%8:s "%9:s"  --%4:s Question  --%2:s "%5:s"  --%6:s Yes  --%6:s No  --%7:s No  --%17:s 7' + sLineBreak
                    + '  >%0:s  -%10:s "%9:s"  -%11:s Question  -%12:s "%5:s"  -%13:s Yes  -%13:s No  -%14:s No  -%18:s 7' + sLineBreak
                    + '  >%0:s  --%15:s' + sLineBreak
                    + '  >%0:s  -%16:s',
                  [{0}ExtractFileName(GetModuleFileName()),
                   {1}GetModuleVersion(),
                   {2}fMessageOption.LongForm,
                   {3}_('This is sample message'),
                   {4}fDialogTypeOption.LongForm,
                   {5}_('This is sample question'),
                   {6}fButtonsOption.LongForm,
                   {7}fDefaultBtnOption.LongForm,
                   {8}fCaptionOption.LongForm,
                   {9}_('We have a problem'),
                   {10}fCaptionOption.ShortForm,
                   {11}fDialogTypeOption.ShortForm,
                   {12}fMessageOption.ShortForm,
                   {13}fButtonsOption.ShortForm,
                   {14}fDefaultBtnOption.ShortForm,
                   {15}fHelpOption.LongForm,
                   {16}fHelpOption.ShortForm,
                   {17}fTimeoutOption.LongForm,
                   {18}fTimeoutOption.ShortForm]),
           mtInformation, ['OK'], [1]);
  Halt(0)
end { ShowHelp };

{$ENDREGION 'TOptions'}


end.

