MsgDlg --help
@echo ErrorLevel = %ErrorLevel%
@pause

MsgDlg --message "Dowolny komunikat" --type Information
@echo ErrorLevel = %ErrorLevel%
@pause

MsgDlg --message "Dowolne pytanie" --type Question --button Tak --button Nie
@echo ErrorLevel = %ErrorLevel%
@pause

MsgDlg --message "Dowolne pytanie" --type Question --button Tak --button Nie --default Nie
@echo ErrorLevel = %ErrorLevel%
@pause

MsgDlg --message "Dowolne pytanie" --type Question --button Tak --button Nie --default Nie --timeout 2
@echo ErrorLevel = %ErrorLevel%
@pause
